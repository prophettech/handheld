package com.prophet.eposapp;

import java.util.ArrayList;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

public class DisplayTableCoversAdapter  extends BaseAdapter
{
	private Context mContext;
	ArrayList<String> mTableCoversList;
    public DisplayTableCoversAdapter(Context in_Context, ArrayList<String> in_TableCoversList) 
    {
        mContext = in_Context;
        mTableCoversList = in_TableCoversList;
    }
    
    public int getCount() 
    {
        //return mThumbIds.length;
        return mTableCoversList.size();    	
    }

    public Object getItem(int position) 
    {
        return mTableCoversList.get(position);
    }

    public long getItemId(int position) 
    {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) 
    {
    	TextView tv = null;
 
    		if (convertView == null) 
    		{
    			tv = new TextView(mContext);
    			tv.setLayoutParams(new GridView.LayoutParams(71, 60));
                tv.setTextSize(14f);
                tv.setTypeface(null, Typeface.BOLD);
    		}
    		else 
    		{
    			tv = (TextView) convertView;
    		}        

    		         
    		tv.setText(mTableCoversList.get(position));  
    		tv.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
    		tv.setBackgroundColor(Color.WHITE);
    		tv.setTextColor(Color.BLACK);

    	return tv;
    }
}
