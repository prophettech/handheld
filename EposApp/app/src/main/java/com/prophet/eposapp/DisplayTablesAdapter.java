package com.prophet.eposapp;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.prophet.eposapp.models.TillTables;

public class DisplayTablesAdapter extends BaseAdapter 
{
	   private Context mContext;
	    //ArrayList<HashMap<String, String>> mCategoryList;
	    ArrayList<TillTables> mTableList; 

	    //CatAdapter(Context in_Context, ArrayList<HashMap<String, String>> in_categoryList) 
	    public DisplayTablesAdapter(Context in_Context, ArrayList<TillTables> in_TableList) 
	    {
	        mContext = in_Context;
	        mTableList = in_TableList;
	    }
	    
	    public int getCount() 
	    {
	        //return mThumbIds.length;
	        return mTableList.size();    	
	    }

	    public Object getItem(int position) 
	    {
	        return mTableList.get(position);
	    }

	    public long getItemId(int position) 
	    {
	        return position;
	    }

	    public View getView(int position, View convertView, ViewGroup parent) 
	    {
	    	TextView tv = null;
	    	
	    	//try
	    	//{
	        
	    		if (convertView == null) 
	    		{
	    			tv = new TextView(mContext);
	    			tv.setLayoutParams(new GridView.LayoutParams(116, 60));
	    			tv.setTextSize(16f);
	    			tv.setTypeface(null, Typeface.BOLD);
	    		}
	    		else 
	    		{
	    			tv = (TextView) convertView;
	    		}  
	    		

	    		TillTables tableDetails = mTableList.get(position);  
	    		String tableDesc = tableDetails.GetTableDesc();
	    		int tableBgColour = Color.WHITE;
	    		int tableFontColour = Color.BLACK;

	    		if( tableDetails.GetTableBookedStatus())
	    		{
	    			tableDesc += "\n"+ tableDetails.GetTableOrderAccRef();
		    		tableBgColour = Color.BLACK;
		    		tableFontColour = Color.WHITE;	    			
	    		}
	    		if( tableDetails.GetTableOrderState() == EposDataTypes.TRANS_STATE_PRINTED)
	    		{
		    		tableBgColour = Color.RED;
		    		tableFontColour = Color.WHITE;	   	    			
	    		}
	    		if( tableDetails.GetTableOrderState() == EposDataTypes.TRANS_STATE_PRINTED_KITCHEN)
	    		{
		    		tableBgColour = Color.rgb(0, 128, 0);
		    		tableFontColour = Color.WHITE;	   	    			
	    		}	    		
	    		tv.setText(tableDesc);   	
	    		tv.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
	    		tv.setBackgroundColor(tableBgColour);
	    		tv.setTextColor(tableFontColour);

	    		/*
	    	}
	    	catch( Exception e)
	    	{
	    		String errorMsg = e.getMessage();
	    	}
	    	*/
	        
	    	return tv;
	    }
}
