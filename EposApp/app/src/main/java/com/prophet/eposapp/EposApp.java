package com.prophet.eposapp;

import java.util.ArrayList;

import com.prophet.eposapp.interfaces.IMenu;
import com.prophet.eposapp.interfaces.IOptions;
import com.prophet.eposapp.interfaces.IOrder;
import com.prophet.eposapp.interfaces.ISecurity;
import com.prophet.eposapp.interfaces.ISplitBill;
import com.prophet.eposapp.interfaces.ITables;
import com.prophet.eposapp.models.ModelManager;
import com.prophet.eposapp.presenters.PresenterMenu;
import com.prophet.eposapp.presenters.PresenterOptions;
import com.prophet.eposapp.presenters.PresenterOrder;
import com.prophet.eposapp.presenters.PresenterReport;
import com.prophet.eposapp.presenters.PresenterSecurity;
import com.prophet.eposapp.presenters.PresenterSplitBill;
import com.prophet.eposapp.presenters.PresenterTables;

import android.app.Application;
import android.content.SharedPreferences;

public class EposApp extends Application
{
    protected ModelManager mModelManager;    
    protected IMenu mIMenu;
    protected IOrder mIOrder;
    protected ITables mITables;   
    protected ISplitBill mISplitBill;     
    protected ISecurity mISecurity;    
    protected IOptions mIOptions;        
    protected PresenterMenu mPresenterMenu;
    protected PresenterOrder mPresenterOrder;
    protected PresenterTables mPresenterTables;
    protected PresenterReport mPresenterReport;
    protected PresenterSplitBill mPresenterSplitBill;
    protected PresenterSecurity mPresenterSecurity;
    protected PresenterOptions mPresenterOptions;
    
    SharedPreferences mAppConfig; 
        
	@Override
	public void onCreate()
	{
		super.onCreate();
		
		mAppConfig = this.getSharedPreferences("EposAppPref", 0);
		
		mModelManager = new ModelManager(mAppConfig);		
		mIMenu = new IMenu();		
		mIOrder = new IOrder();	
		mITables = new ITables();
		mISplitBill = new ISplitBill();
		mISecurity = new ISecurity();
		mIOptions = new IOptions();		
		mPresenterOptions = new PresenterOptions(mModelManager);
		mPresenterOptions.AttachInterface(mIOptions);
		mPresenterOptions.LoadServerIP();
		
		mPresenterMenu = new PresenterMenu(mModelManager,mIMenu);
		mPresenterTables = new PresenterTables(mModelManager,mITables);
		mPresenterOrder = new PresenterOrder(mModelManager);
		mPresenterOrder.AttachInterface(mIOrder);
		mPresenterOrder.SetOrder();
		mPresenterReport = new PresenterReport(mModelManager);
		mPresenterReport.AttachOrderInterface(mIOrder);		
		mPresenterSplitBill = new PresenterSplitBill(mModelManager);
		mPresenterSplitBill.AttachInterface(mISplitBill);
		mPresenterSecurity = new PresenterSecurity(mModelManager);
		mPresenterSecurity.AttachInterface(mISecurity);	
		
	}
	
	public void SetDBPath()
	{
    	mModelManager.SetDBPath();
	}
	
	public PresenterOrder GetPresenterOrder()
	{		
		return mPresenterOrder;
	}
	
	public PresenterMenu GetPresenterMenu()
	{		
		return mPresenterMenu;
	}
	
	public PresenterTables GetPresenterTables()
	{		
		return mPresenterTables;
	}
	
	public PresenterReport GetPresenterReport()
	{		
		return mPresenterReport;
	}	
	
	public PresenterSplitBill GetPresenterSplitBill()
	{		
		return mPresenterSplitBill;
	}		
	
	public PresenterSecurity GetPresenterSecurity()
	{		
		return mPresenterSecurity;
	}		
	
	public PresenterOptions GetPresenterOptions()
	{		
		return mPresenterOptions;
	}	
	
	public IMenu GetIMenu()
	{		
		return mIMenu;
	}
	
	public IOrder GetIOrder()
	{		
		return mIOrder;
	}
	
	public ITables GetITables()
	{		
		return mITables;
	}	
	
	public ISplitBill GetISplitBill()
	{		
		return mISplitBill;
	}	
	
	public ISecurity GetISecurity()
	{		
		return mISecurity;
	}	
	
	public IOptions GetIOptions()
	{		
		return mIOptions;
	}	
	
	public ArrayList<String> FormatItemText(String in_Text, int in_IndentLevel, int in_MaxLineLength, String in_EndWordChar) 
	{
		int LineCount = 1;
		String CurLine = "";
		String CurWord = "";
		String OutputStr = "";
		String IndentStr = "";
		String CurChar = "";
		boolean bCheckLine = true;
		ArrayList<String> retValues = new ArrayList<String>();
		
		for( int k=0; k < in_IndentLevel; k++ )
		{	
			IndentStr += "___"; // 1 indent = 3 spaces			
		}
		CurLine = IndentStr;
		int TextLength = in_Text.length();
		for( int i=0; i <= TextLength; i++ )
		{
			bCheckLine = true;	
			if( i < TextLength )
			{
				CurChar = String.valueOf(in_Text.charAt(i));
				if( CurChar.compareTo(in_EndWordChar) != 0 )
				{
					CurWord += CurChar;
					bCheckLine = false;
				}
				else
				{
					if( in_EndWordChar.compareTo(",") == 0 )
					{
						CurWord += ",";						
					}				
				}
			}
			
			if( bCheckLine )
			{
				if( CurLine.length() + CurWord.length() + 1 > in_MaxLineLength )
				{
					LineCount++;
					OutputStr += CurLine+"<br />";			
					CurLine = IndentStr + CurWord;					
				}	
				else
				{
					if( CurLine.length() > 0)
						CurLine += " "; 
						
					CurLine += CurWord;	
				}
				
				CurWord = "";	
			}
		}
		
		if( CurLine.length() > 0 ) // Make sure current line is output
		{
			OutputStr += CurLine;			
		}
		OutputStr = OutputStr.replaceAll("_", "&nbsp;");		

		retValues.add(OutputStr);
		retValues.add(Integer.toString(LineCount));
		return retValues;
	}
	


}

