package com.prophet.eposapp;

public class  EposDataTypes 
{
    public static final int TRANS_STATE_SAVED = 1;
    public static final int TRANS_STATE_PRINTED = 2;
    public static final int TRANS_STATE_POSTED = 4;
    public static final int TRANS_STATE_PRINTED_KITCHEN = 12;
}
