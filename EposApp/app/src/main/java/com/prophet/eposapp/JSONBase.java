package com.prophet.eposapp;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.zip.GZIPOutputStream;


public class JSONBase 
{
	protected String mErrorMsg;
    // Creating JSON Parser object
	protected JSONParser mjParser = new JSONParser();
    
	//protected static String DB_SERVER_PATH = "http://192.168.5.22/android/";
	protected String DB_SERVER_PATH = "";
	
    // JSON Node names
    protected static final String TAG_SUCCESS = "success";
    protected static final String TAG_MSG = "message";	

    
    protected static final String TAG_BUTID = "But_ID";
    protected static final String TAG_BUTTEXT1 = "But_Text1";    
    protected static final String TAG_BUTTEXT2 = "But_Text2";
    protected static final String TAG_BUTTEXT3 = "But_Text3";
    protected static final String TAG_BUTTEXT4 = "But_Text4";
    protected static final String TAG_BUTTEXT5 = "But_Text5";
    protected static final String TAG_BUTTEXT6 = "But_Text6";
    protected static final String TAG_BUTTEXT7 = "But_Text7";
    protected static final String TAG_BUTTEXT8 = "But_Text8";    
    protected static final String TAG_BUTOFFER = "But_Offer";
    
    protected static final String TAG_BUTCOLOUR = "But_Colour_Default";
    protected static final String TAG_BUTFONTCOLOUR = "But_FontColour_Default";
    //protected static final String TAG_BUTSECTIONID = "But_SectionID";   
    /*
    protected static final int TRANS_STATE_SAVED = 1;
    protected static final int TRANS_STATE_PRINTED = 2;
    protected static final int TRANS_STATE_POSTED = 4;
    protected static final int TRANS_STATE_PRINTED_KITCHEN = 12;
    */
    
    public JSONBase()
    {    	
    	
    }

    protected void SetServerIP(String in_ServerIP)
    {    	
    	/*
    	java.net.InetAddress serverAddr;
    	try 
    	{
    	    serverAddr = java.net.InetAddress.getByName("rays-pc");
    	}
    	catch (java.net.UnknownHostException exception) 
    	{
    	    return;
    	}

    	*/
    	DB_SERVER_PATH = "http://" + in_ServerIP + "/android/";    	
    }

    public String CompressString(String in_data)
    {   
    	byte[] gzipStr = null;
    	GZIPOutputStream zos=null;
    	ByteArrayOutputStream os = new ByteArrayOutputStream();
    	
    	try 
    	{       
    		zos = new GZIPOutputStream(new BufferedOutputStream(os));
    		zos.write(in_data.getBytes());
    		zos.close();
    		gzipStr = os.toByteArray();
    	    os.close();
    	} 
    	catch (IOException e) 
    	{
			e.printStackTrace();
		} 
    	finally 
    	{
    		try {
				zos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}

    	String newStr = "";
		try 
		{
			newStr = new String(gzipStr, "UTF-8");
		} 
		catch (UnsupportedEncodingException e) 
		{
			e.printStackTrace();
		}
    	return newStr;
    }
    
}
