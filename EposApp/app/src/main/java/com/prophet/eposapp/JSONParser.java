package com.prophet.eposapp;


import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.io.UnsupportedEncodingException;

import java.util.List;
import java.util.zip.GZIPInputStream;
 
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;
 
import android.util.Log;
//import com.prophet.eposapp.R;
 
public class JSONParser {
 
    InputStream is = null;
    JSONObject jObj = null;
    String json = "";
    String mErrorMsg = "";
 
    // constructor
    public JSONParser() 
    {
 
    }
    
    public String GetParserErrorMsg()
    {
        return mErrorMsg;
    }
 
    // function get json from url
    // by making HTTP POST or GET mehtod
    public JSONObject makeHttpRequest(String url, String method,
            List<NameValuePair> params) 
    {
    	
    	mErrorMsg = "";
    	HttpResponse httpResponse = null;

        // Making HTTP request
        try { 
        	
            // check for request method
            if(method == "POST"){
                // request method is POST
            	
            	
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                httpPost.addHeader("Accept-Encoding", "gzip");
                httpPost.setEntity(new UrlEncodedFormEntity(params));
                httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            	 
            	
            	
            	/*
                URL url1 = new URL(url);
                URLConnection conn1 = url1.openConnection();
                conn1.setDoOutput(true);
                */
                //conn1.setRequestProperty("Content-encoding", "gzip");
                //conn1.setRequestProperty("Content-type", "application/octet-stream");
                
                /*
                OutputStream os = conn1.getOutputStream();                
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(URLEncodedUtils.format(params, "utf-8"));
                writer.close();
                os.close();
                */
                //OutputStream os = conn1.getOutputStream();   
                /*
                GZIPOutputStream dos1 = new GZIPOutputStream(conn1.getOutputStream());
                //String paramStr = URLEncodedUtils.format(params, "utf-8");
                String paramStr = params.toString();
                dos1.write(paramStr.getBytes());
                dos1.flush();
                dos1.close();
                */
                //BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(dos1, "UTF-8"));
                //writer.write(URLEncodedUtils.format(params, "utf-8"));
                //writer.close();
                //os.close();
                
                //conn1.setRequestProperty("Content-encoding", "gzip");
                //conn1.setRequestProperty("Content-type", "application/octet-stream");
                
                //GZIPOutputStream dos1 = new GZIPOutputStream(conn1.getOutputStream());
                /*
                OutputStreamWriter ow = new OutputStreamWriter(conn1.getOutputStream(), "iso-8859-1");     
                BufferedWriter dos1 = new BufferedWriter(ow);
                dos1.write(body1);
                dos1.flush();
                dos1.close();
                */


                
                //is = conn1.getInputStream();
                

 
            }else if(method == "GET"){
                // request method is GET
            	HttpParams httpParameters = new BasicHttpParams();
            	int timeoutConnection = 60000;
            	HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            	
                DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
                String paramString = URLEncodedUtils.format(params, "utf-8");
                url += "?" + paramString;
                HttpGet httpGet = new HttpGet(url);
 

                httpResponse = httpClient.execute(httpGet);
                
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            }           
 
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();         
            mErrorMsg = "Could not connect to Server";
        } catch (IOException e) {        		
            e.printStackTrace();
        } 
        
        
 
        try {

        	InputStreamReader isReader = null;
        	
        	/*
        	if(method == "POST")
        	{
        		isReader = new InputStreamReader(is, "iso-8859-1");           		
        	}
        	else if(method == "GET")
        	{
        		GZIPInputStream zis = new GZIPInputStream(is);
        		isReader = new InputStreamReader(zis, "iso-8859-1");        		
        	}  
        	*/
        	
    		GZIPInputStream zis = new GZIPInputStream(is);
    		isReader = new InputStreamReader(zis, "iso-8859-1");                          
            BufferedReader reader = new BufferedReader(isReader, 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) 
            {
                sb.append(line + "\n");
            }
            
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
 
        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
            //jObj = null;
        }
 
        // return JSON String
        return jObj;
 
    }

}
