package com.prophet.eposapp;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import com.prophet.eposapp.R;
import com.prophet.eposapp.views.DisplayTablesActivity;
import com.prophet.eposapp.views.LoginActivity;
import com.prophet.eposapp.views.MainMenuActivity;

public class MainActivity extends Activity 
{
	protected EposApp app;

    //private ProgressDialog pDialog;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		app = (EposApp)getApplication();
		
		//new LoadData().execute();  
		displayLogin();
	}
	
	@Override	
	protected void onDestroy()
	{
	}
			

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	public void displayMenu()
	{
		Intent menuIntent =  new Intent(this, MainMenuActivity.class);
		startActivity(menuIntent);					
	}
	
	public void displayTables()
	{
		Intent tablesIntent =  new Intent(this, DisplayTablesActivity.class);
		startActivity(tablesIntent);					
	}	
	
	public void displayLogin()
	{
		Intent loginIntent =  new Intent(this, LoginActivity.class);
		startActivity(loginIntent);					
	}	
	
	 /**
     * Background Async Task to Load all data by making HTTP Request
     * */
	/*
    class LoadData extends AsyncTask<String, String, String> 
    {
    	//MainActivity mParentActivity; 
    	//MainActivity in_parentActivity
    	LoadData( )
    	{
    		//mParentActivity = in_parentActivity;
    	
    	}

        @Override
        protected void onPreExecute() 
        {         
        	super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Loading Data. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();            
        }
        
        protected String doInBackground(String... args) 
        {      
        	//String errorMsg = "";
        	try
        	{        		
        		app.mPresenterMenu.LoadMenu();
        		app.mPresenterOrder.NewOrder();
        	}
        	catch(Exception e)
        	{
        		//errorMsg = e.getMessage();
        		return null;
        		
        	}
            return "1";
        }
        
        protected void onPostExecute(String file_url) 
        {
            // dismiss the dialog after getting all products
            pDialog.dismiss();       
            //mParentActivity.displayMenu();
            displayTables();
        }  
    }
    */

}

