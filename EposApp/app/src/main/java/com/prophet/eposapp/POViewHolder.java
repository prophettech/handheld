package com.prophet.eposapp;

import android.widget.TextView;

import com.prophet.eposapp.models.OFFER_ITEM_TYPE;

public class POViewHolder
{
	public OFFER_ITEM_TYPE mPOType;
	public TextView mDesc1View;	
	public TextView mDesc2View;
	public TextView mSpacerView;
}
