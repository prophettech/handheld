package com.prophet.eposapp;

import java.util.ArrayList;


import com.prophet.eposapp.models.OFFER_ITEM_TYPE;
import com.prophet.eposapp.models.TillProductOfferItem;
import com.prophet.eposapp.models.TillProductOfferItemGroup;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ProductOfferAdapter extends BaseAdapter 
{
	private LayoutInflater mLayoutInflater;
		private Context mContext;
		ArrayList<TillProductOfferItemGroup> mPOItemGroupList;

	    //CatAdapter(Context in_Context, ArrayList<HashMap<String, String>> in_categoryList) 
	    public ProductOfferAdapter(Context in_Context, ArrayList<TillProductOfferItemGroup> in_POItemGroupList) 
	    {
	        mContext = in_Context;
	        mPOItemGroupList = in_POItemGroupList;
	        
	        mLayoutInflater=LayoutInflater.from(mContext);
	    }
	    
	    public int getCount() 
	    {
	        //return mThumbIds.length;
	        return mPOItemGroupList.size();    	
	    }

	    public Object getItem(int position) 
	    {
	    	return mPOItemGroupList.get(position);
	    }

	    public long getItemId(int position) 
	    {
	        return position;
	    }

	    public View getView(int position, View convertView, ViewGroup parent) 
	    {
	    	POViewHolder mVHolder;

            if(convertView == null)
            {
                convertView=mLayoutInflater.inflate(R.layout.pogrid, parent, false);
                mVHolder=new POViewHolder();
                mVHolder.mDesc1View=(TextView)convertView.findViewById(R.id.text1);
                mVHolder.mDesc2View=(TextView)convertView.findViewById(R.id.text2);
                mVHolder.mSpacerView=(TextView)convertView.findViewById(R.id.textSpacer);
                convertView.setTag(mVHolder);
            }else
            {
                mVHolder=(POViewHolder)convertView.getTag();
            }
    		TillProductOfferItemGroup POItemGroup = mPOItemGroupList.get(position);
    		ArrayList<TillProductOfferItem> POItemList = POItemGroup.GetPOItemList();
    		OFFER_ITEM_TYPE POGroupType = POItemGroup.GetPOItemGroupType();

    		String POButDesc = "";
    		String POButTypeDesc = "Select";
    		if(POGroupType == OFFER_ITEM_TYPE.OFFERITEMTYPE_OR )
    		{
    			//POButDesc = "Select from";
    			for( int i=0; i < POItemList.size(); i++ )
    			{
    				ArrayList<String> retItemDetails = POItemList.get(i).GetPOItemDetails(); 
    				
    				if( POButDesc.compareTo("") != 0 )
    					POButDesc += "   or   ";
    				    				
        			if( retItemDetails.get(7).compareTo("1") == 0 )
        			{
        				// If an item has been selected display only that item
        				POButDesc = retItemDetails.get(1);  
            			if( retItemDetails.get(5).compareTo("") != 0 )
            			{
            				POButDesc = retItemDetails.get(5);          				
            			}
        				POButTypeDesc = "Change";
        				break;
        			}

        			POButDesc += retItemDetails.get(1);  
    			}    			
    		}
    		else
    		{
    			//String ItemText = "";
    			//int ItemAction = 0;
    			//int ItemToppingID = 0;
    			//String ItemProdCode = "";
    			//String ItemProdDesc = "";
    			//int ItemProdCatID = 0;
    			//int ItemID = 0;
    			//boolean ItemProdSelected = false;
    			
    			ArrayList<String> retItemDetails = POItemList.get(0).GetPOItemDetails(); 
    			//ItemID = Integer.parseInt(retItemDetails.get(0));
    			//ItemText = retItemDetails.get(1);
    			//ItemAction = Integer.parseInt(retItemDetails.get(2));
    			//ItemToppingID = Integer.parseInt(retItemDetails.get(3));
    			//ItemProdCode = retItemDetails.get(4);
    			//ItemProdDesc = retItemDetails.get(5);
    			//ItemProdCatID = Integer.parseInt(retItemDetails.get(6));
    			//ItemProdSelected = false;
    			//if( retItemDetails.get(7).compareTo("1") != 0 )
    			//	ItemProdSelected = true;

    			POButDesc = retItemDetails.get(1);   
    			mVHolder.mDesc2View.setBackgroundColor(mContext.getResources().getColor(R.color.colourYellow));   	
    			mVHolder.mSpacerView.setBackgroundColor(Color.BLACK);   	
    			if(POGroupType == OFFER_ITEM_TYPE.OFFERITEMTYPE_TOPPINGS )
    			{
    				POButTypeDesc = "Toppings";    				
    			}
    			else
    			{    	
    				
        			if( POGroupType == OFFER_ITEM_TYPE.OFFERITEMTYPE_STD )
        			{
        				POButTypeDesc = "";
        				mVHolder.mDesc2View.setBackgroundColor(mContext.getResources().getColor(R.color.colourLightGreen));   				
        				mVHolder.mSpacerView.setBackgroundColor(mContext.getResources().getColor(R.color.colourLightGreen));  
        			}
        			else  if( retItemDetails.get(7).compareTo("1") == 0 )
        			{
        				// Product has been selected so add it to description

        				POButDesc += ":   " + retItemDetails.get(5);   
        				POButTypeDesc = "Change";
        			}   
    			}	    			
    		}            
            
    		mVHolder.mPOType = POGroupType;
            mVHolder.mDesc1View.setText(POButDesc);
            mVHolder.mDesc2View.setText(POButTypeDesc);
            
	    	return convertView;	    	
	    }
	    

}
