package com.prophet.eposapp.db;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.prophet.eposapp.JSONBase;
import com.prophet.eposapp.models.TillPayType;
import com.prophet.eposapp.models.TillVATCode;


public class DALHelper extends JSONBase
{
	ArrayList<TillPayType> mPayTypesList; 
	ArrayList<TillVATCode> mVATCodesList; 
	
    private String URL_ALL_HELPER = "";

    private static final String TAG_PAYTYPES = "paytypes";
    private static final String TAG_VATCODES = "vatcodes";
    
    JSONArray mPayTypes = null;    
    JSONArray mVATCodes = null;   
    
	public DALHelper()
	{
		mPayTypesList = new ArrayList<TillPayType>();
		mVATCodesList = new ArrayList<TillVATCode>();
		
	}
	
	public void SetDBPath(String in_ServerIP)
	{
		SetServerIP(in_ServerIP);
        URL_ALL_HELPER = DB_SERVER_PATH + "helper.php";

	}
	
	public void LoadHelperData() 
	{		
		mPayTypesList.clear();
		mVATCodesList.clear();

		// Building Parameters            
		List<NameValuePair> params = new ArrayList<NameValuePair>();
            
		try
		{
			// getting JSON string from URL            
			JSONObject json = mjParser.makeHttpRequest(URL_ALL_HELPER, "GET", params);
             
			if( json != null )
			{
           
				// Checking for SUCCESS TAG                
				int success = json.getInt(TAG_SUCCESS);
 
				if (success == 1)
				{                
					TillPayType NewPayType;
				    JSONArray payTypeData = json.getJSONArray(TAG_PAYTYPES);               
					for (int i = 0; i < payTypeData.length(); i++) 
					{                        
						JSONObject pt = payTypeData.getJSONObject(i);   
						NewPayType = new TillPayType();
						NewPayType.setPayTypeCode(pt.getInt("PayTypeCode"));
						NewPayType.setPayTypeDesc(pt.getString("PayTypeDesc"));
						mPayTypesList.add(NewPayType);
					}     
					
					TillVATCode NewVATCode;
				    JSONArray vatCodesData = json.getJSONArray(TAG_VATCODES);              
					for (int i = 0; i < vatCodesData.length(); i++) 
					{                        
						JSONObject vc = vatCodesData.getJSONObject(i);  
						NewVATCode = new TillVATCode();
						NewVATCode.setVATCode(vc.getInt("VATCode"));
						NewVATCode.setVATDesc(vc.getString("VATDesc"));
						NewVATCode.setVATRate(vc.getDouble("VATRate"));
						mVATCodesList.add(NewVATCode);						
					}    
				}                 
				else                             
				{
                
				}     
			}
		}
		catch( JSONException e )
		{
			mErrorMsg = e.getMessage();               
			e.printStackTrace();  			
		}			    	
	}
	
	public ArrayList<TillPayType> GetPayTypesList()	
	{	
		return mPayTypesList;		
	}
	
	public ArrayList<TillVATCode> GetVATCodesList()	
	{	
		return mVATCodesList;		
	}	
}
