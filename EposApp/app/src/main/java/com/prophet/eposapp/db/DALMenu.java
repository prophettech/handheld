package com.prophet.eposapp.db;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.prophet.eposapp.JSONBase;
import com.prophet.eposapp.models.OFFER_ITEM_TYPE;
import com.prophet.eposapp.models.TOPPING_STATE_CODE;
import com.prophet.eposapp.models.TillCategory;
import com.prophet.eposapp.models.TillProduct;
import com.prophet.eposapp.models.TillProductOffer;
import com.prophet.eposapp.models.TillProductOfferItem;
import com.prophet.eposapp.models.TillProductOfferItemGroup;
import com.prophet.eposapp.models.TillProductOptions;
import com.prophet.eposapp.models.TillProductOptionsButGroup;
import com.prophet.eposapp.models.TillProductOptionsButGroupButton;
import com.prophet.eposapp.models.TillProductOptionsButtons_v1;
import com.prophet.eposapp.models.TillSection;
import com.prophet.eposapp.models.TillToppingPrice;
import com.prophet.eposapp.models.TillToppings;


public class DALMenu extends JSONBase
{ 
    ArrayList<TillCategory> mCategoryList;
    ArrayList<TillProduct> mProductList;
    ArrayList<TillProduct> mSubProductList;
    ArrayList<TillSection> mSectionList;
    ArrayList<TillProductOptions> mProdOptionsList;
    ArrayList<TillProductOptionsButtons_v1> mProdOptionsButtonsList;    
    ArrayList<TillToppings> mProductToppingList;        
    ArrayList<TillToppings> mTopGroupToppingList;      
    ArrayList<TillProductOffer> mProductOffersList;
       
    private String URL_ALL_CATEGORIES = "";
    private String URL_ALL_PRODUCTS =  "";
    private String URL_ALL_PRODSECTIONS =  "";
    private String URL_ALL_PRODOPTIONS =  "";
    private String URL_ALL_PRODUCTTOPPINGS = "";    
    private String URL_ALL_PRODUCTOFFERS =  "";    
        
    
    private static final String TAG_CATEGORIES = "categories";
    private static final String TAG_PRODUCTS = "products";
    private static final String TAG_SUBPRODUCTS = "subproducts";
    private static final String TAG_PRODSECTIONS = "sections";
    private static final String TAG_PRODOPTIONS = "prodoptions";
    private static final String TAG_PRODOPTIONSBUTTONS = "prodoptionsbuttons";  
    private static final String TAG_PRODOPTIONSTOPPINGS = "prodoptionstoppings";
    private static final String TAG_PRODUCTTOPPINGS = "prodtoppings";       
    private static final String TAG_PRODUCTOFFERS = "offerlist";       
    private static final String TAG_PRODUCTOFFERITEMS = "offeritemlist";              
    	
    JSONArray categories = null;    
    JSONArray mProducts = null;    
    JSONArray prodSections = null;    
    JSONArray prodOptions = null;    
    JSONArray prodOptionsButtons = null;    
    JSONArray prodOptionsToppings = null;       
    JSONArray prodToppings = null;   
    JSONArray mProductOffers = null;   
    JSONArray mProductOfferItems = null;       
    
    
    
    public DALMenu()
	{       
    	mCategoryList = new ArrayList<TillCategory>();    	
    	mSectionList = new ArrayList<TillSection>();	
    	mProdOptionsList = new ArrayList<TillProductOptions>();	
    	mTopGroupToppingList = new ArrayList<TillToppings>();	
    	mProductOffersList = new ArrayList<TillProductOffer>();    	
	}
    
	public void SetDBPath(String in_ServerIP)
	{
		SetServerIP(in_ServerIP);
        URL_ALL_CATEGORIES = DB_SERVER_PATH + "categories.php";
        URL_ALL_PRODUCTS = DB_SERVER_PATH + "products.php";
        URL_ALL_PRODSECTIONS = DB_SERVER_PATH + "sections.php";
        URL_ALL_PRODOPTIONS = DB_SERVER_PATH + "productoptions.php";
        URL_ALL_PRODUCTTOPPINGS = DB_SERVER_PATH + "producttoppings.php";    
        URL_ALL_PRODUCTOFFERS = DB_SERVER_PATH + "productoffers.php";    
	}
	
	public void LoadMenu()
	{		
		
	    mCategoryList.clear();	    
	    mSectionList.clear();	
	    mProdOptionsList.clear();	
	    mTopGroupToppingList.clear();	
	    mProductOffersList.clear();	
	    
	    
		LoadSections();
		LoadCategories();	
		LoadProductToppingsList();
		LoadProducts();
		LoadProductOptions();	
		LoadProductOffers();
	}
	
	public ArrayList<TillCategory> GetCategoryList()	
	{		
		return mCategoryList;		
	}
	
	public ArrayList<TillProduct> GetProductList()	
	{		
		return mProductList;		
	}
	
	public ArrayList<TillProduct> GetSubProductList()	
	{		
		return mSubProductList;		
	}
	
	public ArrayList<TillProductOptions> GetProductOptionsList()	
	{		
		return mProdOptionsList;		
	}
	
	public ArrayList<TillProductOffer> GetProductOffersList()	
	{		
		return mProductOffersList;		
	}		
	
	protected void LoadSections()         
	{       
		// Building Parameters            
		List<NameValuePair> params = new ArrayList<NameValuePair>();
            
		try
		{
		// getting JSON string from URL            
		JSONObject json = mjParser.makeHttpRequest(URL_ALL_PRODSECTIONS, "GET", params);
             
		if( json != null )
		{
		//try 
		//{                
			// Checking for SUCCESS TAG                
			int success = json.getInt(TAG_SUCCESS);
 
            if (success == 1)
            {                    
            	// Getting Array of Categories                	
            	prodSections = json.getJSONArray(TAG_PRODSECTIONS);
                     
            	TillSection newSection;
            	// looping through All Sections                    
            	for (int i = 0; i < prodSections.length(); i++) 
            	{                        
            		JSONObject c = prodSections.getJSONObject(i); 
                        
            		// Storing each json item in variable 
            		int sectionID = c.getInt(TAG_BUTID);    
            		String sectionPos = c.getString(TAG_BUTTEXT1);  
            		String sectionName = c.getString(TAG_BUTTEXT2);   
            		newSection = new TillSection();
            		newSection.SetSectionDetails(sectionID, Integer.parseInt(sectionPos), sectionName);
            		mSectionList.add(newSection);            		                 
            	}                
            }                 
            else                             
            {
                
            }     
		}
		}
		catch( JSONException e )
		{
			mErrorMsg = e.getMessage();               
			e.printStackTrace();  			
		}				
	}
	
	protected void LoadProductOffers()         
	{       
		// Building Parameters            
		List<NameValuePair> params = new ArrayList<NameValuePair>();
            
		try
		{
		// getting JSON string from URL            
		JSONObject json = mjParser.makeHttpRequest(URL_ALL_PRODUCTOFFERS, "GET", params);
             

		
		if( json != null )
		{
		//try 
		//{                
			// Checking for SUCCESS TAG                
			int success = json.getInt(TAG_SUCCESS);
 
            if (success == 1)
            {                    
            	// Getting Array of Categories                	
            	mProductOffers = json.getJSONArray(TAG_PRODUCTOFFERS);
            	mProductOfferItems = json.getJSONArray(TAG_PRODUCTOFFERITEMS);
                     
                TillProductOffer NewProductOffer = null;
                TillProductOfferItemGroup NewPOItemGroup = null;
                TillProductOfferItem NewPOItem = null;
                String CurPOName = "";
                int CurPOItemGroupID = 0;
                
                int ItemGroupID = 0;
                OFFER_ITEM_TYPE ItemGroupType = OFFER_ITEM_TYPE.OFFERITEMTYPE_STD;
                int ItemGroupColour = 0;
                String ItemText = "";
                int ItemAction = 0;
                int ItemToppingID = 0;
                String ItemProdCode = "";
                int ItemProdCatID = 0;
                int ItemID = 0;
                int ItemButID = 0;
                boolean ItemSelected = false;
                
                OFFER_ITEM_TYPE TmpItemGroupTypeValues[] = OFFER_ITEM_TYPE.values();
            	// looping through offers               
            	for (int i = 0; i < mProductOffers.length(); i++) 
            	{                        
            		JSONObject c = mProductOffers.getJSONObject(i);
                        
            		// Storing each json item in variable 
            		String OfferProdCode = c.getString("MD_ParentCode"); 
            		int OfferButListLinkID = c.getInt("MD_ButListLinkID");              		            		  
            		ItemGroupType = TmpItemGroupTypeValues[c.getInt("MD_ButType")-1]; 
            		
            		if( CurPOName.compareTo(OfferProdCode) != 0 )
            		{
            			CurPOName = OfferProdCode;
            			NewProductOffer = new TillProductOffer(OfferProdCode);
                        NewProductOffer.SetPOAllowSpendOffer(false);
                        mProductOffersList.add(NewProductOffer);
            		}
            		
            		if( OfferButListLinkID != CurPOItemGroupID )
            		{
            			ItemID = 0;
            			CurPOItemGroupID = OfferButListLinkID;
            			NewPOItemGroup = new TillProductOfferItemGroup();
            			
            			NewPOItemGroup.SetPOItemGroupDetails(++ItemGroupID, ItemGroupType, ItemGroupColour);
            			
            			
                    	for (int j = 0; j < mProductOfferItems.length(); j++) 
                    	{   
                    		JSONObject OfferItem = mProductOfferItems.getJSONObject(j); 
                    		ItemButID = OfferItem.getInt("MDItem_ButID");    
                    		
                    		/*
                            ItemText = "";
                            ItemAction = 0;
                            ItemToppingID = 0;
                            ItemProdCode = "";
                            ItemProdCatID = 0;
                            */
                    		if( ItemButID == OfferButListLinkID )
                    		{              
                        		ItemText = OfferItem.getString("MDItem_ButText"); 
                        		ItemAction = OfferItem.getInt("MDItem_ButAction");      
                        		ItemToppingID = OfferItem.getInt("MDItem_ButTopID");      
                        		ItemProdCode = OfferItem.getString("MDItem_ButProdCode");      
                        		ItemProdCatID = OfferItem.getInt("MDItem_ButCat");  
                        		//ItemSelected = true;
                        		//if( ItemGroupType != OFFER_ITEM_TYPE.OFFERITEMTYPE_STD)
                        		//	ItemSelected = false;
        
                        		
                    			NewPOItem = new TillProductOfferItem();
                    			NewPOItem.SetPOItemDetails(++ItemID, ItemText, ItemAction, ItemToppingID, ItemProdCode, "", ItemProdCatID, false, ItemGroupType);
                    			NewPOItemGroup.AddPOItem(NewPOItem); 
                    			if( ItemGroupType != OFFER_ITEM_TYPE.OFFERITEMTYPE_OR )
                    				break;
                    		}
                    	}
                    	NewProductOffer.AddPOItemGroup(NewPOItemGroup);
            		}  		                 
            	}                
            }                 
            else                             
            {                
            }     
		}
		}
		catch( JSONException e )
		{
			mErrorMsg = e.getMessage();               
			e.printStackTrace();  			
		}	
		
			
	}
	        
	protected void LoadCategories()         
	{       
	
		
		
		/*
		TillCategory newCat;
  		newCat = new TillCategory();
		newCat.setCatDetails(1, "test", 0);
		mCategoryList.add(newCat);   
		*/
		
		
        	
		// Building Parameters            
		List<NameValuePair> params = new ArrayList<NameValuePair>();
            
		try
		{
		// getting JSON string from URL            
		JSONObject json = mjParser.makeHttpRequest(URL_ALL_CATEGORIES, "GET", params);
             
		if( json != null )
		{
		//try 
		//{                
			// Checking for SUCCESS TAG                
			int success = json.getInt(TAG_SUCCESS);
 
            if (success == 1)
            {                    
            	// Getting Array of Categories                	
            	categories = json.getJSONArray(TAG_CATEGORIES);
                     
            	TillCategory newCat;
            	// looping through All Categories                    
            	for (int i = 0; i < categories.length(); i++) 
            	{                        
            		JSONObject c = categories.getJSONObject(i); 
                        
            		// Storing each json item in variable               		
            		int id = c.getInt(TAG_BUTID);                        
            		String name = c.getString(TAG_BUTTEXT1);    
            		int BakColour = c.getInt(TAG_BUTCOLOUR);
            		int FontColour = c.getInt(TAG_BUTFONTCOLOUR);
            		//String BakColour = "";
            		//String FontColour = "";
            		newCat = new TillCategory();
            		newCat.SetCatDetails(id, name, BakColour,FontColour);
            		mCategoryList.add(newCat);            		                 
            	}                
            }                 
            else                             
            {
                
            }     
		}
		}
		catch( JSONException e )
		{
			mErrorMsg = e.getMessage();               
			e.printStackTrace();  			
		}	
			
	}

	protected void LoadProducts()         
	{       
		//mProductList = null;
		mProductList = new ArrayList<TillProduct>();
		mSubProductList = new ArrayList<TillProduct>();
        	
		
        	
/*
		TillProduct newProd = new TillProduct();
		newProd.ProductCode = "PROD001";
		newProd.ProductDesc = "Test Prod";
		newProd.ProductPrice = 2.99;
		newProd.ProductClassID = 1;
		newProd.ProductParentClassID = 1;
		//newProd.setProdDetails(pCode, pDesc, pPrice, pCatID, pParentCatID);
		mProductList.add(newProd);   
		*/
		
		// Building Parameters            
		List<NameValuePair> params = new ArrayList<NameValuePair>();
            
		try
		{
		// getting JSON string from URL            
		JSONObject json = mjParser.makeHttpRequest(URL_ALL_PRODUCTS, "GET", params);
             
		if( json != null )
		{         
			// Checking for SUCCESS TAG                
			int success = json.getInt(TAG_SUCCESS);
 
            if (success == 1)
            {                    
            	// Getting Array of Categories                	
            	mProducts = json.getJSONArray(TAG_PRODUCTS);                    
            	ParseProductData(1);    
            	
            	mProducts = json.getJSONArray(TAG_SUBPRODUCTS);                     
            	ParseProductData(2);    
            }                 
            else                             
            {
                
            }     
		}
		}
		catch( JSONException e )
		{
			mErrorMsg = e.getMessage();             
			e.printStackTrace();			
		}					
	}

	private void ParseProductData(int in_ParseType) 
	{
	    
		try
		{	    
			TillProduct newProd;
			// 	looping through All Products                    
			for (int i = 0; i < mProducts.length(); i++) 
			{                  
				JSONObject c = mProducts.getJSONObject(i);               
    		
				// Use short field names to reduce data  size
			    // F1: PCode
			    // F2: PDesc
			    // F3: PPrice
			    // F4: PClass
			    // F5: PParentClass
			    // F6: PSectionID
			    // F7: POptionsID
			    // F8: PHHPrice
			    // F9: PVATCode
			    // F10: PButGroup1ID
			    // F11: PButGroup2ID
			    // F12: PParentCode
			    // F13: PProductOffer
			    // F14: PPrintToKitchen
			    // F15: PPLUSalePrice
			    // F16: PTP1
			    // F17: PTP2
			    // F18: PTP3
			    // F19: PTP4
			    // F20: PTP5
			    // F21: PTP6
			    // F22: PTP7
			    // F23: PTP8
				
				// 	Storing each json item in variable               		
				String pCode = c.getString("F1");                       
				String pDesc = c.getString("F2");  
				String pParentCode = c.getString("F12");   
				double pPrice = -1;
				if( c.getString("F3").compareTo("null") != 0)
					pPrice = c.getDouble("F3");      
				int pCatID = c.getInt("F4");      
				int pParentCatID = c.getInt("F5");      
				int pSectionID = c.getInt("F6");    
				int pOptionsID = c.getInt("F7");     
				int pButGroup1ID = c.getInt("F10");     
				int pButGroup2ID = c.getInt("F11");  
				String pPrintToKitchen = c.getString("F14");
				boolean pProdOffer = false;
				if( c.getString("F13").compareTo("1") == 0)
					pProdOffer = true;     
				
				String pVATCode = c.getString("F9");  
				if( c.getString("F9").compareTo("null") == 0 )
					pVATCode = "0";    
				
				double pHHPrice = -1;
				if( c.getString("F8").compareTo("null") != 0)
					pHHPrice = c.getDouble("F8");   
				
				double pFixedPrice = -1;
				if( c.getString("F15").compareTo("null") != 0)
					pFixedPrice = c.getDouble("F15");       			
				
				newProd = new TillProduct();
				
				TillToppingPrice NewTP;
				String TPStr = "";
				for( int tp=0; tp < 8; tp++ )
				{
					TPStr = "F"+Integer.toString(tp+16);
					if( c.getString(TPStr).compareTo("null") != 0)
					{					
						NewTP = new TillToppingPrice();
						NewTP.TPType = tp+1;
						NewTP.TPStdPrice =  c.getDouble(TPStr);
						NewTP.TPHappyHourPrice = 0;
						newProd.ProductToppingPricesList.add(NewTP);
					}
				}				
				
				newProd.ProductCode = pCode;
				newProd.ProductDesc = pDesc;
				newProd.ProductPrice = pPrice;
				newProd.ProductPriceHH = pHHPrice;
				newProd.ProductFixedPrice = pFixedPrice;
				newProd.ProductClassID = pCatID;
				newProd.ProductParentCode = pParentCode;
				newProd.ProductParentClassID = pParentCatID;
				newProd.ProductOptionsID = pOptionsID;
				newProd.ProductVATCode = Integer.parseInt(pVATCode);
				
				newProd.ProductSectionPos = 0;
				newProd.ProductSectionDesc = "";
				
				newProd.ProductButG1ID = pButGroup1ID;
				newProd.ProductButG2ID = pButGroup2ID;
				
				newProd.PrintToKitchen = 1;
				if( pPrintToKitchen.compareTo("0") == 0)
					newProd.PrintToKitchen = 0;

				newProd.ProductOffer = pProdOffer;
				
				if( pSectionID > 0 )
				{
					TillSection tmpSection = GetSectionDetails(pSectionID);
					if(tmpSection != null )
					{
						newProd.ProductSectionPos = tmpSection.GetSectionPos();
						newProd.ProductSectionDesc = tmpSection.GetSectionDesc();            				
					}            	
				}
				
				if( newProd.ProductOptionsID > 0 )
				{
					int TopListCount = mProductToppingList.size();
					for( int t = 0; t < TopListCount; t++ )
					{
						if( mProductToppingList.get(t).ToppingParentCode.compareTo(pCode) == 0 )
						{
							newProd.AddTopping(mProductToppingList.get(t), TOPPING_STATE_CODE.TOP_FIXED, false);						
						}
					}	
				}
    		
				if( in_ParseType == 1 )
				{
					mProductList.add(newProd);   // Main Product List				
				}	
				else if( in_ParseType == 2 )
				{
					mSubProductList.add(newProd);   // Sub Product List	 		    	
				}
			}    		         		                 
		}
		catch( JSONException e )
		{
			mErrorMsg = e.getMessage();               
			e.printStackTrace();			
		}			
	}

	TillSection GetSectionDetails(int in_SectionID)
	{
    	for (int i = 0; i < mSectionList.size(); i++) 
    	{    
    		if(mSectionList.get(i).GetSectionID() == in_SectionID )
    			return mSectionList.get(i);    		
    	}
    	return null;
	}
	
	protected void LoadProductOptions()         
	{       
       	
		// Building Parameters            
		List<NameValuePair> params = new ArrayList<NameValuePair>();
            
		try
		{
		// getting JSON string from URL            
		JSONObject json = mjParser.makeHttpRequest(URL_ALL_PRODOPTIONS, "GET", params);
             
		if( json != null )
		{
		//try 
		//{                
			// Checking for SUCCESS TAG                
			int success = json.getInt(TAG_SUCCESS);
 
            if (success == 1)
            {     
            	
            	prodOptionsToppings = json.getJSONArray(TAG_PRODOPTIONSTOPPINGS);
            	TillToppings newTopping;
            	for (int i = 0; i < prodOptionsToppings.length(); i++) 
            	{                        
            		JSONObject c = prodOptionsToppings.getJSONObject(i);
    				String topGroupIDStr = c.getString("TopGroupID");   
    				if( topGroupIDStr.compareTo("null") != 0 )
    				{
	    				String topDesc = c.getString("TopDesc");  
	    				String topProdCode = c.getString("TopProdCode");  
	    				int topGroupID = 0;
	    				if( c.getString("TopGroupID").compareTo("") != 0)
	    					topGroupID = c.getInt("TopGroupID");      
	    				
	    				int topType = 0;
	    				if( c.getString("TopType").compareTo("null") != 0)
	    					topType = c.getInt("TopType");   		
	    				
	    				double topFixedPrice = -1;
	    				if( c.getString("TopFixedPrice").compareTo("null") != 0)
	    					topFixedPrice = c.getDouble("TopFixedPrice");   			    				
	
	    				newTopping = new TillToppings();
	    				newTopping.ToppingID = i+1;
	    				newTopping.ToppingCode = topProdCode;
	    				newTopping.ToppingDesc = topDesc;
	    				newTopping.ToppingGroupID = topGroupID;
	    				newTopping.ToppingType = topType;
	    				newTopping.ToppingFixedPrice = topFixedPrice;
	    				mTopGroupToppingList.add(newTopping);  
    				}
            	}            	

            	mProdOptionsButtonsList = new ArrayList<TillProductOptionsButtons_v1>();	
            	
            	TillProductOptionsButtons_v1 newProdOptionButtons;
            	prodOptionsButtons = json.getJSONArray(TAG_PRODOPTIONSBUTTONS);
            	for (int i = 0; i < prodOptionsButtons.length(); i++) 
            	{                        
            		JSONObject c = prodOptionsButtons.getJSONObject(i); 
                        
            		// Storing each json item in variable               		
            		int ButID = c.getInt(TAG_BUTID);  
            		String ButTextName = c.getString(TAG_BUTTEXT6);  
            		
            		if( ButTextName.compareTo("null") != 0 )
            		{
	            		String ButText1 = c.getString(TAG_BUTTEXT1);             			
	            		String ButText2 = c.getString(TAG_BUTTEXT2);            		
	            		String ButText3 = c.getString(TAG_BUTTEXT3);           		
	            		String ButText4 = c.getString(TAG_BUTTEXT4);          		
	            		String ButText5 = c.getString(TAG_BUTTEXT5);
	            		
	            		String ButGroupTypeStr = c.getString(TAG_BUTTEXT7);  
	            		int ButGroupType = 0;
	            		if( ButGroupTypeStr.compareTo("") != 0 )
	            			ButGroupType = Integer.parseInt(ButGroupTypeStr);
	            		
	            		
	            		
	            		newProdOptionButtons = new TillProductOptionsButtons_v1();
	            		newProdOptionButtons.SetProductButtonDetails(ButID, ButText1, ButText2, ButText3, ButText4, ButText5, ButTextName, ButGroupType);
	            		mProdOptionsButtonsList.add(newProdOptionButtons);      
            		}
            	}   
            	
                int TmpButGroupID;
                int TmpButGroupOptionsID;
                int TmpButGroupNo;
                int TmpButGroupPartOfProduct;
                int TmpButGroupReceiptPos;
                String TmpButGroupName;
                String TmpButGroupStartText;
                String TmpButGroupSepChar;
                String TmpButGroupStartChar;
                String TmpButGroupEndChar;
                int TmpButGroupSelReq;
                int TmpButGroupSelAllowed;
                int TmpButGroupDisplayType;
                int TmpButGroupColour;
                int TmpButGroupFontColour;
                int TmpButGroupColourSel;
                int TmpButGroupFontColourSel;
                

                
            	// Getting Array of Categories                	
            	prodOptions = json.getJSONArray(TAG_PRODOPTIONS);   
            	TillProductOptions newProdOptions;            	
                 
            	for (int i = 0; i < prodOptions.length(); i++) 
            	{                        
            		JSONObject c = prodOptions.getJSONObject(i);
                        
            		// Storing each json item in variable               		
            		int POID = c.getInt(TAG_BUTID);  

            		String POButGroup1IDStr = c.getString(TAG_BUTTEXT1);  
            		int POButGroup1ID = 0;
            		if( POButGroup1IDStr.length() > 0 )
            			POButGroup1ID = Integer.parseInt(POButGroup1IDStr);
            			
            		String POButGroup2IDStr = c.getString(TAG_BUTTEXT2);
            		int POButGroup2ID = 0;
            		if( POButGroup2IDStr.length() > 0 )
            			POButGroup2ID = Integer.parseInt(POButGroup2IDStr);
            		
            		String POHalfHalfStr = c.getString(TAG_BUTTEXT3);  
            		boolean POHalfHalf = false;            		
            		if( POHalfHalfStr.length() > 0 )
            			POHalfHalf = true;   

            		String POName = c.getString(TAG_BUTTEXT4);
            		
            		newProdOptions = new TillProductOptions();
            		newProdOptions.SetProductOptionsDetails(POID,POHalfHalf, POName);
            		
            		TillProductOptionsButGroup NewProductOptionsButGroup;
            		TillProductOptionsButGroupButton NewProductOptionsButton;
            		
            		if( POButGroup1ID > 0 )
            		{
            			TillProductOptionsButtons_v1 tmpButtons = GetProdOptionButtonsDetails(POButGroup1ID);
            			if( tmpButtons != null )
            			{            				
                    		String ButText1 = tmpButtons.GetProdButtonText1();             			
                    		String ButText2 = tmpButtons.GetProdButtonText2();            		
                    		String ButText3 = tmpButtons.GetProdButtonText3();           		
                    		String ButText4 = tmpButtons.GetProdButtonText4();          		
                    		String ButText5 = tmpButtons.GetProdButtonText5();
                    		String ButTextName = tmpButtons.GetProdButtonnName(); 
                    		int ButPartOfProduct = 0;
                    		if( tmpButtons.GetProduButtonGroupType() == 0 ) // *** IMPORTANT ***  Till V3 uses 1 to indicate Part of Product, Till V2 uses 0
                    		{                    			
                    			ButPartOfProduct = 1;                    			
                    		}
                    		ArrayList<String> mButtonsList = new ArrayList<String>();	
                    		if( ButText5.compareTo("") != 0 )
                    			mButtonsList.add(ButText5);
                    		if( ButText4.compareTo("") != 0 )
                    			mButtonsList.add(ButText4);
                    		if( ButText3.compareTo("") != 0 )
                    			mButtonsList.add(ButText3);
                    		if( ButText2.compareTo("") != 0 )
                    			mButtonsList.add(ButText2);
                    		if( ButText1.compareTo("") != 0 )
                    			mButtonsList.add(ButText1);
                    		
                    		
                    		
                    		
                            TmpButGroupID = 1;
                            TmpButGroupOptionsID = 1;
                            TmpButGroupNo = 1;
                            TmpButGroupPartOfProduct = ButPartOfProduct;
                            TmpButGroupReceiptPos = 1;
                            TmpButGroupName = ButTextName;
                            TmpButGroupStartText = "";
                            TmpButGroupSepChar = "";
                            TmpButGroupStartChar = "";
                            TmpButGroupEndChar = "";
                            TmpButGroupSelReq = 0;
                            if( ButPartOfProduct == 1 )
                            	TmpButGroupSelReq = 1;
                            	
                            TmpButGroupSelAllowed = 1;
                            TmpButGroupDisplayType = 1;
                            TmpButGroupColour = 0;
                            TmpButGroupFontColour = 0;
                            TmpButGroupColourSel = 0;
                            TmpButGroupFontColourSel = 0;                    		
                    		NewProductOptionsButGroup = new TillProductOptionsButGroup();
                    		
                            NewProductOptionsButGroup.SetProductOptionsButGroupDetails(TmpButGroupID,TmpButGroupOptionsID,TmpButGroupNo,
                                    TmpButGroupPartOfProduct,TmpButGroupReceiptPos,TmpButGroupName,TmpButGroupStartText,
                                    TmpButGroupSepChar,TmpButGroupStartChar,TmpButGroupEndChar,TmpButGroupSelReq,           
                                    TmpButGroupSelAllowed,TmpButGroupDisplayType,TmpButGroupColour, TmpButGroupFontColour,TmpButGroupColourSel,TmpButGroupFontColourSel);

                            	
                            int butID = mButtonsList.size();
                            int butPos = mButtonsList.size();
                    		for (String butText : mButtonsList)        
                            {
                                NewProductOptionsButton = addProdOptionButtonsDetails(butText, butID--, TmpButGroupNo, butPos--);
                                if(NewProductOptionsButton != null )
                                	NewProductOptionsButGroup.AddButton(NewProductOptionsButton);                       			
                            }

                            newProdOptions.AddButGroup(NewProductOptionsButGroup);
            			}            			
            		}
            		
            		if( POButGroup2ID > 0 )
            		{
            			TillProductOptionsButtons_v1 tmpButtons = GetProdOptionButtonsDetails(POButGroup2ID);
            			if( tmpButtons != null )
            			{            				
                    		String ButText1 = tmpButtons.GetProdButtonText1();             			
                    		String ButText2 = tmpButtons.GetProdButtonText2();            		
                    		String ButText3 = tmpButtons.GetProdButtonText3();           		
                    		String ButText4 = tmpButtons.GetProdButtonText4();          		
                    		String ButText5 = tmpButtons.GetProdButtonText5();
                    		String ButTextName = tmpButtons.GetProdButtonnName(); 
                    		int ButPartOfProduct = 0;
                    		if( tmpButtons.GetProduButtonGroupType() == 0 ) // *** IMPORTANT ***  Till V3 uses 1 to indicate Part of Product, Till V2 uses 0
                    		{                    			
                    			ButPartOfProduct = 1;                    			
                    		}
                    		ArrayList<String> mButtonsList = new ArrayList<String>();	
                    		if( ButText5.compareTo("") != 0 )
                    			mButtonsList.add(ButText5);
                    		if( ButText4.compareTo("") != 0 )
                    			mButtonsList.add(ButText4);
                    		if( ButText3.compareTo("") != 0 )
                    			mButtonsList.add(ButText3);
                    		if( ButText2.compareTo("") != 0 )
                    			mButtonsList.add(ButText2);
                    		if( ButText1.compareTo("") != 0 )
                    			mButtonsList.add(ButText1);                    		
                    		
                            TmpButGroupID = 2;
                            TmpButGroupOptionsID = 2;
                            TmpButGroupNo = 2;
                            TmpButGroupPartOfProduct = ButPartOfProduct;
                            TmpButGroupReceiptPos = 1;
                            TmpButGroupName = ButTextName;
                            TmpButGroupStartText = "";
                            TmpButGroupSepChar = "";
                            TmpButGroupStartChar = "";
                            TmpButGroupEndChar = "";
                            TmpButGroupSelReq = 0;
                            if( ButPartOfProduct == 1 )
                            	TmpButGroupSelReq = 1;
                            	
                            TmpButGroupSelAllowed = 1;
                            TmpButGroupDisplayType = 1;
                            TmpButGroupColour = 0;
                            TmpButGroupFontColour = 0;
                            TmpButGroupColourSel = 0;
                            TmpButGroupFontColourSel = 0;                    		
                    		NewProductOptionsButGroup = new TillProductOptionsButGroup();
                    		
                            NewProductOptionsButGroup.SetProductOptionsButGroupDetails(TmpButGroupID,TmpButGroupOptionsID,TmpButGroupNo,
                                    TmpButGroupPartOfProduct,TmpButGroupReceiptPos,TmpButGroupName,TmpButGroupStartText,
                                    TmpButGroupSepChar,TmpButGroupStartChar,TmpButGroupEndChar,TmpButGroupSelReq,           
                                    TmpButGroupSelAllowed,TmpButGroupDisplayType,TmpButGroupColour, TmpButGroupFontColour,TmpButGroupColourSel,TmpButGroupFontColourSel);


                            
                            int butID = mButtonsList.size();
                            int butPos = mButtonsList.size();
                    		for (String butText : mButtonsList)        
                            {
                                NewProductOptionsButton = addProdOptionButtonsDetails(butText, butID--, TmpButGroupNo, butPos--);
                                if(NewProductOptionsButton != null )
                                	NewProductOptionsButGroup.AddButton(NewProductOptionsButton);                       			
                            }                            

                            newProdOptions.AddButGroup(NewProductOptionsButGroup);
            			}            			
            		}            		
            		
            		mProdOptionsList.add(newProdOptions);            		                 
            	}   

            	mProdOptionsButtonsList = null;
            }                 
            else                             
            {
                
            }     
		}
		}
		catch( JSONException e )
		{
			mErrorMsg = e.getMessage();               
			e.printStackTrace();  			
		}
			
	}	
	
	TillProductOptionsButGroupButton addProdOptionButtonsDetails(String in_ButDesc, int in_ButID, int in_ButGroupID, int in_ButPos )
	{
        if( in_ButDesc.length() > 0 )
        {
        	TillProductOptionsButGroupButton NewProductOptionsButton;
        	
        	
            int TmpButtonID;
            int TmpButtonGroupID;
            int TmpButtonPos;
            int TmpButtonSel;
            String TmpButtonName;
            String TmpButtonDesc;
            String TmpButtonReceiptDesc;
            int TmpButtonPriceType;
            double TmpButtonNewPrice;
            double TmpButtonNewPriceHH;
            
            TmpButtonID = in_ButID;
            TmpButtonGroupID = in_ButGroupID;
            TmpButtonPos = in_ButPos;
            TmpButtonSel = 0;
            TmpButtonName = "";
            TmpButtonDesc = in_ButDesc;
            TmpButtonReceiptDesc = "";
            TmpButtonPriceType = 1;
            TmpButtonNewPrice = 0;
            TmpButtonNewPriceHH = 0;
            
        	NewProductOptionsButton = new TillProductOptionsButGroupButton();
        	NewProductOptionsButton.SetTillProductOptionsButtonDetails(TmpButtonID, TmpButtonGroupID, TmpButtonPos,
                                                            TmpButtonSel,TmpButtonName,TmpButtonDesc,
                                                            TmpButtonReceiptDesc,TmpButtonPriceType,TmpButtonNewPrice,TmpButtonNewPriceHH );
            
        	return NewProductOptionsButton;
        }	
        
        return null;
		
	}
	
	TillProductOptionsButtons_v1 GetProdOptionButtonsDetails(int in_ButGroupID)
	{
    	for (int i = 0; i < mProdOptionsButtonsList.size(); i++) 
    	{    
    		if(mProdOptionsButtonsList.get(i).GetProduButtonID() == in_ButGroupID )
    			return mProdOptionsButtonsList.get(i);    		
    	}
    	return null;
	}
	
	public ArrayList<TillToppings> GetTopGroupToppingList()
    {		
		return mTopGroupToppingList;
    }
	
    public void LoadProductToppingsList()
    {

		mProductToppingList = new ArrayList<TillToppings>();

		// Building Parameters            
		List<NameValuePair> params = new ArrayList<NameValuePair>();
            
		try
		{
		// getting JSON string from URL            
		JSONObject json = mjParser.makeHttpRequest(URL_ALL_PRODUCTTOPPINGS, "GET", params);
             
		if( json != null )
		{         
			// Checking for SUCCESS TAG                
			int success = json.getInt(TAG_SUCCESS);
 
            if (success == 1)
            {                    
            	// Getting Array of Categories                	
            	prodToppings = json.getJSONArray(TAG_PRODUCTTOPPINGS);   
            	
            	TillToppings newTopping;
    			// 	looping through All Products                    
    			for (int i = 0; i < prodToppings.length(); i++) 
    			{                  
    				JSONObject c = prodToppings.getJSONObject(i);               
        		          		
    				String topProdCode = c.getString("TopProdCode");   
    				String topSubCode = c.getString("TopSubCode");                       
    				String topDesc = c.getString("TopDesc");     
    				int topQty = 0;
    				if( c.getString("TopQty").compareTo("") != 0)
    					topQty = c.getInt("TopQty");    

    				int topType = 0;
    				if( c.getString("TopType").compareTo("null") != 0)
    					topType = c.getInt("TopType");   		
    				
    				newTopping = new TillToppings();
    				newTopping.ToppingParentCode = topProdCode;
    				newTopping.ToppingCode = topSubCode;
    				newTopping.ToppingDesc = topDesc;
    				newTopping.ToppingQty = topQty;
    				newTopping.ToppingType = topType;
    				mProductToppingList.add(newTopping);   

    			}    	
            }                 
            else                             
            {
                
            }     
		}
		}
		catch( JSONException e )
		{
			mErrorMsg = e.getMessage();             
			e.printStackTrace();			
		}	   	
    }
    
}
