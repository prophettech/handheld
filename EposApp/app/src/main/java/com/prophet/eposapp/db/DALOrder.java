package com.prophet.eposapp.db;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.prophet.eposapp.EposDataTypes;
import com.prophet.eposapp.JSONBase;
import com.prophet.eposapp.models.ITEM_TYPE;
import com.prophet.eposapp.models.OFFER_ITEM_TYPE;
import com.prophet.eposapp.models.OrderItemRecordData;
import com.prophet.eposapp.models.TOPPING_STATE_CODE;
import com.prophet.eposapp.models.TillOrderDetails;
import com.prophet.eposapp.models.TillOrderItem;
import com.prophet.eposapp.models.TillProduct;
import com.prophet.eposapp.models.TillProductOffer;
import com.prophet.eposapp.models.TillProductOfferItem;
import com.prophet.eposapp.models.TillProductOfferItemGroup;
import com.prophet.eposapp.models.TillSplitItem;
import com.prophet.eposapp.models.TillSplitPayDetails;
import com.prophet.eposapp.models.TillToppings;

public class DALOrder extends JSONBase 
{
    private String url_loadorder = "";
    private String url_saveorder = "";
    private String url_eposdbhelper = "";    

    private static final String TAG_ORDERDATA = "orderData";
    private static final String TAG_ORDERITEMDATA = "orderItemData";    
    private static final String TAG_SPLITBILLDATA = "splitItemData";    
    private static final String TAG_SPLITPAYDATA = "splitPayData";    
    	
    TillOrderDetails mOrderDetails;
    ArrayList<TillOrderItem> mOrderItems;
    ArrayList<TillSplitItem> mSplitBillItems;
    String mOrderDetailsStr;
    //JSONArray categories = null;  
    JSONArray mOrderData = null;   
    JSONArray mOrderItemsData = null;   
    JSONArray mSplitBillData = null; 
    JSONArray mSplitPayData = null;     

    public DALOrder()
	{

	}
    
	public void SetDBPath(String in_ServerIP)
	{
    	SetServerIP(in_ServerIP);	
        url_loadorder = DB_SERVER_PATH + "loadorder.php";
        url_saveorder = DB_SERVER_PATH + "saveorder.php";
        url_eposdbhelper = DB_SERVER_PATH + "eposdbhelper.php";    
	}
    
    public String GetErrorMsg()
    {
        return mErrorMsg;
    }
    
    public int GetNextOrderNo()
    {
    	
		try 
		{
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("DALAction","1" ));
				
			JSONObject json = mjParser.makeHttpRequest(url_eposdbhelper,"GET", params);

			
			int success = json.getInt(TAG_SUCCESS);
			
			if (success == 1) 
			{	
				String dbMsg = json.getString(TAG_MSG);
					
				if( dbMsg.length() > 0 )
					return Integer.parseInt(dbMsg);
			} 
			else 
			{
			}
			
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
        return 0;
    }
	
    public retTypeOrderInfo LoadOrder(int in_OrderID)         
	{   
    	//boolean mOrderLoaded = false;
    	retTypeOrderInfo mRetOrder = new retTypeOrderInfo();
    	mRetOrder.mOrderDetails.ClearOrderDetails();
    	mRetOrder.mOrderLoaded = false;
		try 
		{
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("orderID",Integer.toString(in_OrderID) ));
		
			JSONObject json = mjParser.makeHttpRequest(url_loadorder,"GET", params);	
			ITEM_TYPE[] ItemTypeValues = ITEM_TYPE.values();
			int success = json.getInt(TAG_SUCCESS);
			if (success == 1) 
			{	
				mOrderData = json.getJSONArray(TAG_ORDERDATA);	
            	for (int i = 0; i < mOrderData.length(); i++) 
            	{              		
    				JSONObject tmpOrderDetails = mOrderData.getJSONObject(i);
    				mRetOrder.mOrderDetails.setOrderID(tmpOrderDetails.getInt("orderID"));
    				mRetOrder.mOrderDetails.setOrderNo(tmpOrderDetails.getInt("orderNo"));
    				mRetOrder.mOrderDetails.setOrderTable(tmpOrderDetails.getInt("orderTableNo"),tmpOrderDetails.getInt("orderTableNo"));
    				mRetOrder.mOrderDetails.setOrderTransID(tmpOrderDetails.getInt("orderLastTransID"));
    				mRetOrder.mOrderDetails.setOrderPayTypeCode(tmpOrderDetails.getInt("orderPayType"));
    				mRetOrder.mOrderDetails.setOrderTableCovers(tmpOrderDetails.getInt("orderCovers"));
    				mRetOrder.mOrderDetails.setOrderPayment(tmpOrderDetails.getDouble("orderTotal")-tmpOrderDetails.getDouble("orderBalance"));
    				if( tmpOrderDetails.getInt("orderTransState") == EposDataTypes.TRANS_STATE_PRINTED_KITCHEN )
    				{
    					mRetOrder.mOrderDetails.SetPrintedToKitchen(true);    					
    				}
    				if( tmpOrderDetails.getInt("orderTransState") == EposDataTypes.TRANS_STATE_PRINTED )
    				{
    					mRetOrder.mOrderDetails.SetBillPrinted(true);    					
    				}    				
            	}		
            	TillOrderItem NewOrderItem = null;
                TillProduct NewProduct1 = new TillProduct(); 
                TillProduct NewProduct2 = new TillProduct(); 
                TillProduct NewProductHalfHalf = new TillProduct(); 
                TillToppings ItemTopping = new TillToppings();
                
                TillProductOffer NewProductOffer = null; 
                TillProductOfferItemGroup NewPOItemGroup = null;
                TillProductOfferItem NewPOItem = null;
                
                int CurItemID = 0;
            	int DBItemTable = 0;
                String ItemCode, ItemDesc;
                String ItemParentCode, ItemParentDesc;
                //String ItemInfo;
                double ItemQty,ItemPrice, ItemExtraToppingCharge; 
                double ItemSplitQty;
                double ItemPrintedQty;
                double ItemSplitPrintedQty;
                //double ItemVATRate;
                ITEM_TYPE ItemType;
                int ItemCatID;
                int ItemSectionPos;
                String ItemSectionDesc;
                boolean bHalf2 = false;
                //boolean bOfferMode = false;
                //boolean bHalfHalfMode = false;
                //boolean bFirstItem = true;

                //bHalfHalfMode = false;
                bHalf2 = false;
                //bOfferMode = false;
                //bFirstItem = true;  
                ItemPrice = 0;
                ItemExtraToppingCharge = 0;
                ItemQty = 0;                      
                ItemSplitQty = 0;   
                ItemPrintedQty = 0;
                ItemSplitPrintedQty = 0;
                //LastPosID = 0;
                ItemType = ITEM_TYPE.PT_STD;
                String ItemFreeText = "";
                int ItemMDID = 0;
                int LastItemMDID = 0;
                int POItemGroupID = 0;    
                int POItemID = 0;  
                
        		mOrderItemsData = json.getJSONArray(TAG_ORDERITEMDATA);
        		
            	for (int j = 0; j < mOrderItemsData.length(); j++) 
            	{   
            		JSONObject orderItemDetails = mOrderItemsData.getJSONObject(j);
            		   
            		DBItemTable = orderItemDetails.getInt("itemItemTable");         
                    ItemCode = orderItemDetails.getString("itemCode");
                    ItemDesc = orderItemDetails.getString("itemDesc");
                    ItemMDID = orderItemDetails.getInt("itemMDID");                         
                    
                    if( orderItemDetails.getString("itemProdType").compareTo("null") != 0 )
                    	ItemType = ItemTypeValues[orderItemDetails.getInt("itemProdType")];
                    
                    if( DBItemTable == 1 || DBItemTable == 10 )	// Main item                                
                    {             
            			if( LastItemMDID > 0 && LastItemMDID != ItemMDID )
            			{
            				// Create Order Item
            				NewOrderItem = new TillOrderItem();
            				NewOrderItem.SetProductOffer(CurItemID, ItemQty, NewProductOffer, true);
            				NewOrderItem.SetItemSplitQty(ItemSplitQty);
            				mRetOrder.mOrderItems.add(NewOrderItem); 
            				NewProduct1.ClearProduct();
            			}
            			else if( LastItemMDID == 0 && NewProduct1.ProductCode.compareTo("") != 0 )
                    	{
                    		NewOrderItem = new TillOrderItem();                    		
                            NewOrderItem.SetProduct(CurItemID, ItemQty, NewProduct1, NewProduct2, NewProductHalfHalf, false, true);
                            NewOrderItem.SetItemFreeText(ItemFreeText);  
                            NewOrderItem.SetItemSplitQty(ItemSplitQty);
                            NewOrderItem.SetItemPrintedQty(ItemPrintedQty);
                            NewOrderItem.SetItemSplitPrintedQty(ItemSplitPrintedQty);
                            mRetOrder.mOrderItems.add(NewOrderItem);
                            NewProduct1.ClearProduct();
                            ItemFreeText = "";
                            ItemSplitQty = 0;
                    	}     
            			
                    	if( ItemMDID > 0 )
                    	{
                    		if( ItemMDID != LastItemMDID )
                    		{     	                            
    	                    	CurItemID = orderItemDetails.getInt("itemReceiptID"); 
    	                    	ItemQty = orderItemDetails.getDouble("itemQty");        
    	                    	ItemSplitQty = orderItemDetails.getDouble("itemSplitQty");   
    	                        ItemPrintedQty = orderItemDetails.getDouble("itemPrintedQty");   
    	                        ItemSplitPrintedQty = orderItemDetails.getDouble("itemSplitPrintedQty");   
                    			ItemPrice = orderItemDetails.getDouble("itemPrice");  
                    			
                    			// Create Product Offer
                    			NewProductOffer = new TillProductOffer(ItemCode);                     			  
                    			NewProductOffer.SetProductOfferDetails(ItemDesc, ItemPrice);   
                    			LastItemMDID = ItemMDID;
                    		}
                    		else
                    		{
    	                        ItemParentCode = ItemCode;
    	                        ItemParentDesc = ItemDesc;
    	                        ItemCatID = orderItemDetails.getInt("itemProdCat");
    	                        ItemType = ItemTypeValues[orderItemDetails.getInt("itemProdType")];
    	                        ItemSectionPos = orderItemDetails.getInt("itemSectionPos");
    	                        ItemSectionDesc = orderItemDetails.getString("itemSectionDesc");
    	                        ItemExtraToppingCharge = 0;
    	                        if( orderItemDetails.getString("itemTopPrice").compareTo("null") != 0 )
    	                        {
    	                        	ItemExtraToppingCharge = orderItemDetails.getDouble("itemTopPrice");                     
    	                        }
                    			NewProduct1.ClearProduct();
    	                        NewProduct1.ProductCode = ItemCode;
    	                        NewProduct1.ProductDesc = ItemDesc.trim();
    	                        NewProduct1.ProductParentCode = ItemParentCode;
    	                        NewProduct1.ProductParentDesc = ItemParentDesc.trim();
    	                        NewProduct1.ProductClassID = ItemCatID;
    	                        NewProduct1.ProductParentClassID = ItemCatID;
    	                        NewProduct1.ProductPrice = 0;
    	                        NewProduct1.ProductSectionPos = ItemSectionPos;
    	                        NewProduct1.ProductSectionDesc = ItemSectionDesc;
    	                        NewProduct1.ProductToppingCharges = ItemExtraToppingCharge;
    	                        
    	                        NewProduct1.PrintToKitchen = 1;	  
    	                        if( DBItemTable == 10 )
    	                        {
    	                        	NewProduct1.PrintToKitchen = 0;	                        	
    	                        }
    	                        
                    			// Add Product Offer Item
                    			NewPOItemGroup = new TillProductOfferItemGroup();
                    			NewPOItemGroup.SetPOItemGroupDetails(++POItemGroupID, OFFER_ITEM_TYPE.OFFERITEMTYPE_STD, 0);                  			                    			
                    			NewProductOffer.AddPOItemGroup(NewPOItemGroup);
                    			
                    			NewPOItem = new TillProductOfferItem();
                    			NewPOItem.SetPOItemDetails(++POItemID, "", 0, 0, ItemCode, ItemDesc.trim(), 0, true,OFFER_ITEM_TYPE.OFFERITEMTYPE_STD); 
                                NewPOItem.SetPOProducts(NewProduct1, null, true);
                                NewPOItemGroup.AddPOItem(NewPOItem);
                    		}                    		
                    	}
                    	else
                    	{
                    		LastItemMDID = 0;
	                    	if( NewProduct1.ProductCode.compareTo("") != 0 )
	                    	{
	                    		NewOrderItem = new TillOrderItem();
	                            NewOrderItem.SetProduct(CurItemID, ItemQty, NewProduct1, NewProduct2, NewProductHalfHalf, false, true);
	                            NewOrderItem.SetItemFreeText(ItemFreeText);  
	                            NewOrderItem.SetItemSplitQty(ItemSplitQty);
	                            NewOrderItem.SetItemPrintedQty(ItemPrintedQty);
	                            NewOrderItem.SetItemSplitPrintedQty(ItemSplitPrintedQty);
	                            mRetOrder.mOrderItems.add(NewOrderItem);
	                            NewProduct1.ClearProduct();
	                            ItemFreeText = "";
	                            ItemSplitQty = 0;
	                    	}     
	                    	
	
	                    	CurItemID = orderItemDetails.getInt("itemReceiptID"); 
	                    	ItemSplitQty = orderItemDetails.getDouble("itemSplitQty");   
	                        ItemParentCode = ItemCode;
	                        ItemParentDesc = ItemDesc;
	                        ItemCatID = orderItemDetails.getInt("itemProdCat");
	                        ItemType = ItemTypeValues[orderItemDetails.getInt("itemProdType")];
	                        //ItemInfo = orderItemDetails.getString("itemReceiptID");
	                        ItemSectionPos = orderItemDetails.getInt("itemSectionPos");
	                        ItemSectionDesc = orderItemDetails.getString("itemSectionDesc");
	                        ItemQty = orderItemDetails.getDouble("itemQty");    
	                        ItemPrintedQty = orderItemDetails.getDouble("itemPrintedQty");   
	                        ItemSplitPrintedQty = orderItemDetails.getDouble("itemSplitPrintedQty");   
	                        ItemPrice = orderItemDetails.getDouble("itemPrice");   
	                        ItemExtraToppingCharge = 0;
	                        if( orderItemDetails.getString("itemTopPrice").compareTo("null") != 0 )
	                        {
	                        	ItemExtraToppingCharge = orderItemDetails.getDouble("itemTopPrice");                     
	                        }
	                        
	                        NewProduct1.ProductCode = ItemCode;
	                        NewProduct1.ProductDesc = ItemDesc;
	                        NewProduct1.ProductParentCode = ItemParentCode;
	                        NewProduct1.ProductParentDesc = ItemParentDesc;
	                        NewProduct1.ProductClassID = ItemCatID;
	                        NewProduct1.ProductParentClassID = ItemCatID;
	                        NewProduct1.ProductPrice = ItemPrice;
	                        NewProduct1.ProductSectionPos = ItemSectionPos;
	                        NewProduct1.ProductSectionDesc = ItemSectionDesc;
	                        NewProduct1.ProductToppingCharges = ItemExtraToppingCharge;
	                        
	                        
	                        NewProduct1.PrintToKitchen = 1;	  
	                        if( DBItemTable == 10 )
	                        {
	                        	NewProduct1.PrintToKitchen = 0;	                        	
	                        }
                    	}
                    }                       
                    else if( DBItemTable == 2 || DBItemTable == 11 )	// Sub Item                                
                    {
                    	if( ItemCode.compareTo("TOPITEM") != 0 )	// Ignore TOPITEM
                    	{
	                        ItemTopping.ToppingCode = ItemCode;
	                        ItemTopping.ToppingDesc = ItemDesc;
	                        if (ItemType == ITEM_TYPE.PT_TOPPLUS)
	                        {
	                            ItemTopping.ToppingState = TOPPING_STATE_CODE.TOP_ADDED;
	                        }
	                        else if (ItemType == ITEM_TYPE.PT_TOPMINUS)
	                        {
	                            ItemTopping.ToppingState = TOPPING_STATE_CODE.TOP_FIXED_REMOVED;
	                        }
	
	                        if (bHalf2)
	                        {
	                            NewProduct2.AddTopping(ItemTopping, ItemTopping.ToppingState, false);
	                        }
	                        else
	                        {
	                            NewProduct1.AddTopping(ItemTopping, ItemTopping.ToppingState, false);
	                        }   
	                        
	                        if( ItemMDID > 0 )
	                        {
	                        	NewPOItem.SetPOProducts(NewProduct1, null, true);	                        	
	                        }
	                        else
	                        {
	                        	/*
		                    	if( NewProduct1.ProductCode.compareTo("") != 0 )
		                    	{
		                    		NewOrderItem.SetProduct(CurItemID, ItemQty, NewProduct1, NewProduct2, NewProductHalfHalf, false, true);
		                    	}
		                    	*/
	                        }
                    	}
                    }    
                    else if( DBItemTable == 3 )	// Instruction                                
                    {
                    	                                                     
                    }    
                    else if( DBItemTable == 4 )	// Free Text                                
                    {
                    	ItemFreeText = ItemDesc;                    	                                                     
                    }   
                }
            	// Add last item
    			if( LastItemMDID > 0 )
    			{
    				// Create Order Item
    				NewOrderItem = new TillOrderItem();
    				NewOrderItem.SetProductOffer(CurItemID, ItemQty, NewProductOffer, true);
    				NewOrderItem.SetItemSplitQty(ItemSplitQty);
                    NewOrderItem.SetItemPrintedQty(ItemPrintedQty);
                    NewOrderItem.SetItemSplitPrintedQty(ItemSplitPrintedQty);
    				mRetOrder.mOrderItems.add(NewOrderItem); 
    				LastItemMDID = 0;
    			}
    			else if( NewProduct1.ProductCode.compareTo("") != 0 )
            	{
            		NewOrderItem = new TillOrderItem();
                    NewOrderItem.SetProduct(CurItemID, ItemQty, NewProduct1, NewProduct2, NewProductHalfHalf, false, true);
                    NewOrderItem.SetItemFreeText(ItemFreeText);  
                    NewOrderItem.SetItemSplitQty(ItemSplitQty);
                    NewOrderItem.SetItemPrintedQty(ItemPrintedQty);
                    NewOrderItem.SetItemSplitPrintedQty(ItemSplitPrintedQty);
                    mRetOrder.mOrderItems.add(NewOrderItem);
                    NewProduct1.ClearProduct();
            	}  

    			mRetOrder.mOrderDetails.setItemIDCounter(CurItemID);
            	
            	// Load Split Bill Data
            	mRetOrder.mSplitBillItems.clear();
            	int mSplitNo;
            	String mSplitRef;
            	int mSplitItemID;
            	double mSplitItemQty;
            	double mSplitItemPrintedQty;
            	mSplitBillData = json.getJSONArray(TAG_SPLITBILLDATA);	
            	for (int s = 0; s < mSplitBillData.length(); s++) 
            	{   
            		JSONObject splitItemDetails = mSplitBillData.getJSONObject(s);  
            		mSplitNo = splitItemDetails.getInt("SplitNo");         
            		mSplitRef = splitItemDetails.getString("SplitRef");         
            		mSplitItemID = splitItemDetails.getInt("SplitItemID");         
            		mSplitItemQty = splitItemDetails.getDouble("SplitItemQty");         
            		mSplitItemPrintedQty = splitItemDetails.getDouble("SplitItemPrintedQty");         
            		TillSplitItem NewSplitItem = new TillSplitItem();
            		NewSplitItem.mSplitID = mSplitNo;
            		NewSplitItem.mSplitRef = mSplitRef;
            		NewSplitItem.mItemID = mSplitItemID;
            		NewSplitItem.mItemQty = mSplitItemQty;
            		NewSplitItem.mItemPrintedQty = mSplitItemPrintedQty;
            		mRetOrder.mSplitBillItems.add(NewSplitItem);
            	}
            	
            	// Load Split Pay Data
            	mSplitPayData = json.getJSONArray(TAG_SPLITPAYDATA);	
            	for (int s = 0; s < mSplitPayData.length(); s++) 
            	{   
            		JSONObject splitPayItemDetails = mSplitPayData.getJSONObject(s);  
                                                        
            		TillSplitPayDetails NewSplitPayItem = new TillSplitPayDetails();
            		NewSplitPayItem.setSpliyPayID(s+1);
            		NewSplitPayItem.setSpliyPayTypeCode(splitPayItemDetails.getInt("SplitPayTypeCode"));
            		NewSplitPayItem.setSpliyPayDesc(splitPayItemDetails.getString("SplitPayTypeDesc"));
            		NewSplitPayItem.setSpliyPayAmount(splitPayItemDetails.getDouble("SplitPayAmt"));
            		mRetOrder.mOrderDetails.setOrderSplitPayItem(NewSplitPayItem);
            	}
            	
            	mRetOrder.mOrderLoaded = true;
			}
			
			
			
			/*
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("orderID",Integer.toString(in_OrderID) ));
		
			JSONObject json = mjParser.makeHttpRequest(url_loadorder,"GET", params);	
			ITEM_TYPE[] ItemTypeValues = ITEM_TYPE.values();
			int success = json.getInt(TAG_SUCCESS);
			if (success == 1) 
			{	
				//mOrderDetails = new TillOrderDetails();
		    	//mOrderItems	= new ArrayList<TillOrderItem>();	
				mOrderData = json.getJSONArray(TAG_ORDERDATA);	
            	for (int i = 0; i < mOrderData.length(); i++) 
            	{   
            		//JSONObject orderDetails = mOrderData.getJSONObject(i);  
            		//mRetOrder.mOrderDetails.add(orderDetails);
            		
    				JSONObject tmpOrderDetails = mOrderData.getJSONObject(i);
    				mRetOrder.mOrderDetails.setOrderID(tmpOrderDetails.getInt("orderID"));
    				mRetOrder.mOrderDetails.setOrderNo(tmpOrderDetails.getInt("orderNo"));
    				mRetOrder.mOrderDetails.setOrderTable(tmpOrderDetails.getInt("orderTableNo"),tmpOrderDetails.getInt("orderTableNo"));
    				mRetOrder.mOrderDetails.setOrderTransID(tmpOrderDetails.getInt("orderLastTransID"));
            	}
                
            	//boolean OrderItemsProcessed = false;                                                
                int ItemID = 0;
                int DBItemID = 0;
                                        
                String ItemCode, ItemDesc;
                String ItemParentCode, ItemParentDesc;
                String ItemInfo;
                double ItemQty,ItemPrice; 
                //double ItemSplitQty, ItemVATAmt, ItemVatableAmt, ItemToppingCharges; 
                double ItemVATRate;
                ITEM_TYPE ItemType;
                int ItemCatID;
                int ItemSectionPos;
                String ItemSectionDesc;
                //int ItemLinkID;
                //int OfferID = 0;
                //double OfferQty = 0;
                //int POItemGroupID = 0;
                //int POItemID = 0;
                boolean bHalf2 = false;
                boolean bOfferMode = false;
                boolean bHalfHalfMode = false;
                boolean bFirstItem = true;
                //int ItemPosID = 0;
                //int ItemSubPosID = 0;
                //int LastPosID = 0;
                int CurItemID = 0;

                List<Integer> ItemIDList = new ArrayList<Integer>();
                TillOrderItem NewOrderItem = null;
                TillProduct NewProduct1 = new TillProduct(); 
                TillProduct NewProduct2 = new TillProduct(); 
                TillProduct NewProductHalfHalf = new TillProduct(); 
                //TillProductOffer NewProductOffer = null;
                TillToppings ItemTopping = new TillToppings();
                //TillProductOfferItemGroup NewPOItemGroup = null;
                //TillProductOfferItem NewOfferItem = null;
                
        		mOrderItemsData = json.getJSONArray(TAG_ORDERITEMDATA);
        		
            	for (int j = 0; j < mOrderItemsData.length(); j++) 
            	{   
            		JSONObject orderItemDetails = mOrderItemsData.getJSONObject(j);  
                    DBItemID = orderItemDetails.getInt("itemReceiptID");                               
                    if (ItemID != DBItemID)                                
                    {
                        ItemID = DBItemID;
                        ItemIDList.add(DBItemID);                                                            
                    }                                                
                }
                
            	for (int k = 0; k < ItemIDList.size(); k++) 
            	{   
                    NewProduct1.ClearProduct();
                    NewProduct2.ClearProduct();
                    NewProductHalfHalf.ClearProduct();
                    bHalfHalfMode = false;
                    bHalf2 = false;
                    bOfferMode = false;
                    bFirstItem = true;                            
                    ItemQty = 0;
                    ItemPrice = 0;
                    //LastPosID = 0;
                    ItemType = ITEM_TYPE.PT_STD;
                    
                    CurItemID = ItemIDList.get(k);
                    
                    NewOrderItem = new TillOrderItem();
                    
                	for (int l = 0; l < mOrderItemsData.length(); l++) 
                	{   
                		
                		JSONObject orderItemDetails = mOrderItemsData.getJSONObject(l);  
                		if( orderItemDetails.getInt("itemReceiptID") == CurItemID )
                		{
                            ItemCode = orderItemDetails.getString("itemCode");
                            ItemDesc = orderItemDetails.getString("itemDesc");
                            ItemParentCode = ItemCode;
                            ItemParentDesc = ItemDesc;
                            ItemCatID = orderItemDetails.getInt("itemProdCat");
                            //ItemSubPosID = orderItemDetails.getString("itemReceiptID");
                            ItemType = ItemTypeValues[orderItemDetails.getInt("itemProdType")];
                            //ItemType = ITEM_TYPE.PT_STD;

                            ItemInfo = orderItemDetails.getString("itemReceiptID");
                            ItemSectionPos = orderItemDetails.getInt("itemSectionPos");
                            ItemSectionDesc = orderItemDetails.getString("itemSectionDesc");
                            
                            // First item row contains qty and price
                            if (bFirstItem)
                            {
                                ItemQty = 0;
                                //ItemSplitQty = 0;
                                ItemPrice = 0;
                                ItemQty = orderItemDetails.getDouble("itemQty");                                   
                                //ItemSplitQty = orderItemDetails.getDouble("itemQty");
                                ItemPrice = orderItemDetails.getDouble("itemPrice");                                        
                            }
                            //ItemLinkID = 0;                                
                            //ItemLinkID = orderItemDetails.getInt("itemLinkID");  
                            ItemVATRate = 0;
                            //ItemVATRate = orderItemDetails.getDouble("itemVATRate");  
                            //ItemVATAmt = 0;
                            //ItemVATAmt = orderItemDetails.getDouble("itemVATAmt");  
                            //ItemVatableAmt = 0;
                            //ItemVatableAmt = orderItemDetails.getDouble("itemVatableAmt");  
                            //ItemToppingCharges = 0;
                            //ItemToppingCharges = orderItemDetails.getDouble("itemTopPrice");  
                            
                            if (ItemType == ITEM_TYPE.PT_STD)
                            {
                                if (bHalf2)
                                {
                                }
                                else
                                {
                                    //NewProduct1 = new TillProduct();
                                    NewProduct1.ProductCode = ItemCode;
                                    NewProduct1.ProductDesc = ItemDesc;
                                    NewProduct1.ProductParentCode = ItemParentCode;
                                    NewProduct1.ProductParentDesc = ItemParentDesc;
                                    NewProduct1.ProductClassID = ItemCatID;
                                    NewProduct1.ProductParentClassID = ItemCatID;
                                    NewProduct1.ProductPrice = ItemPrice;
                                    NewProduct1.ProductSectionPos = ItemSectionPos;
                                    NewProduct1.ProductSectionDesc = ItemSectionDesc;

                                   // NewProduct1.CopyProductToppingPrices(GetProductToppingPriceList(ItemCode));

                                }
                                if (bFirstItem)
                                {
                                    bFirstItem = false;
                                }
                                //LastPosID = ItemPosID;
                            }
                            else if (ItemType == ITEM_TYPE.PT_HH)
                            {
                            }
                            else if (ItemType == ITEM_TYPE.PT_TOPPLUS || ItemType == ITEM_TYPE.PT_TOPMINUS)
                            {
                                ItemTopping.ToppingCode = ItemCode;
                                ItemTopping.ToppingDesc = ItemDesc;
                                if (ItemInfo.compareTo("") != 0)
                                    ItemTopping.ToppingType = Integer.parseInt(ItemInfo);
                                ItemTopping.ToppingVATCode = 0;
                                if (ItemVATRate > 0)
                                    ItemTopping.ToppingVATCode = 1;

                                if (ItemType == ITEM_TYPE.PT_TOPPLUS)
                                {
                                    ItemTopping.ToppingState = TOPPING_STATE_CODE.TOP_ADDED;
                                }
                                else if (ItemType == ITEM_TYPE.PT_TOPMINUS)
                                {
                                    ItemTopping.ToppingState = TOPPING_STATE_CODE.TOP_FIXED_REMOVED;
                                }

                                if (bHalf2)
                                {
                                    NewProduct2.AddTopping(ItemTopping, ItemTopping.ToppingState);
                                }
                                else
                                {
                                    NewProduct1.AddTopping(ItemTopping, ItemTopping.ToppingState);
                                }                            	
                            }
                		}   
                	}
                		
                	if (bOfferMode)
                    {

                    }
                    else
                    {
                        NewOrderItem.SetProduct(CurItemID, ItemQty, NewProduct1, NewProduct2, NewProductHalfHalf, bHalfHalfMode, true);
                    }
                    
                    mRetOrder.mOrderItems.add(NewOrderItem);

            	}
            	
            	
            	//mOrderLoaded = true;
            	mRetOrder.mOrderLoaded = true;
			} 
			else 
			{
				mErrorMsg = json.getString(TAG_MSG);
				if( mErrorMsg == null )
					mErrorMsg = "Could not load Order";
			}
			*/
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		return mRetOrder;
	}
    
    public ArrayList<Integer> SaveOrder(TillOrderDetails in_OrderDetails, ArrayList<TillOrderItem> in_OrderItems, ArrayList<TillSplitItem> in_SplitBillItems)         
	{   
    	ArrayList<Integer> retRes = new ArrayList<Integer>();
    	int mSavedOrderID = 0;
    	int mNewTransID = 0;    	
    	
    	//boolean mOrderSaved = false;
    	mOrderDetails = in_OrderDetails;
    	mOrderItems = in_OrderItems;
    	mSplitBillItems = null;
    	mSplitBillItems = in_SplitBillItems;

    	mErrorMsg = "";
    	
		try 
		{
			PrepareOrder();							
		    //String gzipStr = CompressString(mOrderDetailsStr);
		        
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			//params.add(new BasicNameValuePair("orderDetails",gzipStr ));
			params.add(new BasicNameValuePair("orderDetails",mOrderDetailsStr ));
		
			JSONObject json = mjParser.makeHttpRequest(url_saveorder,"POST", params);

			int success = json.getInt(TAG_SUCCESS);
			if (success == 1) 
			{	
				//mOrderSaved = true;		
				mSavedOrderID = json.getInt("orderID");
				mNewTransID = json.getInt("transID");
				retRes.add(mSavedOrderID);
				retRes.add(mNewTransID);
			} 
			else 
			{
				mErrorMsg = json.getString(TAG_MSG);
				if( mErrorMsg.compareTo("") == 0 )
					mErrorMsg = "Could not save Order";
			}
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
		return retRes;
		//return mSavedOrderID;
		
	}
    
	private void PrepareOrder()
	{			
		
		try
		{
			JSONObject orderDetailsObj=new JSONObject();
			// Use short field names to reduce data size
		    // O1: orderID
		    // O2: orderNo
		    // O3: orderDate
		    // O4: orderTableNo
		    // O5: orderPaidAmt
		    // O6: orderTotal
		    // O7: orderAccRef
		    // O8: orderPrepayment
		    // O9: orderNet
		    // O10: orderVat
		    // O11: orderVATCode
		    // O12: orderDiscAmt
		    // O13: orderDiscRate
		    // O14: orderTableCovers
		    // O15: orderDeliveryType
		    // O16: orderPayType
		    // O17: orderLastTransID
		    // O18: orderInsert
		    // O19: orderDeposit
		    // O20: orderTip
		    // O21: orderTipCC
		    // O22: orderServiceCharge
		    // O23: orderServiceChargePerc		
			// O24: orderPrintedToKitchen
			// O25: orderBillPrinted
			
			orderDetailsObj.put("O1", mOrderDetails.getOrderID());
			orderDetailsObj.put("O2", mOrderDetails.getOrderNo());
			orderDetailsObj.put("O3", mOrderDetails.getOrderDateString());
			orderDetailsObj.put("O4",Integer.toString(mOrderDetails.getOrderTableNo()));				
			orderDetailsObj.put("O5", mOrderDetails.getOrderPayment());				
			orderDetailsObj.put("O6", mOrderDetails.getOrderTotal());				
			orderDetailsObj.put("O7", mOrderDetails.OrderCustDetails.CustNo);				
			orderDetailsObj.put("O8", mOrderDetails.getOrderPayment());						
			orderDetailsObj.put("O9", mOrderDetails.getOrderNET());				
			orderDetailsObj.put("O10", mOrderDetails.getOrderVAT());				
			orderDetailsObj.put("O11", mOrderDetails.getOrderVATCode());				
			orderDetailsObj.put("O12", mOrderDetails.getOrderDiscountAmt());				
			orderDetailsObj.put("O13", mOrderDetails.getOrderDiscountPerc());					
			orderDetailsObj.put("O14", Integer.toString(mOrderDetails.getOrderTableCovers()));				
			orderDetailsObj.put("O15", "");				
			orderDetailsObj.put("O16", mOrderDetails.getOrderPayTypeCode());				
			orderDetailsObj.put("O17", mOrderDetails.getOrderTransID());					
			orderDetailsObj.put("O18", mOrderDetails.getbInsertOrder());	
			orderDetailsObj.put("O19", mOrderDetails.getOrderDeposit());	
			orderDetailsObj.put("O20", mOrderDetails.getOrderTipCash());	
			orderDetailsObj.put("O21", mOrderDetails.getOrderTipCC());	
			orderDetailsObj.put("O22", mOrderDetails.getOrderServiceChargeAmt());	
			orderDetailsObj.put("O23", mOrderDetails.getOrderServiceChargePerc());	
			if( mOrderDetails.GetPrintedToKitchen() )
			{
				orderDetailsObj.put("O24", 1);	
			}
			else
			{
				orderDetailsObj.put("O24", 0);		
			}
			
			if( mOrderDetails.GetBillPrinted() )
			{
				orderDetailsObj.put("O25", 1);	
			}
			else
			{
				orderDetailsObj.put("O25", 0);		
			}			

			TillProduct tmpProduct;		    
			TillProductOffer tmpProductOffer;		
			
			int posID;
			boolean AddItem = false;
			ArrayList<String> retPOItemDetails;
			ArrayList<TillProductOfferItemGroup> TmpPOItemGroupList;
			ArrayList<TillProductOfferItem> TmpPOItemsList;
			retTypeCreateProductRes prodRes;
			
            OrderItemRecordData TmpItemRecordData = new OrderItemRecordData();
			JSONArray orderItems = new JSONArray();
			for( TillOrderItem mOrderItem : mOrderItems)
			{					
                posID=1;
                TmpItemRecordData.ClearOrderItemRecordData();

            	TmpItemRecordData.ItemID = mOrderItem.GetItemID();
            	TmpItemRecordData.ItemDesc = mOrderItem.GetItemDesc();
            	TmpItemRecordData.ItemQty = mOrderItem.GetItemQty();
            	TmpItemRecordData.ItemNET = mOrderItem.GetItemNET();
            	TmpItemRecordData.ItemVAT = mOrderItem.GetItemVAT();
            	TmpItemRecordData.ItemVATCode = mOrderItem.GetItemVATCode();
            	TmpItemRecordData.ItemVATRate = mOrderItem.GetItemVATRate();
            	if (mOrderItem.GetItemNET() > 0)
            		TmpItemRecordData.ItemSalePrice = TmpItemRecordData.ItemNET / TmpItemRecordData.ItemQty;
            	TmpItemRecordData.ItemType = ITEM_TYPE.PT_OFFER;
            	TmpItemRecordData.ItemToppingNET = mOrderItem.GetItemToppingNET();
            	TmpItemRecordData.ItemToppingVAT = mOrderItem.GetItemToppingVAT();
            	TmpItemRecordData.ItemFreeText = mOrderItem.GetItemFreeText();            	
            	TmpItemRecordData.ItemSplitQty = mOrderItem.GetItemSplitQty();
            	TmpItemRecordData.ItemPrintedQty = mOrderItem.GetItemPrintedQty();
            	TmpItemRecordData.ItemSplitPrintedQty = mOrderItem.GetItemSplitPrintedQty();    
            	
                if (mOrderItem.GetItemOffer())
                {
                	tmpProductOffer = mOrderItem.GetProductOffer();
                	
                	int ItemID = TmpItemRecordData.ItemID;     
                	int MDID = ItemID;                	            	
                	
                	TmpItemRecordData.ItemCode = tmpProductOffer.GetPOCode();   
                	TmpItemRecordData.ItemMDID = MDID;
                	TmpItemRecordData.ItemPrintToKitchen = 1;
       	
                	prodRes = CreateProductRecord(TmpItemRecordData, posID);  
                	if( prodRes.NewJSONObj != null)
                		orderItems.put(prodRes.NewJSONObj);	

                	
	                TmpPOItemGroupList = tmpProductOffer.GetPOItemGroupList();	                
	        		for (TillProductOfferItemGroup POItemGroup : TmpPOItemGroupList)
	        		{
	        			TmpItemRecordData.ClearOrderItemRecordData();
	                	TmpItemRecordData.ItemQty = mOrderItem.GetItemQty();
	                	TmpItemRecordData.ItemSplitQty = mOrderItem.GetItemSplitQty();
	                	TmpItemRecordData.ItemPrintedQty = mOrderItem.GetItemPrintedQty();
	                	TmpItemRecordData.ItemSplitPrintedQty = mOrderItem.GetItemSplitPrintedQty(); 
	                	TmpItemRecordData.ItemMDID = MDID;
	                	
	                	
	        			TmpPOItemsList = POItemGroup.GetPOItemList();
	        			for (TillProductOfferItem POItem : TmpPOItemsList)
	        			{		
	        				AddItem = true;
	        				retPOItemDetails = POItem.GetPOItemDetails();
	            			if( retPOItemDetails.get(7).compareTo("1") != 0 )
	            			{
	            				AddItem = false;	            				
	            			}
	            			if( AddItem )
	            			{
	            				
	                        	tmpProduct = POItem.GetPOProduct1();

	                        	TmpItemRecordData.Product1Code = POItem.GetPOProdCode();
	                        	TmpItemRecordData.Product1Desc = tmpProduct.ProductDesc;
	                        	TmpItemRecordData.Product1ParentCode = tmpProduct.ProductParentCode;
	                        	TmpItemRecordData.Product1ParentDesc = tmpProduct.ProductParentDesc;
	                        	TmpItemRecordData.Product1ParentClassID = tmpProduct.ProductParentClassID;
	                        	TmpItemRecordData.Product1Price = tmpProduct.ProductPrice;
	                        	TmpItemRecordData.Product1ToppingNET = tmpProduct.ProductToppingNET;
	                        	TmpItemRecordData.Product1ToppingVAT = tmpProduct.ProductToppingVAT;
	                        	TmpItemRecordData.Product1VatableAmount = tmpProduct.ProductVatableAmount;
	                        	TmpItemRecordData.Product1ToppingCharges = tmpProduct.ProductToppingCharges;
	                        	TmpItemRecordData.Product1SectionPos = tmpProduct.ProductSectionPos;
	                        	TmpItemRecordData.Product1SectionDesc = tmpProduct.ProductSectionDesc;
	                        	TmpItemRecordData.Product1TopBut1 = tmpProduct.ProductTopBut1;
	                        	TmpItemRecordData.Product1TopBut2 = tmpProduct.ProductTopBut2;
	                        	TmpItemRecordData.Product1ToppingsList = tmpProduct.ProductToppingsList;	            				
	            				
	                        	TmpItemRecordData.ItemID = ++ItemID;
	                        	TmpItemRecordData.ItemCode = POItem.GetPOProdCode();     
	                        	TmpItemRecordData.ItemDesc = "   " + TmpItemRecordData.Product1Desc;                       	
	                        	TmpItemRecordData.ItemPrintToKitchen = tmpProduct.PrintToKitchen;
	                        	
	                        	prodRes = CreateProductRecord(TmpItemRecordData, posID);  
	                        	if( prodRes.NewJSONObj != null)
	                        		orderItems.put(prodRes.NewJSONObj);	
	                        	
	            				break;
	            			}
	        			}
	        		}
                }
                else
                {
                	tmpProduct = mOrderItem.GetProduct1();

                	TmpItemRecordData.Product1Code = tmpProduct.ProductCode;
                	TmpItemRecordData.Product1Desc = tmpProduct.ProductDesc;
                	TmpItemRecordData.Product1ParentCode = tmpProduct.ProductParentCode;
                	TmpItemRecordData.Product1ParentDesc = tmpProduct.ProductParentDesc;
                	TmpItemRecordData.Product1ParentClassID = tmpProduct.ProductParentClassID;
                	TmpItemRecordData.Product1Price = tmpProduct.ProductPrice;
                	TmpItemRecordData.Product1ToppingNET = tmpProduct.ProductToppingNET;
                	TmpItemRecordData.Product1ToppingVAT = tmpProduct.ProductToppingVAT;
                	TmpItemRecordData.Product1VatableAmount = tmpProduct.ProductVatableAmount;
                	TmpItemRecordData.Product1ToppingCharges = tmpProduct.ProductToppingCharges;
                	TmpItemRecordData.Product1SectionPos = tmpProduct.ProductSectionPos;
                	TmpItemRecordData.Product1SectionDesc = tmpProduct.ProductSectionDesc;
                	TmpItemRecordData.Product1TopBut1 = tmpProduct.ProductTopBut1;
                	TmpItemRecordData.Product1TopBut2 = tmpProduct.ProductTopBut2;
                	TmpItemRecordData.Product1ToppingsList = tmpProduct.ProductToppingsList;

                	
                	TmpItemRecordData.ItemCode = TmpItemRecordData.Product1Code;
                	TmpItemRecordData.ItemPrintToKitchen = tmpProduct.PrintToKitchen;
                	
                	/*
                	TmpItemRecordData.ItemID = mOrderItem.GetItemID();
                	TmpItemRecordData.ItemCode = TmpItemRecordData.Product1Code;
                	TmpItemRecordData.ItemDesc = mOrderItem.GetItemDesc();
                	TmpItemRecordData.ItemQty = mOrderItem.GetItemQty();
                	TmpItemRecordData.ItemNET = mOrderItem.GetItemNET();
                	TmpItemRecordData.ItemVAT = mOrderItem.GetItemVAT();
                	TmpItemRecordData.ItemVATCode = mOrderItem.GetItemVATCode();
                	TmpItemRecordData.ItemVATRate = mOrderItem.GetItemVATRate();
                	if (mOrderItem.GetItemNET() > 0)
                		TmpItemRecordData.ItemSalePrice = TmpItemRecordData.ItemNET / TmpItemRecordData.ItemQty;
                	TmpItemRecordData.ItemType = ITEM_TYPE.PT_STD;
                	TmpItemRecordData.ItemToppingNET = mOrderItem.GetItemToppingNET();
                	TmpItemRecordData.ItemToppingVAT = mOrderItem.GetItemToppingVAT();
                	TmpItemRecordData.ItemFreeText = mOrderItem.GetItemFreeText();
                	
                	TmpItemRecordData.ItemSplitQty = mOrderItem.GetItemSplitQty();
                	TmpItemRecordData.ItemPrintedQty = mOrderItem.GetItemPrintedQty();
                	TmpItemRecordData.ItemSplitPrintedQty = mOrderItem.GetItemSplitPrintedQty();  
                	*/     			

                	TmpItemRecordData.ItemHalfHalf = false;
                	if (mOrderItem.GetItemHalfHalf())
                	{
                		TmpItemRecordData.ItemProductHalfHalf = mOrderItem.GetProductHalfHalf();
                		TmpItemRecordData.ItemHalfHalf = true;
                		tmpProduct = mOrderItem.GetProduct2();
                		TmpItemRecordData.Product2Code = tmpProduct.ProductCode;
                		TmpItemRecordData.Product2Desc = tmpProduct.ProductDesc;
                		TmpItemRecordData.Product2ParentCode = tmpProduct.ProductParentCode;
                		TmpItemRecordData.Product2ParentDesc = tmpProduct.ProductParentDesc;
                		TmpItemRecordData.Product2ParentClassID = tmpProduct.ProductParentClassID;
                		TmpItemRecordData.Product2Price = tmpProduct.ProductPrice;
                		TmpItemRecordData.Product2ToppingNET = tmpProduct.ProductToppingNET;
                		TmpItemRecordData.Product2ToppingVAT = tmpProduct.ProductToppingVAT;
                		TmpItemRecordData.Product2VatableAmount = tmpProduct.ProductVatableAmount;
                		TmpItemRecordData.Product2ToppingCharges = tmpProduct.ProductToppingCharges;
                		//	TmpItemRecordData.Product2ToppingsList = tmpProduct.ProductToppingsList;
                		TmpItemRecordData.ItemProductHalfHalf = mOrderItem.GetProductHalfHalf();
                    	TmpItemRecordData.Product2TopBut1 = tmpProduct.ProductTopBut1;
                    	TmpItemRecordData.Product2TopBut2 = tmpProduct.ProductTopBut2;
                	}
                	prodRes = CreateProductRecord(TmpItemRecordData, posID);  
                	if( prodRes.NewJSONObj != null)
                		orderItems.put(prodRes.NewJSONObj);	
                }

			}						
			orderDetailsObj.put("orderItems", orderItems);
			
			// Split Bill Items
			JSONArray splitItems = new JSONArray();
			for( TillSplitItem mSplitItem : mSplitBillItems)
			{		
				JSONObject splitItemObj=new JSONObject();
				splitItemObj.put("splitNo", mSplitItem.mSplitID);
				splitItemObj.put("splitRef", mSplitItem.mSplitRef);
				splitItemObj.put("splitItemID", mSplitItem.mItemID);				
				splitItemObj.put("splitItemQty", mSplitItem.mItemQty);
				splitItemObj.put("splitItemPrintedQty", mSplitItem.mItemPrintedQty);
				splitItems.put(splitItemObj);				
			}
			orderDetailsObj.put("splitItems", splitItems);
			
			// Split Pay Items
			ArrayList<TillSplitPayDetails> TmpSplitPayList = mOrderDetails.getOrderSplitPayList();
			JSONArray splitPayItems = new JSONArray();
			for( TillSplitPayDetails mSplitPayItem : TmpSplitPayList)
			{		
				JSONObject splitPayItemObj=new JSONObject();
				splitPayItemObj.put("splitPayTypeCode", mSplitPayItem.getSpliyPayTypeCode());
				splitPayItemObj.put("splitPayTypeDesc", mSplitPayItem.getSpliyPayDesc());		
				splitPayItemObj.put("splitPayAmt", mSplitPayItem.getSpliyPayAmount());		
				splitPayItems.put(splitPayItemObj);				
			}
			orderDetailsObj.put("splitPayItems", splitPayItems);
			
			mOrderDetailsStr = orderDetailsObj.toString();	
		}
		catch (JSONException e) 
		{
			e.printStackTrace();
		}					
	}			


	retTypeCreateProductRes CreateProductRecord(OrderItemRecordData in_ItemRecordData, int in_PosID )
	{   	
		int SubPosID = 1;
    
		/*
    	if (in_ItemRecordData.ItemHalfHalf)
    	{
        	CreateHHCode(in_ItemRecordData, ref in_PosID, ref SubPosID);
        	//in_PosID++;
        	SubPosID++;
    	}
		 */

		retTypeCreateProductRes prodRes = new retTypeCreateProductRes();
		prodRes.NewPosID = in_PosID;
		prodRes.NewJSONObj = null;
    	
		try
		{
			// Use short field names to reduce data transfer size
		    // I1: itemCode
		    // I2: itemDesc
		    // I3: itemQty
		    // I4: itemSalePrice
		    // I5: itemNET
		    // I6: itemVAT
		    // I7: itemVATCode
		    // I8: itemVATRate
		    // I9: itemID
		    // I10: itemPosID
		    // I11: itemSubPosID
		    // I12: itemProdType
		    // I13: itemProdClass
		    // I14: itemParentProdCode
		    // I15: itemParentProdDesc
		    // I16: itemVatableAmt
		    // I17: itemExtraToppingPrice
		    // I18: itemLinkID
		    // I19: itemSectionPos
		    // I20: itemSectionDesc
		    // I21: itemTopBut1
		    // I22: itemTopBut2
		    // I23: itemFreeText		
			// I24: itemSplitQty
			// I25: itemPrintedQty
			// I26: temSplitPrintedQty
			// I27: itemMDID
			// I28: itemPrintToKitchen
			// I29: itemToppingList
			JSONObject orderItemObj=new JSONObject();
			orderItemObj.put("I1", in_ItemRecordData.ItemCode);
			orderItemObj.put("I2", in_ItemRecordData.ItemDesc);
			orderItemObj.put("I3", in_ItemRecordData.ItemQty);
			orderItemObj.put("I4", in_ItemRecordData.ItemSalePrice);
			orderItemObj.put("I5", in_ItemRecordData.ItemNET);
			orderItemObj.put("I6", in_ItemRecordData.ItemVAT);
			orderItemObj.put("I7", in_ItemRecordData.ItemVATCode);
			orderItemObj.put("I8", in_ItemRecordData.ItemVATRate);       
			orderItemObj.put("I9", in_ItemRecordData.ItemID);  
			orderItemObj.put("I10", in_PosID);  
			orderItemObj.put("I11", SubPosID++);  
			orderItemObj.put("I12", in_ItemRecordData.ItemType.ordinal());  
			orderItemObj.put("I13", in_ItemRecordData.Product1ParentClassID);  
			orderItemObj.put("I14", in_ItemRecordData.Product1ParentCode);  
			orderItemObj.put("I15", in_ItemRecordData.Product1ParentDesc);  			
			orderItemObj.put("I16", in_ItemRecordData.Product1VatableAmount);   
			orderItemObj.put("I17", in_ItemRecordData.Product1ToppingCharges);   
			orderItemObj.put("I18", 0); // TODO   
			orderItemObj.put("I19", in_ItemRecordData.Product1SectionPos);  
			orderItemObj.put("I20", in_ItemRecordData.Product1SectionDesc); 
			orderItemObj.put("I21", in_ItemRecordData.Product1TopBut1);     	
			orderItemObj.put("I22", in_ItemRecordData.Product1TopBut2);     
			orderItemObj.put("I23", in_ItemRecordData.ItemFreeText);  
			orderItemObj.put("I24", in_ItemRecordData.ItemSplitQty); 
			orderItemObj.put("I25", in_ItemRecordData.ItemPrintedQty);    
			orderItemObj.put("I26", in_ItemRecordData.ItemSplitPrintedQty);    
			orderItemObj.put("I27", in_ItemRecordData.ItemMDID);    
			orderItemObj.put("I28", in_ItemRecordData.ItemPrintToKitchen);  			
			
			

			JSONArray itemToppings = new JSONArray();
			if( in_ItemRecordData.Product1ToppingsList != null )
			{
				for( TillToppings topDetails : in_ItemRecordData.Product1ToppingsList)
				{	
					
					if( topDetails.ToppingState == TOPPING_STATE_CODE.TOP_ADDED ||  topDetails.ToppingState == TOPPING_STATE_CODE.TOP_FIXED_REMOVED )
					{
						// Use short field names to reduce data size
					    // T1: itemTopState
					    // T2: itemTopCode
					    // T3: itemTopDesc
					    // T4: itemTopID

						JSONObject orderItemTopping=new JSONObject();
						orderItemTopping.put("T1", topDetails.ToppingState.ordinal());    
						orderItemTopping.put("T2", topDetails.ToppingCode);    
						orderItemTopping.put("T3", topDetails.ToppingDesc); 
						orderItemTopping.put("T4", topDetails.ToppingID); 
						itemToppings.put(orderItemTopping);
					}
				}			
			}		
			orderItemObj.put("I29", itemToppings);  
			
			// TODO: Half & Half
			/*
    		if( in_ItemRecordData.ItemHalfHalf)
    		{
    			orderItemObj.put("itemQty", 0);
    			orderItemObj.put("itemDesc", in_ItemRecordData.Product1Desc);
    			orderItemObj.put("itemSalePrice", in_ItemRecordData.Product1Price);
    			orderItemObj.put("itemNET", in_ItemRecordData.ItemQty * in_ItemRecordData.Product1Price);
    			orderItemObj.put("itemVAT", 0);        	

    			if (in_ItemRecordData.Product1VATCode > 0)
    			{
    				orderItemObj.put("itemVATCode", in_ItemRecordData.Product1VATCode);
    				orderItemObj.put("itemVATRate", in_ItemRecordData.ItemVATRate);
    			}
    		}
			 */
			prodRes.NewJSONObj = orderItemObj;
		}
		catch (JSONException e) 
		{
			e.printStackTrace();
		}	
		return prodRes;  
	}	
}
