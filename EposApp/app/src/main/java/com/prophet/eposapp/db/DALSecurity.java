package com.prophet.eposapp.db;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.prophet.eposapp.JSONBase;


public class DALSecurity  extends JSONBase
{
    private String URL_SECURITY = "";
    
	public DALSecurity()
	{		

	}

	public void SetDBPath(String in_ServerIP)
	{
		SetServerIP(in_ServerIP);
		URL_SECURITY = DB_SERVER_PATH + "security.php";		
	}
	
    public String GetErrorMsg()
    {
        return mErrorMsg;
    }
	
	public ArrayList<Integer> UserLogin(String in_Passcode, boolean in_LogoutUser)
	{
		boolean mLoginError = false;
		ArrayList<Integer> retVal = new ArrayList<Integer>();
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		if( in_LogoutUser )
		{
			params.add(new BasicNameValuePair("functype","3" ));    
		}
		else
		{
			params.add(new BasicNameValuePair("functype","1" ));    			
		}		
		params.add(new BasicNameValuePair("userpasscode",in_Passcode ));   		

		retVal.add(0);	// Login Result
		retVal.add(0);	// User No
		retVal.add(0);	// Session ID
		
		mErrorMsg = "";
		try
		{
			// getting JSON string from URL            
			JSONObject json = mjParser.makeHttpRequest(URL_SECURITY, "GET", params);
             
			if( json != null )
			{           
				// Checking for SUCCESS TAG                
				int success = json.getInt(TAG_SUCCESS);
 
				retVal.set(0, success);
				if (success == 1 || success == -1 ) // 1: logged in, -1 user already logged in
				{      				    
					int mUserNo = json.getInt("userno");
				    int mSessionID = json.getInt("sessionid");
					retVal.set(1, mUserNo);
					retVal.set(2, mSessionID);					
				}                 
				else                             
				{  
					
				}     
			}
			else
			{
				mErrorMsg = mjParser.GetParserErrorMsg();
				mLoginError = true;
			}
		}
		catch( JSONException e )
		{
			mErrorMsg = e.getMessage();  
			mLoginError = true;
			e.printStackTrace();  			
		}	
		if( mLoginError && mErrorMsg.compareTo("") == 0 )
			mErrorMsg = "Unexpected Login error";
			
			
		//if( in_Passcode.compareTo("2009") == 0 )
		//	mUserFound = true;  			
		
		return retVal;		
	}
	
	public void UserLogout(int in_UserNo, int in_SessionID)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("functype","2" ));  
		params.add(new BasicNameValuePair("userno",Integer.toString(in_UserNo) ));    
		params.add(new BasicNameValuePair("sessionid",Integer.toString(in_SessionID) ));    

		try
		{
			// getting JSON string from URL            
			JSONObject json = mjParser.makeHttpRequest(URL_SECURITY, "GET", params);
             
			if( json != null )
			{           
				// Checking for SUCCESS TAG                
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1 ) 
				{    				    
				}                 
				else                             
				{                
				}     
			}
		}
		catch( JSONException e )
		{
			mErrorMsg = e.getMessage();               
			e.printStackTrace();  			
		}	
	}
}
