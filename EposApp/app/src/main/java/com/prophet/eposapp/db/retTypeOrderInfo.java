package com.prophet.eposapp.db;

import java.util.ArrayList;

import com.prophet.eposapp.models.TillOrderDetails;
import com.prophet.eposapp.models.TillOrderItem;
import com.prophet.eposapp.models.TillSplitItem;

public class retTypeOrderInfo 
{
	boolean mOrderLoaded;
	TillOrderDetails mOrderDetails;
	ArrayList<TillOrderItem> mOrderItems;
	ArrayList<TillSplitItem> mSplitBillItems;

	public retTypeOrderInfo()
	{
		mOrderDetails = new TillOrderDetails();		
		mOrderItems = new ArrayList<TillOrderItem>();
		mSplitBillItems= new ArrayList<TillSplitItem>();
	}
	
	public boolean getOrderLoaded()
	{
		return mOrderLoaded;
	}
	
	public TillOrderDetails getOrderDetails()
	{
		return mOrderDetails;
	}
	
	public ArrayList<TillOrderItem> getOrderItems()
	{
		return mOrderItems;
	}	
	
	public ArrayList<TillSplitItem> getSplitItems()
	{
		return mSplitBillItems;
	}	
}
