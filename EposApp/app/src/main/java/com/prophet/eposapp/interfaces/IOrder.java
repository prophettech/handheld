package com.prophet.eposapp.interfaces;

import java.util.ArrayList;

import com.prophet.eposapp.models.ORDERTYPE;
import com.prophet.eposapp.models.TillOrderDetails;
import com.prophet.eposapp.models.TillOrderItem;

public class IOrder 
{
    public TillOrderDetails mCurrentOrderDetails;
    public ArrayList<TillOrderItem> mCurrentOrderItems;
    public int mLoadOrderID;
    public int mCurrentOrderItemID;
    public String mOrderErrorMsg;
    public boolean mOrderRes;
    public boolean mOrderChanged;
    
    public TillOrderItem mSelectedOrderItemDetails;
    public double mEditedOrderItemQty;
    public double mEditedOrderItemPrice;
    public String mEditedOrderItemFreeText;
    
    public ORDERTYPE mNewOrderType;
    public int mSelectedTableID;
    public int mSelectedTableNo;
    public int mSelectedTableCovers;    
    
    public double mOrderPaymentAmount;
    public boolean mOrderPaymentAdd;
    
    public double mOrderServiceRate;
    public double mOrderServiceAmount;
    public double mOrderDiscountRate;
    public double mOrderDiscountAmount;
    
	public String mSplitPayTypeDesc;
	public double mSplitPayAmount;
	public int mOrderSplitPayIdx;
	
	public IOrder()
	{
		mLoadOrderID = 0;
		mCurrentOrderItemID = 0;
		mOrderErrorMsg = "";
		mSplitPayTypeDesc = "";
		mSplitPayAmount = 0;
		mOrderSplitPayIdx = 0;
		mOrderRes = false;
		mOrderChanged = false;
		
		mEditedOrderItemQty = 0;
		mEditedOrderItemPrice = 0;
		mEditedOrderItemFreeText = "";
		mSelectedTableID = 0;
		mSelectedTableNo = 0;
		mSelectedTableCovers = 0;
		mOrderPaymentAmount = 0;
		mOrderPaymentAdd = false;
		mOrderServiceRate = 0;
		mOrderServiceAmount = 0;
		mOrderDiscountRate = 0;
		mOrderDiscountAmount = 0;
		
	}
    
}
