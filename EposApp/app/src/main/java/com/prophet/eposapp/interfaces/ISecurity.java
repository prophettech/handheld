package com.prophet.eposapp.interfaces;

public class ISecurity 
{
	public String mPasscode;
	public boolean mValidUser;
	public boolean mUserAlreadyLoggedIn;	
	public String mLoginErrorMsg;
	
	public ISecurity() 
	{
		mPasscode = "";
		mValidUser = false;	
		mUserAlreadyLoggedIn = false;	
		mLoginErrorMsg = "";
	}
}
