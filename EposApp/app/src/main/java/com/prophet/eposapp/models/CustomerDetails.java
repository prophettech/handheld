package com.prophet.eposapp.models;

//C# TO JAVA CONVERTER WARNING: Java does not allow user-defined value types. The behavior of this class will differ from the original:
//ORIGINAL LINE: public struct CustomerDetails
public final class CustomerDetails
{
	public String CustNo;
	public String CustName;
	public String CustAdd1;
	public String CustAdd2;
	public String CustAdd3;
	public String CustAdd4;
	public String CustPostCode;
	public String CustNote;
	public String CustDiscRate;
	public double CustMiles;
	public String CustFixedDelCharge;
	public int CustOrderCount;
	public int CustOrderSales;
	public boolean CustBarred;
	public String CustDateReq;
	public String CustTimeReq;

	public CustomerDetails clone()
	{
		CustomerDetails varCopy = new CustomerDetails();

		varCopy.CustNo = this.CustNo;
		varCopy.CustName = this.CustName;
		varCopy.CustAdd1 = this.CustAdd1;
		varCopy.CustAdd2 = this.CustAdd2;
		varCopy.CustAdd3 = this.CustAdd3;
		varCopy.CustAdd4 = this.CustAdd4;
		varCopy.CustPostCode = this.CustPostCode;
		varCopy.CustNote = this.CustNote;
		varCopy.CustDiscRate = this.CustDiscRate;
		varCopy.CustMiles = this.CustMiles;
		varCopy.CustFixedDelCharge = this.CustFixedDelCharge;
		varCopy.CustOrderCount = this.CustOrderCount;
		varCopy.CustOrderSales = this.CustOrderSales;
		varCopy.CustBarred = this.CustBarred;
		varCopy.CustDateReq = this.CustDateReq;
		varCopy.CustTimeReq = this.CustTimeReq;

		return varCopy;
	}
}