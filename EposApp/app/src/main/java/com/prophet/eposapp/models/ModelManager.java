package com.prophet.eposapp.models;

import android.content.SharedPreferences;

public class ModelManager
{
	public ModelMenu mModelMenu;
	public ModelOrder mModelOrder;
	public ModelTables mModelTables;
	public ModelReport mModelReport;	
	public ModelSplitBill mModelSplitBill;	
	public ModelSecurity mModelSecurity;		
	public ModelOptions mModelOptions;	
	public ModelHelper mModelHelper;
	
	public ModelManager(SharedPreferences in_AppConfig)
	{
		mModelHelper = new ModelHelper();
		mModelOptions = new ModelOptions(in_AppConfig);
		mModelReport = new ModelReport();
		mModelOrder = new ModelOrder(mModelHelper);	
		mModelOrder.SetOptions(mModelOptions);
		mModelMenu = new ModelMenu();	
		mModelTables = new ModelTables();
		mModelSplitBill = new ModelSplitBill();
		mModelSecurity = new ModelSecurity();
		
		SetDBPath();		
	};
	
	public void SetDBPath()
	{
		String mTmpServerIP = mModelOptions.LoadServerIP();
		mModelOptions.SetDBPath(mTmpServerIP);
		mModelReport.SetDBPath(mTmpServerIP);
		mModelOrder.SetDBPath(mTmpServerIP);
		mModelMenu.SetDBPath(mTmpServerIP);
		mModelTables.SetDBPath(mTmpServerIP);
		mModelSecurity.SetDBPath(mTmpServerIP);
		mModelHelper.SetDBPath(mTmpServerIP);
	}
}
