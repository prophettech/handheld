package com.prophet.eposapp.models;

import java.util.ArrayList;

import com.prophet.eposapp.db.DALMenu;


public class ModelMenu 
{
	DALMenu mDALMenu;
	private TillProduct mSelectedProduct1;
    private TillProduct mProductToAdd1;
	ArrayList<TillProduct> mActiveProductList;
	ArrayList<TillProductOfferButton> mActiveProductOfferButtonList;
    String mMenuErrorMsg;
    
    int mSelectedProductToppingID;
	int mSelectedProductOptionsID;
	TillProductOptions mSelectedProductOptions;
	ArrayList<TillToppings> mActiveTopGroupToppingsList; 
	
	TillProductOffer mSelectedProductOffer;
	
	boolean mMenuOnHalf2;
	int mMenuLinkID;
	boolean mMenuHalfHalf;
	boolean mMenuOfferMode;
	boolean mMenuHappyHourMode;
	
	
	int mMenuTopBut1;
	int mMenuTopBut2;
    
	public ModelMenu()
	{
		mDALMenu = new DALMenu();	
		mActiveProductOfferButtonList = new ArrayList<TillProductOfferButton>();
		mActiveProductList = new ArrayList<TillProduct>();
		mSelectedProduct1 = new TillProduct();
		mSelectedProductOptions = new TillProductOptions();
		mSelectedProductOptionsID = 0;
		mSelectedProductToppingID = 0;
		mActiveTopGroupToppingsList = new ArrayList<TillToppings>();
		mMenuTopBut1 = 0;
		mMenuTopBut2 = 0;
		mMenuHappyHourMode = false;
		
		mProductToAdd1 = new TillProduct();
	}
	
	public void SetDBPath(String in_ServerIP)
	{
		mDALMenu.SetDBPath(in_ServerIP);		
	}
	
    public String GetMenuErrorMsg()
    {
        return mMenuErrorMsg;
    }
	
	public void LoadMenu()
	{	
		if( mDALMenu != null )
			mDALMenu.LoadMenu();		
	}
	
	//public ArrayList<HashMap<String, String>> getCategoryList()
	public ArrayList<TillCategory> GetCategoryList()
	{		
		return mDALMenu.GetCategoryList();		
	}
	
	public ArrayList<TillProduct> GetProductList()
	{		
		return mDALMenu.GetProductList();		
	}
	
	public TillProduct GetProductToAdd1() 
	{
		return mProductToAdd1;
	}

	public TillProduct GetProductToAdd2() 
	{
		return null;
	}

	public TillProduct GetProductToAddHalfHalf() 
	{
		return null;
	}

	public boolean GetHalfHalf() 
	{

		return false;
	}

	public void ResetSelectedProduct() 
	{
        ClearProductOptionSelections();
        mSelectedProductOptionsID = 0;
        mSelectedProductOptions = null;
        mSelectedProductToppingID = 0;
        mSelectedProduct1.ClearProduct();
        //mSelectedProduct2.ClearProduct();		
	}
	
	public void ResetMenu() 
	{
        mMenuLinkID = 0;
        mMenuErrorMsg = "";
        mMenuOnHalf2 = false;
        mMenuHalfHalf = false;
        mProductToAdd1.ClearProduct();
		mMenuTopBut1 = 0;
		mMenuTopBut2 = 0;
        //mProductToAdd2.ClearProduct();
		//mProductToAddHalfHalf.ClearProduct();     		
	}    

	
	private TillProductOptions GetProductOptions(int in_productOptionsID) 
	{
		ArrayList<TillProductOptions> mTmpProductOptions = mDALMenu.GetProductOptionsList();
		
		for (TillProductOptions ProdOption : mTmpProductOptions) 
		{
		    if (ProdOption.GetOptionID() == in_productOptionsID) 
		    {
		    	return ProdOption;
		    }
		}
		
		return null;
	}
	
	public ArrayList<TillProduct> SetActiveProducts(int in_CatID) 
    {    
		ArrayList<TillProduct> mFullProductList = mDALMenu.GetProductList();
		mActiveProductList.clear();

		for (TillProduct ProdDetails : mFullProductList) 
		{
		    if (ProdDetails.ProductParentClassID == in_CatID) 
		    {
		    	mActiveProductList.add(ProdDetails);
		    }
		}
		
		return mActiveProductList; 		
    }	
	
	public TillProduct GetProductDetails(String in_ProdCode) 
    {    
		ArrayList<TillProduct> mFullProductList = mDALMenu.GetProductList();

		for (TillProduct ProdDetails : mFullProductList) 
		{
		    if (ProdDetails.ProductCode.compareTo(in_ProdCode) == 0) 
		    {
		    	return ProdDetails;
		    }
		}
		
		return null; 		
    }	
	
	public int SelectPOProduct(int in_POButIdx)
	{
		int ProdAction = 0; 
        if( in_POButIdx < mActiveProductOfferButtonList.size() )
        {
        	TillProductOfferButton TmpPOBut = mActiveProductOfferButtonList.get(in_POButIdx);
        	if( TmpPOBut.mProduct != null )
        	{
            	ProdAction = 1;
                if (TmpPOBut.mProduct.ProductOptionsID > 0)
                {
                    if (mSelectedProductOptionsID != TmpPOBut.mProduct.ProductOptionsID)
                    {
                        mSelectedProductOptionsID = TmpPOBut.mProduct.ProductOptionsID;
                        mSelectedProductOptions = GetProductOptions(TmpPOBut.mProduct.ProductOptionsID);
                        GetTopGroupToppings(TmpPOBut.mProduct.ProductOptionsID);
                    }       
                    ProdAction = 2;
                }
                else
                {
                }    
        		mSelectedProduct1.ClearProduct();
        		mSelectedProduct1.CopyProduct(TmpPOBut.mProduct);
        	}
        }
		
		return ProdAction;
	}
	
	public int SelectProduct(int in_ProdIdx)
	{
        int ProdAction = 0;  // 0: No action, 1: Add to order, 2: Display Options
        mMenuErrorMsg = "";
        if( in_ProdIdx < mActiveProductList.size() )
        {
        	TillProduct TmpProduct = mActiveProductList.get(in_ProdIdx);
        	ProdAction = 1;
            if (TmpProduct.ProductOptionsID > 0)
            {
                if (mSelectedProductOptionsID != TmpProduct.ProductOptionsID)
                {
                    mSelectedProductOptionsID = TmpProduct.ProductOptionsID;
                    mSelectedProductOptions = GetProductOptions(TmpProduct.ProductOptionsID);
                    GetTopGroupToppings(TmpProduct.ProductOptionsID);
                }       
                ProdAction = 2;
            }
            else
            {
                if (TmpProduct.ProductPrice == -1)
                {
                    mMenuErrorMsg = "Product not available for selection";
                    ProdAction = -1;
                }
            }      
            mSelectedProduct1.ClearProduct();
            mSelectedProduct1.CopyProduct(TmpProduct);
        }
        
        return ProdAction;		
	}	
	
	public int SelectExistingPOItem(int in_SelectedPOItemGroupID)
	{		
		TillProductOfferItemGroup POItemGroup = mSelectedProductOffer.GetPOItemGroupByID(in_SelectedPOItemGroupID);
		if( POItemGroup != null )
		{
			ArrayList<TillProductOfferItem> POItemList = POItemGroup.GetPOItemList();
			OFFER_ITEM_TYPE POGroupType = POItemGroup.GetPOItemGroupType();
			
			if( POGroupType == OFFER_ITEM_TYPE.OFFERITEMTYPE_TOPPINGS )
			{
				mSelectedProduct1 = POItemList.get(0).GetPOProduct1();
				mSelectedProductOptionsID = mSelectedProduct1.ProductOptionsID;
				mSelectedProductOptions = mSelectedProduct1.ProductOptions;
			}
		}
        return 2;			
	}
	
	public TillProduct GetSelectedProduct1()
	{
		return mSelectedProduct1;	
	}
	
    public TillProductOptions GetSelectedProductOptions()
    {
        return mSelectedProductOptions;
    }

	public int SelectProductOption(int in_ButGroupID,int in_ButID) 
	{
        int prodOptAction = 0;

        if (mSelectedProductOptions != null)
        {
            SetProductOptionSelected(in_ButGroupID, in_ButID);
            
            int linkID = ProductOptionsValid();

            if (linkID > 0 && linkID != mMenuLinkID)
            {
                mMenuLinkID = linkID;
                //GetProductSubPrices(mSelectedProduct1.ProductParentClassID, mMenuLinkID);
                prodOptAction = 2;
            }
            
        }

        return prodOptAction;
	}

    private void SetProductOptionSelected(int in_ButGroupID, int in_ButID)
    {
        ArrayList<TillProductOptionsButGroup> tmpButGroupList;
        ArrayList<TillProductOptionsButGroupButton> tmpButtonList;

        // For Till V2 compatibility. Remove when using V3
		if( in_ButGroupID == 1 )
			mMenuTopBut1 = in_ButID;
		if( in_ButGroupID == 2 )					
			mMenuTopBut2 = in_ButID;
		
		
        tmpButGroupList = mSelectedProductOptions.GetButGroups();

		for (TillProductOptionsButGroup ButGroup : tmpButGroupList) 
		{     
            if (ButGroup.GetButGroupID() == in_ButGroupID)
            {
                tmpButtonList = ButGroup.GetButtons();
                if (ButGroup.GetButGroupSelAllowed() == 1)
                {
                	for (TillProductOptionsButGroupButton OptionBut : tmpButtonList) 
                    {
                        ButGroup.SetButSelCount(0);
                        OptionBut.SetButSelected(0);
                    }
                }

                for (TillProductOptionsButGroupButton OptionBut : tmpButtonList) 
                {
                    if (OptionBut.GetButID() == in_ButID)
                    {
                        if (OptionBut.GetButSelected() == 1)
                        {
                            if (ButGroup.GetButGroupSelAllowed() > 1)
                            {
                                OptionBut.SetButSelected(0);
                                ButGroup.SetButSelCount(-1);
                            }
                        }
                        else
                        {
                            if (ButGroup.GetButGroupSelAllowed() > 1)
                            {
                                if (ButGroup.GetButSelCount() < ButGroup.GetButGroupSelAllowed())
                                {
                                    OptionBut.SetButSelected(1);
                                    ButGroup.SetButSelCount(1);
                                }
                            }
                            else
                            {
                                OptionBut.SetButSelected(1);
                                ButGroup.SetButSelCount(1);
                            }
                        }
                        break;
                    }
                }
                break;
            }              
        }
    }
    
    private int ProductOptionsValid()
    {
        String linkIDStr = "0";

        ArrayList<TillProductOptionsButGroup> tmpButGroupList;
        ArrayList<TillProductOptionsButGroupButton> tmpButtonList;

        if( mSelectedProductOptions != null )
        {
            tmpButGroupList = mSelectedProductOptions.GetButGroups();

    		for (TillProductOptionsButGroup butGroup : tmpButGroupList)        
            {
                if (butGroup.GetButGroupPartOfProduct() == 1)	
                {
                    if (butGroup.GetButSelCount() == butGroup.GetButGroupSelReq())
                    {
                        linkIDStr += Integer.toString(butGroup.GetButGroupNo());
                        tmpButtonList = butGroup.GetButtons();
                        for (TillProductOptionsButGroupButton optionBut : tmpButtonList) 
                        {
                            if (optionBut.GetButSelected() == 1)
                            {
                                linkIDStr += Integer.toString(optionBut.GetButtonPos());
                                break;
                            }
                        }
                    }
                    else
                    {
                        linkIDStr = "-1";
                        break;
                    }
                }
            }
        }

        return Integer.parseInt(linkIDStr);  
    }

	public boolean CanAddItemToOrder() 
	{
        boolean bAddItem = true;
        boolean bProd1Selected = false;
        //boolean bProd2Selected = false;                        
        int linkID = 0;
                       
        if (mSelectedProduct1.ProductCode.compareTo("") != 0)
        {
            if (mSelectedProduct1.ProductOptionsID > 0)
            {
                linkID = ProductOptionsValid();
                if (linkID == -1)
                {
                    mMenuErrorMsg = "Please make sure all options are selected";
                    bAddItem = false;
                }
                else 
                {
                    if (linkID > 0)
                    {
                    	TillProduct tmpProduct = GetSubProductDetails(mSelectedProduct1.ProductCode, linkID);
                    	if( tmpProduct == null )
                    	{
                            mMenuErrorMsg = "Could not get Product Details";
                            bAddItem = false;
                    		
                    	}
                    	else
                    	{
                    		mProductToAdd1.CopyProduct(tmpProduct);
                            if (mProductToAdd1.ProductCode.length() == 0)
                            {
                                mProductToAdd1.ProductCode = mSelectedProduct1.ProductCode;
                                mProductToAdd1.ProductDesc = mSelectedProduct1.ProductDesc;
                            }

                            mProductToAdd1.ProductParentCode = mSelectedProduct1.ProductCode;
                            mProductToAdd1.ProductParentDesc = mSelectedProduct1.ProductDesc;
                            //mProductToAdd1.CopyProductToppings(mSelectedProduct1.ProductToppingsList);                    	
                    	}
                    	/*
                        if (!GetSubProductDetails(mSelectedProduct1.ProductCode, linkID, ref ))
                        {
                            mMenuErrorMsg = "Could not get Product Details";
                            bAddItem = false;
                        }
                        else
                        {
                            if (ProductToAdd1.ProductCode.Length == 0)
                            {
                                mProductToAdd1.ProductCode = SelectedProduct1.ProductCode;
                                mProductToAdd1.ProductDesc = SelectedProduct1.ProductDesc;
                            }

                            ProductToAdd1.ProductParentCode = SelectedProduct1.ProductCode;
                            ProductToAdd1.ProductParentDesc = SelectedProduct1.ProductDesc;
                            ProductToAdd1.CopyProductToppings(SelectedProduct1.ProductToppingsList);
                        }
                        */
                    }
                    else
                    {
                    	mProductToAdd1.CopyProduct(mSelectedProduct1);
                    }
                    mProductToAdd1.ProductOptions.CopyProductOptions(mSelectedProductOptions);
                    mProductToAdd1.ProductTopBut1 = mMenuTopBut1;
                    mProductToAdd1.ProductTopBut2 = mMenuTopBut2;
                    
                }
            }
            else
            {
            	mProductToAdd1.CopyProduct(mSelectedProduct1);
            }
            bProd1Selected = true;                    
        }
        
        if (mMenuHalfHalf)
        {

        }
        else
        {
            if (!bProd1Selected)
            {
                bAddItem = false;
                mMenuErrorMsg = "No Product selected";
            }

            if (bAddItem && !mMenuOfferMode && mProductToAdd1.ProductPrice == -1 )
            {
                bAddItem = false;
                mMenuErrorMsg = "Product not available for selection";
            }
        }

        return bAddItem;           

	}

	private TillProduct GetSubProductDetails(String in_ProductCode, int in_LinkID) 
	{
		ArrayList<TillProduct> tmpSubProductList = mDALMenu.GetSubProductList();

		int butG1ID = 0;
		int butG2ID = 0;
		String tmpStr = Integer.toString(in_LinkID);
		butG1ID = Integer.parseInt(tmpStr.substring(1,2));	
		if( tmpStr.length() > 2 )
		{
			 butG2ID = 	Integer.parseInt(tmpStr.substring(2));		
		}
		
		boolean prodBG1Valid;
		boolean prodBG2Valid;
		boolean returnProd;
		for (TillProduct prodDetails : tmpSubProductList)        
        {
			prodBG1Valid = true;
			prodBG2Valid = true;
			returnProd = false;
			if( prodDetails.ProductParentCode.compareTo(in_ProductCode) == 0 )
			{
				if( prodDetails.ProductButG1ID > 0 ) 
				{
					if(butG1ID > 0 && butG1ID == prodDetails.ProductButG1ID )
					{
						returnProd = true;						
					}
					else
					{
						prodBG1Valid = false;
					}
				}
				
				if( prodDetails.ProductButG2ID > 0 ) 
				{					
					if(butG2ID >0 && butG2ID == prodDetails.ProductButG2ID )
					{
						returnProd = true;						
					}		
					else
					{
						prodBG2Valid = false;
					}					
				}	
				
				if( returnProd && prodBG1Valid && prodBG2Valid )
				{
					//prodDetails.ProductTopBut1 = butG1ID;
					//prodDetails.ProductTopBut2 = butG2ID;					
					return prodDetails;
				}
			}			
        }
		
		return null;
	}
	
    public TillProductOffer GetProductOffer(String in_OfferCode)
    {
    	
		ArrayList<TillProductOffer> tmpProductOffersList = mDALMenu.GetProductOffersList();
		
        for (TillProductOffer ProdOffer : tmpProductOffersList)
        {
            if (ProdOffer.GetPOCode().compareTo(in_OfferCode) == 0)
            {
                mSelectedProductOffer = null;
                mSelectedProductOffer = new TillProductOffer(ProdOffer.GetPOCode());
                mSelectedProductOffer.CopyOffer(ProdOffer,false);
                return mSelectedProductOffer;
            }
        }

        return null;
    }
    
    public TillProductOffer GetSelectedProductOffer()
    {
    	return mSelectedProductOffer;
    }    
	
    public void GetTopGroupToppings(int in_ToppingID)
    {
    	mActiveTopGroupToppingsList.clear();
    	ArrayList<TillToppings> tmpTopGroupToppingsList = mDALMenu.GetTopGroupToppingList();
    	TillToppings newTopping;
		for (TillToppings topDetails : tmpTopGroupToppingsList)        
        {
			if( topDetails.ToppingGroupID == in_ToppingID ) 
			{
				newTopping = new TillToppings();
				newTopping.ToppingID = mActiveTopGroupToppingsList.size()+1;
				newTopping.ToppingCode = topDetails.ToppingCode;
				newTopping.ToppingDesc = topDetails.ToppingDesc;
				newTopping.ToppingGroupID = topDetails.ToppingGroupID;
				newTopping.ToppingType = topDetails.ToppingType;
				newTopping.ToppingFixedPrice = topDetails.ToppingFixedPrice;
				mActiveTopGroupToppingsList.add(newTopping);				
			}
        }
    }
    
    public ArrayList<TillToppings> GetTopGroupToppingsList()
    {
    	return mActiveTopGroupToppingsList;
    }


	private void ClearProductOptionSelections() 
	{
        if (mSelectedProductOptions != null)
        {
        	ArrayList<TillProductOptionsButGroup> tmpButGroupList;
        	ArrayList<TillProductOptionsButGroupButton> tmpButtonList;

            tmpButGroupList = mSelectedProductOptions.GetButGroups();

            for (TillProductOptionsButGroup butGroup : tmpButGroupList)   
            {
                butGroup.SetButSelCount(0);
                tmpButtonList = butGroup.GetButtons();
                        
                for (TillProductOptionsButGroupButton optionBut : tmpButtonList)                        
                {
                    optionBut.SetButSelected(0);                            
                }
            }
        }		
	}
	
    public boolean OnHalf2()
    {
        return mMenuOnHalf2;
    }

    public void AddProductTopping(TillToppings in_Topping)
    {
        if (OnHalf2())
        {
            //if (mSelectedProduct2.ProductCode.CompareTo("") != 0)
           //     mSelectedProduct2.AddTopping(in_Topping, TOPPING_STATE_CODE.TOP_ADDED);
        }
        else
        {
            if( mSelectedProduct1.ProductCode.compareTo("") != 0 )
            {        		
            	mSelectedProduct1.AddTopping(in_Topping, TOPPING_STATE_CODE.TOP_ADDED, true);
            }
        }
    }

    public void RemoveProductTopping(int in_TopID)
    {
        if (OnHalf2())
        {
            //mSelectedProduct2.RemoveTopping(in_TopID);
        }
        else
        {
            mSelectedProduct1.RemoveTopping(in_TopID);
        }
    }

	public void SetMenuOfferMode(boolean inMenuOfferMode) 
	{
		mMenuOfferMode = inMenuOfferMode;		
	}

	public ArrayList<TillProductOfferButton> CreatePOItemProducts(int in_SelectedPOItemGroupID) 
	{
		mActiveProductOfferButtonList.clear();
		if( mSelectedProductOffer != null )
		{			
			TillProductOfferButton NewProductOfferButton;
			ArrayList<String> retItemDetails; 
			TillProduct NewProduct;
			TillProductOfferItemGroup POItemGroup = mSelectedProductOffer.GetPOItemGroupByID(in_SelectedPOItemGroupID);
			if( POItemGroup != null )
			{
				ArrayList<TillProductOfferItem> POItemList = POItemGroup.GetPOItemList();
				for( int i=0; i < POItemList.size(); i++ )
				{
					NewProductOfferButton = new TillProductOfferButton();
					retItemDetails = POItemList.get(i).GetPOItemDetails(); 

					TillProduct TmpProduct = GetProductDetails(retItemDetails.get(4));
					NewProduct = new TillProduct();
					if( TmpProduct != null )
					{
						NewProduct.CopyProduct(TmpProduct);						
					}
					NewProduct.ProductCode = retItemDetails.get(4);
					NewProduct.ProductDesc = retItemDetails.get(1);
					NewProductOfferButton.mProduct = NewProduct;
					NewProductOfferButton.mButDesc = retItemDetails.get(1);
					if( retItemDetails.get(8).compareTo("") != 0 )
						NewProductOfferButton.mButType = OFFER_ITEM_TYPE.values()[Integer.parseInt(retItemDetails.get(8))];
					if( retItemDetails.get(2).compareTo("") != 0 )
						NewProductOfferButton.mButAction = Integer.parseInt(retItemDetails.get(2));
					NewProductOfferButton.mButProdCatID = -1;
					if( retItemDetails.get(6).compareTo("") != 0 )
						NewProductOfferButton.mButProdCatID = Integer.parseInt(retItemDetails.get(6));
					NewProductOfferButton.mButGroupID = POItemGroup.GetPOItemGroupID();
					NewProductOfferButton.mButItemID = Integer.parseInt(retItemDetails.get(0));
					mActiveProductOfferButtonList.add(NewProductOfferButton);
				}		
			}
		}
		return mActiveProductOfferButtonList;
		/*
		mActiveProductList.clear();
		if( mSelectedProductOffer != null )
		{
			ArrayList<String> retItemDetails; 
			TillProduct NewProduct;
			TillProductOfferItemGroup POItemGroup = mSelectedProductOffer.GetPOItemGroupByID(in_SelectedPOItemGroupID);
			if( POItemGroup != null )
			{
				ArrayList<TillProductOfferItem> POItemList = POItemGroup.GetPOItemList();
				for( int i=0; i < POItemList.size(); i++ )
				{
					retItemDetails = POItemList.get(i).GetPOItemDetails(); 

					TillProduct TmpProduct = GetProductDetails(retItemDetails.get(4));
					NewProduct = new TillProduct();
					if( TmpProduct != null )
					{
						NewProduct.CopyProduct(TmpProduct);						
					}
					NewProduct.ProductCode = retItemDetails.get(4);
					NewProduct.ProductDesc = retItemDetails.get(1);
					mActiveProductList.add(NewProduct);
				}	
			}
		}	
		*/	
	}
	
    public void SetPOItem(int in_GroupID, int in_ItemID)
    {
    	if( mSelectedProductOffer != null )
    		mSelectedProductOffer.SetPOItem(in_GroupID, in_ItemID, mProductToAdd1, null);
    }

	public boolean CanAddOfferToOrder() 
	{		
		boolean ValidOffer = false;
		
		if( mSelectedProductOffer != null )
		{
			ArrayList<TillProductOfferItemGroup> TmpPOItemGroupList = mSelectedProductOffer.GetPOItemGroupList();
			ArrayList<TillProductOfferItem> TmpPOItemList;
			for (TillProductOfferItemGroup POItemGroup : TmpPOItemGroupList)
			{
				TmpPOItemList = POItemGroup.GetPOItemList();
				if( POItemGroup.GetPOItemGroupType() == OFFER_ITEM_TYPE.OFFERITEMTYPE_OR )
				{
					ValidOffer = false;					
				}
				
				for (TillProductOfferItem POItem : TmpPOItemList)
				{
					if( POItemGroup.GetPOItemGroupType() == OFFER_ITEM_TYPE.OFFERITEMTYPE_SELECT ||
						POItemGroup.GetPOItemGroupType() == OFFER_ITEM_TYPE.OFFERITEMTYPE_OR )
					{
						if( POItem.GetPOItemSelected() )
						{
							ValidOffer = true;
							break;
						}	
					}
					else
					{
						ValidOffer = true;	
						if( !POItem.GetPOItemSelected() )
						{
							ArrayList<String> retItemDetails = POItem.GetPOItemDetails(); 
							TillProduct TmpProduct = new TillProduct();
							TmpProduct.ProductCode = retItemDetails.get(4);
							TmpProduct.ProductDesc = retItemDetails.get(1);
							POItem.SetPOProducts(TmpProduct, null, false);
							POItem.SetPOItemSelected(true);
							TmpProduct = null;
						}
					}
				}	
				if( !ValidOffer )
					break;
			}
		}
		
		return ValidOffer;
	}
	
	public void SetMenuHappyHourMode(boolean in_MenuHappyHourMode) 
	{
		mMenuHappyHourMode = in_MenuHappyHourMode;
	}
	
	public boolean GetMenuHappyHourMode() 
	{
		return mMenuHappyHourMode;
	}

	public void SetProductOfferORSelection(int pGroupID, int pBuID)
    {
        String SelectedProdCode = mSelectedProductOffer.SetProductOfferORSelection(pGroupID, pBuID);
        if (SelectedProdCode.compareTo("") != 0)
        {
            TillProduct TmpProduct = GetProductDetails(SelectedProdCode);
            mSelectedProduct1.CopyProduct(TmpProduct);

            GetTopGroupToppings(TmpProduct.ProductToppingsID);
            mSelectedProductOffer.SetPOItem(pGroupID, pBuID, TmpProduct, null);                                
        }
    }
}
