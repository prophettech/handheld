package com.prophet.eposapp.models;

import java.util.ArrayList;

import com.prophet.eposapp.db.DALOrder;
import com.prophet.eposapp.db.retTypeOrderInfo;

public class ModelOrder
{
	DALOrder mDALOrder;
	TillOrder mCurrentOrder;
	ModelOptions mModelOptions;
	ModelHelper mModelHelper;
	String mOrderErrorMsg;
	boolean mOrderChanged;
	
	public ModelOrder(ModelHelper in_ModelHelper)
	{
		mOrderErrorMsg = "";
		mModelHelper = in_ModelHelper; 
		mDALOrder = new DALOrder();	
		mCurrentOrder = new TillOrder(mModelHelper);
	}
	
    public void SetOptions(ModelOptions in_Options)
    {    
    	mModelOptions = in_Options;    	
    }
	
	public void SetDBPath(String in_ServerIP)
	{
		mDALOrder.SetDBPath(in_ServerIP);		
	}
	
    public String GetOrderErrorMsg()
    {
        return mOrderErrorMsg;
    }
    
    public boolean GetOrderChangedFlag()
    {
        return mOrderChanged;
    }    
    
    public void ResetOrderChangedFlag()
    {
        mOrderChanged = false;
    }   

    public int GetNextOrderNo()
    {
        return mDALOrder.GetNextOrderNo();
    }

    public void NewOrder()
    {
    	mOrderErrorMsg = "";
    	int newOrderNo = GetNextOrderNo();
    	if( newOrderNo > 0)
    		mCurrentOrder.NewOrder(newOrderNo);   	
    	mOrderChanged = false;
    }
    
    public void NewTableOrder(int in_TableID, int in_TableNo)
    {
    	NewOrder();
    	mCurrentOrder.SetOrderType(ORDERTYPE.ORD_TYPE_TABLE);
    	mCurrentOrder.setOrderTable(in_TableID, in_TableNo);  
    	mCurrentOrder.setOrderTableCovers(2);
    	mOrderChanged = false;
    }
    
    public void NewBarOrder()
    {
    	NewOrder();
    	mCurrentOrder.SetOrderType(ORDERTYPE.ORD_TYPE_BAR);	
    	mOrderChanged = false;
    }    
    
	public final void SetSplitBillItems(ArrayList<TillSplitItem> in_SplitBillItems)
	{
		mCurrentOrder.SetSplitBillItems(in_SplitBillItems);
	}
	
	public boolean SaveOrder()
	{	
		mOrderErrorMsg = "";
		if( mOrderChanged )
		{
			ArrayList<Integer> retRes = mDALOrder.SaveOrder(mCurrentOrder.GetOrderDetails(),mCurrentOrder.GetOrderItemsList(),mCurrentOrder.GetSplitBillItems());
			
			if( retRes.size() > 0 )		
			{
				int SavedOrderID = retRes.get(0);
				int NewTransID = retRes.get(1);
				mCurrentOrder.GetOrderDetails().setOrderTransID(NewTransID);
				
				if( mCurrentOrder.GetOrderDetails().getbInsertOrder())
				{
					mCurrentOrder.GetOrderDetails().setbInsertOrder(false);
					mCurrentOrder.GetOrderDetails().setOrderID(SavedOrderID);						
				}
				
				mOrderChanged = false;
				mOrderErrorMsg = "";
				return true;			
			}
			else
			{
				mOrderErrorMsg = mDALOrder.GetErrorMsg();
				if( mOrderErrorMsg.compareTo("") == 0 )
					mOrderErrorMsg = "ERROR: Could not save Order";
					
				return false;				
			}	
		}
		else
		{
			return true;
		}
	}
	
	public boolean LoadOrder(int in_OrderID)
	{	
		mOrderErrorMsg = "";
		retTypeOrderInfo mRetOrder = mDALOrder.LoadOrder(in_OrderID);
		if( mRetOrder.getOrderLoaded() )
		{
			mCurrentOrder.CopyOrder(mRetOrder);
			mCurrentOrder.SetOrderLoaded();
			mOrderErrorMsg = "";
			if( mCurrentOrder.GetOrderDetails().getOrderAmountDue() == 0 )
			{
				mOrderErrorMsg = "Warning: Order has been paid"; 				
			}
			mOrderChanged = false;
			return true;
		}
		else
		{
			mOrderErrorMsg = mDALOrder.GetErrorMsg();
			return false;			
		}
		
	}
    public void AddOffer(TillProductOffer in_Offer, boolean in_HappyHour)
    {
        mCurrentOrder.AddOfferToOrder(1f,in_Offer,in_HappyHour);
        mOrderChanged = true;
    }    

	public void AddItem(TillProduct in_Product1, TillProduct in_Product2, TillProduct in_ProductHalfHalf, boolean in_HalfHalf, boolean in_HappyHour) 
	{
		boolean AddItem = true;
		if( mModelOptions.GetOptions().GetAccumulatedQty() )
		{
			if( in_Product1.ProductOptionsID == 0 && in_Product1.ProductMinusToppingDesc.compareTo("") == 0 && in_Product1.ProductPlusToppingDesc.compareTo("") == 0 )
			{				
				ArrayList<TillOrderItem> mTmpOrderItems = GetOrderItems();
				if( mTmpOrderItems.size() > 0 )
				{		
					for(TillOrderItem mOrderItem : mTmpOrderItems )
					{	
						if( mOrderItem.GetProduct1() != null )
						{
							if(mOrderItem.GetProduct1().ProductCode.compareTo(in_Product1.ProductCode) == 0 )
							{
								AddItem = false;
								mOrderItem.SetItemQty(mOrderItem.GetItemQty()+1);
								mCurrentOrder.CalculateOrderTotal();
								break;
							}
						}
					}
				}
			}
		}
		
		if( AddItem )
		{
			mCurrentOrder.AddItemToOrder(1, in_Product1, in_Product2, in_ProductHalfHalf, in_HalfHalf, in_HappyHour);		
		}
		mOrderChanged = true;
	}

	public TillOrderDetails GetOrderDetails() 
	{
		return mCurrentOrder.GetOrderDetails();
	}

	public ArrayList<TillOrderItem> GetOrderItems() 
	{
		return mCurrentOrder.GetOrderItemsList();
	}

	public TillOrderItem GetOrderItemDetails(int in_OrerItemID) 
	{
		ArrayList<TillOrderItem> mTmpOrderItems = GetOrderItems();
		if( mTmpOrderItems.size() > 0 )
		{		
			for(TillOrderItem mOrderItem : mTmpOrderItems )
			{		
				if(mOrderItem.GetItemID() == in_OrerItemID )
				{
					return mOrderItem;
				}				
			}
		}
		return null;
	}

	public void UpdateEditedOrderItem(int in_ItemID, double in_Qty,double in_Price, String in_FreeText) 
	{
		mCurrentOrder.UpdateEditedOrderItem(in_ItemID, in_Qty, in_Price, in_FreeText);
		mOrderChanged = true;
	}
	
	public final ArrayList<TillSplitItem> GetSplitBillItems()
	{
		return mCurrentOrder.GetSplitBillItems();
	}
	
    public int DeleteCurrentOrderItem(int in_OrderItemID)
    {
    	mOrderChanged = true;
        return mCurrentOrder.DeleteCurrentOrderItem(in_OrderItemID);
    }

	public boolean DecreaseItemSplitQty(int in_ItemID) 
	{
		mOrderChanged = true;
		return mCurrentOrder.DecreaseItemSplitQty(in_ItemID);
	}
	
	public void IncreaseItemSplitQty(int in_ItemID, boolean in_UpdatePrintedQty)
	{
		mOrderChanged = true;
		mCurrentOrder.IncreaseItemSplitQty(in_ItemID, in_UpdatePrintedQty);		
	}
	
	public void SetOrderTableCovers(int in_TableCovers) 
	{
		mOrderChanged = true;
		mCurrentOrder.setOrderTableCovers(in_TableCovers);		
	}   
	
    public void SetPayment(double in_PayAmt, boolean in_Add)
    {
    	mOrderChanged = true;
    	mCurrentOrder.SetOrderPayment(in_PayAmt,in_Add);              
    }
    
    public void SetServiceChargeRate(double in_ServiceChargeRate)
    {
    	mOrderChanged = true;
    	mCurrentOrder.SetServiceChargePerc(in_ServiceChargeRate);
    	mCurrentOrder.CalculateOrderTotal();
    }

    public void SetServiceChargeAmt(double in_ServiceChargeAmt)
    {
    	mOrderChanged = true;
    	mCurrentOrder.SetServiceChargeAmt(in_ServiceChargeAmt);
    	mCurrentOrder.CalculateOrderTotal();
    }

    public void SetDiscountAmt(double in_DiscountAmt)
    {
    	mOrderChanged = true;
    	mCurrentOrder.SetDiscountAmt(in_DiscountAmt);
    	mCurrentOrder.CalculateOrderTotal();
    }

    public void SetDiscountRate(double in_DiscountRate)
    {
    	mOrderChanged = true;
    	mCurrentOrder.SetDiscountPerc(in_DiscountRate);
    	mCurrentOrder.CalculateOrderTotal();
    }

	public void SetPayType(int in_PayTypeCode, String in_PayTypeDesc) 
	{
		mOrderChanged = true;
		mCurrentOrder.SetOrderPayType(in_PayTypeCode, in_PayTypeDesc);		
	}
	
	public void SetPrintedToKitchen(boolean in_Value) 
	{	
		mOrderChanged = true;
		mCurrentOrder.SetPrintedToKitchen(in_Value);		
	}
	
	public void SetBillPrinted(boolean in_Value) 
	{	
		mOrderChanged = true;
		mCurrentOrder.SetBillPrinted(in_Value);	
	}

	public boolean PayOrder() 
	{
		mOrderChanged = true;
        if (!mCurrentOrder.CanPayOrder())
        {
            mOrderErrorMsg = "ERROR: Amount tendered is insufficient";
            return false;
        }
        return true;
	}
	
    public void AddSplitPayment(int in_SplitPayType, String in_SplitPayTypeDesc, double in_SplitPayAmount, int in_EditIdx)
    {
    	mOrderChanged = true;
    	mCurrentOrder.AddSplitPayment(in_SplitPayType, in_SplitPayTypeDesc, in_SplitPayAmount, in_EditIdx);
        //mOrderChanged = true;
    }

    public void RemoveSplitPayment(int in_SplitPayIdx)
    {
    	mOrderChanged = true;
    	mCurrentOrder.RemoveSplitPayment(in_SplitPayIdx);
        //mOrderChanged = true;
    }

	public void UpdatePrintedQty() 
	{
		mOrderChanged = true;
		mCurrentOrder.UpdatePrintedQty();
		
	}
}
