package com.prophet.eposapp.models;

import java.util.ArrayList;

import com.prophet.eposapp.db.DALSplitBill;

public class ModelSplitBill 
{
	DALSplitBill mDALSplitBill;
	TillSplitBill mSplitBill;
	
	public ModelSplitBill()
	{
		mDALSplitBill = new DALSplitBill();	
		mSplitBill = new TillSplitBill();
	}
	
	public void NewSplitBill()
	{
		mSplitBill.NewSplitBill();
	}  
	
	public void ClearSplitBill()
	{
		mSplitBill.ClearSplitBill();
	}  
	
	public void LoadSplitBill(ArrayList<TillSplitItem> in_SplitBillList)
	{
		mSplitBill.LoadSplitBill(in_SplitBillList);		
	}
	
	public void AddSplitItem(int in_ItemID, boolean in_UpdatePrintQty) 
	{
		mSplitBill.AddSplitItem(in_ItemID, in_UpdatePrintQty);		
	}
	
	public boolean RemoveSplitItem(int in_ItemID) 
	{
		return mSplitBill.RemoveSplitItem(in_ItemID);		
	}
	
	public ArrayList<TillSplitItem> GetSplitItemsList()
	{ 
		return mSplitBill.GetSplitItemsList();
	}
	
	public ArrayList<TillSplitItem> GetAllSplitItemsList()
	{ 
		return mSplitBill.GetAllSplitItemsList();
	}
	
	public String GetSplitBillRef()
	{
		return mSplitBill.GetSplitBillRef();
	}
	
	public int GetSplitBillID()
	{
		return mSplitBill.GetSplitBillID();
	}
	
	public int GetSplitBillCount()
	{
		return mSplitBill.GetSplitBillCount();
	}
	
	public void NextSplit()
	{
		mSplitBill.NextSplit();
	} 
	  
	public void PrevSplit()
	{
		mSplitBill.PrevSplit();		
	}
	
	public void UpdateSplitPrintedQty()
	{ 
		mSplitBill.UpdateSplitPrintedQty();		
	}
}
