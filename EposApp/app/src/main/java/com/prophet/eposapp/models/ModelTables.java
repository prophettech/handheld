package com.prophet.eposapp.models;

import java.util.ArrayList;

import com.prophet.eposapp.db.DALTables;

public class ModelTables 
{
	DALTables mDALTables;
    String mTablesErrorMsg;
    
	public ModelTables()
	{
		mDALTables = new DALTables();	
	}
	
	public void SetDBPath(String in_ServerIP)
	{
		mDALTables.SetDBPath(in_ServerIP);		
	}	

	public void loadTables()
	{	
		if( mDALTables != null )
		{
			mDALTables.LoadTableOrders();
			mDALTables.LoadTables();	
			UpdateTablesStatus();
		}
	}
	
	public void UpdateTablesStatus()
	{	
		ArrayList<TillTables> mTablesList = GetTablesList();
		ArrayList<TillTableOrder> mTableOrdersList = GetTableOrdersList();	
		
		for (TillTableOrder tableOrder : mTableOrdersList) 
		{ 
			int tableId = tableOrder.GetTableID();
			for (TillTables tableDetails : mTablesList) 
			{ 
				if( tableDetails.GetTableID() == tableId )
				{
					tableDetails.SetTableStatus(tableOrder.GetOrderAccRef(),tableOrder.GetOrderID(),tableOrder.GetOrderState());
					break;
				}				
			}
		}		
	}
	
	public ArrayList<TillTables> GetTablesList()
	{		
		return mDALTables.GetTablesList();		
	}
	
	public ArrayList<TillTableOrder> GetTableOrdersList()
	{		
		return mDALTables.GetTableOrdersList();		
	}	
	
    
}
