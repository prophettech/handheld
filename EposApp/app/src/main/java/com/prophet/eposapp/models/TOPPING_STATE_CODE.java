package com.prophet.eposapp.models;

public enum TOPPING_STATE_CODE 
{
	TOP_FIXED, TOP_FIXED_REMOVED, TOP_ADDED
}
