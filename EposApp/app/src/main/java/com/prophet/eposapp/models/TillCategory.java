package com.prophet.eposapp.models;

import java.io.Serializable;

import android.graphics.Color;

public class TillCategory implements Serializable 
{
	private static final long serialVersionUID = 1L;
	private int mCatID;
	private String mCatName;
	private int mCatColour;
	private int mCatFontColour;
	
	public void SetCatDetails(int in_CatID, String in_CatName, int in_CatColour, int in_CatFontColour)
	{
		mCatID = in_CatID;
		mCatName = in_CatName;

		int mR = 147;
		int mG = 230;
		int mB = 203;			
		if( in_CatColour != -1 )
		{
			mR = in_CatColour & 0xff;
			mB = (in_CatColour & 0xff00) >> 8;
			mG = (in_CatColour & 0xff0000) >> 16;			
		}
		mCatColour = Color.rgb(mR, mB, mG);
		
		int mFontR = 0;
		int mFontG = 0;
		int mFontB = 0;		
		if( in_CatFontColour != -1 )
		{
			mFontR = in_CatFontColour & 0xff;
			mFontG = (in_CatFontColour & 0xff00) >> 8;
			mFontB = (in_CatFontColour & 0xff0000) >> 16;					
		}
		mCatFontColour = Color.rgb(mFontR, mFontG, mFontB);	
	}

	public int GetCatID()
	{
		return mCatID;	
	}
	
	public String GetCatName()
	{
		return mCatName;	
	}
	
	public int GetCatColour()
	{
		return mCatColour;	
	}
	
	public int GetCatFontColour()
	{
		return mCatFontColour;	
	}
	
	
}
