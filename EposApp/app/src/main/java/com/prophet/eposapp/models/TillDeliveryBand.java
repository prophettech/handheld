package com.prophet.eposapp.models;

public class TillDeliveryBand
{
	public String DelBandAmount;
	public String DelBandMiles;
	public String DelBandCharge;

	public TillDeliveryBand()
	{
		DelBandAmount = "";
		DelBandMiles = "";
		DelBandCharge = "";
	}
}