package com.prophet.eposapp.models;

public class TillPayType 
{
	private int mPayTypeCode;
	public final int getPayTypeCode()
	{
		return mPayTypeCode;
	}
	public final void setPayTypeCode(int in_PayTypeCode)
	{
		mPayTypeCode = in_PayTypeCode;
	}
	
	private String mPayTypeDesc;
	public final String getPayTypeDesc()
	{
		return mPayTypeDesc;
	}
	public final void setPayTypeDesc(String in_PayTypeDesc)
	{
		mPayTypeDesc = in_PayTypeDesc;
	}
	

}
