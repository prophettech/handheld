package com.prophet.eposapp.models;

public class TillProductOffer
{
	private String POCode;
	private String PODesc;
	private double POPrice;
	private boolean POAllowSpendOffer;

	private java.util.ArrayList<TillProductOfferItemGroup> POItemGroupList;

	public TillProductOffer(String in_MDCode)
	{

		POCode = in_MDCode;
		PODesc = "";
		POPrice = 0;
		POAllowSpendOffer = true;
		POItemGroupList = new java.util.ArrayList<TillProductOfferItemGroup>();
	}

	public final void ResetProductOfferDetails()
	{
		for (TillProductOfferItemGroup POItemGroup : POItemGroupList)
		{
			POItemGroup.ResetPOItemGroupDetails();	
		}		
	}
	
	public final void SetProductOfferDetails(String in_PODesc, double in_POPrice)
	{
		PODesc = in_PODesc;
		POPrice = in_POPrice;
	}

	public final String GetPODesc()
	{
		return PODesc;
	}
	
	public final double GetPOPrice()
	{
		return POPrice;
	}

	public final void SetPOAllowSpendOffer(boolean in_AllowSpendOffer)
	{
		POAllowSpendOffer = in_AllowSpendOffer;
	}

	public final void AddPOItemGroup(TillProductOfferItemGroup in_ButGroup)
	{
		POItemGroupList.add(in_ButGroup);
	}

	public final String GetPOCode()
	{
		return POCode;
	}

	public final java.util.ArrayList<TillProductOfferItemGroup> GetPOItemGroupList()
	{
		return POItemGroupList;
	}

	public final void SetPOItem(int in_GroupID, int in_ItemID, TillProduct in_Product1, TillProduct in_Product2)
	{
		for (TillProductOfferItemGroup POItemGroup : POItemGroupList)
		{
			if (POItemGroup.GetPOItemGroupID() == in_GroupID)
			{
				//in_Product1.SetModelHelper(_ModelHelper);
				//in_Product1.SetProductOptions(_AddVATToPrice);
				//in_Product2.SetModelHelper(_ModelHelper);
				//in_Product2.SetProductOptions(_AddVATToPrice);
				
				if( POItemGroup.GetPOItemGroupType() != OFFER_ITEM_TYPE.OFFERITEMTYPE_OR )
				{
					in_ItemID = 1;						
				}
			
				POItemGroup.SetPOItemDetails(in_ItemID, in_Product1, in_Product2);
			}
		}
	}

	/*
	public final void GetPOItem(int in_GroupID, int in_ItemID, tangible.RefObject<TillProduct> out_Product1, tangible.RefObject<TillProduct> out_Product2)
	{
		for (TillProductOfferItemGroup POItemGroup : POItemGroupList)
		{
			if (POItemGroup.GetPOItemGroupID() == in_GroupID)
			{
				POItemGroup.GetPOItemDetails(in_ItemID, out_Product1, out_Product2);
				break;
			}
		}
	}
	*/

	public final void CopyOffer(TillProductOffer in_Offer, boolean in_LoadingOffer)
	{
		String PODesc = "";
		double POPrice = 0;
		PODesc = in_Offer.GetPODesc();
		POPrice = in_Offer.GetPOPrice();
		SetProductOfferDetails(PODesc, POPrice);

		TillProductOfferItemGroup NewPOItemGroup = null;
		java.util.ArrayList<TillProductOfferItemGroup> TmpPOItemGroup = in_Offer.GetPOItemGroupList();
		for (TillProductOfferItemGroup POItemGroup : TmpPOItemGroup)
		{
			NewPOItemGroup = new TillProductOfferItemGroup();
			NewPOItemGroup.SetPOItemGroupDetails(POItemGroup.GetPOItemGroupID(), POItemGroup.GetPOItemGroupType(), 0);
			NewPOItemGroup.CopyItemGroupItemList(POItemGroup.GetPOItemList(),in_LoadingOffer);
			AddPOItemGroup(NewPOItemGroup);


		}
	}

	public final String SetProductOfferORSelection(int in_GroupID, int in_ItemID)
	{
		String SelectedProdCode = "";
		for (TillProductOfferItemGroup POItemGroup : POItemGroupList)
		{
			if (POItemGroup.GetPOItemGroupID() == in_GroupID)
			{
				SelectedProdCode = POItemGroup.SetPOItemSelection(in_ItemID);
				break;
			}
		}
		return SelectedProdCode;
	}

	public final double GetOfferToppingCharges()
	{
		double TmpToppingCharge = 0;
		for (TillProductOfferItemGroup POItemGroup : POItemGroupList)
		{
			TmpToppingCharge += POItemGroup.GetPOItemToppingCharges();

		}
		return TmpToppingCharge;
	}

	public TillProductOfferItemGroup GetPOItemGroupByID( int in_GroupID ) 
	{
		for (TillProductOfferItemGroup POItemGroup : POItemGroupList)
		{
			if (POItemGroup.GetPOItemGroupID() == in_GroupID)
			{
				return POItemGroup;
			}
		}
		return null;
	}

}