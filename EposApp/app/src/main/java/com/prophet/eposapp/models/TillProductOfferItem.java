package com.prophet.eposapp.models;

import java.util.ArrayList;

public class TillProductOfferItem
{
	private int ItemID;
	private OFFER_ITEM_TYPE ItemType = OFFER_ITEM_TYPE.values()[0];
	private String ItemText;
	private int ItemToppingID;
	private int ItemAction; // 1 = Display Sub MD

	private String ProdCode;
	private String ProdDesc;
	private int ProdCatID;
	private boolean ProdSelected;

	private TillProduct POProduct1;
	private TillProduct POProduct2;
	private TillProduct ProductHalfHalf;

	public TillProductOfferItem()
	{
		ItemType = OFFER_ITEM_TYPE.OFFERITEMTYPE_STD;
		ItemText = "";
		ItemToppingID = 0;
		ProdCode = "";
		ProdDesc = "";
		ProdCatID = 0;
		ProdSelected = false;
		POProduct1 = new TillProduct();
		POProduct2 = new TillProduct();
		ProductHalfHalf = new TillProduct();
	}

	public final void SetPOItemDetails(int in_ID, String in_Text, int in_Action, int in_ToppingID, String in_ProdCode, String in_ProdDesc, int in_ProdCatID, boolean in_ProdSelected, OFFER_ITEM_TYPE in_GroupType)
	{
		ItemID = in_ID;
		ItemText = in_Text;
		ItemAction = in_Action;
		ItemToppingID = in_ToppingID;
		ProdCode = in_ProdCode;
		ProdDesc = in_ProdDesc;
		ProdCatID = in_ProdCatID;
		ProdSelected = in_ProdSelected;

		
		if (ProdCatID > 0)
		{
			ItemType = OFFER_ITEM_TYPE.OFFERITEMTYPE_SELECT;
		}
		else if (ItemToppingID > 0)
		{
			ItemType = OFFER_ITEM_TYPE.OFFERITEMTYPE_TOPPINGS;
		}
		else
		{
			if( in_GroupType != OFFER_ITEM_TYPE.OFFERITEMTYPE_OR)
			{
				//ItemType = OFFER_ITEM_TYPE.OFFERITEMTYPE_STD;	
				//ProdSelected = true;
			}		
		}
	}

	public final void SetPOProducts(TillProduct in_Product1, TillProduct in_Product2, boolean in_Loading)
	{
		POProduct1.ClearProduct();
		if (in_Product1 != null)
		{
			POProduct1.CopyProduct(in_Product1);
			if( !in_Loading )
			{
				POProduct1.ProductToppingCharges = 0;
				POProduct1.CalculateItemToppingCharge();
			}
			POProduct1.GenerateProductToppingDesc();
		}

		ProductHalfHalf.ClearProduct();
		POProduct2.ClearProduct();
		if (in_Product2 != null)
		{
			POProduct2.CopyProduct(in_Product2);
			if( !in_Loading )
			{
				POProduct2.ProductToppingCharges = 0;
				POProduct2.CalculateItemToppingCharge();
			}
			POProduct2.GenerateProductToppingDesc();

			ProductHalfHalf.ProductCode = "PHALFHALF";
			ProductHalfHalf.ProductDesc = "Half & Half";
			ProductHalfHalf.ProductParentCode = "PHALFHALF";
			ProductHalfHalf.ProductParentDesc = "Half & Half";
		}
	}

	public final TillProduct GetPOProduct1()
	{
		return POProduct1;
	}

	public final TillProduct GetPOProduct2()
	{
		return POProduct2;
	}

	public final TillProduct GetPOProductHalfHalf()
	{
		return ProductHalfHalf;
	}


	public final ArrayList<String> GetPOItemDetails()
	{
		ArrayList<String> POItemDetails = new ArrayList<String>();
		POItemDetails.add(Integer.toString(ItemID));
		POItemDetails.add(ItemText);
		POItemDetails.add(Integer.toString(ItemAction));
		POItemDetails.add(Integer.toString(ItemToppingID));
		POItemDetails.add(ProdCode);
		POItemDetails.add(ProdDesc);
		POItemDetails.add(Integer.toString(ProdCatID));
		if( ProdSelected )
		{
			POItemDetails.add("1");
		}
		else
		{
			POItemDetails.add("0");			
		}
		
		POItemDetails.add(Integer.toString(ItemType.ordinal()));
		return POItemDetails;
	}

	public final void SetSelectedProdDetails(String in_ProdCode, String in_ProdDesc)
	{
		ProdCode = in_ProdCode;
		ProdDesc = in_ProdDesc;
		ProdSelected = true;
	}

	public final void SetPOItemSelected(boolean in_Selected)
	{
		ProdSelected = in_Selected;
	}

	public final boolean GetPOItemSelected()
	{
		return ProdSelected;
	}

	public final int GetPOItemID()
	{
		return ItemID;
	}

	public final String GetPOProdCode()
	{
		return ProdCode;
	}

	public final double GetPOProdToppingCharges()
	{
		return POProduct1.ProductToppingCharges + POProduct2.ProductToppingCharges;
	}
}