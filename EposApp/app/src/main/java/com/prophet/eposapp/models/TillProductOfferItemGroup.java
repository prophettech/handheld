package com.prophet.eposapp.models;

import java.util.ArrayList;

public class TillProductOfferItemGroup
{
	private int POGroupID;
	private OFFER_ITEM_TYPE POGroupType = OFFER_ITEM_TYPE.values()[0];
	private java.util.ArrayList<TillProductOfferItem> POItemList;
	//private int SelectedColour;   

	public TillProductOfferItemGroup()
	{
		POGroupType = OFFER_ITEM_TYPE.OFFERITEMTYPE_STD;
		//SelectedColour = 0;
		POItemList = new java.util.ArrayList<TillProductOfferItem>();
	}

	public final void SetPOItemGroupDetails(int in_ItemGroupID, OFFER_ITEM_TYPE in_ItemGroupType, int in_ItemGroupSelectedColour)
	{
		POGroupID = in_ItemGroupID;
		POGroupType = in_ItemGroupType;
		//SelectedColour = in_ItemGroupSelectedColour;
	}
	
	public final void ResetPOItemGroupDetails()
	{
		for (TillProductOfferItem POItem : POItemList)
		{		
			POItem.SetSelectedProdDetails("", "");
			POItem.SetPOProducts(null, null, false);
			POItem.SetPOItemSelected(false);
		}
	}

	public final void AddPOItem(TillProductOfferItem in_Item)
	{
		POItemList.add(in_Item);
	}

	public final java.util.ArrayList<TillProductOfferItem> GetPOItemList()
	{
		return POItemList;
	}

	public final OFFER_ITEM_TYPE GetPOItemGroupType()
	{
		return POGroupType;
	}

	public final int GetPOItemGroupID()
	{
		return POGroupID;
	}

	public final void SetPOItemDetails(int in_ItemID, TillProduct in_Product1, TillProduct in_Product2)
	{
		for (TillProductOfferItem POItem : POItemList)
		{
			POItem.SetPOItemSelected(false);
			if (POItem.GetPOItemID() == in_ItemID)
			{
				String ProdCode = in_Product1.ProductCode;
				String ProdDesc = in_Product1.ProductDesc;
				if (in_Product2 != null && in_Product2.ProductCode.compareTo("") != 0)
				{
					ProdCode = "PHALFHALF";
					ProdDesc = "H1: " + in_Product1.ProductDesc + "  \\  H2: " + in_Product2.ProductDesc;
				}
				ProdDesc = in_Product1.GenerateProductDesc();
				POItem.SetSelectedProdDetails(ProdCode, ProdDesc);
				POItem.SetPOProducts(in_Product1, in_Product2, false);
				//break;
			}
		}
	}
	
	public final TillProduct GetPOItemDetailsP1(int in_ItemID)
	{
		for (TillProductOfferItem POItem : POItemList)
		{
			if (POItem.GetPOItemID() == in_ItemID)
			{
				return POItem.GetPOProduct1();	
			}
		}
		return null;			
	}
	
	public final TillProduct GetPOItemDetailsP2(int in_ItemID)
	{
		for (TillProductOfferItem POItem : POItemList)
		{
			if (POItem.GetPOItemID() == in_ItemID)
			{
				return POItem.GetPOProduct2();	
			}
		}
		return null;		
	}

	public final void CopyItemGroupItemList(java.util.ArrayList<TillProductOfferItem> in_ItemList, boolean in_LoadingOffer)
	{
		String ItemText = "";
		int ItemAction = 0;
		int ItemToppingID = 0;
		String ItemProdCode = "";
		String ItemProdDesc = "";
		int ItemProdCatID = 0;
		int ItemID = 0;
		boolean ItemProdSelected = false;
		OFFER_ITEM_TYPE ItemType = OFFER_ITEM_TYPE.OFFERITEMTYPE_STD;

		TillProductOfferItem NewPOItem = null;
		for (TillProductOfferItem POItem : in_ItemList)
		{
			/*
			tangible.RefObject<Integer> tempRef_ItemID = new tangible.RefObject<Integer>(ItemID);
			tangible.RefObject<String> tempRef_ItemText = new tangible.RefObject<String>(ItemText);
			tangible.RefObject<Integer> tempRef_ItemAction = new tangible.RefObject<Integer>(ItemAction);
			tangible.RefObject<Integer> tempRef_ItemToppingID = new tangible.RefObject<Integer>(ItemToppingID);
			tangible.RefObject<String> tempRef_ItemProdCode = new tangible.RefObject<String>(ItemProdCode);
			tangible.RefObject<String> tempRef_ItemProdDesc = new tangible.RefObject<String>(ItemProdDesc);
			tangible.RefObject<Integer> tempRef_ItemProdCatID = new tangible.RefObject<Integer>(ItemProdCatID);
			tangible.RefObject<Boolean> tempRef_ItemProdSelected = new tangible.RefObject<Boolean>(ItemProdSelected);
			tangible.RefObject<OFFER_ITEM_TYPE> tempRef_ItemType = new tangible.RefObject<OFFER_ITEM_TYPE>(ItemType);
			*/
			//POItem.GetPOItemDetails(tempRef_ItemID, tempRef_ItemText, tempRef_ItemAction, tempRef_ItemToppingID, tempRef_ItemProdCode, tempRef_ItemProdDesc, tempRef_ItemProdCatID, tempRef_ItemProdSelected, tempRef_ItemType);
			ArrayList<String> retItemDetails = POItem.GetPOItemDetails(); 
			ItemID = Integer.parseInt(retItemDetails.get(0));
			ItemText = retItemDetails.get(1);
			ItemAction = Integer.parseInt(retItemDetails.get(2));
			ItemToppingID = Integer.parseInt(retItemDetails.get(3));
			ItemProdCode = retItemDetails.get(4);
			ItemProdDesc = retItemDetails.get(5);
			ItemProdCatID = Integer.parseInt(retItemDetails.get(6));
			ItemProdSelected = false;
			if( retItemDetails.get(7).compareTo("1") == 0 )
				ItemProdSelected = true;			

			OFFER_ITEM_TYPE tmpOfferTypeValues[] = OFFER_ITEM_TYPE.values();
			ItemType = tmpOfferTypeValues[Integer.parseInt(retItemDetails.get(8))];

			NewPOItem = new TillProductOfferItem();
			NewPOItem.SetPOItemDetails(ItemID, ItemText, ItemAction, ItemToppingID, ItemProdCode, ItemProdDesc, ItemProdCatID, ItemProdSelected, ItemType);
			NewPOItem.SetPOProducts(POItem.GetPOProduct1(), POItem.GetPOProduct2(), in_LoadingOffer);
			AddPOItem(NewPOItem);
		}
	}

	public final String SetPOItemSelection(int in_ItemID)
	{
		String SelectedProdCode = "";
		boolean bSelected = false;
		for (TillProductOfferItem POItem : POItemList)
		{
			bSelected = false;
			if (POItem.GetPOItemID() == in_ItemID)
			{
				SelectedProdCode = POItem.GetPOProdCode();
				bSelected = true;
			}
			POItem.SetPOItemSelected(bSelected);
		}
		return SelectedProdCode;
	}

	public final double GetPOItemToppingCharges()
	{
		double TmpToppingCharge = 0;
		for (TillProductOfferItem POItem : POItemList)
		{
			TmpToppingCharge += POItem.GetPOProdToppingCharges();
		}

		return TmpToppingCharge;
	}
}
