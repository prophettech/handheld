package com.prophet.eposapp.models;

public class TillProductOptionsButGroup
{
	private int ButGroupID;
	private int ButGroupOptionsID;
	private int ButGroupNo;
	private int ButGroupPartOfProduct;
	private int ButGroupReceiptPos;
	private String ButGroupName;
	private String ButGroupStartText;
	private String ButGroupSepChar;
	private String ButGroupStartChar;
	private String ButGroupEndChar;
	private int ButGroupSelCount;
	private int ButGroupSelReq;
	private int ButGroupSelAllowed;
	private int ButGroupDisplayType;
	private int ButGroupColour;
	private int ButGroupFontColour;
	private int ButGroupColourSel;
	private int ButGroupFontColourSel;

	private java.util.ArrayList<TillProductOptionsButGroupButton> ButGroupButtonsList;


	public TillProductOptionsButGroup()
	{
		ButGroupID = 0;
		ButGroupOptionsID = 0;
		ButGroupNo = 0;
		ButGroupPartOfProduct = 0;
		ButGroupReceiptPos = 0;
		ButGroupName = "";
		ButGroupStartText = "";
		ButGroupSepChar = "";
		ButGroupStartChar = "";
		ButGroupEndChar = "";
		ButGroupSelCount = 0;
		ButGroupSelReq = 0;
		ButGroupSelAllowed = 0;
		ButGroupDisplayType = 0;
		ButGroupColour = 0;
		ButGroupFontColour = 0;
		ButGroupColourSel = 0;
		ButGroupFontColourSel = 0;
		ButGroupButtonsList = new java.util.ArrayList<TillProductOptionsButGroupButton>();
	}

	public final void SetProductOptionsButGroupDetails(int in_ID, int in_OptionsID, int in_GroupNo, int in_PartOfProduct, int in_ReceiptPos, String in_Name, String in_StartText, String in_SepChar, String in_StartChar, String in_EndChar, int in_SelReq, int in_SelAllowed, int in_DisplayType, int in_Colour, int in_FontColour, int in_ColourSel, int in_FondColourSel)

	{
		ButGroupID = in_ID;
		ButGroupOptionsID = in_OptionsID;
		ButGroupNo = in_GroupNo;
		ButGroupPartOfProduct = in_PartOfProduct;
		ButGroupReceiptPos = in_ReceiptPos;
		ButGroupName = in_Name;
		ButGroupStartText = in_StartText;
		ButGroupSepChar = in_SepChar;
		ButGroupStartChar = in_StartChar;
		ButGroupEndChar = in_EndChar;
		ButGroupSelCount = 0;
		ButGroupSelReq = in_SelReq;
		ButGroupSelAllowed = in_SelAllowed;
		ButGroupDisplayType = in_DisplayType;
		ButGroupColour = in_Colour;
		ButGroupFontColour = in_FontColour;
		ButGroupColourSel = in_ColourSel;
		ButGroupFontColourSel = in_FondColourSel;
	}

	public final void AddButton(TillProductOptionsButGroupButton in_Button)
	{
		ButGroupButtonsList.add(in_Button);
	}

	public final java.util.ArrayList<TillProductOptionsButGroupButton> GetButtons()
	{
		return ButGroupButtonsList;
	}

	public final void CopyButGroup(TillProductOptionsButGroup in_ButGroup)
	{
		SetProductOptionsButGroupDetails(in_ButGroup.ButGroupID, in_ButGroup.ButGroupOptionsID, in_ButGroup.ButGroupNo, in_ButGroup.ButGroupPartOfProduct, in_ButGroup.ButGroupReceiptPos, in_ButGroup.ButGroupName, in_ButGroup.ButGroupStartText, in_ButGroup.ButGroupSepChar, in_ButGroup.ButGroupStartChar, in_ButGroup.ButGroupEndChar, in_ButGroup.ButGroupSelReq, in_ButGroup.ButGroupSelAllowed, in_ButGroup.ButGroupDisplayType, in_ButGroup.ButGroupColour, in_ButGroup.ButGroupFontColour, in_ButGroup.ButGroupColourSel, in_ButGroup.ButGroupFontColourSel);

		TillProductOptionsButGroupButton NewOptionBut;
		ButGroupButtonsList.clear();
		for (TillProductOptionsButGroupButton ProductOptionsBut : in_ButGroup.ButGroupButtonsList)
		{
			NewOptionBut = new TillProductOptionsButGroupButton();
			NewOptionBut.CopyBututon(ProductOptionsBut);
			AddButton(NewOptionBut);
		}
	}

	public final int GetButGroupNo()
	{
		return ButGroupNo;
	}

	public final int GetButGroupSelReq()
	{
		return ButGroupSelReq;
	}

	public final int GetButGroupPartOfProduct()
	{
		return ButGroupPartOfProduct;
	}

	public final int GetButGroupSelAllowed()
	{
		return ButGroupSelAllowed;
	}

	public final int GetButGroupID()
	{
		return ButGroupID;
	}

	public final void SetButSelCount(int in_Value)
	{
		if (in_Value == 0)
		{
			ButGroupSelCount = 0;
		}
		else
		{
			ButGroupSelCount += in_Value;
		}
	}

	public final int GetButSelCount()
	{
		return ButGroupSelCount;
	}

}