package com.prophet.eposapp.models;

public class TillProductOptionsButGroupButton
{
	private int ButtonID;
	private int ButtonGroupID;
	private int ButtonPos;
	private int ButtonSel;
	private String ButtonName;
	private String ButtonDesc;
	private String ButtonReceiptDesc;
	private int ButtonPriceType;
	private double ButtonNewPrice;
	private double ButtonNewPriceHH;

	public TillProductOptionsButGroupButton()
	{
		ButtonID = 0;
		ButtonGroupID = 0;
		ButtonPos = 0;
		ButtonSel = 0;
		ButtonName = "";
		ButtonDesc = "";
		ButtonReceiptDesc = "";
		ButtonPriceType = 0;
		ButtonNewPrice = 0;
		ButtonNewPriceHH = 0;
	}

	public final void SetTillProductOptionsButtonDetails(int in_ButtonID, int in_ButtonGroupID, int in_ButtonPos, int in_ButtonSel, String in_ButtonName, String in_ButtonDesc, String in_ButtonReceiptDesc, int in_ButtonPriceType, double in_ButtonNewPrice, double in_ButtonNewPriceHH)
	{
		ButtonID = in_ButtonID;
		ButtonGroupID = in_ButtonGroupID;
		ButtonPos = in_ButtonPos;
		ButtonSel = in_ButtonSel;
		ButtonName = in_ButtonName;
		ButtonDesc = in_ButtonDesc;
		ButtonReceiptDesc = in_ButtonReceiptDesc;
		ButtonPriceType = in_ButtonPriceType;
		ButtonNewPrice = in_ButtonNewPrice;
		ButtonNewPriceHH = in_ButtonNewPriceHH;
	}

	public final void CopyBututon(TillProductOptionsButGroupButton in_Button)
	{
		SetTillProductOptionsButtonDetails(in_Button.ButtonID, in_Button.ButtonGroupID, in_Button.ButtonPos, in_Button.ButtonSel, in_Button.ButtonName, in_Button.ButtonDesc, in_Button.ButtonReceiptDesc, in_Button.ButtonPriceType, in_Button.ButtonNewPrice, in_Button.ButtonNewPriceHH);
	}

	public final String GetButtonDesc()
	{
		return ButtonDesc;
	}

	public final int GetButtonPos()
	{
		return ButtonPos;
	}

	public final int GetButGroupID()
	{
		return ButtonGroupID;
	}

	public final int GetButID()
	{
		return ButtonID;
	}

	public final int GetButSelected()
	{
		return ButtonSel;
	}

	public final void SetButSelected(int in_Selected)
	{
		ButtonSel = in_Selected;
	}
}