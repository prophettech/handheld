package com.prophet.eposapp.models;

public class TillProductOptionsButtons_v1 
{
	private int ProdButtonID;

	private String ProdButtonnName;
	private String ProdButtonText1;
	private String ProdButtonText2;
	private String ProdButtonText3;
	private String ProdButtonText4;
	private String ProdButtonText5;
	private int ProdButtonGroupType;

	public TillProductOptionsButtons_v1()
	{
		ClearOptions();
	}

	public final void ClearOptions()
	{
		ProdButtonID = 0;
		ProdButtonnName = "";
		ProdButtonText1 = "";
		ProdButtonText2 = "";
		ProdButtonText3 = "";
		ProdButtonText4 = "";
		ProdButtonText5 = "";
		ProdButtonGroupType = 0;
	}

	public final void SetProductButtonDetails(int in_ID, String in_B1Text, String in_B2Text, String in_B3Text, String in_B4Text, String in_B5Text, String in_Name, int in_GroupType)
	{
		ProdButtonID = in_ID;
		ProdButtonnName = in_Name;
		ProdButtonText1 = in_B1Text;
		ProdButtonText2 = in_B2Text;
		ProdButtonText3 = in_B3Text;
		ProdButtonText4 = in_B4Text;
		ProdButtonText5 = in_B5Text;
		ProdButtonGroupType = in_GroupType;
	}
	
	public final String GetProdButtonText1()
	{
		return ProdButtonText1;
	}
	
	public final String GetProdButtonText2()
	{
		return ProdButtonText2;
	}
	
	public final String GetProdButtonText3()
	{
		return ProdButtonText3;
	}
	
	public final String GetProdButtonText4()
	{
		return ProdButtonText4;
	}
	
	public final String GetProdButtonText5()
	{
		return ProdButtonText5;
	}	
	
	public final String GetProdButtonnName()
	{
		return ProdButtonnName;
	}		
	
	public final int GetProduButtonGroupType()
	{
		return ProdButtonGroupType;
	}

	public final int GetProduButtonID()
	{
		return ProdButtonID;
	}

}
