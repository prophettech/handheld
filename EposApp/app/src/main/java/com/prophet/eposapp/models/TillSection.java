package com.prophet.eposapp.models;

public class TillSection 
{
	private int mSectionID;
	private int mSectionPos;
	private String mSectionDesc;
	
	public TillSection()
	{

	}
	
	public int GetSectionID() 
	{
		return mSectionID;
	}
	public int GetSectionPos() 
	{
		return mSectionPos;
	}
	public String GetSectionDesc() 
	{
		return mSectionDesc;
	}
	
	public void SetSectionDetails(int in_SectionID,int in_SectionPos,String in_SectionDesc) 
	{
		mSectionID = in_SectionID;
		mSectionPos = in_SectionPos;
		mSectionDesc = in_SectionDesc;	
	}
		



}
