package com.prophet.eposapp.models;

public class TillSplitItem 
{
	public int mSplitID;
	public int mItemID;
	public double mItemQty;
	public double mItemPrintedQty;
	public String mSplitRef;
}
