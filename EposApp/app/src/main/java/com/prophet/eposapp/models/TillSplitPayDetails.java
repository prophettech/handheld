package com.prophet.eposapp.models;

public class TillSplitPayDetails 
{
	int mSplitPayID;
	int mSplitPayTypeCode;
	String mSplitPayDesc;
	double mSplitPayAmount;

	public TillSplitPayDetails()
	{
		mSplitPayID = 0;
		mSplitPayTypeCode = 0;
		mSplitPayDesc = "";
		mSplitPayAmount = 0;		
	}
	
	public final int getSpliyPayID()
	{
		return mSplitPayID;
	}
	public final void setSpliyPayID(int in_SpliyPayID)
	{
		mSplitPayID = in_SpliyPayID;
	}
	
	public final int getSpliyPayTypeCode()
	{
		return mSplitPayTypeCode;
	}
	public final void setSpliyPayTypeCode(int in_SpliyPayTypeCode )
	{
		mSplitPayTypeCode = in_SpliyPayTypeCode;
	}
	
	public final String getSpliyPayDesc()
	{
		return mSplitPayDesc;
	}
	public final void setSpliyPayDesc(String in_SpliyPayDesc )
	{
		mSplitPayDesc = in_SpliyPayDesc;
	}
	
	public final double getSpliyPayAmount()
	{
		return mSplitPayAmount;
	}
	public final void setSpliyPayAmount(double in_SpliyPayAmount )
	{
		mSplitPayAmount = in_SpliyPayAmount;
	}
	
}
