package com.prophet.eposapp.models;

public class TillTableOrder 
{
	private int mTableID;
	private int mOrderID;
	private int mOrderNo;	
	private String mOrderDate;	
	private String mOrderAccRef;
	private String mOrderContact;
	private String mOrderAdd1;
	private String mOrderAdd2;
	private String mOrderAddPostcode;
	private double mOrderPrePayAmt;
	private double mOrderTotal;
	private String mOrderNote1;
	private String mOrderNote2;
	private int mOrderRouteID;
	private int mOrderState;
	
	public int GetTableID() {
		return mTableID;
	}
	public void SetTableID(int mTableID) {
		this.mTableID = mTableID;
	}
	public int GetOrderID() {
		return mOrderID;
	}
	public void SetOrderID(int mOrderID) {
		this.mOrderID = mOrderID;
	}
	public int GetOrderNo() {
		return mOrderNo;
	}
	public void SetOrderNo(int mOrderNo) {
		this.mOrderNo = mOrderNo;
	}
	public String GetOrderDate() {
		return mOrderDate;
	}
	public void SetOrderDate(String mOrderDate) {
		this.mOrderDate = mOrderDate;
	}
	public String GetOrderAccRef() {
		return mOrderAccRef;
	}
	public void SetOrderAccRef(String mOrderAccRef) {
		this.mOrderAccRef = mOrderAccRef;
	}
	public String GetOrderContact() {
		return mOrderContact;
	}
	public void SetOrderContact(String mOrderContact) {
		this.mOrderContact = mOrderContact;
	}
	public String GetOrderAdd1() {
		return mOrderAdd1;
	}
	public void SetOrderAdd1(String mOrderAdd1) {
		this.mOrderAdd1 = mOrderAdd1;
	}
	public String GetOrderAdd2() {
		return mOrderAdd2;
	}
	public void SetOrderAdd2(String mOrderAdd2) {
		this.mOrderAdd2 = mOrderAdd2;
	}
	public String GetOrderAddPostcode() {
		return mOrderAddPostcode;
	}
	public void SetOrderAddPostcode(String mOrderAddPostcode) {
		this.mOrderAddPostcode = mOrderAddPostcode;
	}
	public double GetOrderPrePayAmt() {
		return mOrderPrePayAmt;
	}
	public void SetOrderPrePayAmt(double mOrderPrePayAmt) {
		this.mOrderPrePayAmt = mOrderPrePayAmt;
	}
	public double GetOrderTotal() {
		return mOrderTotal;
	}
	public void SetOrderTotal(double mOrderTotal) {
		this.mOrderTotal = mOrderTotal;
	}
	public String GetOrderNote1() {
		return mOrderNote1;
	}
	public void SetOrderNote1(String mOrderNote1) {
		this.mOrderNote1 = mOrderNote1;
	}
	public String GetOrderNote2() {
		return mOrderNote2;
	}
	public void SetOrderNote2(String mOrderNote2) {
		this.mOrderNote2 = mOrderNote2;
	}
	public int GetOrderRouteID() {
		return mOrderRouteID;
	}
	public void SetOrderRouteID(int mOrderRouteID) 
	{
		this.mOrderRouteID = mOrderRouteID;
	}
	
	public void SetOrderState(int in_State) 
	{
		mOrderState = in_State;		
	}
	
	public int GetOrderState() 
	{
		return mOrderState;		
	}

                    
 
}
