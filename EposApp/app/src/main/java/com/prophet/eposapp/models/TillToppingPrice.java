package com.prophet.eposapp.models;

public class TillToppingPrice 
{
    public int TPType;
    public double TPStdPrice;
    public double TPHappyHourPrice;

    public TillToppingPrice()
    {
        TPType = 0;
        TPStdPrice = 0;
        TPHappyHourPrice = 0;                
    }       
}
