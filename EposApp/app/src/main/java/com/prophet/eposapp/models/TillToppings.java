package com.prophet.eposapp.models;



public class TillToppings 
{
    public int ToppingID;
    public int ToppingGroupID;
    public int ToppingQty;
    public int ToppingVATCode;
    public String ToppingParentCode;
    public String ToppingCode;
    public String ToppingDesc;
    public int ToppingType;
    public TOPPING_STATE_CODE ToppingState;
    public double ToppingFixedPrice;

    public TillToppings()
    {
        ToppingID = 0;
        ToppingGroupID = 0;
        ToppingQty = 0;
        ToppingVATCode = 0;   
        ToppingParentCode = "";
        ToppingCode = "";
        ToppingDesc = "";
        ToppingType = 0;
        ToppingFixedPrice = 0;
        ToppingState = TOPPING_STATE_CODE.TOP_FIXED;
    }
}
