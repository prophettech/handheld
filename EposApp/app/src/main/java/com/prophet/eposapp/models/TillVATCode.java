package com.prophet.eposapp.models;

public class TillVATCode
{
	private int mVATCode;
	public final int geVATCode()
	{
		return mVATCode;
	}
	public final void setVATCode(int in_VATCode)
	{
		mVATCode = in_VATCode;
	}
	
	private String mVATDesc;
	public final String getVATDesc()
	{
		return mVATDesc;
	}
	public final void setVATDesc(String in_VATDesc)
	{
		mVATDesc = in_VATDesc;
	}
	
	private double mVATRate;
	public final double getVATRate()
	{
		return mVATRate;
	}
	public final void setVATRate(double in_VATRate)
	{
		mVATRate = in_VATRate;
	}
}
