package com.prophet.eposapp.presenters;

import com.prophet.eposapp.interfaces.IOptions;
import com.prophet.eposapp.models.ModelManager;

public class PresenterOptions 
{
    IOptions mIOptions;
    ModelManager mModelManager;
    
	public PresenterOptions(ModelManager in_ModelManager)
	{
		mModelManager = in_ModelManager;		
	}
	
    public void AttachInterface(IOptions in_Options)
    {
        mIOptions = in_Options;
    }

    public String GetErrorMsg()
    {
        return mModelManager.mModelOptions.GetErrorMsg();
    }
       
    public void InitServerIP()
    {
        LoadServerIP();
    }
    
	public void LoadOptions() 
    {    
    	mModelManager.mModelOptions.LoadOptions();
    	mIOptions.mEposOptions = mModelManager.mModelOptions.GetOptions();
    }
    
    public void SaveServerIP()
    {
    	mModelManager.mModelOptions.SaveServerIP(mIOptions.mServerIP);    	
    }
    
    public void LoadServerIP()
    {
    	mIOptions.mServerIP = mModelManager.mModelOptions.LoadServerIP();    	
    }


}
