package com.prophet.eposapp.presenters;

import com.prophet.eposapp.interfaces.ISecurity;
import com.prophet.eposapp.models.ModelManager;

public class PresenterSecurity 
{
	ModelManager mModelManager;
    ISecurity mISecurity;    
	
	public PresenterSecurity(ModelManager in_ModelManager)
	{
		mModelManager = in_ModelManager;		
	}

	public void AttachInterface(ISecurity in_ISecurity) 
	{
		mISecurity = in_ISecurity;		
	}	
	
	public void ProcessLogin()
	{
		mISecurity.mValidUser = false;
		//mISecurity.mUserAlreadyLoggedIn = false;	
		int mLoginRes = mModelManager.mModelSecurity.ProcessLogin(mISecurity.mPasscode,mISecurity.mUserAlreadyLoggedIn );
		mISecurity.mLoginErrorMsg = mModelManager.mModelSecurity.GetErrorMsg();		
		if( mLoginRes == 1 )
		{
			mISecurity.mValidUser = true;
			mISecurity.mUserAlreadyLoggedIn = false;	
		}		
		else if( mLoginRes == -1 )
		{
			//mISecurity.mLoginErrorMsg = mModelManager.mModelSecurity.GetErrorMsg();		
			mISecurity.mUserAlreadyLoggedIn = true;		
		}
		else
		{
				
		}
	}
	
	public void AdminConfigLogin()
	{
		mISecurity.mValidUser = false;
		if( mModelManager.mModelSecurity.AdminConfigLogin(mISecurity.mPasscode) )
		{
			mISecurity.mValidUser = true;
		}		
	}
	
	public void ProcessLogout()
	{
		mModelManager.mModelSecurity.LogoutUser();
	}
}
