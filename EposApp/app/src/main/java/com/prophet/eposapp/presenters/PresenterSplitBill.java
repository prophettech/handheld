package com.prophet.eposapp.presenters;

import java.io.Serializable;

import com.prophet.eposapp.interfaces.ISplitBill;
import com.prophet.eposapp.models.ModelManager;

public class PresenterSplitBill  implements Serializable 
{
	private static final long serialVersionUID = 1L;
	ModelManager mModelManager;
	ISplitBill mISplitBill;
	
	public PresenterSplitBill(ModelManager in_ModelManager)
	{
		mModelManager = in_ModelManager;			
	}
	
    public void AttachInterface(ISplitBill in_ISplitBill)
    {
    	mISplitBill = in_ISplitBill;
    }

	public void SetSplitBill() 
	{
		mISplitBill.mSplitItemsList = mModelManager.mModelSplitBill.GetSplitItemsList();
		mISplitBill.mSplitID = mModelManager.mModelSplitBill.GetSplitBillID();
		mISplitBill.mSplitRef = mModelManager.mModelSplitBill.GetSplitBillRef();
		mISplitBill.mSplitCount = mModelManager.mModelSplitBill.GetSplitBillCount();			
	}
	
	public void NewSplitBill()
	{
		mModelManager.mModelSplitBill.NewSplitBill();
	}  
	
	public void ClearSplitBill()
	{
		mModelManager.mModelSplitBill.ClearSplitBill();
		SetSplitBill();
	}  
	
	public void AddSplitItem() 
	{
		boolean mSetPrintQty = mModelManager.mModelOrder.DecreaseItemSplitQty(mISplitBill.mSplitItemID);
		mModelManager.mModelSplitBill.AddSplitItem(mISplitBill.mSplitItemID, mSetPrintQty);
		SetSplitBill();
	}
	
	public void RemoveSplitItem() 
	{		
		boolean mUpdatePrintedQty = mModelManager.mModelSplitBill.RemoveSplitItem(mISplitBill.mSplitItemID);
		mModelManager.mModelOrder.IncreaseItemSplitQty(mISplitBill.mSplitItemID, mUpdatePrintedQty);
		SetSplitBill();
	}
	
	public void NextSplit()
	{
		mModelManager.mModelSplitBill.NextSplit();
		SetSplitBill();
	} 
	  
	public void PrevSplit()
	{
		mModelManager.mModelSplitBill.PrevSplit();	
		SetSplitBill();
	}

}
