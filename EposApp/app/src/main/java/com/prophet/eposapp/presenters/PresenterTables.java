package com.prophet.eposapp.presenters;

import java.io.Serializable;

import com.prophet.eposapp.interfaces.ITables;
import com.prophet.eposapp.models.ModelManager;

public class PresenterTables  implements Serializable 
{
	private static final long serialVersionUID = 1L;
	ModelManager mModelManager;
	ITables mITables;
	
	public PresenterTables(ModelManager in_ModelManager,ITables in_ITables)
	{
		mITables = in_ITables;
		mModelManager = in_ModelManager;			
	}
	
	public void loadTables() 
    {    
    	mModelManager.mModelTables.loadTables();  
    	mITables.mTablesList = mModelManager.mModelTables.GetTablesList();
    }	

}
