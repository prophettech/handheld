package com.prophet.eposapp.views;

import com.prophet.eposapp.EposApp;
import com.prophet.eposapp.R;
import com.prophet.eposapp.interfaces.IOptions;
import com.prophet.eposapp.interfaces.ISecurity;
import com.prophet.eposapp.presenters.PresenterOptions;
import com.prophet.eposapp.presenters.PresenterSecurity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.app.NavUtils;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends Activity
{
	protected EposApp app;
    String mPassCode;
    PresenterSecurity mPresenterSecurity;
    ISecurity mISecurity;   
    PresenterOptions mPresenterOptions;
    IOptions mIOptions;   
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		// Show the Up button in the action bar.
		
		app = (EposApp)getApplication();
		
		mPresenterSecurity = app.GetPresenterSecurity();
		mISecurity = app.GetISecurity();
		
		mPresenterOptions = app.GetPresenterOptions();
		mIOptions = app.GetIOptions();
		
		mPassCode = "";
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);

	    // Checks the orientation of the screen
	    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) 
	    {
	    	newConfig.orientation = Configuration.ORIENTATION_PORTRAIT;
	    }
	}
	
	public void KeyPadNumberButton(View view) 
	{
		TextView tv = (TextView)view;
		String mKeyNumber = (String) tv.getText();
		if( mKeyNumber.compareTo("") != 0 )
		{
			mPassCode += mKeyNumber;
			SetPassCode();
		}
	}
	
	public void SetPassCode() 
	{
		TextView tv = (TextView) findViewById(R.id.tvLoginCode);
		String mHiddenCode = mPassCode.replaceAll("[0-9]", "*");
		//String mHiddenCode = mPassCode;
		tv.setText(mHiddenCode);

	}	
	
	public void KeyPadClearButton(View view) 
	{
		if( mPassCode.length() > 0 )
		{
			mPassCode = mPassCode.substring(0,mPassCode.length()-1);
			SetPassCode();
		}
	}	
	
	public void KeyPadAdminButton(View view) 
	{
		mISecurity.mPasscode = mPassCode;
		mPresenterSecurity.AdminConfigLogin();
		if( mISecurity.mValidUser )
		{
			DisplayAdminConfig();
		}
		else
		{
			mISecurity.mLoginErrorMsg = "Invalid Passcode";
			DisplayLoginErrorMsg();   	
		}
	}	
	
	public void KeyPadLoginButton(View view)  
	{
		mISecurity.mPasscode = mPassCode;
		new ProcessLogin().execute();
	}	
	
	public void DisplayAdminConfig() 
	{	
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Server IP");

		// Set up the input
		final EditText input = new EditText(this);
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		input.setText(mIOptions.mServerIP);
		builder.setView(input);
		// Set up the buttons
		builder.setNegativeButton("Close", new DialogInterface.OnClickListener() 
		{
		    @Override
		    public void onClick(DialogInterface dialog, int which) 
		    {
		    	mPassCode = "";
		    	SetPassCode();
		        dialog.cancel();
		    }
		});

		builder.setPositiveButton("Set Server IP", new DialogInterface.OnClickListener() 
		{ 
		    @Override
		    public void onClick(DialogInterface dialog, int which) 
		    {
		    	mIOptions.mServerIP = input.getText().toString();
		    	mPresenterOptions.SaveServerIP();
		    	//app.SetDBPath();		
		    	mPassCode = "";
		    	SetPassCode();
		    }
		});

		builder.show();
	}
	
	public void DisplayLoginErrorMsg() 
	{
		AlertDialog alertDialog;
		alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
		alertDialog.setTitle("");
		alertDialog.setMessage(mISecurity.mLoginErrorMsg);
		alertDialog.show();    			
	}
	
	public void displayTables()
	{
		Intent tablesIntent =  new Intent(this, DisplayTablesActivity.class);
		startActivity(tablesIntent);					
	}	
	
	public void DisplayLogoutUserBox() 
	{    
		AlertDialog.Builder builder = new AlertDialog.Builder(this);


		builder.setTitle("User Login");
		//builder.setMessage("This user is already logged in on another client. If you continue, this user will be logged out." );
		builder.setMessage(mISecurity.mLoginErrorMsg);
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
		{			
			public void onClick(DialogInterface dialog, int which) 
			{					
				new ProcessLogin().execute();
			}			
		});

		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				dialog.dismiss();
				mISecurity.mUserAlreadyLoggedIn = false;
			}
		});

		AlertDialog alert = builder.create();
		alert.setCanceledOnTouchOutside(false);
		alert.show();
	}
	
	class LoadData extends AsyncTask<String, String, String> 
	{
		   private ProgressDialog pDialog;
	    	//MainActivity mParentActivity; 
	    	//MainActivity in_parentActivity
	    	LoadData( )
	    	{
	    		//mParentActivity = in_parentActivity;
	    	
	    	}
	        /**
	         * Before starting background thread Show Progress Dialog
	         * */
	        @Override
	        protected void onPreExecute() 
	        {         
	        	super.onPreExecute();
	            pDialog = new ProgressDialog(LoginActivity.this);
	            pDialog.setMessage("Loading Data. Please wait...");
	            pDialog.setIndeterminate(false);
	            pDialog.setCancelable(false);
	            pDialog.show();            
	        }
	        
	        protected String doInBackground(String... args) 
	        {      
	        	//String errorMsg = "";
	        	try
	        	{
	        		app.GetPresenterOptions().LoadOptions();
	        		app.GetPresenterMenu().LoadMenu();
	        		app.GetPresenterOrder().NewOrder();
	        	}
	        	catch(Exception e)
	        	{
	        		//errorMsg = e.getMessage();
	        		return null;
	        		
	        	}
	            return "1";
	        }
	        
	        protected void onPostExecute(String file_url) 
	        {
	            pDialog.dismiss();       
	            displayTables();
	        }  
	}
	
	class ProcessLogin extends AsyncTask<String, String, String> 
	{
		   private ProgressDialog pDialog;

		   ProcessLogin( )
	    	{
		
	    	
	    	}
	        /**
	         * Before starting background thread Show Progress Dialog
	         * */
	        @Override
	        protected void onPreExecute() 
	        {         
	        	super.onPreExecute();
	            pDialog = new ProgressDialog(LoginActivity.this);
	            pDialog.setMessage("Processing Login. Please wait...");
	            pDialog.setIndeterminate(false);
	            pDialog.setCancelable(false);
	            pDialog.show();            
	        }
	        
	        
	        protected String doInBackground(String... args) 
	        {    
	        	mPresenterSecurity.ProcessLogin();
	        	
	        	/*
	        	try 
	        	{
	                System.out.println("Host addr: " + InetAddress.getLocalHost().getHostAddress());  // often returns "127.0.0.1"
	                Enumeration<NetworkInterface> n = null;
					try {
						n = NetworkInterface.getNetworkInterfaces();
					} catch (SocketException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					byte[] localIP = null;
					if(n != null )
					{
		                for (; n.hasMoreElements();)
		                {
		                        NetworkInterface e = n.nextElement();
		                        System.out.println("Interface: " + e.getName());
		                        Enumeration<InetAddress> a = e.getInetAddresses();
		                        for (; a.hasMoreElements();)
		                        {
		                                InetAddress addr = a.nextElement();
		                                String tmpStr = addr.toString();
		                                tmpStr = tmpStr.substring(1,4);
		                                if( tmpStr.compareTo("192") == 0 )
		                                {
		                                	localIP = addr.getAddress();
		                                	System.out.println("  " + addr.getHostAddress());
		                                	break;
		                                }		                                
		                        }
		                }
		                if( localIP.length != 0) 
		                {
		                	InetAddress addr = null;
		            		for (int i = 1; i <= 254; i++)
		            		{
		            			localIP[3] = (byte)i;
		            			addr = InetAddress.getByAddress(localIP);
			            		if (addr.isReachable(1000))
			            		{
			            			System.out.println(addr + " machine is turned on and can be pinged");
			            		}
		            		}
		                }
		                else
		                {
		                	mISecurity.mLoginErrorMsg = "Could not get network IP";
		                }
					}
	        	}
	        	catch (java.net.UnknownHostException exception) 
	        	{
	        		mISecurity.mLoginErrorMsg = exception.getMessage();	        		
	        	} catch (IOException e) {
					// TODO Auto-generated catch block
	        		mISecurity.mLoginErrorMsg = e.getMessage();	        
					e.printStackTrace();
				}
                */
	            return "1";
	        }
	        
	        protected void onPostExecute(String file_url) 
	        {
	            pDialog.dismiss();       
	    		if( mISecurity.mValidUser )
	    		{
	    			new LoadData().execute();
	    		}	    		
	    		else if( mISecurity.mUserAlreadyLoggedIn )
	    		{
	    			DisplayLogoutUserBox();    			
	    		}
	    		else
	    		{
	    			if( mISecurity.mLoginErrorMsg.compareTo("") == 0)
	    				mISecurity.mLoginErrorMsg = "Unknown login error";
	    				
	    			DisplayLoginErrorMsg();
	    		}
	    		
	        }  	        
	}
}
		
	

	

