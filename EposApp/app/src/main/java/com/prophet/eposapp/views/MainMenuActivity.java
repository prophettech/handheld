package com.prophet.eposapp.views;

import java.util.ArrayList;
import java.util.Locale;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.app.NavUtils;
import android.text.Html;

import com.prophet.eposapp.CatAdapter;
import com.prophet.eposapp.EposApp;
import com.prophet.eposapp.ProdAdapter;
import com.prophet.eposapp.R;
import com.prophet.eposapp.interfaces.IMenu;
import com.prophet.eposapp.interfaces.IOrder;
import com.prophet.eposapp.models.TillCategory;
import com.prophet.eposapp.models.TillOrderItem;
import com.prophet.eposapp.models.TillProduct;
import com.prophet.eposapp.models.TillProductOfferItem;
import com.prophet.eposapp.models.TillProductOfferItemGroup;
import com.prophet.eposapp.presenters.PresenterMenu;
import com.prophet.eposapp.presenters.PresenterOrder;
import com.prophet.eposapp.presenters.PresenterReport;


public class MainMenuActivity  extends Activity
{	
	IMenu mIMenu;
	IOrder mIOrder;
	PresenterMenu mPresenterMenu;	
	PresenterOrder mPresenterOrder;
	PresenterReport mPresenterReport;
	boolean mDisplayingProds;
	int mIDCounter;
	
    private ProgressDialog pDialog;
    
    
    static final int REFRESH_ORDER = 1;
    static final int PAY_REQUEST = 2;    
    

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_mainmenu);
		// Show the Up button in the action bar.
		
		mPresenterMenu = ((EposApp)getApplication()).GetPresenterMenu();
		mIMenu = ((EposApp)getApplication()).GetIMenu();

		mPresenterOrder = ((EposApp)getApplication()).GetPresenterOrder();
		mIOrder = ((EposApp)getApplication()).GetIOrder();
		
		mPresenterReport = ((EposApp)getApplication()).GetPresenterReport();
		
		DisplayCats();	
		DisplayOrderDetails();
		DisplayOrderItems();
		mIDCounter = 1;
		if( mIOrder.mOrderErrorMsg.compareTo("") != 0 )
		{
			// Display any warnings
			AlertDialog alertDialog;
			alertDialog = new AlertDialog.Builder(MainMenuActivity.this).create();
			alertDialog.setTitle("");
			alertDialog.setMessage(mIOrder.mOrderErrorMsg);
			alertDialog.show();    		
		}   
		mIOrder.mOrderErrorMsg = "";
		/*
		long heapSize = Runtime.getRuntime().totalMemory();
		long heapMaxSize = Runtime.getRuntime().maxMemory();
		long heapFreeSize = Runtime.getRuntime().freeMemory();
		String MemStr = "Heap Size: " + String.valueOf(heapSize) + "\tHeap Max Size: "+ String.valueOf(heapMaxSize) + "\tHeap Free Size: "+ String.valueOf(heapFreeSize); 
		AlertDialog alertDialog;
		alertDialog = new AlertDialog.Builder(MainMenuActivity.this).create();
		alertDialog.setTitle("");
		alertDialog.setMessage(MemStr);
		alertDialog.show();
		*/
	}
	
	public void MenuBackButton(View view) 
	{
		if( mDisplayingProds)
		{
			DisplayCats();
		}
		else
		{
			//createTableRow();
		}
	}	
	
	private void DisplayCats() 
	{
		mDisplayingProds = false;
        if(mIMenu != null)
        {        	
            runOnUiThread
            (
            	new Runnable() 
            	{
            		public void run()
            		{     
            		    GridView gridview = (GridView) findViewById(R.id.catgridview);
            			
            		    if( gridview != null )
            		    {
            		    	//gridview.setAdapter(new ProdAdapter(MainMenuActivity.this,mIMenu.mProductList));
            		    	gridview.setAdapter(new CatAdapter(MainMenuActivity.this,mIMenu.mCategoryList));               	
            		    	gridview.setOnItemClickListener(menuGridOnItemClickListener);
            		    }
                	}             
            	}
            );             
        }		
	}

	OnItemClickListener menuGridOnItemClickListener = new OnItemClickListener()
	{	  
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
		{
			if( mDisplayingProds)
			{
				TillProduct tmpProd = (TillProduct)parent.getItemAtPosition(position);	
				if( tmpProd.ProductOffer )
				{

                    mIMenu.mSelectedPOCode = tmpProd.ProductCode;
                    mIMenu.mSelectedPODesc = tmpProd.ProductDesc;
                    mIMenu.mSelectedPOPrice = tmpProd.ProductPrice;
                    mIMenu.mMenuOfferMode = true;
                    mPresenterMenu.SetMenuOfferMode();
                    //DisplayMenuProductOffer(_IMenu._MenuOfferMode, true);
					DisplayProductOffer();				
				}
				else
				{
					String prompt = tmpProd.ProductDesc;					
					mIMenu.mSelectedProdID = (int)id;
					int prodAction = mPresenterMenu.ProductClicked();
					if (prodAction > -1)
					{
						if (prodAction == 1) // Add to order, no options/toppings
						{
							Toast.makeText(getApplicationContext(), prompt, Toast.LENGTH_SHORT).show();	
							DisplayOrderDetails();
							DisplayOrderItems();                    	
						}
						else if (prodAction == 2) // Options 
						{
							if( mIMenu.mSelectedProductOptions != null )
							{	
								if( mIMenu.mSelectedProductOptions.GetButGroups().size() > 0 )
								{
									DisplayProdOptions();                	                        		
								}
								else if(  mIMenu.mSelectedTopGroupToppings.size() > 0 || mIMenu.mSelectedProduct1.ProductToppingsList.size() > 0 )
								{
									DisplayProdToppings();                        			
								}

							}
						}
					}
					else
					{
		    			AlertDialog alertDialog;
		    			alertDialog = new AlertDialog.Builder(MainMenuActivity.this).create();
		    			alertDialog.setTitle("");
		    			alertDialog.setMessage(mPresenterMenu.GetMenuErrorMsg());
		    			alertDialog.show();
					}
				}
			}
			else
			{
				TillCategory tmpCat = (TillCategory)parent.getItemAtPosition(position);	
				mIMenu.mSelectedCatID = tmpCat.GetCatID();
				DisplayProds();			 				
			}
		}	
	};
	
	public void DisplayProductOffer()
	{
		Intent POIntent =  new Intent(this, ProductOfferActivity.class);
		//startActivity(MDIntent);	
		startActivityForResult(POIntent,REFRESH_ORDER);	
	}
	
	public void DisplayProdOptions()
	{
		mIMenu.mProdOptionsDisplayed = true;
		Intent prodOptionsIntent =  new Intent(this, ProdOptionsActivity.class);
		//startActivity(prodOptionsIntent);	
		startActivityForResult(prodOptionsIntent,REFRESH_ORDER);		
	}
	
	public void DisplayProdToppings()
	{
		Intent prodToppingsIntent =  new Intent(this, ProdToppingsActivity.class);
		//startActivity(prodToppingsIntent);	
		startActivityForResult(prodToppingsIntent,REFRESH_ORDER);
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) 
    {
        if (requestCode == REFRESH_ORDER) 
        {
            if (resultCode == RESULT_OK)
            {
            	mIMenu.mProdOptionsDisplayed = false;
            	DisplayOrderDetails();
            	DisplayOrderItems();
            }
        }
        else if(requestCode == PAY_REQUEST )
        {
            if (resultCode == RESULT_OK)
            {
            	DisplayTables();
            }
        }               
    }
	
	private void DisplayProds() 
	{
		mDisplayingProds = true;
        if(mIMenu != null)
        {      
        	runOnUiThread
        	(
        		new Runnable() 
        		{
        			public void run()
        			{     
        				GridView gridview = (GridView) findViewById(R.id.catgridview);
        			
        				if( gridview != null )
        				{        					
        					mPresenterMenu.SetActiveProducts();
        					gridview.setAdapter(new ProdAdapter(MainMenuActivity.this,mIMenu.mActiveProductList));           	
        					//	gridview.setOnItemClickListener(catOnItemClickListener);
        				}
        			}             
        		}
        	);           
        }		
	}
	
	private void DisplayEditItem() 
	{
		Intent EditItemIntent =  new Intent(this, EditItemActivity.class);
		startActivityForResult(EditItemIntent,REFRESH_ORDER);
		//startActivity(EditItemIntent);			
	}
	
	OnClickListener orderItemOnClickListener = new OnClickListener()
	{	  
		public void onClick(View v) 
		{
			mIOrder.mCurrentOrderItemID = v.getId();
			DisplayEditItem();
			//Toast.makeText(getApplicationContext(), "row " + Integer.toString(rowid), Toast.LENGTH_SHORT).show();				
		}		
	};
	
	private void DisplayOrderDetails() 
	{
		TextView tv = (TextView) findViewById(R.id.tvOrderTableNoValue);		
		tv.setText(Integer.toString(mIOrder.mCurrentOrderDetails.getOrderTableNo()));
		
		tv = (TextView) findViewById(R.id.tvOrderTableCoversValue);		
		tv.setText(Integer.toString(mIOrder.mCurrentOrderDetails.getOrderTableCovers()));
				
		tv = (TextView) findViewById(R.id.tvOrderTotal);		
		tv.setText(String.format("%.2f", mIOrder.mCurrentOrderDetails.getOrderTotal()));	
		
	}
	

	private void DisplayOrderItems() 
	{
		TableLayout tl = (TableLayout) findViewById(R.id.tableLayout1);		  
		tl.removeAllViews();
		mIDCounter = 1;
		
		TableRow tr;		
		TextView txtQty;	
		TextView txtDesc;
		TextView txtPrice;	
		TextView txtTotal;;	
		ArrayList<String> retTextFormat;
		ArrayList<String> retPOItemDetails;
	 
		//EposApp mEposApp = ((EposApp)getApplication());
		
		if( mIOrder.mCurrentOrderItems.size() > 0 )
		{			
			LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);	  
			TableRow.LayoutParams lp; 
			int mRowHeight = 50;
			String LineDesc = "";
            TillProduct TmpItemProduct1;
			String ItemDesc = "";
			String ItemTopPlusDesc = "";
			String ItemTopMinusDesc = "";
			String ItemFreeText = "";
			double ItemQty = 0;
			double ItemPrice = 0;
			double ItemTotal = 0;
			double ItemToppingCharge = 0;
			int LineCount = 0;
			boolean DisplayItem = false;
			for(TillOrderItem mOrderItem : mIOrder.mCurrentOrderItems )
			{		
				LineDesc = "";
				LineCount = 0;
				ItemDesc = mOrderItem.GetItemDesc();
				ItemQty = mOrderItem.GetItemQty();
				ItemPrice = mOrderItem.GetItemPrice();
				ItemTotal = mOrderItem.GetItemTotal();
				ItemTopMinusDesc = mOrderItem.GetItemMinusToppingDescH1();
				ItemTopPlusDesc = mOrderItem.GetItemPlusToppingDescH1();	
				ItemToppingCharge = mOrderItem.GetItemToppingCharge();
				ItemFreeText = mOrderItem.GetItemFreeText();
				if( mOrderItem.GetItemOffer() )
				{				
					retTextFormat = GenerateItemDisplayDesc(ItemDesc,"","","",0);
					LineDesc = retTextFormat.get(0);
					LineCount = Integer.parseInt(retTextFormat.get(1));
					
	                ArrayList<TillProductOfferItemGroup> TmpPOItemGroupList = mOrderItem.GetProductOffer().GetPOItemGroupList();
	                ArrayList<TillProductOfferItem> TmpPOItemsList;
	        		for (TillProductOfferItemGroup POItemGroup : TmpPOItemGroupList)
	        		{
	        			LineDesc += "<br />";
	        			TmpPOItemsList = POItemGroup.GetPOItemList();
	        			for (TillProductOfferItem POItem : TmpPOItemsList)
	        			{		
	        				DisplayItem = true;
	        				retPOItemDetails = POItem.GetPOItemDetails();
	            			if( retPOItemDetails.get(7).compareTo("1") != 0 )
	            			{
	            				DisplayItem = false;	            				
	            			}
	            			
	            			if( DisplayItem )
	            			{
	            				ItemDesc = "";
	            				ItemTopPlusDesc = "";
	            				ItemTopMinusDesc = "";
	            				ItemFreeText = "";
	                            TmpItemProduct1 = POItem.GetPOProduct1();
	                            
	                            ItemDesc = retPOItemDetails.get(5);
	                            if (TmpItemProduct1 != null && TmpItemProduct1.ProductCode.compareTo("") != 0)
	                            {
	                            	ItemDesc = TmpItemProduct1.ProductDesc;
	                                ItemTopMinusDesc = TmpItemProduct1.GetProductMinusToppingDesc();
	                                ItemTopPlusDesc = TmpItemProduct1.GetProductPlusToppingDesc();		                            	
	                                //ItemToppingCharge = TmpItemProduct1.ProductToppingCharges;
	                            }

	                            
	        					retTextFormat = GenerateItemDisplayDesc(ItemDesc,ItemTopMinusDesc,ItemTopPlusDesc,ItemFreeText,1);
	        					LineDesc += retTextFormat.get(0);
	        					LineCount += Integer.parseInt(retTextFormat.get(1));	
	        					break;
	            			}
	        			}
	        		}	
				}
				else
				{
					retTextFormat = GenerateItemDisplayDesc(ItemDesc,ItemTopMinusDesc,ItemTopPlusDesc,ItemFreeText,0);
					LineDesc = retTextFormat.get(0);
					LineCount = Integer.parseInt(retTextFormat.get(1));
				}				
				
				mRowHeight = LineCount*20;
				if( mRowHeight < 50 )
					mRowHeight = 50;
				
				tr = new TableRow(this);
				tr.setBackgroundColor(Color.rgb(227, 228, 229));
				tr.setOnClickListener(orderItemOnClickListener);
				tr.setId(mOrderItem.GetItemID());
				
				txtQty=new TextView(this);	
				lp = new TableRow.LayoutParams(0, mRowHeight, 0.07f);
				lp.setMargins(1, 0, 0, 1);	
				txtQty.setLayoutParams(lp);
				txtQty.setTextSize(16f);
				txtQty.setText(String.format("%.0f", ItemQty));	
				//txtQty.setText(Integer.toString(LineCount));					
				txtQty.setBackgroundColor(Color.WHITE);
				txtQty.setTextColor(Color.BLACK);
				txtQty.setPadding(4, 0, 0, 0);
				tr.addView(txtQty);			
										
				
				txtDesc=new TextView(this);
				lp = new TableRow.LayoutParams(0, mRowHeight, 0.7f);
				lp.setMargins(1, 0, 0, 1);	
				txtDesc.setLayoutParams(lp);
				txtDesc.setTextSize(16f);
				txtDesc.setText(Html.fromHtml(LineDesc));
				txtDesc.setBackgroundColor(Color.WHITE);
				txtDesc.setTextColor(Color.BLACK);				
				txtDesc.setPadding(4, 0, 0, 0);				
				tr.addView(txtDesc);	

				txtPrice=new TextView(this);	
				lp = new TableRow.LayoutParams(0, mRowHeight, 0.11f);
				lp.setMargins(1, 0, 0, 1);				
				txtPrice.setLayoutParams(lp);
				txtPrice.setTextSize(16f);
				txtPrice.setText(String.format("%.2f", ItemPrice));	
				txtPrice.setBackgroundColor(Color.WHITE);
				txtPrice.setTextColor(Color.BLACK);
				txtPrice.setGravity(Gravity.RIGHT);
				txtPrice.setPadding(0, 0, 4, 0);
				
				tr.addView(txtPrice);
				  
				txtTotal=new TextView(this);	
				lp = new TableRow.LayoutParams(0, mRowHeight, 0.12f);
				lp.setMargins(1, 0, 0, 1);				
				txtTotal.setLayoutParams(lp);
				txtTotal.setTextSize(16f);
				String TotalStr = String.format(Locale.UK,"%.2f", ItemTotal);
				//if( ItemTopPlusDesc.compareTo("") != 0 && ItemToppingCharge > 0 )
				if( ItemToppingCharge > 0 )
				{
					TotalStr += "<br />" +  String.format(Locale.UK,"%.2f", ItemToppingCharge);					
				}				
				txtTotal.setText(Html.fromHtml(TotalStr));
				//txtTotal.setText(String.format("%.2f", ItemTotal));	
				
				txtTotal.setBackgroundColor(Color.WHITE);
				txtTotal.setTextColor(Color.BLACK);
				txtTotal.setGravity(Gravity.RIGHT);
				txtTotal.setPadding(0, 0, 4, 0);
				tr.addView(txtTotal);		
				
				tl.addView(tr,new TableLayout.LayoutParams(layoutParams));					
			}	

			((ScrollView) findViewById(R.id.scrollView1)).post(new Runnable() 
			{
                public void run() {
                    ((ScrollView) findViewById(R.id.scrollView1)).fullScroll(View.FOCUS_DOWN);
                }

            });

		}
	}
	
	public ArrayList<String> GenerateItemDisplayDesc(String in_Desc, String in_TopMinusDesc, String in_TopPlusDesc, String in_FreeText, int in_Indent) 
	{
		String TmpDesc = "";
		int TmpLineCount = 0;
		ArrayList<String> retTextFormat;
		ArrayList<String> retItemDesc = new ArrayList<String>();
		retItemDesc.add("");
		retItemDesc.add("");
		
		EposApp mEposApp = ((EposApp)getApplication());
		
		retTextFormat = mEposApp.FormatItemText(in_Desc,in_Indent,43, " ");
		TmpDesc = retTextFormat.get(0);
		TmpLineCount = Integer.parseInt(retTextFormat.get(1));
					
		if( in_TopMinusDesc.compareTo("") != 0 )
		{					
			retTextFormat = mEposApp.FormatItemText(in_TopMinusDesc,in_Indent+1,43, ",");
			TmpDesc += "<br /><font color=\'red\'>"+retTextFormat.get(0)+ "<//font>";
			TmpLineCount += Integer.parseInt(retTextFormat.get(1));
		}
		
		if( in_TopPlusDesc.compareTo("") != 0  )
		{
			retTextFormat = mEposApp.FormatItemText(in_TopPlusDesc,in_Indent+1,43, ",");				
			TmpDesc += "<br /><font color=\'green\'>"+retTextFormat.get(0)+ "<//font>";
			TmpLineCount += Integer.parseInt(retTextFormat.get(1));
		}
		
		if( in_FreeText.compareTo("") != 0  )
		{
			retTextFormat = mEposApp.FormatItemText(in_FreeText,in_Indent+1,43, " ");				
			TmpDesc += "<br /><font color=\'black\'>"+retTextFormat.get(0)+ "<//font>";
			TmpLineCount += Integer.parseInt(retTextFormat.get(1));
		}
		retItemDesc.set(0, TmpDesc);
		retItemDesc.set(1, Integer.toString(TmpLineCount));
		
		return retItemDesc;
	}	
	

	
	public void DisplayTablesButton(View view) 
	{		
		CheckOrderNeedsSaving();
	}
	
	public void CheckOrderNeedsSaving() 
	{
		mIOrder.mOrderChanged = false;		
		mPresenterOrder.GetOrderChangedFlag();
		if( mIOrder.mOrderChanged )
		{
			DisplaySaveOrderBox();			
		}
		else
		{
			DisplayTables();
		}
	}
	
	public void DisplaySaveOrderBox() 
	{    
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle("Save Order?");
		builder.setMessage("The order has changed. Do you want to save now?");

		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() 
		{			
			public void onClick(DialogInterface dialog, int which) 
			{							
				dialog.dismiss();
				StartSaveOrderTask();
			}			
		});

		builder.setNegativeButton("No", new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				DisplayTables();
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();
	}
	
	public void DisplayTables() 
	{
		mIOrder.mLoadOrderID = 0;
		Intent tablesIntent =  new Intent(this, DisplayTablesActivity.class);
		startActivity(tablesIntent);	
	}	

	public void SaveOrderButton(View view) 
	{		
		StartSaveOrderTask();
	}	
	
	public void StartSaveOrderTask() 
	{		
		new SaveOrderTask().execute();
	}	
	
	public void PrintOrderButton(View view) 
	{
		new PrintOrderTask().execute();
	}	
		
	public void PayOrderButton(View view) 
	{
		Intent payIntent =  new Intent(this, PayActivity.class);
		//startActivity(payIntent);		
		startActivityForResult(payIntent,PAY_REQUEST);	
	}	
	
    public class SaveOrderTask extends AsyncTask<String, String, String> 
	{
        @Override
        protected void onPreExecute() 
        {         
        	super.onPreExecute();
            pDialog = new ProgressDialog(MainMenuActivity.this);
            pDialog.setMessage("Saving Order. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();            
        }
        
        protected void onPostExecute(String file_url) 
        {
        	
            // dismiss the dialog after getting all products
            pDialog.dismiss();  
        	//displayOrderDetails();
        	//displayOrderItems();	
            
            if( mIOrder.mOrderRes )
            {
    			//Toast.makeText(getApplicationContext(), "Order Saved", Toast.LENGTH_SHORT).show();				            	
            	//displayOrderDetails();
            	//displayOrderItems();	
            	DisplayTables();    
    		}
    		else
    		{
    			//Toast.makeText(getApplicationContext(), mIOrder.mOrderErrorMsg, Toast.LENGTH_LONG).show();				
    			AlertDialog alertDialog;
    			alertDialog = new AlertDialog.Builder(MainMenuActivity.this).create();
    			alertDialog.setTitle("");
    			alertDialog.setMessage(mIOrder.mOrderErrorMsg);
    			alertDialog.show();
    		}               
        }  
        
		@Override
		protected String doInBackground(String... arg0) 
		{
			mPresenterOrder.SaveOrder();
			//displayTables();
			
			return null;
		}    	
	}
    
    public class PrintOrderTask extends AsyncTask<String, String, String> 
    {

        ProgressDialog pDialog;     

        
        @Override
        protected void onPreExecute() 
        {         
        	super.onPreExecute();
        	
            pDialog = new ProgressDialog(MainMenuActivity.this);
            pDialog.setMessage("Printing Order. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();   
                     
        }
        
        protected void onPostExecute(String file_url) 
        {        	

            pDialog.dismiss();  
            if( !mIOrder.mOrderRes )
            {
    			AlertDialog alertDialog;
    			alertDialog = new AlertDialog.Builder(MainMenuActivity.this).create();
    			alertDialog.setTitle("");
    			alertDialog.setMessage(mIOrder.mOrderErrorMsg);
    			alertDialog.show();    
            }
            else
            {
            	if( mIOrder.mLoadOrderID == 0 )
            	{
            		DisplayTables();
            	}
            }
        }  
        
    	@Override
    	protected String doInBackground(String... arg0) 
    	{
    		mPresenterOrder.SaveOrder();
            if( mIOrder.mOrderRes )
            {
            	mPresenterReport.PrintOrder(1);
            	if( mIOrder.mOrderRes )
            	{
            		mPresenterOrder.SaveOrder(); // Save to update printed qty 
            	}
            }
    		return null;
    	}    	
    }
}

