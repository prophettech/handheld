package com.prophet.eposapp.views;

import java.util.Locale;

import com.prophet.eposapp.EposApp;
import com.prophet.eposapp.R;
import com.prophet.eposapp.interfaces.IOrder;
import com.prophet.eposapp.presenters.PresenterOrder;
import com.prophet.eposapp.presenters.PresenterReport;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.app.NavUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class PayActivity   extends Activity
{
	IOrder mIOrder;	
	PresenterOrder mPresenterOrder;
	PresenterReport mPresenterReport;
	boolean mSpinnerInit;	
	boolean mPrintOrder;
	static final int REFRESH_ORDER = 1;
    static final int REFRESH_PAY = 2;
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pay);
		// Show the Up button in the action bar.
		
		mSpinnerInit = true;
		
		mIOrder = ((EposApp)getApplication()).GetIOrder();
		mPresenterOrder = ((EposApp)getApplication()).GetPresenterOrder();
		mPresenterReport = ((EposApp)getApplication()).GetPresenterReport();

		mIOrder.mOrderServiceRate = 0;
		mIOrder.mOrderServiceAmount = 0;
		mIOrder.mOrderDiscountRate = 0;
		mIOrder.mOrderDiscountAmount = 0;
				
		//SetPayTypeEvents();
		SetTenderedEvents();
		SetServiceRateEvents();
		SetServiceAmtEvents();
		SetDiscountPercEvents();
		SetDiscountAmtEvents();
		
		SetOrderValues();
		SetPayValues();
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) 
    {
        if (requestCode == REFRESH_PAY) 
        {
            if (resultCode == RESULT_OK)
            {
            	SetPayValues();
            }
        }        
    }
	
	public void SetPayTypeEvents() 
	{	
/*
		Spinner sp = (Spinner)findViewById(R.id.spinnerPayType);

		sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
		    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) 
		    { 
		    	if( !mSpinnerInit )
		    	{
		    		mPresenterOrder.SetPayType(adapterView.getItemAtPosition(position).toString());
		    	}
		    	mSpinnerInit = false;
		    } 

		    public void onNothingSelected(AdapterView<?> adapterView) 
		    {
		        return;
		    } 
		}); 
		*/

	}
	
	public void SetTenderedEvents() 
	{	
		EditText et = (EditText) findViewById(R.id.etTendered);
		et.setOnEditorActionListener(new OnEditorActionListener()
	    {

	        @Override
	        public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
	        {
	            if(actionId == EditorInfo.IME_ACTION_DONE)
	            {
	            	mIOrder.mOrderPaymentAdd = false;
	            	PayAmtChanged();
	            }
	            return true;
	        }

	    });
		
		et.setOnFocusChangeListener(new View.OnFocusChangeListener()
	    {
	        @Override
	        public void onFocusChange(View v, boolean hasFocus)
	        {
	            if( !hasFocus )
	            {
	            	mIOrder.mOrderPaymentAdd = false;
	            	PayAmtChanged();	// Update order payment
	            	SetPayAmtValue();	// Update on screen value so it displays 2dp
	            }
	        }
	    });
	}

	public void SetServiceRateEvents() 
	{	
		EditText et = (EditText) findViewById(R.id.etServiceRate);
		et.setOnEditorActionListener(new OnEditorActionListener()
	    {
	        @Override
	        public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
	        {
	            if(actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT)
	            {
	            	ServiceChargeRateChanged();
	            }
	            return true;
	        }

	    });
		
		et.setOnFocusChangeListener(new View.OnFocusChangeListener()
	    {
	        @Override
	        public void onFocusChange(View v, boolean hasFocus)
	        {
	            if( !hasFocus )
	            {
	            	ServiceChargeRateChanged();
	            }
	        }
	    });
	}
	
	public void SetServiceAmtEvents() 
	{	
		EditText et = (EditText) findViewById(R.id.etServiceAmt);
		et.setOnEditorActionListener(new OnEditorActionListener()
	    {
	        @Override
	        public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
	        {
	            if(actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT)
	            {
	            	ServiceChargeAmtChanged();
	            }
	            return true;
	        }

	    });
		
		et.setOnFocusChangeListener(new View.OnFocusChangeListener()
	    {
	        @Override
	        public void onFocusChange(View v, boolean hasFocus)
	        {
	            if( !hasFocus )
	            {
	            	ServiceChargeAmtChanged();
	            }
	        }
	    });
	}
	
	public void SetDiscountPercEvents() 
	{	
		EditText et = (EditText) findViewById(R.id.etDiscRate);
		et.setOnEditorActionListener(new OnEditorActionListener()
	    {
	        @Override
	        public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
	        {
	            if(actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT)
	            {
	            	DiscountRateChanged();
	            }
	            return true;
	        }

	    });
		
		et.setOnFocusChangeListener(new View.OnFocusChangeListener()
	    {
	        @Override
	        public void onFocusChange(View v, boolean hasFocus)
	        {
	            if( !hasFocus )
	            {
	            	DiscountRateChanged();
	            }
	        }
	    });
	}
	
	public void SetDiscountAmtEvents() 
	{	
		EditText et = (EditText) findViewById(R.id.etDiscAmt);
		et.setOnEditorActionListener(new OnEditorActionListener()
	    {
	        @Override
	        public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
	        {
	            if(actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT)
	            {
	            	DiscountAmtChanged();
	            }
	            return true;
	        }

	    });
		
		et.setOnFocusChangeListener(new View.OnFocusChangeListener()
	    {
	        @Override
	        public void onFocusChange(View v, boolean hasFocus)
	        {
	            if( !hasFocus )
	            {
	            	DiscountAmtChanged();
	            }
	        }
	    });
	}
	
	public void SetOrderValues() 
	{
    	TextView tv = (TextView) findViewById(R.id.tvOrderTableNoValue);	
    	if( tv != null )
    	{		
    		tv.setText(Integer.toString(mIOrder.mCurrentOrderDetails.getOrderTableNo()));
    	}  	
		
		tv = (TextView) findViewById(R.id.tvOrderTableCoversValue);		
    	if( tv != null )
    	{		
        	tv.setText(Integer.toString(mIOrder.mCurrentOrderDetails.getOrderTableCovers()));	
    	}  			
	}
	
	public void SetPayValues() 
	{
        if(mIOrder != null)
        {    
        	int butID = 0;
    		String mPayTypeDesc = mIOrder.mCurrentOrderDetails.getOrderPayTypeDesc();
    		mPayTypeDesc = mPayTypeDesc.toUpperCase(Locale.ENGLISH);
    		if( mPayTypeDesc.compareTo("CC") == 0 )
    		{    			
    			butID = R.id.butpaytypecc;          			
    		}
    		else if( mPayTypeDesc.compareTo("CHQ") == 0 )
    		{    		
    			butID = R.id.butpaytypechq;           			
    		}
    		else if( mPayTypeDesc.compareTo("ACC") == 0 )
    		{    			
    			butID = R.id.butpaytypeacc;           			
    		}  
    		else if( mPayTypeDesc.compareTo("SPLIT") == 0 )
    		{    			
    			butID = R.id.butpaytypesplit;           			
    		}  
    		else
    		{
    			butID = R.id.butpaytypecash;        			
    		}

    		//Reset pay type colours
    		SetPayTypeColour(R.id.butpaytypecash, Color.parseColor("#00E171"));
    		SetPayTypeColour(R.id.butpaytypecc, Color.parseColor("#00E171"));
    		SetPayTypeColour(R.id.butpaytypechq, Color.parseColor("#00E171"));
    		SetPayTypeColour(R.id.butpaytypeacc, Color.parseColor("#00E171"));
    		SetPayTypeColour(R.id.butpaytypesplit, Color.parseColor("#00E171"));
    		
    		// Set selected pay type colour
    		SetPayTypeColour(butID, Color.parseColor("#03AD57"));
        	
        	EditText ei = (EditText) findViewById(R.id.etServiceRate);		
        	if( ei != null )
        	{		
            	ei.setText(String.format("%.1f", mIOrder.mCurrentOrderDetails.getOrderServiceChargePerc()));		
        	}  	    		
        	
        	ei = (EditText) findViewById(R.id.etServiceAmt);		
        	if( ei != null )
        	{		
            	ei.setText(String.format("%.2f", mIOrder.mCurrentOrderDetails.getOrderServiceChargeAmt()));            	
        	}  	    
        	
        	ei = (EditText) findViewById(R.id.etDiscRate);		
        	if( ei != null )
        	{		
            	ei.setText(String.format("%.1f", mIOrder.mCurrentOrderDetails.getOrderDiscountPerc()));            	
        	}  	    		
        	
        	ei = (EditText) findViewById(R.id.etDiscAmt);		
        	if( ei != null )
        	{		
            	ei.setText(String.format("%.2f", mIOrder.mCurrentOrderDetails.getOrderDiscountAmt()));            	
        	}  	
        	SetPayAmtValue();
        	SetPayAmtDueValue();
        	SetPayChangeDueValue();
        }
	}
	
    private void SetPayTypeColour(int in_ButID, int in_Color)
    {
		Button but = (Button)findViewById(in_ButID);
    	if( but != null )
    	{	
    		but.setBackgroundColor(in_Color);        		
    	}		
	}

	void SetPayAmtValue()
    {
    	EditText et = (EditText) findViewById(R.id.etTendered);		
    	if( et != null )
    	{		
    		et.setText(String.format("%.2f", mIOrder.mCurrentOrderDetails.getOrderPayment()));	        		
    	}    
    }
	
    void SetPayAmtDueValue()
    {
    	TextView tv = (TextView) findViewById(R.id.tvPayAmtDueValue);		
    	if( tv != null )
    	{		
        	tv.setText(String.format("%.2f", mIOrder.mCurrentOrderDetails.getOrderAmountDue()));            	
    	}  	 
    }
    
    void SetPayChangeDueValue()
    {
    	TextView tv = (TextView) findViewById(R.id.tvPayChangeDueValue);		
    	if( tv != null )
    	{	
        	tv.setText(String.format("%.2f", mIOrder.mCurrentOrderDetails.getOrderChangeDue()));	
    	}  
    }
	
    void PayAmtFixedChanged()
    {
        mPresenterOrder.SetPayment();
        SetPayValues();          
    }	
    
    void PayAmtChanged()
    {
    	double TmpPayAmt = 0;
    	EditText et = (EditText) findViewById(R.id.etTendered);		
   		if( et != null )
   		{
   			String mTmpVal = et.getText().toString();
   			if( mTmpVal.compareTo("") != 0 )
   				TmpPayAmt = Double.parseDouble(mTmpVal);    			
   		} 

   		
   		mIOrder.mOrderPaymentAmount = TmpPayAmt;
   		mPresenterOrder.SetPayment();
        SetPayAmtDueValue();
        SetPayChangeDueValue();
    }
	
    void ServiceChargeRateChanged()
    {
    	double TmpServiceRate = 0;
    	EditText et = (EditText) findViewById(R.id.etServiceRate);		
    	if( et != null )
    	{		
   			String mTmpVal = et.getText().toString();
   			if( mTmpVal.compareTo("") != 0 )
   				TmpServiceRate = Double.parseDouble(mTmpVal);        	
   		}  	  

    	mIOrder.mOrderServiceRate = TmpServiceRate;
    	mPresenterOrder.SetServiceChargeRate();        
    	SetPayValues();
    }
    
    void ServiceChargeAmtChanged()
    {
    	double TmpServiceAmt = 0;
    	EditText et = (EditText) findViewById(R.id.etServiceAmt);		
    	if( et != null )
    	{		
   			String mTmpVal = et.getText().toString();
   			if( mTmpVal.compareTo("") != 0 )
   				TmpServiceAmt = Double.parseDouble(mTmpVal);        	
   		}  	  

    	mIOrder.mOrderServiceAmount = TmpServiceAmt;
    	mPresenterOrder.SetServiceChargeAmt();
        SetPayValues();
    }
    
    void DiscountRateChanged()
    {
    	double TmpDiscountRate = 0;
    	EditText et = (EditText) findViewById(R.id.etDiscRate);		
    	if( et != null )
    	{		
   			String mTmpVal = et.getText().toString();
   			if( mTmpVal.compareTo("") != 0 )
   				TmpDiscountRate = Double.parseDouble(mTmpVal);        	
   		}  	  

    	mIOrder.mOrderDiscountRate = TmpDiscountRate;
    	mPresenterOrder.SetDiscountRate();       
    	SetPayValues();
    }
    
    void DiscountAmtChanged()
    {
    	double TmpDiscAmt = 0;
    	EditText et = (EditText) findViewById(R.id.etDiscAmt);		
    	if( et != null )
    	{		
   			String mTmpVal = et.getText().toString();
   			if( mTmpVal.compareTo("") != 0 )
   				TmpDiscAmt = Double.parseDouble(mTmpVal);        	
   		}  	  

    	mIOrder.mOrderDiscountAmount = TmpDiscAmt;
    	mPresenterOrder.SetDiscountAmt();
        SetPayValues();
    }   
    
    void PayTypeChanged(String in_PayTypeDesc)
    {
    	mPresenterOrder.SetPayType(in_PayTypeDesc);
    }
    
    void PayOrder()
    {  
		new ProcessPaymentTask().execute();
    }
    
	public void DisplaySplitPay() 
	{	
		Intent splitPayIntent =  new Intent(this, SplitPayActivity.class);
		//startActivity(splitPayIntent);		
		startActivityForResult(splitPayIntent,REFRESH_PAY);
	}
    
	public void PayTypeButtonEvent(View view) 
	{	
		String PayDesc = ((Button)view).getText().toString();
		mPresenterOrder.SetPayType(PayDesc);
		SetPayValues();
		PayDesc = PayDesc.toUpperCase(Locale.ENGLISH);
		if( PayDesc.compareTo("SPLIT") == 0 )
		{
			DisplaySplitPay();
		}
	}    
    
	public void PayFullAmtButtonEvent(View view) 
	{
        mIOrder.mOrderPaymentAmount = -1;
        PayAmtFixedChanged();
	}
	
	public void PrintBillButtonEvent(View view) 
	{
		new PrintBillTask().execute();
	}
	
	public void PayBillButtonEvent(View view) 
	{
		mPrintOrder = false;
        PayOrder();
	}
	
	public void PayPrintButtonEvent(View view) 
	{
		mPrintOrder = true;
        PayOrder();
	}
	
	public void PayDoneButtonEvent(View view) 
	{
		ClosePay();	
	}
	
	public void ClosePay() 
	{
		PayActivity.this.finish();	
	}
	
	public class ProcessPaymentTask extends AsyncTask<String, String, String> 
	{

	    private ProgressDialog pDialog;
	    
	    public ProcessPaymentTask()
	    {

	    }
	    
	    @Override
	    protected void onPreExecute() 
	    {         
	    	super.onPreExecute();
	    	
	        pDialog = new ProgressDialog(PayActivity.this);
	        pDialog.setMessage("Processing Payment. Please wait...");
	        pDialog.setIndeterminate(false);
	        pDialog.setCancelable(false);
	        pDialog.show();   	        	    
	    }
	    
	    protected void onPostExecute(String file_url) 
	    {      	
	    	pDialog.dismiss();
	        if( mIOrder.mOrderRes )
	        {	   
	        	setResult(Activity.RESULT_OK);	   
	            ClosePay();	
	        }
	        else
	        {
	            String mPayErrMsg = mIOrder.mOrderErrorMsg;
	            if (mPayErrMsg.compareTo("") == 0)
	            	mPayErrMsg = "ERROR. Order could not be paid";

	    		AlertDialog alertDialog;
	    		alertDialog = new AlertDialog.Builder(PayActivity.this).create();
	    		alertDialog.setTitle("");
	    		alertDialog.setMessage(mPayErrMsg);
	    		alertDialog.show();   
	        }        	  
	    }  
	    
		@Override
		protected String doInBackground(String... arg0) 
		{
	    	mPresenterOrder.PayOrder();
        	if( mPrintOrder )
        	{
        		mPresenterReport.PrintOrder(2);
        		// 	Print
        		/*
        		mPresenterOrder.SaveOrder();
                if( mIOrder.mOrderRes )
                {
                	mPresenterReport.PrintOrder(2);
                }
                */
        	}
	        if( mIOrder.mOrderRes )
	        {	    
	        	mPresenterOrder.NewOrder();
	        }
			return null;
		}    	
	}
	
    public class PrintBillTask extends AsyncTask<String, String, String> 
    {

        ProgressDialog pDialog;     

        
        @Override
        protected void onPreExecute() 
        {         
        	super.onPreExecute();
        	
            pDialog = new ProgressDialog(PayActivity.this);
            pDialog.setMessage("Printing Order. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();   
                     
        }
        
        protected void onPostExecute(String file_url) 
        {        	

            pDialog.dismiss();  
            if( !mIOrder.mOrderRes )
            {
    			AlertDialog alertDialog;
    			alertDialog = new AlertDialog.Builder(PayActivity.this).create();
    			alertDialog.setTitle("");
    			alertDialog.setMessage(mIOrder.mOrderErrorMsg);
    			alertDialog.show();    
            }
        }  
        
    	@Override
    	protected String doInBackground(String... arg0) 
    	{
    		mPresenterOrder.SaveOrder();
            if( mIOrder.mOrderRes )
            {
            	mPresenterReport.PrintOrder(3);
                if( mIOrder.mOrderRes )
                {
                	mPresenterOrder.SaveOrder();                	
                }
            }
    		return null;
    	}    	
    }
}
