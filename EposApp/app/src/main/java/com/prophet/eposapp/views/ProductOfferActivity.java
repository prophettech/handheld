package com.prophet.eposapp.views;

import com.prophet.eposapp.EposApp;

import com.prophet.eposapp.ProductOfferAdapter;
import com.prophet.eposapp.R;
import com.prophet.eposapp.interfaces.IMenu;
import com.prophet.eposapp.interfaces.IOrder;

import com.prophet.eposapp.models.OFFER_ITEM_TYPE;
import com.prophet.eposapp.models.TillProductOfferItemGroup;
import com.prophet.eposapp.presenters.PresenterMenu;
import com.prophet.eposapp.presenters.PresenterOrder;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class ProductOfferActivity extends Activity
{
	IMenu mIMenu;
	IOrder mIOrder;
	PresenterMenu mPresenterMenu;	
	PresenterOrder mPresenterOrder;
	int mActiveCatID;
	
    static final int REFRESH_PRODOFFER = 1;
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_productoffer);
		// Show the Up button in the action bar.
		
		mPresenterMenu = ((EposApp)getApplication()).GetPresenterMenu();
		mIMenu = ((EposApp)getApplication()).GetIMenu();

		mPresenterOrder = ((EposApp)getApplication()).GetPresenterOrder();
		mIOrder = ((EposApp)getApplication()).GetIOrder();
		
		mPresenterMenu.GetProductOffer();
		mActiveCatID = mIMenu.mSelectedCatID;
		DisplayProductOffer();
		
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) 
    {
        if (requestCode == REFRESH_PRODOFFER) 
        {
            if (resultCode == RESULT_OK)
            {
            	DisplayProductOffer();
            }
        }        
    }
	
	private void DisplayProductOffer() 
	{
        if(mIMenu != null)
        {        	
        	
            runOnUiThread
            (
            	new Runnable() 
            	{
            		public void run()
            		{    
            			
            			//mPresenterMenu.GetProductOffer();
            			if( mIMenu.mSelectedPO != null )
            			{
            				TextView tv = (TextView) findViewById(R.id.tvPODesc);	
            				if( tv != null )
            					tv.setText(mIMenu.mSelectedPODesc+ ": �" +String.format("%.2f", mIMenu.mSelectedPOPrice));
            				
            				
            		    	
            		    	GridView gridview = (GridView) findViewById(R.id.pogridview);
            		    
            		    	if( gridview != null )
            		    	{          		   
            		    		gridview.setAdapter(new ProductOfferAdapter(ProductOfferActivity.this,mIMenu.mSelectedPO.GetPOItemGroupList()));               	
            		    		gridview.setOnItemClickListener(POGridOnItemClickListener);
            		    	}
            			}
                	}             
            	}
            );             
        }	        
	}
	
	OnItemClickListener POGridOnItemClickListener = new OnItemClickListener()
	{	  
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
		{	
			//ViewHolder tmpVHolder = (ViewHolder)parent.getItemAtPosition(position);
			TillProductOfferItemGroup tmpItemGroup = (TillProductOfferItemGroup)parent.getItemAtPosition(position);
			if( tmpItemGroup != null )
			{
				mIMenu.mSelectedPOItemGroupID = tmpItemGroup.GetPOItemGroupID();	
				
				mIMenu.mSelectedPOItemID = 1;			
				if( tmpItemGroup.GetPOItemGroupType() == OFFER_ITEM_TYPE.OFFERITEMTYPE_OR )
				{
					mIMenu.mSelectedPOItemID = -1;			
				}
				
				if( tmpItemGroup.GetPOItemGroupType() == OFFER_ITEM_TYPE.OFFERITEMTYPE_OR || tmpItemGroup.GetPOItemGroupType() == OFFER_ITEM_TYPE.OFFERITEMTYPE_SELECT )
				{
					DisplayPOProds();
				}
				else if( tmpItemGroup.GetPOItemGroupType() == OFFER_ITEM_TYPE.OFFERITEMTYPE_TOPPINGS )
				{
					mPresenterMenu.GetPOItemAction();
					if( mIMenu.mPOItemAction == 1 )
					{
						DisplayProdOptions();					
					}
					else if( mIMenu.mPOItemAction == 2 )
					{
						DisplayProdToppings();					
					}				
				}	
			}
		}	
	};
	
	private void DisplayPOProds() 
	{
		Intent POItemIntent =  new Intent(this, ProductOfferItemsActivity.class);
		startActivityForResult(POItemIntent,REFRESH_PRODOFFER);	
		//startActivity(POItemIntent);	
	}
	
	public void DisplayProdOptions()
	{
		mIMenu.mProdOptionsDisplayed = true;
		Intent prodOptionsIntent =  new Intent(this, ProdOptionsActivity.class);
		startActivityForResult(prodOptionsIntent,REFRESH_PRODOFFER);		
	}
	
	public void DisplayProdToppings()
	{
		Intent prodToppingsIntent =  new Intent(this, ProdToppingsActivity.class);
		startActivityForResult(prodToppingsIntent,REFRESH_PRODOFFER);
	}
		
	public void PODoneButton(View view) 
	{
		//Intent POItemIntent =  new Intent(this, ProductOfferItemsActivity.class);
		//startActivity(POItemIntent);	
		if( mPresenterMenu.AddOfferToOrder() )
		{
			setResult(RESULT_OK);
			ClosePO();
		}
		else
		{
			AlertDialog alertDialog;
			alertDialog = new AlertDialog.Builder(ProductOfferActivity.this).create();
			alertDialog.setTitle("");
			alertDialog.setMessage("Please make sure all options are selected");
			alertDialog.show();
		}
	}	
		
	public void POCancelButton(View view) 
	{
		
		ClosePO();
	}
	
	public void ClosePO() 
	{			
		mIMenu.mMenuOfferMode = false;
		mIMenu.mSelectedPO.ResetProductOfferDetails();
		mIMenu.mSelectedCatID = mActiveCatID;
		mPresenterMenu.SetActiveProducts();
		ProductOfferActivity.this.finish();	
	}
}
