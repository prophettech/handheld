package com.prophet.eposapp.views;

import java.util.ArrayList;

import com.prophet.eposapp.EposApp;
import com.prophet.eposapp.POProdsAdapter;
import com.prophet.eposapp.POProdsButtonAdapter;
import com.prophet.eposapp.R;
import com.prophet.eposapp.interfaces.IMenu;
import com.prophet.eposapp.interfaces.IOrder;
import com.prophet.eposapp.models.OFFER_ITEM_TYPE;
import com.prophet.eposapp.models.TillProductOfferButton;
import com.prophet.eposapp.models.TillProductOfferItem;
import com.prophet.eposapp.models.TillProductOfferItemGroup;
import com.prophet.eposapp.presenters.PresenterMenu;
import com.prophet.eposapp.presenters.PresenterOrder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.AdapterView.OnItemClickListener;

public class ProductOfferItemsActivity extends Activity
{
	IMenu mIMenu;
	IOrder mIOrder;
	PresenterMenu mPresenterMenu;	
	PresenterOrder mPresenterOrder;
	
    static final int OPTIONS_DONE = 1;
    static final int TOPPINGS_DONE = 2;    
    
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_productofferitem);
		setFinishOnTouchOutside(false);
		// Show the Up button in the action bar.
		
		mPresenterMenu = ((EposApp)getApplication()).GetPresenterMenu();
		mIMenu = ((EposApp)getApplication()).GetIMenu();

		mPresenterOrder = ((EposApp)getApplication()).GetPresenterOrder();
		mIOrder = ((EposApp)getApplication()).GetIOrder();
		
		DisplayPOItemList();
		
	}
	
	private void DisplayPOItemList() 
	{
        if(mIMenu != null)
        {        	
            runOnUiThread
            (
            	new Runnable() 
            	{
            		public void run()
            		{ 
            			if( mIMenu.mPOItemSelectionMode )
            			{
        					setTitle("Select Item");
        					
        					GridView gridview = (GridView) findViewById(R.id.poitemgridview);
        					if( gridview != null )
        					{        					
        						gridview.setAdapter(new POProdsAdapter(ProductOfferItemsActivity.this,mIMenu.mActiveProductList));           	
        						gridview.setOnItemClickListener(POProdOnItemClickListener);
        					}            			
            			}
            			else
            			{
	        				if( mIMenu.mSelectedPO != null && mIMenu.mSelectedPOItemGroupID > 0 )
	        				{
	        					boolean mDisplayToppings = false;
	        					boolean mDisplayProds = false;
	        					//TillProductOfferItemGroup POItemGroup = mIMenu.mSelectedPO.GetPOItemGroupList().get(mIMenu.mSelectedPOItemGroupID);
	        					TillProductOfferItemGroup POItemGroup = mIMenu.mSelectedPO.GetPOItemGroupByID(mIMenu.mSelectedPOItemGroupID);
	        					if( POItemGroup != null )
	        					{
		        					ArrayList<TillProductOfferItem> POItemList = POItemGroup.GetPOItemList();
		        					OFFER_ITEM_TYPE POGroupType = POItemGroup.GetPOItemGroupType();
		            				
		        					ArrayList<String> retItemDetails = POItemList.get(0).GetPOItemDetails(); 
		        					String TitleDesc = "";
		            				
		            				if( POGroupType == OFFER_ITEM_TYPE.OFFERITEMTYPE_OR )
		            				{
		            					TitleDesc = "Select Item";
		            					mDisplayProds = true;   
		            					mPresenterMenu.CreatePOItemProducts();	
		            					setTitle(TitleDesc);
		            					
		            					GridView gridview = (GridView) findViewById(R.id.poitemgridview);
		            					if( gridview != null )
		            					{        					
		            						gridview.setAdapter(new POProdsButtonAdapter(ProductOfferItemsActivity.this,mIMenu.mActivePOButtonList));           	
		            						gridview.setOnItemClickListener(POProdButOnItemClickListener);
		            					}
		            				}
		            				else
		            				{
			            				if( POGroupType == OFFER_ITEM_TYPE.OFFERITEMTYPE_SELECT )
			            				{
			                				mIMenu.mSelectedCatID = Integer.parseInt(retItemDetails.get(6));
			                				mPresenterMenu.SetActiveProducts();            					
			            					mDisplayProds = true;  
			            					TitleDesc = "Select " + retItemDetails.get(1);
			            				}
			            				else if( POGroupType == OFFER_ITEM_TYPE.OFFERITEMTYPE_TOPPINGS )
			            				{
			            					mDisplayToppings = true;            					
			            				}            		
			            				
			            				if( mDisplayProds )
			            				{            	
			            					setTitle(TitleDesc);
			            					
			            					GridView gridview = (GridView) findViewById(R.id.poitemgridview);
			            					if( gridview != null )
			            					{        					
			            						gridview.setAdapter(new POProdsAdapter(ProductOfferItemsActivity.this,mIMenu.mActiveProductList));           	
			            						gridview.setOnItemClickListener(POProdOnItemClickListener);
			            					}
			            				}
			            				else if( mDisplayToppings )
			            				{
			            					
			            				}
		            				}
	        					}
	        				}
            			}
                	}             
            	}
            );             
        }	        
	}
	
	OnItemClickListener POProdOnItemClickListener = new OnItemClickListener()
	{	  
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
		{			 
			//if( !mIMenu.mMenuOfferSelectionMode )
			//{
				mIMenu.mSelectedProdID = position;
				mIMenu.mSelectedPOItemID = position+1;
			//}
			ProcessPOProdItemSelection();	 		 
		}	
	};
	
	OnItemClickListener POProdButOnItemClickListener = new OnItemClickListener()
	{	  
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
		{	
			TillProductOfferButton tmpPOProdBut = (TillProductOfferButton)parent.getItemAtPosition(position);
			if( tmpPOProdBut != null )
			{
				mIMenu.mSelectedPOButID = position;
				if( tmpPOProdBut.mButProdCatID != -1 )
				{									
					mIMenu.mPOItemSelectionMode = true;
    				mIMenu.mSelectedCatID = tmpPOProdBut.mButProdCatID;
    				mPresenterMenu.SetActiveProducts();    
    				DisplayCatProds();
				}
				else
				{
					//if( !mIMenu.mMenuOfferSelectionMode )
					//{
						mIMenu.mSelectedProdID = position;
						mIMenu.mSelectedPOItemID = position+1;
					//}
					int prodAction = mPresenterMenu.POButClicked();
					if (prodAction == 1) // Add to order, no options/toppings
					{
						setResult(RESULT_OK);
						CloseProductOfferItemsActivity();               	
					}
					else if (prodAction == 2) // Options 
					{
						
					}
					//ProcessPOProdItemSelection();
				}
			}			
		}	
	};
	
    protected void ProcessPOProdItemSelection() 
    {		
		int prodAction = mPresenterMenu.ProductClicked();
		if (prodAction > -1)
		{
			if (prodAction == 1) // Add to order, no options/toppings
			{
				setResult(RESULT_OK);
				CloseProductOfferItemsActivity();               	
			}
			else if (prodAction == 2) // Options 
			{
				if( mIMenu.mSelectedProductOptions != null )
				{	
					if( mIMenu.mSelectedProductOptions != null )
					{							
						if( mIMenu.mSelectedProductOptions.GetButGroups().size() > 0 )
						{
							DisplayProdOptions();                	                        		
						}
						else if( mIMenu.mSelectedTopGroupToppings.size() > 0 || mIMenu.mSelectedProduct1.ProductToppingsList.size() > 0 )
						{
							DisplayProdToppings();                        			
						}
					}
				}
			}				 
		}  	
    }
	
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) 
    {		
        if (requestCode == OPTIONS_DONE || requestCode == TOPPINGS_DONE) 
        {
            if (resultCode == RESULT_OK)
            {
				setResult(RESULT_OK);
				CloseProductOfferItemsActivity();     
            }
        }        
    }
	
	public void DisplayCatProds()
	{
		Intent prodsIntent =  new Intent(this, ProductOfferItemsActivity.class);
		startActivityForResult(prodsIntent,OPTIONS_DONE);		
	}
	
	public void DisplayProdOptions()
	{
		mIMenu.mProdOptionsDisplayed = true;
		Intent prodOptionsIntent =  new Intent(this, ProdOptionsActivity.class);
		startActivityForResult(prodOptionsIntent,OPTIONS_DONE);		
	}
	
	public void DisplayProdToppings()
	{
		Intent prodToppingsIntent =  new Intent(this, ProdToppingsActivity.class);
		startActivityForResult(prodToppingsIntent,TOPPINGS_DONE);
	}
	
	public void POItemCancelButton(View view) 
	{
		CloseProductOfferItemsActivity();			
	}	
	
	public void CloseProductOfferItemsActivity() 
	{
		mIMenu.mPOItemSelectionMode = false;
		ProductOfferItemsActivity.this.finish();			
	}
	
}
