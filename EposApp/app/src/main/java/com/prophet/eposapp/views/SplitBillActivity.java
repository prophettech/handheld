package com.prophet.eposapp.views;

import java.util.ArrayList;

import com.prophet.eposapp.EposApp;
import com.prophet.eposapp.R;
import com.prophet.eposapp.interfaces.IOrder;
import com.prophet.eposapp.interfaces.ISplitBill;
import com.prophet.eposapp.models.TillOrderItem;
import com.prophet.eposapp.models.TillSplitItem;
import com.prophet.eposapp.presenters.PresenterOrder;
import com.prophet.eposapp.presenters.PresenterSplitBill;

import android.app.Activity;
import android.app.ActionBar.LayoutParams;
import android.graphics.Color;

import android.os.Bundle;
import androidx.core.app.NavUtils;
import android.text.Html;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class SplitBillActivity  extends Activity
{
	ISplitBill  mISplitBill;
	PresenterSplitBill mPresenterSplitBill;	
	IOrder mIOrder;	
	PresenterOrder mPresenterOrder;
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splitbill);
		// Show the Up button in the action bar.
		
		mPresenterSplitBill = ((EposApp)getApplication()).GetPresenterSplitBill();
		mISplitBill = ((EposApp)getApplication()).GetISplitBill();

		mPresenterOrder = ((EposApp)getApplication()).GetPresenterOrder();
		mIOrder = ((EposApp)getApplication()).GetIOrder();

		DisplaySplitBill();		
	}
	
	private void DisplaySplitBill() 
	{
		TextView tv = (TextView) findViewById(R.id.labSplitBillDesc);	
		if( tv != null )
		{
			String SplitCountDesc = Integer.toString(mISplitBill.mSplitID) + " of " + Integer.toString(mISplitBill.mSplitCount);
			tv.setText(mISplitBill.mSplitRef + "\n" + SplitCountDesc);			
		}
		
		DisplayUnSplitItems();
		DisplaySplitItems();		
	}
	
	private void DisplayUnSplitItems() 
	{
        if(mIOrder != null)
        {       	
   		
        		runOnUiThread
        		(
        			new Runnable() 
        			{
        				public void run()
        				{           					
        					CreateUnSplitItemsTable();
        				}             
        			}
        		);  
        }	        
	}
	
	private void CreateUnSplitItemsTable() 
	{
		TableLayout tl = (TableLayout) findViewById(R.id.tlUnSplitItems);		  
		tl.removeAllViews();
		//int mIDCounter = 1;
		
		TableRow tr;		
		TextView txtQty;	
		TextView txtDesc;
		TextView txtPrice;	
		TextView txtTotal;;	
		ArrayList<String> retTextFormat;
	 
		EposApp mEposApp = ((EposApp)getApplication());
		
		if( mIOrder.mCurrentOrderItems.size() > 0 )
		{			
			LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);	  
			TableRow.LayoutParams lp; 
			int mRowHeight = 50;
			String ItemDesc = "";
			double ItemQty = 0;
			double ItemPrice = 0;
			double ItemTotal = 0;
			
			int LineCount = 0;
			String LineDesc = "";
			for(TillOrderItem mOrderItem : mIOrder.mCurrentOrderItems )
			{		
				if( mOrderItem.GetItemSplitQty() > 0 )
				{
					ItemDesc = mOrderItem.GetItemDesc();
					ItemQty = mOrderItem.GetItemSplitQty();
					ItemPrice = mOrderItem.GetItemPrice();
					ItemTotal = ItemQty*ItemPrice;
									
					retTextFormat = mEposApp.FormatItemText(ItemDesc,0,43, " ");
					LineDesc = retTextFormat.get(0);
					LineCount = Integer.parseInt(retTextFormat.get(1));			
					
					mRowHeight = LineCount*20;
					if( mRowHeight < 50 )
						mRowHeight = 50;
					
					tr = new TableRow(this);
					tr.setBackgroundColor(Color.rgb(227, 228, 229));
					tr.setOnClickListener(unsplitItemOnClickListener);
					tr.setId(mOrderItem.GetItemID());
					
					txtQty=new TextView(this);	
					lp = new TableRow.LayoutParams(0, mRowHeight, 0.07f);
					lp.setMargins(1, 0, 0, 1);	
					txtQty.setLayoutParams(lp);
					txtQty.setTextSize(16f);
					txtQty.setText(String.format("%.0f", ItemQty));	
					//txtQty.setText(Integer.toString(LineCount));					
					txtQty.setBackgroundColor(Color.WHITE);
					txtQty.setTextColor(Color.BLACK);
					txtQty.setPadding(4, 0, 0, 0);
					tr.addView(txtQty);			
											
					
					txtDesc=new TextView(this);
					lp = new TableRow.LayoutParams(0, mRowHeight, 0.7f);
					lp.setMargins(1, 0, 0, 1);	
					txtDesc.setLayoutParams(lp);
					txtDesc.setTextSize(16f);
					txtDesc.setText(Html.fromHtml(LineDesc));
					txtDesc.setBackgroundColor(Color.WHITE);
					txtDesc.setTextColor(Color.BLACK);				
					txtDesc.setPadding(4, 0, 0, 0);				
					tr.addView(txtDesc);	
	
					txtPrice=new TextView(this);	
					lp = new TableRow.LayoutParams(0, mRowHeight, 0.11f);
					lp.setMargins(1, 0, 0, 1);				
					txtPrice.setLayoutParams(lp);
					txtPrice.setTextSize(16f);
					txtPrice.setText(String.format("%.2f", ItemPrice));	
					txtPrice.setBackgroundColor(Color.WHITE);
					txtPrice.setTextColor(Color.BLACK);
					txtPrice.setGravity(Gravity.RIGHT);
					txtPrice.setPadding(0, 0, 4, 0);
					
					tr.addView(txtPrice);
					  
					txtTotal=new TextView(this);	
					lp = new TableRow.LayoutParams(0, mRowHeight, 0.12f);
					lp.setMargins(1, 0, 0, 1);				
					txtTotal.setLayoutParams(lp);
					txtTotal.setTextSize(16f);
					txtTotal.setText(String.format("%.2f", ItemTotal));	
					txtTotal.setBackgroundColor(Color.WHITE);
					txtTotal.setTextColor(Color.BLACK);
					txtTotal.setGravity(Gravity.RIGHT);
					txtTotal.setPadding(0, 0, 4, 0);
					tr.addView(txtTotal);		
					
					tl.addView(tr,new TableLayout.LayoutParams(layoutParams));	
				}
			}	

			/*
			((ScrollView) findViewById(R.id.svUnSplitItems)).post(new Runnable() 
			{
                public void run() {
                    ((ScrollView) findViewById(R.id.svUnSplitItems)).fullScroll(View.FOCUS_DOWN);
                }

            });
            */

		}
	}
	
	OnClickListener unsplitItemOnClickListener = new OnClickListener()
	{	  
		public void onClick(View v) 
		{			
			mISplitBill.mSplitItemID = v.getId();
			AddSplitItem();
		}		
	};

	private void DisplaySplitItems() 
	{
        if(mIOrder != null)
        {      	   		
        		runOnUiThread
        		(
        			new Runnable() 
        			{
        				public void run()
        				{            					
        					CreateSplitItemsTable();
        				}
        			}
        		);  
        }	        
	}
	
	private void CreateSplitItemsTable() 
	{
		
		TableLayout tl = (TableLayout) findViewById(R.id.tlSplitItems);		  
		tl.removeAllViews();
		//int mIDCounter = 1;
		
		TableRow tr;		
		TextView txtQty;	
		TextView txtDesc;
		TextView txtPrice;	
		TextView txtTotal;;	
		ArrayList<String> retTextFormat;
	 
		EposApp mEposApp = ((EposApp)getApplication());
		
		if( mIOrder.mCurrentOrderItems.size() > 0 )
		{			
			LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);	  
			TableRow.LayoutParams lp; 
			int mRowHeight = 50;
			String ItemDesc = "";
			double ItemQty = 0;
			double ItemPrice = 0;
			double ItemTotal = 0;
			TillOrderItem mOrderItem;
			
			for(TillSplitItem mSplitItem: mISplitBill.mSplitItemsList  )
			{		
				mIOrder.mCurrentOrderItemID = mSplitItem.mItemID;
				mPresenterOrder.GetOrderItemDetails();
				mOrderItem = mIOrder.mSelectedOrderItemDetails;
				if( mOrderItem != null )
				{
				
					ItemDesc = mOrderItem.GetItemDesc();
					ItemQty = mSplitItem.mItemQty;
					ItemPrice = mOrderItem.GetItemPrice();
					ItemTotal = mOrderItem.GetItemTotal();
					
					retTextFormat = mEposApp.FormatItemText(ItemDesc,0,43, " ");
					ItemDesc = retTextFormat.get(0);
					int LineCount = Integer.parseInt(retTextFormat.get(1));
	
					mRowHeight = LineCount*20;
					if( mRowHeight < 50 )
						mRowHeight = 50;
					
					tr = new TableRow(this);
					tr.setBackgroundColor(Color.rgb(227, 228, 229));
					tr.setOnClickListener(splitItemOnClickListener);
					tr.setId(mOrderItem.GetItemID());
					
					txtQty=new TextView(this);	
					lp = new TableRow.LayoutParams(0, mRowHeight, 0.07f);
					lp.setMargins(1, 0, 0, 1);	
					txtQty.setLayoutParams(lp);
					txtQty.setTextSize(16f);
					txtQty.setText(String.format("%.0f", ItemQty));	
					//txtQty.setText(Integer.toString(LineCount));					
					txtQty.setBackgroundColor(Color.WHITE);
					txtQty.setTextColor(Color.BLACK);
					txtQty.setPadding(4, 0, 0, 0);
					tr.addView(txtQty);			
											
					
					txtDesc=new TextView(this);
					lp = new TableRow.LayoutParams(0, mRowHeight, 0.7f);
					lp.setMargins(1, 0, 0, 1);	
					txtDesc.setLayoutParams(lp);
					txtDesc.setTextSize(16f);
					txtDesc.setText(Html.fromHtml(ItemDesc));
					txtDesc.setBackgroundColor(Color.WHITE);
					txtDesc.setTextColor(Color.BLACK);				
					txtDesc.setPadding(4, 0, 0, 0);				
					tr.addView(txtDesc);	
	
					txtPrice=new TextView(this);	
					lp = new TableRow.LayoutParams(0, mRowHeight, 0.11f);
					lp.setMargins(1, 0, 0, 1);				
					txtPrice.setLayoutParams(lp);
					txtPrice.setTextSize(16f);
					txtPrice.setText(String.format("%.2f", ItemPrice));	
					txtPrice.setBackgroundColor(Color.WHITE);
					txtPrice.setTextColor(Color.BLACK);
					txtPrice.setGravity(Gravity.RIGHT);
					txtPrice.setPadding(0, 0, 4, 0);
					
					tr.addView(txtPrice);
					  
					txtTotal=new TextView(this);	
					lp = new TableRow.LayoutParams(0, mRowHeight, 0.12f);
					lp.setMargins(1, 0, 0, 1);				
					txtTotal.setLayoutParams(lp);
					txtTotal.setTextSize(16f);
					txtTotal.setText(String.format("%.2f", ItemTotal));	
					txtTotal.setBackgroundColor(Color.WHITE);
					txtTotal.setTextColor(Color.BLACK);
					txtTotal.setGravity(Gravity.RIGHT);
					txtTotal.setPadding(0, 0, 4, 0);
					tr.addView(txtTotal);		
					
					tl.addView(tr,new TableLayout.LayoutParams(layoutParams));	
				}
			}	
	
			/*
			((ScrollView) findViewById(R.id.svUnSplitItems)).post(new Runnable() 
			{
	            public void run() {
	                ((ScrollView) findViewById(R.id.svUnSplitItems)).fullScroll(View.FOCUS_DOWN);
	            }
	
	        });
	        */
		}

	}   
	
	OnClickListener splitItemOnClickListener = new OnClickListener()
	{	  
		public void onClick(View v) 
		{			
			mISplitBill.mSplitItemID = v.getId();
			RemoveSplitItem();
		}		
	};
         

	private void AddSplitItem() 
	{
		mPresenterSplitBill.AddSplitItem();	
		DisplaySplitBill();		
	}
	
	private void RemoveSplitItem() 
	{
		mPresenterSplitBill.RemoveSplitItem();
		DisplaySplitBill();			
	}
		
	public void NextSplitButton(View view) 
	{
		mPresenterSplitBill.NextSplit();
		DisplaySplitBill();
	}	
	
	public void PrevSplitButton(View view) 
	{
		mPresenterSplitBill.PrevSplit();
		DisplaySplitBill();
	}
	
	public void NoSplitBillButton(View view) 
	{
		SplitBillActivity.this.finish();	
	}

	public void SplitBillDoneButton(View view) 
	{
		SplitBillActivity.this.finish();	
	}


}
