package com.prophet.eposapp;

import java.util.ArrayList;

import com.prophet.eposapp.models.TillCategory;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

public class CatAdapter extends BaseAdapter
{
    private Context mContext;
    //ArrayList<HashMap<String, String>> mCategoryList;
    ArrayList<TillCategory> mCategoryList; 

    //CatAdapter(Context in_Context, ArrayList<HashMap<String, String>> in_categoryList) 
    public CatAdapter(Context in_Context, ArrayList<TillCategory> in_categoryList) 
    {
        mContext = in_Context; 
        mCategoryList = in_categoryList;
    }
    
    public int getCount() 
    {
        //return mThumbIds.length;
        return mCategoryList.size();    	
    }

    public Object getItem(int position) 
    {
        return mCategoryList.get(position);
    }

    public long getItemId(int position) 
    {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) 
    {
    	TextView tv = null;
    	
    	//try
    	//{
        
        if (convertView == null) 
        {
            tv = new TextView(mContext);
            tv.setLayoutParams(new GridView.LayoutParams(116, 60));
            tv.setTextSize(16f);
            tv.setTypeface(null, Typeface.BOLD);
        }
        else 
        {
            tv = (TextView) convertView;
        }        

        TillCategory catDetails = mCategoryList.get(position);          
        tv.setText(catDetails.GetCatName());    
        tv.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        tv.setBackgroundColor(catDetails.GetCatColour());
        tv.setTextColor(catDetails.GetCatFontColour());

        
    	return tv;
    }
    

}
