package com.prophet.eposapp;

import java.util.ArrayList;

import com.prophet.eposapp.models.TillProduct;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

public class POProdsAdapter  extends BaseAdapter
{
    private Context mContext;
    ArrayList<TillProduct> mProductList; 

    //CatAdapter(Context in_Context, ArrayList<HashMap<String, String>> in_categoryList) 
    public POProdsAdapter(Context in_Context, ArrayList<TillProduct> in_productList) 
    {
        mContext = in_Context;
        mProductList = in_productList;
    }
    
    public int getCount() 
    {
        //return mThumbIds.length;
        return mProductList.size();    	
    }

    public Object getItem(int position) 
    {
        return mProductList.get(position);
    }

    public long getItemId(int position) 
    {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) 
    {
    	TextView tv = null;
 
    		if (convertView == null) 
    		{
    			tv = new TextView(mContext);
    			tv.setLayoutParams(new GridView.LayoutParams(116, 60));
                tv.setTextSize(14f);
                tv.setTypeface(null, Typeface.BOLD);
    		}
    		else 
    		{
    			tv = (TextView) convertView;
    		}        

    		TillProduct prodDetails = mProductList.get(position);            
    		tv.setText(prodDetails.ProductDesc);  
    		tv.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
    		tv.setBackgroundColor(Color.WHITE);
    		tv.setTextColor(Color.BLACK);

    	return tv;
    }
}
