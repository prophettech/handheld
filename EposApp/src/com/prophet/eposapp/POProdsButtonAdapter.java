package com.prophet.eposapp;

import java.util.ArrayList;

import com.prophet.eposapp.models.TillProductOfferButton;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

public class POProdsButtonAdapter  extends BaseAdapter
{
    private Context mContext;
    ArrayList<TillProductOfferButton> mProdButList; 

    //CatAdapter(Context in_Context, ArrayList<HashMap<String, String>> in_categoryList) 
    public POProdsButtonAdapter(Context in_Context, ArrayList<TillProductOfferButton> in_ProdButList) 
    {
        mContext = in_Context;
        mProdButList = in_ProdButList;
    }
    
    public int getCount() 
    {
        //return mThumbIds.length;
        return mProdButList.size();    	
    }

    public Object getItem(int position) 
    {
        return mProdButList.get(position);
    }

    public long getItemId(int position) 
    {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) 
    {
    	TextView tv = null;
 
    		if (convertView == null) 
    		{
    			tv = new TextView(mContext);
    			tv.setLayoutParams(new GridView.LayoutParams(116, 60));
                tv.setTextSize(14f);
                tv.setTypeface(null, Typeface.BOLD);
    		}
    		else 
    		{
    			tv = (TextView) convertView;
    		}        

    		TillProductOfferButton prodButDetails = mProdButList.get(position);            
    		tv.setText(prodButDetails.mButDesc);  
    		tv.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
    		tv.setBackgroundColor(Color.WHITE);
    		tv.setTextColor(Color.BLACK);

    	return tv;
    }
}
