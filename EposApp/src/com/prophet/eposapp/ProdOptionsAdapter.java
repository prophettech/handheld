package com.prophet.eposapp;

import java.util.ArrayList;

import com.prophet.eposapp.models.TillProductOptionsButGroupButton;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

public class ProdOptionsAdapter  extends BaseAdapter 
{
	   
		private Context mContext;
	    //ArrayList<String> mProdOptionsList;
	    //ArrayList<TillTables> mTableList; 
		ArrayList<TillProductOptionsButGroupButton> mProdOptionButtonsList;

	    //CatAdapter(Context in_Context, ArrayList<HashMap<String, String>> in_categoryList) 
	    public ProdOptionsAdapter(Context in_Context, ArrayList<TillProductOptionsButGroupButton> in_ProdOptionButtonsList) 
	    {
	        mContext = in_Context;
	        mProdOptionButtonsList = in_ProdOptionButtonsList;
	    }
	    
	    public int getCount() 
	    {
	        //return mThumbIds.length;
	        return mProdOptionButtonsList.size();    	
	    }

	    public Object getItem(int position) 
	    {
	        return mProdOptionButtonsList.get(position);
	    }

	    public long getItemId(int position) 
	    {
	        return position;
	    }

	    public View getView(int position, View convertView, ViewGroup parent) 
	    {
	    	TextView tv = null;

	    		if (convertView == null) 
	    		{
	    			tv = new TextView(mContext);
	    			tv.setLayoutParams(new GridView.LayoutParams(150, 60));
	    			tv.setTextSize(16f);
	    			tv.setTypeface(null, Typeface.BOLD);
	    		}
	    		else 
	    		{
	    			tv = (TextView) convertView;
	    		}  
	    		TillProductOptionsButGroupButton tmpButton = mProdOptionButtonsList.get(position);
	    		//String tmpDesc = mProdOptionsList.get(position);
	    		String tmpDesc = tmpButton.GetButtonDesc();
	    		
	    		int optionFontColour = Color.BLACK;	    		
	    		int optionBgColour = Color.WHITE;
	    		if( tmpButton.GetButSelected() == 1 )
	    		{
	    			optionBgColour = Color.rgb(255, 251, 77);	    			
	    		}
	    		
	    		tv.setText(tmpDesc);   
	    		tv.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
	    		tv.setBackgroundColor(optionBgColour);
	    		tv.setTextColor(optionFontColour);
	    		

	    		/*
	    		TillTables tableDetails = mTableList.get(position);  
	    		String tableDesc = tableDetails.getmTableDesc();
	    		int tableBgColour = Color.WHITE;
	    		int tableFontColour = Color.BLACK;

	    		if( tableDetails.getTableBookedStatus())
	    		{
	    			tableDesc += "\n"+ tableDetails.getTableOrderAccRef();
		    		tableBgColour = Color.BLACK;
		    		tableFontColour = Color.WHITE;
	    			
	    		}
	    		tv.setText(tableDesc);   	    		
	    		tv.setBackgroundColor(tableBgColour);
	    		tv.setTextColor(tableFontColour);
	    		*/
	        
	    	return tv;
	    }
}
