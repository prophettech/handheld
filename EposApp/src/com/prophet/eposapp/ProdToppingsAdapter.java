package com.prophet.eposapp;

import java.util.ArrayList;

import com.prophet.eposapp.models.TOPPING_STATE_CODE;
import com.prophet.eposapp.models.TillToppings;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

public class ProdToppingsAdapter extends BaseAdapter 
{
	   
		private Context mContext;
	    //ArrayList<String> mProdOptionsList;
	    //ArrayList<TillTables> mTableList; 
		ArrayList<TillToppings> mProdToppingsList;

	    //CatAdapter(Context in_Context, ArrayList<HashMap<String, String>> in_categoryList) 
	    public ProdToppingsAdapter(Context in_Context, ArrayList<TillToppings> in_ProdToppingsList) 
	    {
	        mContext = in_Context;
	        mProdToppingsList = in_ProdToppingsList;
	    }
	    
	    public int getCount() 
	    {
	        //return mThumbIds.length;
	        return mProdToppingsList.size();    	
	    }

	    public Object getItem(int position) 
	    {
	        return mProdToppingsList.get(position);
	    }

	    public long getItemId(int position) 
	    {
	        return position;
	    }

	    public View getView(int position, View convertView, ViewGroup parent) 
	    {
	    	TextView tv = null;

	    		if (convertView == null) 
	    		{
	    			tv = new TextView(mContext);
	    			tv.setLayoutParams(new GridView.LayoutParams(150, 60));
	    			tv.setTextSize(16f);
	    			tv.setTypeface(null, Typeface.BOLD);
	    		}
	    		else 
	    		{
	    			tv = (TextView) convertView;
	    		}  
	    		int optionFontColour = Color.BLACK;	    		
	    		int optionBgColour = Color.WHITE;
	    		
	    		TillToppings tmpTopping = mProdToppingsList.get(position);
	    		//String tmpDesc = mProdOptionsList.get(position);
	    		String tmpDesc = tmpTopping.ToppingDesc;
	    		if( tmpTopping.ToppingState == TOPPING_STATE_CODE.TOP_ADDED)
	    		{
	    			optionFontColour = Color.rgb(0, 100, 0);
	    			//tmpDesc = "+"+tmpDesc;	   
	    		}
	    		else if( tmpTopping.ToppingState == TOPPING_STATE_CODE.TOP_FIXED_REMOVED)
	    		{
	    			optionFontColour = Color.RED;
	    			//tmpDesc = "-"+tmpDesc;		    			
	    		}  		

	    		
	    		tv.setText(tmpDesc);   
	    		tv.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
	    		tv.setBackgroundColor(optionBgColour);
	    		tv.setTextColor(optionFontColour);

	        
	    	return tv;
	    }
}
