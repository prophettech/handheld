package com.prophet.eposapp;

import java.util.ArrayList;
import com.prophet.eposapp.models.TillSplitPayDetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SplitPayAdapter extends BaseAdapter 
{
	private LayoutInflater mLayoutInflater;
	private Context mContext;
	ArrayList<TillSplitPayDetails> mSplitPayList;

    //CatAdapter(Context in_Context, ArrayList<HashMap<String, String>> in_categoryList) 
    public SplitPayAdapter(Context in_Context, ArrayList<TillSplitPayDetails> in_SplitPayList) 
    {
        mContext = in_Context;
        mSplitPayList = in_SplitPayList;
        
        mLayoutInflater=LayoutInflater.from(mContext);
    }
    
    public int getCount() 
    {
        return mSplitPayList.size();    	
    }

    public Object getItem(int position) 
    {
    	return mSplitPayList.get(position);
    }

    public long getItemId(int position) 
    {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) 
    {
    	SplitPayViewHolder mVHolder;

        if(convertView == null)
        {
            convertView=mLayoutInflater.inflate(R.layout.splitpaygrid, parent, false);
            mVHolder=new SplitPayViewHolder();
            mVHolder.mDesc1View=(TextView)convertView.findViewById(R.id.text1);
            mVHolder.mDesc2View=(TextView)convertView.findViewById(R.id.text2);
            convertView.setTag(mVHolder);
        }
        else
        {
            mVHolder=(SplitPayViewHolder)convertView.getTag();
        }
        
        TillSplitPayDetails TmpSplitPayDetails = mSplitPayList.get(position);        
        mVHolder.mDesc1View.setText(TmpSplitPayDetails.getSpliyPayDesc());
        mVHolder.mDesc2View.setText(String.format("%.2f", TmpSplitPayDetails.getSpliyPayAmount()));
        
        
    	return convertView;	    	
    }
    
}
