package com.prophet.eposapp.db;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.prophet.eposapp.JSONBase;
import com.prophet.eposapp.models.TillOptions;

public class DALOptions  extends JSONBase
{
	TillOptions mOptions;
    
	private String URL_ALL_OPTIONS = "";
    private static final String TAG_OPTIONS = "options";
    
    public DALOptions()
	{
    	mOptions = new 	TillOptions();    	
	}
    
	public void SetDBPath(String in_ServerIP)
	{
    	SetServerIP(in_ServerIP);
    	URL_ALL_OPTIONS = DB_SERVER_PATH + "options.php";
	}
    
    public String GetErrorMsg()
    {
        return mErrorMsg;
    }
    
    public void LoadOptions()
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
            
		try
		{
			// getting JSON string from URL            
			JSONObject json = mjParser.makeHttpRequest(URL_ALL_OPTIONS, "GET", params);
             
			if( json != null )
			{           
				// Checking for SUCCESS TAG                
				int success = json.getInt(TAG_SUCCESS);
 
				if (success == 1)
				{                    
					
				    JSONArray tablesData = json.getJSONArray(TAG_OPTIONS);             
					for (int i = 0; i < tablesData.length(); i++) 
					{                        
						JSONObject c = tablesData.getJSONObject(i); 
                        
						// 	Storing each json item in variable               		
						String tmpStr = c.getString("AccumulatedQty");    
						if( tmpStr.compareTo("1") == 0 )
						{
							mOptions.SetAccumulatedQty(true);							
						}
						
						tmpStr = c.getString("NETDiscount"); 
						if( tmpStr.compareTo("1") == 0 )
						{
							mOptions.SetNETDiscount(true);					
						}
						
						tmpStr = c.getString("PrintNewItemsOnly"); 
						if( tmpStr.compareTo("1") == 0 )
						{
							mOptions.SetPrintNewItemsOnly(true);					
						}						
						
					}                
				}                 
				else                             
				{
                
				}     
			}
		}
		catch( JSONException e )
		{
			mErrorMsg = e.getMessage();               
			e.printStackTrace();  			
		}			    	    	
	}
    
    public TillOptions GetTillOptions()
    {
    	return mOptions;
    }
}
