package com.prophet.eposapp.db;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.prophet.eposapp.JSONBase;
import com.prophet.eposapp.models.TillTableOrder;
import com.prophet.eposapp.models.TillTables;

public class DALTables extends JSONBase
{
    ArrayList<TillTables> mTablesList;
    ArrayList<TillTableOrder> mTableOrdersList;
    
    private String URL_ALL_TABLES = "";
    private static final String TAG_TABLES = "tables";
    private static final String TAG_TABLEORDERS = "tableOrders";    
    
    
	public DALTables() 
	{   	
		mTablesList = new ArrayList<TillTables>();		
		mTableOrdersList = new ArrayList<TillTableOrder>();	
	}
	
	public void SetDBPath(String in_ServerIP)
	{
    	SetServerIP(in_ServerIP);
    	URL_ALL_TABLES = DB_SERVER_PATH + "tables.php";
	}
	
	public void LoadTables() 
	{		
		mTablesList.clear();
		
		
		/*
    	for( int i=0;i<40;i++)
    	{
    		TillTables newTable = new TillTables(); 
    		mTablesList.add(newTable);
    	}
    	*/
		// Building Parameters            
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("DALAction","1" ));
            
		try
		{
			// getting JSON string from URL            
			JSONObject json = mjParser.makeHttpRequest(URL_ALL_TABLES, "GET", params);
             
			if( json != null )
			{
           
				// Checking for SUCCESS TAG                
				int success = json.getInt(TAG_SUCCESS);
 
				if (success == 1)
				{                    
  
				    JSONArray tablesData = json.getJSONArray(TAG_TABLES);
				    TillTables newTable;                 
					for (int i = 0; i < tablesData.length(); i++) 
					{                        
						JSONObject c = tablesData.getJSONObject(i); 
                        
						// 	Storing each json item in variable               		
						int tID = c.getInt(TAG_BUTID);    
						int tNo = c.getInt(TAG_BUTTEXT1); 
						String tDesc = c.getString(TAG_BUTTEXT2);    
						//int BakColour = c.getInt(TAG_BUTCOLOUR);
						//int FontColour = c.getInt(TAG_BUTFONTCOLOUR);
						//String BakColour = "";
						//	String FontColour = "";
						newTable = new TillTables();
						newTable.SetTableID(tID);
						newTable.SetTableNo(tNo);
						if( tDesc.compareTo("") == 0 )
						{
							tDesc = "Table " + Integer.toString(i+1);							
						}
						newTable.SetTableDesc(tDesc);
						//newTable.setTableDetails(id, name, BakColour,FontColour);
						mTablesList.add(newTable);            		                 
					}                
				}                 
				else                             
				{
                
				}     
			}
		}
		catch( JSONException e )
		{
			mErrorMsg = e.getMessage();               
			e.printStackTrace();  			
		}			    	
	}
	
	public void LoadTableOrders() 
	{
		
		mTableOrdersList.clear();

		// Building Parameters            
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("DALAction","2" ));
            
		try
		{
			// getting JSON string from URL            
			JSONObject json = mjParser.makeHttpRequest(URL_ALL_TABLES, "GET", params);
             
			if( json != null )
			{
           
				// Checking for SUCCESS TAG                
				int success = json.getInt(TAG_SUCCESS);
 
				if (success == 1)
				{                    
					// Getting Array of Categories    
				    JSONArray tableOrdersData = json.getJSONArray(TAG_TABLEORDERS);
                     
				    
				    TillTableOrder newTableOrder; 
					// 	looping through All Categories                    
					for (int i = 0; i < tableOrdersData.length(); i++) 
					{                        
						JSONObject c = tableOrdersData.getJSONObject(i); 

						// 	Storing each json item in variable               		
						int toOrdNo = c.getInt(TAG_BUTID);    
						String toOrdAccRef = c.getString(TAG_BUTTEXT1);    
						String toOrdContact = c.getString(TAG_BUTTEXT2);   
						int toTableID = c.getInt(TAG_BUTTEXT3); 
						int toOrdID = c.getInt(TAG_BUTTEXT4); 
						int toOrdState = c.getInt(TAG_BUTTEXT5); 
						
						//int BakColour = c.getInt(TAG_BUTCOLOUR);
						//int FontColour = c.getInt(TAG_BUTFONTCOLOUR);
						//String BakColour = "";
						//	String FontColour = "";
						newTableOrder = new TillTableOrder();
						newTableOrder.SetOrderID(toOrdID);
						newTableOrder.SetOrderNo(toOrdNo);
						newTableOrder.SetTableID(toTableID);
						newTableOrder.SetOrderAccRef(toOrdAccRef);
						newTableOrder.SetOrderContact(toOrdContact);
						newTableOrder.SetOrderState(toOrdState);
						
						mTableOrdersList.add(newTableOrder);            		                 
					}    
					            
				}                 
				else                             
				{
                
				}     
			}
		}
		catch( JSONException e )
		{
			mErrorMsg = e.getMessage();               
			e.printStackTrace();  			
		}			    	
	}
	
	public ArrayList<TillTables> GetTablesList()	
	{	
		return mTablesList;		
	}
	
	public ArrayList<TillTableOrder> GetTableOrdersList()
	{		
		return mTableOrdersList;		
	}	
}
