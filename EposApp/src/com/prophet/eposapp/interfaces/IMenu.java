package com.prophet.eposapp.interfaces;

import java.util.ArrayList;

import java.io.Serializable;

import com.prophet.eposapp.models.TillCategory;
import com.prophet.eposapp.models.TillProduct;
import com.prophet.eposapp.models.TillProductOffer;
import com.prophet.eposapp.models.TillProductOfferButton;
import com.prophet.eposapp.models.TillProductOptions;
import com.prophet.eposapp.models.TillToppings;


public class IMenu implements Serializable 
{
	private static final long serialVersionUID = 1L;
	public boolean mMenuHappyHourMode;
	
	public ArrayList<TillCategory> mCategoryList;
	public ArrayList<TillProduct> mFullProductList;	
	public ArrayList<TillProduct> mActiveProductList;	
	public ArrayList<TillToppings> mSelectedTopGroupToppings;	
	public ArrayList<TillProductOfferButton> mActivePOButtonList;	
	public int mSelectedCatID;
	public int mSelectedProdID;
	public boolean mMenuOfferMode;
	public TillProduct mSelectedProduct1;
    public TillProductOptions mSelectedProductOptions;
    public int mSelectedOptionButGroupID;
    public int mSelectedOptionButID;
    public boolean mProdOptionsDisplayed;
    
	
	public TillProductOffer mSelectedPO;
	public String mSelectedPOCode;
	public String mSelectedPODesc;	
	public double mSelectedPOPrice;		
	public int mSelectedPOItemGroupID;
	public int mSelectedPOItemID;
	public int mSelectedPOButID;
	public int mPOItemAction;	// 1: Display Options, 2: Display Toppings, 3: Display Prods
	public boolean mPOItemSelectionMode;

	public IMenu ()
	{
		mSelectedCatID = 0;
		mSelectedProdID = 0;
		mSelectedOptionButGroupID = 0;
		mSelectedOptionButID = 0;
		mSelectedPOCode = "";
		mSelectedPODesc = "";
		mSelectedPOPrice = 0;
		mSelectedPOItemGroupID = 0;
		mSelectedPOItemID = 0;
		mSelectedPOButID = 0;
		mPOItemAction = 0;
		mProdOptionsDisplayed = false;
		mMenuOfferMode = false;
		mPOItemSelectionMode = false;
		
	}
	
}
