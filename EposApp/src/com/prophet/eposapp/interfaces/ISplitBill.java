package com.prophet.eposapp.interfaces;

import java.util.ArrayList;

import com.prophet.eposapp.models.TillSplitItem;

public class ISplitBill 
{
	public ArrayList<TillSplitItem> mSplitItemsList;
    public int mSplitID;
    public String mSplitRef;
    public int mSplitCount;
    
    public int mSplitItemID;
    
    public ISplitBill()
    {
    	mSplitItemsList = new ArrayList<TillSplitItem>();
    	mSplitID = 0;
    	mSplitRef = "";
    	mSplitCount = 0;
    	mSplitItemID = 0;    	
    }
}
