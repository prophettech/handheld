package com.prophet.eposapp.models;

import java.util.ArrayList;
import java.util.Locale;

import com.prophet.eposapp.db.DALHelper;

public class ModelHelper 
{
	DALHelper mDALHelper;
	
    public ModelHelper()
    {
        mDALHelper = new DALHelper();

    }
    
	public void SetDBPath(String in_ServerIP)
	{
		mDALHelper.SetDBPath(in_ServerIP);		
	}
	
    public void LoadHelperData()
    {
    	mDALHelper.LoadHelperData();    	
    }
    
    public int LookupPayType(String in_PayTypeDesc)
    {
    	ArrayList<TillPayType> mTmpPayList = mDALHelper.GetPayTypesList();

		for(TillPayType mPayType : mTmpPayList )
		{		
			if(mPayType.getPayTypeDesc().toUpperCase(Locale.ENGLISH).compareTo(in_PayTypeDesc.toUpperCase(Locale.ENGLISH)) == 0 )
			{
				return mPayType.getPayTypeCode();
			}				
		}
        return -1;
    }
    
    public String LookupPayTypeDesc(int in_PayTypeCode)
    {
    	ArrayList<TillPayType> mTmpPayList = mDALHelper.GetPayTypesList();

		for(TillPayType mPayType : mTmpPayList )
		{		
			if(mPayType.getPayTypeCode() == in_PayTypeCode )
			{
				return mPayType.getPayTypeDesc();
			}				
		}
        return "";
    }    
    
    
    public double LookupVATCode(int in_VATCode)
    {
    	ArrayList<TillVATCode> mTmpVATCodesList = mDALHelper.GetVATCodesList();

		for(TillVATCode mVATCode : mTmpVATCodesList )
		{		
			if(mVATCode.geVATCode() == in_VATCode )
			{
				return mVATCode.getVATRate();
			}				
		}
        return 0;
    }
    
    public ArrayList<Double> CalcVAT(double in_Amount, int in_VATCode, int in_VATCalcType)
    {
    	ArrayList<Double> retVal = new ArrayList<Double>();
        double TmpVATRate=0;
        double TmpVATAmt=0;
        TmpVATRate = LookupVATCode(in_VATCode);
        if( TmpVATRate > 0 )
        {
            if( in_VATCalcType == 0 )
            {
                TmpVATAmt = TillFunctions.round2((in_Amount / 100 * TmpVATRate));
            }
            else if( in_VATCalcType == 1 )
            {                    
                double VATAmount = TillFunctions.round2(in_Amount/(1+(TmpVATRate/100)));
                TmpVATAmt = TillFunctions.round2(in_Amount-VATAmount);
            }
        }
        retVal.add(TmpVATRate);
        retVal.add(TmpVATAmt);

        return retVal;
    }
}
