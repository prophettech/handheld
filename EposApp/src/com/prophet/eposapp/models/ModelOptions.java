package com.prophet.eposapp.models;

import android.content.SharedPreferences;

import com.prophet.eposapp.db.DALOptions;

public class ModelOptions 
{
	DALOptions mDALOptions;
    SharedPreferences mAppConfig; 
	
    public ModelOptions(SharedPreferences in_AppConfig)
    {
    	mDALOptions = new DALOptions();
    	mAppConfig = in_AppConfig;   	    	
    }
    
    public void LoadOptions()
    {
    	mDALOptions.LoadOptions();
    }
    
    public TillOptions GetOptions()
    {
    	return mDALOptions.GetTillOptions();
    }
    
    public String GetErrorMsg()
    {
        return mDALOptions.GetErrorMsg();
    }
    
	public void SetDBPath(String in_ServerIP)
	{
		mDALOptions.SetDBPath(in_ServerIP);		
	}
    
    public void SaveServerIP(String in_ServerIP)
    {    	
    	if(mAppConfig != null )
    	{
    		SharedPreferences.Editor editor = mAppConfig.edit();
    		editor.putString("gEposServerIP", in_ServerIP);
    		editor.commit();
    	}
    }
    
    public String LoadServerIP()
    {    
    	String mEposServerIP = "";
    	if(mAppConfig != null )
    	{
    		mEposServerIP = mAppConfig.getString("gEposServerIP", "");
    	}
    	return mEposServerIP;
    }
}
