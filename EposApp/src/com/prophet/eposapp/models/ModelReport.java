package com.prophet.eposapp.models;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.prophet.eposapp.JSONBase;

public class ModelReport extends JSONBase
{
	private String URL_PRINTORDER = "";
	
	public ModelReport()
    {
    }
    
	public String GetErrorMsg()
    {
        return mErrorMsg;
    }
    
	public void SetDBPath(String in_ServerIP)
	{
		SetServerIP(in_ServerIP);
		URL_PRINTORDER = DB_SERVER_PATH + "printorder.php";
	}

	public void PrintOrder(int in_OrderIDF, int in_OrderIDT, int in_PrintType, int in_SplitBill)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("TillOrderID",Integer.toString(in_OrderIDF) ));
		params.add(new BasicNameValuePair("TillPrintOrderType",Integer.toString(in_PrintType) ));
		params.add(new BasicNameValuePair("TillSplitBill",Integer.toString(in_SplitBill) ));
            
		mErrorMsg = "";
		try
		{
			// getting JSON string from URL            
			JSONObject json = mjParser.makeHttpRequest(URL_PRINTORDER, "GET", params);
             
			if( json != null )
			{
           
				// Checking for SUCCESS TAG                
				int success = json.getInt(TAG_SUCCESS);
 
				if (success == 1)
				{                    
             
				}                 
				else                             
				{
					mErrorMsg = json.getString(TAG_MSG);
					if( mErrorMsg.compareTo("") == 0 )
						mErrorMsg = "Print Error";                
				}     
			}
		}
		catch( JSONException e )
		{
			mErrorMsg = e.getMessage();               
			e.printStackTrace();  			
		}				
	}
}
