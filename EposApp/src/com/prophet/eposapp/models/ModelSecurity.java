package com.prophet.eposapp.models;

import java.util.ArrayList;

import com.prophet.eposapp.db.DALSecurity;

public class ModelSecurity 
{
	DALSecurity mDALSecurity;
	int mUserNo;
	int mSessionID;
	String mErrorMsg;
	//boolean mUserAlreadyLoggedIn;
	
	
	public ModelSecurity()
	{
		mDALSecurity = new DALSecurity();	
		mUserNo = 0;
		mSessionID = 0;
	}
	
	public void SetDBPath(String in_ServerIP)
	{
		mDALSecurity.SetDBPath(in_ServerIP);
	}
	
    public String GetErrorMsg()
    {
        return mErrorMsg;
    }
    
	public Integer ProcessLogin(String in_Passcode, boolean in_UserAlreadyLoggedIn)
	{
		ArrayList<Integer> mRetVal = mDALSecurity.UserLogin(in_Passcode,in_UserAlreadyLoggedIn);
		
		mUserNo = mRetVal.get(1);
		mSessionID = mRetVal.get(2);
		
		if( mRetVal.get(0) == 0 )
		{	
			mErrorMsg = mDALSecurity.GetErrorMsg();	// Connection error msg
			if( mDALSecurity.GetErrorMsg().compareTo("") == 0 )
			{
				// No connection error msg so it must be an invalid code
				mErrorMsg = "Invalid Passcode";				
			}			
		}
		else if( mRetVal.get(0) == -1 )
		{
			mErrorMsg = "This user is already logged in on another client. If you continue, this user will be logged out.";
		}
			
		return mRetVal.get(0);
	}
	
	public void LogoutUser()
	{
		 mDALSecurity.UserLogout(mUserNo,mSessionID);
	}

	public boolean AdminConfigLogin(String in_Passcode) 
	{
		if( in_Passcode.compareTo("6825") == 0 )
		{
			return true;
		}
		return false;
	}
}
