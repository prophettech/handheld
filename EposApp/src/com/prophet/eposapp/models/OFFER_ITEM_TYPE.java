﻿package com.prophet.eposapp.models;

public enum OFFER_ITEM_TYPE
{
	OFFERITEMTYPE_STD(1),
	OFFERITEMTYPE_TOPPINGS(2),
	OFFERITEMTYPE_SELECT(3),
	OFFERITEMTYPE_OR(4);

	private int intValue;
	private static java.util.HashMap<Integer, OFFER_ITEM_TYPE> mappings;
	private static java.util.HashMap<Integer, OFFER_ITEM_TYPE> getMappings()
	{
		if (mappings == null)
		{
			synchronized (OFFER_ITEM_TYPE.class)
			{
				if (mappings == null)
				{
					mappings = new java.util.HashMap<Integer, OFFER_ITEM_TYPE>();
				}
			}
		}
		return mappings;
	}

	private OFFER_ITEM_TYPE(int value)
	{
		intValue = value;
		getMappings().put(value, this);
	}

	public int getValue()
	{
		return intValue;
	}

	public static OFFER_ITEM_TYPE forValue(int value)
	{
		return getMappings().get(value);
	}
}