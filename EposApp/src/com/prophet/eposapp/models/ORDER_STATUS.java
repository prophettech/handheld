package com.prophet.eposapp.models;

public enum ORDER_STATUS { ORDSTATUS_NONE, ORDSTATUS_PREPARING, ORDSTATUS_READY, ORDSTATUS_PAID, ORDSTATUS_REQUIREPAYMENT, ORDSTATUS_OUTFORDELIVERY, ORDSTATUS_RETURNED, ORDSTATUS_UNPAID, ORDSTATUS_PARTPAID };
