package com.prophet.eposapp.models;

import java.util.ArrayList;

public class OrderItemRecordData 
{
        public int OrderID;
        public int OrderNo;
        public int ItemID;
        public ITEM_TYPE ItemType;
        public String ItemFreeText;
        public String ItemCode;
        public String ItemDesc;
        public double ItemQty;
        public double ItemSalePrice;
        public double ItemNET;
        public double ItemVAT;
        public double ItemVatableAmount;
        public int ItemVATCode;
        public double ItemVATRate;
        public boolean ItemHalfHalf;
        public double ItemToppingNET;
        public double ItemToppingVAT;
        public double ItemToppingPrice;
        public double ItemSplitQty;
        public double ItemPrintedQty;
        public double ItemSplitPrintedQty;	
        public int ItemSplitID;
        public String ItemSplitRef;
        public int ItemMDID;
        public int ItemPrintToKitchen;

        //public TillProduct ItemProduct1;
        //public TillProduct ItemProduct2;
        public TillProduct ItemProductHalfHalf;

        public int Product1ParentClassID;
        public int Product1ClassID;        
        public String Product1Code;
        public String Product1Desc;
        public String Product1ParentCode;
        public String Product1ParentDesc;
        public double Product1Price;
        public int Product1VATCode;
        public double Product1VatableAmount;
        public double Product1ToppingCharges;                    
        public double Product1ToppingNET;                    
        public double Product1ToppingVAT;
        public ITEM_TYPE Product1Type;
    	public int Product1SectionPos;
    	public String Product1SectionDesc;
    	public int Product1TopBut1;
    	public int Product1TopBut2;
        public ArrayList<TillToppings> Product1ToppingsList;

        public int Product2ParentClassID;
        public int Product2ClassID; 
        public String Product2Code;
        public String Product2Desc;
        public String Product2ParentCode;
        public String Product2ParentDesc;
        public double Product2Price;
        public int Product2VATCode;
        public double Product2VatableAmount;
        public double Product2ToppingCharges;
        public double Product2ToppingNET;
        public double Product2ToppingVAT;
        public ITEM_TYPE Product2Type;
    	public int Product2SectionPos;
    	public String Product2SectionDesc;
    	public int Product2TopBut1;
    	public int Product2TopBut2;
        //public ArrayList<TillToppings> Product2ToppingsList;
		
		

        public OrderItemRecordData()
        {

        }

        public void ClearOrderItemRecordData()
        {
            OrderID = 0;
            OrderNo = 0;
            ItemID = 0;
            //ItemType = 0;
            ItemCode = "";
            ItemDesc = "";
            ItemQty = 0;
            ItemSalePrice = 0;
            ItemNET = 0;
            ItemVAT = 0;
            ItemVatableAmount = 0;
            ItemVATCode = 0;
            ItemVATRate = 0;
            ItemHalfHalf = false;
            ItemToppingNET = 0;
            ItemToppingVAT = 0;
            ItemToppingPrice = 0;      
            ItemFreeText = "";
    		ItemSplitQty = 0;
    		ItemPrintedQty = 0;
    		ItemSplitPrintedQty = 0;
    		ItemSplitID = 0;
    		ItemSplitRef = "";
    		ItemMDID = 0;
    		ItemPrintToKitchen = 1;
            
            ItemProductHalfHalf = null;

            Product1ParentClassID = 0;
            Product1Code = "";
            Product1Desc = "";
            Product1ParentCode = "";
            Product1ParentDesc = "";
            Product1Price = 0;
            Product1VATCode = 0;
            Product1VatableAmount = 0;
            Product1ToppingCharges = 0;                   
            Product1ToppingNET = 0;                   
            Product1ToppingVAT = 0;
            Product1TopBut1 = 0;
            Product1TopBut2 = 0;            
            //Product1Type = 0;
            Product1ToppingsList = null;
                    
            Product2ParentClassID = 0;
            Product2Code = "";
            Product2Desc = "";
            Product2ParentCode = "";
            Product2ParentDesc = "";
            Product2Price = 0;
            Product2VATCode = 0;
            Product2VatableAmount = 0;
            Product2ToppingCharges = 0;                   
            Product2ToppingNET = 0;                   
            Product2ToppingVAT = 0;
            Product2TopBut1 = 0;
            Product2TopBut2 = 0;                
            //Product2Type = 0;
            //Product2ToppingsList = null;
        }

}
