﻿package com.prophet.eposapp.models;

public class TillDeliveryOptionsDTO
{
	public String DelStdCharge;
	public String DelFreeOverWithinMiles;
	public String DelFreeOver;
	public String DelExtraCharge;
	public String DelExtraChargeMiles;
	public boolean DelIncVAT;

	public java.util.ArrayList<TillDeliveryBand> DelBandList;

	public TillDeliveryOptionsDTO()
	{
		DelBandList = new java.util.ArrayList<TillDeliveryBand>();
		DelStdCharge = "";
		DelFreeOverWithinMiles = "";
		DelFreeOver = "";
		DelExtraCharge = "";
		DelExtraChargeMiles = "";
		DelIncVAT = false;
	}
}