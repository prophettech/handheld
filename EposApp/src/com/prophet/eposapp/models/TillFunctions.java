package com.prophet.eposapp.models;

public final class TillFunctions 
{
	public static double round2(double in_value) 
	{
		double result = in_value * 100;
		result = Math.round(result);
		result = result / 100;
		return result;
	}
}
