package com.prophet.eposapp.models;

public class TillOptions 
{
	boolean mAccumulatedQty;
	boolean mNETDiscount;
	boolean mPrintNewItemsOnly;
	
	
	public TillOptions()
	{
		mAccumulatedQty = false;
		mNETDiscount = false;		
	}
	
	public void SetAccumulatedQty(boolean in_Value)
	{
		mAccumulatedQty = in_Value;		
	}
	
	public boolean GetAccumulatedQty()
	{
		return mAccumulatedQty;		
	}

	public void SetNETDiscount(boolean in_Value)
	{
		mNETDiscount = in_Value;		
	}
	
	public boolean GetNETDiscount()
	{
		return mNETDiscount;		
	}
	
	public void SetPrintNewItemsOnly(boolean in_Value)
	{
		mPrintNewItemsOnly = in_Value;		
	}
	
	public boolean GetPrintNewItemsOnly()
	{
		return mPrintNewItemsOnly;		
	}
}
