﻿package com.prophet.eposapp.models;

import java.util.ArrayList;
import com.prophet.eposapp.db.retTypeOrderInfo;

public class TillOrder
{
	private ModelHelper mModelHelper;
	//private boolean mOrderNeedsSaving;
	private TillOrderDetails OrderDetails;
	private java.util.ArrayList<TillOrderItem> OrderItems;

	private boolean OptionUseStdHHPrice;
	private boolean OptionIncToppingsInHHPrice;
	private boolean OptionExcludeDeliveryOnDiscount;
	private boolean OptionAddVATToPrice;
	private TillDeliveryOptionsDTO DelOptions;

	private boolean bUseFixedServiceCharge;
	private boolean bUseFixedDiscount;
	int mSplitPayID;
	
	ArrayList<TillSplitItem> mSplitBillItems;

	public TillOrder(ModelHelper in_ModelHelper)
	{
		mModelHelper = in_ModelHelper;
		OptionUseStdHHPrice = false;
		OptionIncToppingsInHHPrice = false;
		OptionExcludeDeliveryOnDiscount = false;
		OrderDetails = new TillOrderDetails();
		OrderItems = new java.util.ArrayList<TillOrderItem>();
		mSplitBillItems = new ArrayList<TillSplitItem>();
		//mOrderNeedsSaving = false;
	}

	public final void SetOrderOptions(boolean in_StdHHPrice, boolean in_IncToppings, boolean in_ExcDeliveryDisc, boolean in_AddVATToPrice, TillDeliveryOptionsDTO in_DelOptions)
	{
		OptionUseStdHHPrice = in_StdHHPrice;
		OptionIncToppingsInHHPrice = in_IncToppings;
		OptionExcludeDeliveryOnDiscount = in_ExcDeliveryDisc;
		OptionAddVATToPrice = in_AddVATToPrice;
		DelOptions = in_DelOptions;
		OrderDetails.OptionAddVATToPrice = OptionAddVATToPrice;
	}

	public final void CopyOrder(retTypeOrderInfo in_OrderInfo)
	{
		TillOrderDetails tmpOrderDetails = in_OrderInfo.getOrderDetails();
		OrderDetails.setOrderID(tmpOrderDetails.getOrderID());
    	OrderDetails.setOrderNo(tmpOrderDetails.getOrderNo());
    	OrderDetails.setOrderTable(tmpOrderDetails.getOrderTableNo(),tmpOrderDetails.getOrderTableNo());
    	OrderDetails.setOrderTransID(tmpOrderDetails.getOrderTransID());
    	OrderDetails.setOrderTableCovers(tmpOrderDetails.getOrderTableCovers());
    	OrderDetails.setOrderPayTypeCode(tmpOrderDetails.getOrderPayTypeCode());
    	OrderDetails.setItemIDCounter(tmpOrderDetails.getItemIDCounter());
    	OrderDetails.setOrderPayment(tmpOrderDetails.getOrderPayment());
    	
    	ArrayList<TillOrderItem> tmpOrderItems = in_OrderInfo.getOrderItems();
    	OrderItems = (ArrayList<TillOrderItem>)tmpOrderItems;
    	
    	ArrayList<TillSplitItem> tmpSplitItems = in_OrderInfo.getSplitItems();
    	mSplitBillItems = (ArrayList<TillSplitItem>)tmpSplitItems;		
    	
    	
    	ArrayList<TillSplitPayDetails> tmpSplitPayItems = tmpOrderDetails.getOrderSplitPayList();
		for(TillSplitPayDetails mSplitPayDetails : tmpSplitPayItems )
		{	
			OrderDetails.setOrderSplitPayItem(mSplitPayDetails);
		}		
	}
	
	public final TillOrderDetails GetOrderDetails()
	{
		return OrderDetails;
	}

	public final java.util.ArrayList<TillOrderItem> GetOrderItemsList()
	{
		return OrderItems;
	}

	public void SetPrintedToKitchen(boolean in_Value) 
	{	
		OrderDetails.SetPrintedToKitchen(in_Value);		
	}
	
	public boolean GetPrintedToKitchen() 
	{	
		return OrderDetails.GetPrintedToKitchen();		
	}			

	public void SetBillPrinted(boolean in_Value) 
	{	
		OrderDetails.SetBillPrinted(in_Value);
	}
	
	public boolean GetBillPrinted() 
	{			
		return OrderDetails.GetBillPrinted();		
	}	
	
	public final void ClearOrder()
	{	
		ClearCustDetails();
		OrderDetails.ClearOrderDetails();	
		OrderItems.clear();
		mSplitBillItems.clear();	
		OrderDetails.getOrderSplitPayList().clear();
		mSplitPayID = 0;

		bUseFixedServiceCharge = false;
		bUseFixedDiscount = false;
		SetOrderType(ORDERTYPE.ORD_TYPE_TAKEAWAY);
		OrderDetails.setItemIDCounter(0);
		OrderItems.clear();
		CalculateOrderTotal();
		SetServiceChargePerc(OrderDetails.getOrderServiceChargePerc(), true);
		SetDiscountPerc(OrderDetails.getOrderDiscountPerc(), true);
	}

	public final void ClearCustDetails()
	{
		OrderDetails.OrderCustDetails.CustNo = "";
		OrderDetails.OrderCustDetails.CustName = "";
		OrderDetails.OrderCustDetails.CustAdd1 = "";
		OrderDetails.OrderCustDetails.CustAdd2 = "";
		OrderDetails.OrderCustDetails.CustAdd3 = "";
		OrderDetails.OrderCustDetails.CustAdd4 = "";
		OrderDetails.OrderCustDetails.CustPostCode = "";
		OrderDetails.OrderCustDetails.CustNote = "";
		OrderDetails.OrderCustDetails.CustDiscRate = "0";
		OrderDetails.OrderCustDetails.CustFixedDelCharge = "";
		OrderDetails.setOrderDelDriver("");
		OrderDetails.setOrderDelDriverID(0);
		OrderDetails.setOrderDelDistance(-1);
		OrderDetails.setOrderDateReq("");
		OrderDetails.setOrderTimeReq("");
	}

	
	public final void ClearSplitPay()
	{
		
		/*
		int idx = 0;
		while (idx < OrderDetails.OrderSplitPayData.Rows.size())
		{
			int curCount = OrderDetails.OrderSplitPayData.Rows.size();
			OrderDetails.OrderSplitPayData.Rows[idx].Delete();
			if (curCount == OrderDetails.OrderSplitPayData.Rows.size())
			{
				idx++;
			}
		}
		*/
	}

	public final void SetCustDetails(CustomerDetails in_CustDetails)
	{
		OrderDetails.OrderCustDetails.CustNo = in_CustDetails.CustNo;
		OrderDetails.OrderCustDetails.CustName = in_CustDetails.CustName;
		OrderDetails.OrderCustDetails.CustAdd1 = in_CustDetails.CustAdd1;
		OrderDetails.OrderCustDetails.CustAdd2 = in_CustDetails.CustAdd2;
		OrderDetails.OrderCustDetails.CustAdd3 = in_CustDetails.CustAdd3;
		OrderDetails.OrderCustDetails.CustAdd4 = in_CustDetails.CustAdd4;
		OrderDetails.OrderCustDetails.CustPostCode = in_CustDetails.CustPostCode;
		OrderDetails.OrderCustDetails.CustNote = in_CustDetails.CustNote;
		OrderDetails.OrderCustDetails.CustDiscRate = in_CustDetails.CustDiscRate;
		OrderDetails.OrderCustDetails.CustFixedDelCharge = in_CustDetails.CustFixedDelCharge;
		OrderDetails.setOrderDelDistance(in_CustDetails.CustMiles);
		if (OrderDetails.OrderCustDetails.CustDiscRate.length() == 0)
		{
			OrderDetails.OrderCustDetails.CustDiscRate = "0";
		}

		
		SetDiscountPerc(Double.parseDouble(OrderDetails.OrderCustDetails.CustDiscRate), true);

		if (OrderDetails.OrderCustDetails.CustFixedDelCharge.length() == 0)
		{
			ResetDeliveryOverride();
		}
		else
		{
			SetDeliveryCharge(Double.parseDouble(OrderDetails.OrderCustDetails.CustFixedDelCharge));
		}
		

		OrderDetails.setOrderDateReq(in_CustDetails.CustDateReq);
		OrderDetails.setOrderTimeReq(in_CustDetails.CustTimeReq);

	}

	public final void SetOrderLoaded()
	{
		OrderDetails.setbInsertOrder(false);
		//OrderDetails.setItemIDCounter(OrderItems.size());

		for (TillOrderItem OrderItem : OrderItems)
		{
			OrderItem.SetModelHelper(mModelHelper);
			OrderItem.SetItemOptions(OptionUseStdHHPrice, OptionIncToppingsInHHPrice, OptionAddVATToPrice);
			OrderItem.UpdateItemDetails();
		}

		CalculateOrderTotal();
	}

	public final void NewOrder(int in_OrderNo)
	{
		ClearOrder();
		OrderDetails.setOrderNo(in_OrderNo);
		OrderDetails.setbInsertOrder(true);
	}

	public final void AddItemToOrder(double in_Qty, TillProduct in_Product1, TillProduct in_Product2, TillProduct in_ProductHalfHalf, boolean in_HalfHalf, boolean in_HappyHour)
	{
		TillOrderItem NewOrderItem = new TillOrderItem();
		NewOrderItem.SetModelHelper(mModelHelper);
		NewOrderItem.SetItemOptions(OptionUseStdHHPrice, OptionIncToppingsInHHPrice, OptionAddVATToPrice);
		NewOrderItem.SetHappyHour(in_HappyHour);		
		int NextItemID = OrderDetails.getItemIDCounter() + 1;
		OrderDetails.setItemIDCounter(NextItemID);
		NewOrderItem.SetProduct(OrderDetails.getItemIDCounter(), in_Qty, in_Product1, in_Product2, in_ProductHalfHalf, in_HalfHalf, false);
		OrderItems.add(NewOrderItem);
		CalculateOrderTotal();
	}

    public void AddOfferToOrder(double in_Qty, TillProductOffer in_Offer, boolean in_HappyHour)
    {
        TillOrderItem NewOrderItem = new TillOrderItem();
        NewOrderItem.SetModelHelper(mModelHelper);
        NewOrderItem.SetItemOptions(OptionUseStdHHPrice, OptionIncToppingsInHHPrice, OptionAddVATToPrice);
        NewOrderItem.SetHappyHour(in_HappyHour);
        OrderDetails.setItemIDCounter(OrderDetails.getItemIDCounter() +1);
        NewOrderItem.SetProductOffer(OrderDetails.getItemIDCounter(), in_Qty, in_Offer, false);                          
        OrderItems.add(NewOrderItem);
        CalculateOrderTotal();
        // V2 Till needs item id to be consecutive for sub items
        OrderDetails.setItemIDCounter(OrderDetails.getItemIDCounter() + in_Offer.GetPOItemGroupList().size());        
    }     

	public final void CalculateOrderTotal()
	{
		OrderDetails.setOrderNET(0);
		OrderDetails.setOrderVAT(0);
		OrderDetails.setOrderTotal(0);
		OrderDetails.setOrderDiscount(0);
		OrderDetails.setOrderToppingNET(0);
		OrderDetails.setOrderToppingVAT(0);

		double TmpTotalItemNET = 0;
		double TmpTotalItemVAT = 0;
		double TmpTotalItemToppingNET = 0;
		double TmpTotalItemToppingVAT = 0;		
		for (TillOrderItem OrderItem : OrderItems)
		{
			TmpTotalItemNET += OrderItem.GetItemNET();			
			TmpTotalItemVAT += OrderItem.GetItemVAT();	
			TmpTotalItemToppingNET += OrderItem.GetItemToppingNET();	
			TmpTotalItemToppingVAT += OrderItem.GetItemToppingVAT();	
			/*
			OrderDetails.setOrderNET(OrderDetails.getOrderNET() + OrderItem.GetItemNET());
			OrderDetails.setOrderVAT(OrderDetails.getOrderVAT() + OrderItem.GetItemVAT());
			OrderDetails.setOrderToppingNET(OrderDetails.getOrderToppingNET() + OrderItem.GetItemToppingNET());
			OrderDetails.setOrderToppingVAT(OrderDetails.getOrderToppingVAT() + OrderItem.GetItemToppingVAT());
		*/
		}

		OrderDetails.setOrderNET(TillFunctions.round2(TmpTotalItemNET + TmpTotalItemToppingNET));
		OrderDetails.setOrderVAT(TillFunctions.round2(TmpTotalItemVAT + TmpTotalItemToppingVAT));
		OrderDetails.setOrderToppingNET(TillFunctions.round2(TmpTotalItemToppingNET));
		OrderDetails.setOrderToppingVAT(TillFunctions.round2(TmpTotalItemToppingVAT));				
		OrderDetails.setOrderTotal(TillFunctions.round2(OrderDetails.getOrderNET() + OrderDetails.getOrderVAT()));					

		CalculateDeliveryTotal();
		
		if (bUseFixedServiceCharge)
		{
			SetServiceChargeAmt(OrderDetails.getOrderServiceChargeAmt(), true); // Recalculate percent
		}
		else
		{
			SetServiceChargePerc(OrderDetails.getOrderServiceChargePerc(), true);
		}

		OrderDetails.setOrderTotal(OrderDetails.getOrderTotal() + OrderDetails.getOrderServiceChargeAmt());
		CalculateOrderTotalDiscount();
		OrderDetails.setOrderTotal(TillFunctions.round2(OrderDetails.getOrderTotal()));

		CalcOrderAmountDue();
		CalcOrderChangeDue();
	}

	private void CalculateOrderTotalDiscount()
	{
		if (bUseFixedDiscount)
		{
			SetDiscountAmt(OrderDetails.getOrderDiscountAmt(), true); // Recalculate percent
		}
		else
		{
			SetDiscountPerc(OrderDetails.getOrderDiscountPerc(), true);
		}
		OrderDetails.setOrderTotal(OrderDetails.getOrderTotal() - TillFunctions.round2(OrderDetails.getOrderDiscountAmt()));
	}

	private void CalculateDeliveryTotal()
	{
		/*
		double DeliveryCharge = 0;

		
		if (!OrderDetails.getOrderDeliveryOverride())
		{
			OrderDetails.setOrderDeliveryVAT(0);
			if (OrderDetails.getOrderType() == ORDERTYPE.ORD_TYPE_DELIVERY)
			{
				//double TmpTotal = OrderDetails.OrderNET + OrderDetails.OrderVAT
				//                            + OrderDetails.OrderToppingCharges;
				double TmpTotal = OrderDetails.getOrderNET() + OrderDetails.getOrderVAT() + OrderDetails.getOrderToppingNET() + OrderDetails.getOrderToppingVAT();


//C# TO JAVA CONVERTER TODO TASK: There is no equivalent in Java to Math.Round used with MidpointRounding:
				TmpTotal = Math.Round(TmpTotal, 2, MidpointRounding.AwayFromZero);

				if (OrderDetails.OrderCustDetails.CustFixedDelCharge.CompareTo("") != 0)
				{
					// Use fixed customer delivery charge
					DeliveryCharge = (double)OrderDetails.OrderCustDetails.CustFixedDelCharge;
				}
				else
				{
					if (DelOptions.DelBandList.size() > 0)
					{
						// Use Delivery Bands
						for (TillDeliveryBand DelBand : DelOptions.DelBandList)
						{
							if (DelBand.DelBandMiles.CompareTo("") != 0 && DelBand.DelBandCharge.CompareTo("") != 0)
							{
								if (OrderDetails.getOrderDelDistance() >= (double)DelBand.DelBandMiles)
								{
									DeliveryCharge = (double)DelBand.DelBandCharge;
								}
							}
							else if (DelBand.DelBandAmount.CompareTo("") != 0 && DelBand.DelBandCharge.CompareTo("") != 0)
							{
								if (TmpTotal >= (double)DelBand.DelBandAmount)
								{
									DeliveryCharge = (double)DelBand.DelBandCharge;
								}
							}
						}
					}
					else
					{
						if (DelOptions.DelStdCharge.CompareTo("") != 0)
						{
							DeliveryCharge = (double)DelOptions.DelStdCharge;
							if (DelOptions.DelFreeOverWithinMiles.CompareTo("") != 0)
							{
								if (DelOptions.DelFreeOver.CompareTo("") != 0 && DelOptions.DelFreeOverWithinMiles.CompareTo("") != 0)
								{
									if (TmpTotal >= (double)DelOptions.DelFreeOver && OrderDetails.getOrderDelDistance() <= (double)DelOptions.DelFreeOverWithinMiles)
									{
										if (OrderDetails.OrderCustDetails.CustNo.CompareTo("") != 0)
										{
											DeliveryCharge = 0;
										}
									}
								}
							}
							else
							{
								if (DelOptions.DelFreeOver.CompareTo("") != 0 && TmpTotal >= (double)DelOptions.DelFreeOver)
								{
									DeliveryCharge = 0;
								}
							}
						}
					}

					if (DelOptions.DelExtraCharge.CompareTo("") != 0)
					{
						if (DelOptions.DelExtraChargeMiles.CompareTo("") != 0)
						{
							if (OrderDetails.getOrderDelDistance() > (double)DelOptions.DelExtraChargeMiles)
							{
								DeliveryCharge += (double)DelOptions.DelExtraCharge;
							}
						}
						else
						{
							DeliveryCharge += (double)DelOptions.DelExtraCharge;
						}
					}
				}
				if (DelOptions.DelIncVAT)
				{
					double VATRate = 0;
					tangible.RefObject<Double> tempRef_VATRate = new tangible.RefObject<Double>(VATRate);
					OrderDetails.setOrderDeliveryVAT(_ModelHelper.CalcVAT(DeliveryCharge, 1, 1, tempRef_VATRate));
					VATRate = tempRef_VATRate.argvalue;
				}
			}

			OrderDetails.setOrderDeliveryTotal(DeliveryCharge);
			OrderDetails.setOrderDeliveryNET(DeliveryCharge - OrderDetails.getOrderDeliveryVAT());
			//OrderDetails.OrderDeliveryTotal = DeliveryCharge;
		}

//C# TO JAVA CONVERTER TODO TASK: There is no equivalent in Java to Math.Round used with MidpointRounding:
		OrderDetails.setOrderTotal(OrderDetails.getOrderTotal() + Math.Round(OrderDetails.getOrderDeliveryTotal(), 2, MidpointRounding.AwayFromZero));
*/
	}

	private void CalcOrderAmountDue()
	{
		//OrderDetails.setOrderAmountDue(Math.Round(OrderDetails.getOrderTotal() - OrderDetails.getOrderPayment(), 2, MidpointRounding.AwayFromZero));
		OrderDetails.setOrderAmountDue(TillFunctions.round2(OrderDetails.getOrderTotal()- OrderDetails.getOrderPayment()));
		if (OrderDetails.getOrderAmountDue() < 0)
		{
			OrderDetails.setOrderAmountDue(0);
		}
	}

	private void CalcOrderChangeDue()
	{
		OrderDetails.setOrderChangeDue(0);
		double TmpChange = TillFunctions.round2(OrderDetails.getOrderPayment() - OrderDetails.getOrderTotal());
		if (TmpChange > 0)
		{
			//OrderDetails.setOrderChangeDue(Math.Round(TmpChange, 2, MidpointRounding.AwayFromZero));
			OrderDetails.setOrderChangeDue(TillFunctions.round2(TmpChange));
		}
	}

	public final void ClearCustNo()
	{
		OrderDetails.OrderCustDetails.CustNo = "";
	}

	public final int DeleteCurrentOrderItem(int in_OrderItemID)
	{
		int OrderItemListCount = OrderItems.size();

		if (OrderItemListCount > 0)
		{
			for (int i = 0; i < OrderItemListCount; i++)
			{
				if (OrderItems.get(i).GetItemID() == in_OrderItemID)
				{
					OrderItems.remove(i);
					CalculateOrderTotal();
					return i;
				}
			}
		}
		return -1;
	}

	public final int GetOrderItemID(int in_OrderItemIdx)
	{
		if (OrderItems.size() > 0)
		{
			if (in_OrderItemIdx >= OrderItems.size())
			{
				in_OrderItemIdx = OrderItems.size() - 1;
			}

			return OrderItems.get(in_OrderItemIdx).GetItemID();
		}

		return 0;
	}


	public final void SetOrderPayment(double in_PayAmt, boolean in_Add)
	{

		if (in_PayAmt == -1)
		{
			// Full Amount
			OrderDetails.setOrderPayment(OrderDetails.getOrderTotal());
		}
		else
		{
			if (in_Add)
			{
				OrderDetails.setOrderPayment(OrderDetails.getOrderPayment() + Math.round(in_PayAmt * Math.pow(10, 2)) / Math.pow(10, 2));
			}
			else
			{
				OrderDetails.setOrderPayment(Math.round(in_PayAmt * Math.pow(10, 2)) / Math.pow(10, 2));
			}
		}
		CalcOrderAmountDue();
		CalcOrderChangeDue();
	}

	public final boolean CanPayOrder()
	{
		if (OrderDetails.getOrderPayment() >= OrderDetails.getOrderTotal())
		{
			return true;
		}

		return false;
	}

	public final void SetOrderType(ORDERTYPE in_OrdType)
	{
		/*
		OrderDetails.OrderType = in_OrderType;
		if (OrderDetails.OrderType == ORDERTYPE.ORD_TYPE_TAKEAWAY)
		{
		    ClearCustDetails();
		    OrderDetails.OrderCustDetails.CustNo = "TAKEAWAY";
		}
		*/

		OrderDetails.setOrderType(in_OrdType);

		ORDERTYPE mCurOrderType = OrderDetails.getOrderType();
		
		// If order type is already TAKEAWAY clear TAKEAWAY A/C Ref
		// If being changed to TAKEAWAY clears all customer details
		if (OrderDetails.OrderCustDetails.CustNo.equals("TAKEAWAY") || mCurOrderType == ORDERTYPE.ORD_TYPE_TAKEAWAY)
		{
			ClearCustDetails();
		}
				

		if (mCurOrderType == ORDERTYPE.ORD_TYPE_TAKEAWAY || mCurOrderType == ORDERTYPE.ORD_TYPE_COLLECTION)
		{
			OrderDetails.setOrderDelDriverID(0);
			OrderDetails.setOrderDelDriver("");
		}

		// Use default A/C Ref for TAKEAWAY order type
		if (mCurOrderType == ORDERTYPE.ORD_TYPE_TAKEAWAY)
		{
			OrderDetails.OrderCustDetails.CustNo = "TAKEAWAY";
		}
		else if ((mCurOrderType == ORDERTYPE.ORD_TYPE_COLLECTION || mCurOrderType == ORDERTYPE.ORD_TYPE_DELIVERY) && OrderDetails.OrderCustDetails.CustNo.compareTo("") == 0)
		{
			OrderDetails.OrderCustDetails.CustNo = "NO NUMBER";
		}
		else if (mCurOrderType == ORDERTYPE.ORD_TYPE_TABLE && OrderDetails.OrderCustDetails.CustNo.compareTo("") == 0)
		{
			OrderDetails.OrderCustDetails.CustNo = "RESTAURANT";
		}	
		else if (mCurOrderType == ORDERTYPE.ORD_TYPE_BAR)
		{
			OrderDetails.OrderCustDetails.CustNo = "BAR";
		}				
		 
		CalculateOrderTotal(); // This updates delivery charge as well
	}

	public final void SetOrderPayType(int in_PayTypeCode, String in_PayTypeDesc)
	{
		OrderDetails.setOrderPayTypeCode(in_PayTypeCode);
		OrderDetails.setOrderPayTypeDesc(in_PayTypeDesc);
	}

	public final void SetDeposit(double in_Deposit)
	{
		OrderDetails.setOrderDeposit(in_Deposit);
	}


	public final void SetServiceChargePerc(double in_ServiceChargePerc)
	{
		SetServiceChargePerc(in_ServiceChargePerc, false);
	}

	public final void SetServiceChargePerc(double in_ServiceChargePerc, boolean in_Override)
	{
		if (in_ServiceChargePerc != OrderDetails.getOrderServiceChargePerc() || in_Override)
		{
			if (!in_Override)
			{
				bUseFixedServiceCharge = false;
			}
			OrderDetails.setOrderServiceChargePerc(TillFunctions.round2(in_ServiceChargePerc));
			OrderDetails.setOrderServiceChargeAmt(TillFunctions.round2((((OrderDetails.getOrderNET() + OrderDetails.getOrderVAT() + OrderDetails.getOrderDeliveryTotal()) / 100) * in_ServiceChargePerc)));
		}
	}


	public final void SetServiceChargeAmt(double in_ServiceChargeAmt)
	{
		SetServiceChargeAmt(in_ServiceChargeAmt, false);
	}

	public final void SetServiceChargeAmt(double in_ServiceChargeAmt, boolean in_Override)
	{
		if (in_ServiceChargeAmt != OrderDetails.getOrderServiceChargeAmt() || in_Override)
		{
			if (!in_Override)
			{
				bUseFixedServiceCharge = true;
			}
			OrderDetails.setOrderServiceChargePerc(TillFunctions.round2(((in_ServiceChargeAmt / (OrderDetails.getOrderNET() + OrderDetails.getOrderVAT() + OrderDetails.getOrderDeliveryTotal())) * 100)));
			OrderDetails.setOrderServiceChargeAmt(TillFunctions.round2(in_ServiceChargeAmt));
		}
	}


	public final void SetDiscountAmt(double in_DiscountAmt)
	{
		SetDiscountAmt(in_DiscountAmt, false);
	}


	public final void SetDiscountAmt(double in_DiscountAmt, boolean in_Override)
	{

		if (in_DiscountAmt != OrderDetails.getOrderDiscountAmt() || in_Override)
		{
			if (!in_Override)
			{
				bUseFixedDiscount = true;
			}


			double OrderValue = 0;
			if (OptionExcludeDeliveryOnDiscount)
			{
				OrderValue = OrderDetails.getOrderNET() + OrderDetails.getOrderVAT() + OrderDetails.getOrderServiceChargeAmt();
			}
			else
			{
				OrderValue = OrderDetails.getOrderNET() + OrderDetails.getOrderVAT() + OrderDetails.getOrderServiceChargeAmt() + OrderDetails.getOrderDeliveryTotal();
			}

			double TmpVal = (in_DiscountAmt / OrderValue) * 100;
			OrderDetails.setOrderDiscountAmt(in_DiscountAmt);
			OrderDetails.setOrderDiscountPerc(TmpVal);
		}
	}


	public final void SetDiscountPerc(double in_DiscountPerc)
	{
		SetDiscountPerc(in_DiscountPerc, false);
	}


	public final void SetDiscountPerc(double in_DiscountPerc, boolean in_Override)
	{

		if (in_DiscountPerc != OrderDetails.getOrderDiscountPerc() || in_Override)
		{
			if (!in_Override)
			{
				bUseFixedDiscount = false;
			}

			double OrderValue = 0;
			if (OptionExcludeDeliveryOnDiscount)
			{
				OrderValue = OrderDetails.getOrderNET() + OrderDetails.getOrderVAT() + OrderDetails.getOrderServiceChargeAmt();
			}
			else
			{
				OrderValue = OrderDetails.getOrderNET() + OrderDetails.getOrderVAT() + OrderDetails.getOrderServiceChargeAmt() + OrderDetails.getOrderDeliveryTotal();
			}
			double TmpVal = (OrderValue / 100) * in_DiscountPerc;

			OrderDetails.setOrderDiscountPerc(in_DiscountPerc);
			OrderDetails.setOrderDiscountAmt(TmpVal);
		}
	}

	public final void SetTipCash(double in_TipCash)
	{
		OrderDetails.setOrderTipCash(Math.round(in_TipCash * Math.pow(10, 2)) / Math.pow(10, 2));
	}

	public final void SetTipCC(double in_TipCC)
	{
		OrderDetails.setOrderTipCC(Math.round(in_TipCC * Math.pow(10, 2)) / Math.pow(10, 2));
	}

	public final void ResetDeliveryOverride()
	{
		OrderDetails.setOrderDeliveryOverride(false);
		CalculateOrderTotal();
	}

	public final void SetDeliveryCharge(double in_DeliveryCharge)
	{
		OrderDetails.setOrderDeliveryOverride(true);
		OrderDetails.setOrderDeliveryTotal(in_DeliveryCharge);
		OrderDetails.setOrderDeliveryVAT(0);
		OrderDetails.setOrderDeliveryVATCode(0);

		if (DelOptions.DelIncVAT)
		{
			//double VATRate = 0;
			OrderDetails.setOrderDeliveryVATCode(1);
			//OrderDetails.OrderDeliveryVAT = CalcVAT(OrderDetails.OrderDeliveryTotal, 1, 1, VATRate);
		}

		OrderDetails.setOrderDeliveryNET(Math.round((OrderDetails.getOrderDeliveryTotal() - OrderDetails.getOrderDeliveryVAT()) * Math.pow(10, 2)) / Math.pow(10, 2));
		CalculateOrderTotal();
	}

	public final void AddSplitPayment(int in_SplitPayTypeCode, String in_SplitPayTypeDesc, double in_SplitPayAmount, int in_EditIdx)
	{
		
		if (in_EditIdx == -1)
		{
			TillSplitPayDetails NewSplitPayDetails = new TillSplitPayDetails();
			NewSplitPayDetails.setSpliyPayID(++mSplitPayID);
			NewSplitPayDetails.setSpliyPayTypeCode(in_SplitPayTypeCode);
			NewSplitPayDetails.setSpliyPayDesc(in_SplitPayTypeDesc);
			NewSplitPayDetails.setSpliyPayAmount(in_SplitPayAmount);
			OrderDetails.setOrderSplitPayItem(NewSplitPayDetails);
			//OrderDetails.mSplitPayList.add(NewSplitPayDetails);
		}
		else
		{
			for(TillSplitPayDetails mSplitPayDetails : OrderDetails.mSplitPayList )
			{		
				if(mSplitPayDetails.getSpliyPayID() == in_EditIdx )
				{
					mSplitPayDetails.setSpliyPayTypeCode(in_SplitPayTypeCode);
					mSplitPayDetails.setSpliyPayDesc(in_SplitPayTypeDesc);
					mSplitPayDetails.setSpliyPayAmount(in_SplitPayAmount);
				}				
			}
		}

		UpdateSplitPaymentAmount();
	}

	public final void RemoveSplitPayment(int in_SplitPayID)
	{
		for(TillSplitPayDetails mSplitPayDetails : OrderDetails.mSplitPayList )
		{		
			if(mSplitPayDetails.getSpliyPayID() == in_SplitPayID )
			{
				OrderDetails.mSplitPayList.remove(mSplitPayDetails);
				UpdateSplitPaymentAmount();
				break;
			}				
		}
	}

	public final void UpdateSplitPaymentAmount()
	{
		double SplitTotal = 0;
		for(TillSplitPayDetails mSplitPayDetails : OrderDetails.mSplitPayList )
		{	
			SplitTotal += mSplitPayDetails.getSpliyPayAmount();
		}
		SetOrderPayment(SplitTotal, false);
	}

	public final void SetDeliveryDriver(int in_DriverID, String in_DriverName)
	{
		OrderDetails.setOrderDelDriverID(in_DriverID);
		OrderDetails.setOrderDelDriver(in_DriverName);
	}
	
	public final void setOrderTable(int in_TableID, int in_TableNo)
	{
		OrderDetails.setOrderTable(in_TableID,in_TableNo);
	}
	
	public final void setOrderTableCovers(int in_TableCovers)
	{
		OrderDetails.setOrderTableCovers(in_TableCovers);
	}
	
	public void UpdateEditedOrderItem(int in_ItemID, double in_Qty,
			double in_Price, String in_FreeText) 
	{
		for(TillOrderItem mOrderItem : OrderItems )
		{		
			if(mOrderItem.GetItemID() == in_ItemID )
			{
				mOrderItem.SetItemQty(in_Qty);
				mOrderItem.SetItemPrice(in_Price);
				mOrderItem.SetItemFreeText(in_FreeText);
				mOrderItem.UpdateItemDetails();
				CalculateOrderTotal();
				break;
			}				
		}
	}
	
	
	public final void SetSplitBillItems(ArrayList<TillSplitItem> in_SplitBillItems)
	{
		mSplitBillItems = in_SplitBillItems;
	}
	
	public final ArrayList<TillSplitItem> GetSplitBillItems()
	{
		return mSplitBillItems;
	}
	

	public boolean DecreaseItemSplitQty(int in_ItemID)
	{
		if( OrderItems.size() > 0 )
		{		
			for(TillOrderItem mOrderItem : OrderItems )
			{		
				if(mOrderItem.GetItemID() == in_ItemID )
				{
					//mOrderNeedsSaving = true;
					return mOrderItem.DecreaseItemSplitQty();
				}				
			}
		}
	    
	    return false;
	} 
	
	public void IncreaseItemSplitQty(int in_ItemID, boolean in_UpdatePrintedQty)
	{
		if( OrderItems.size() > 0 )
		{		
			for(TillOrderItem mOrderItem : OrderItems )
			{		
				if(mOrderItem.GetItemID() == in_ItemID )
				{
					//mOrderNeedsSaving = true;						
					mOrderItem.IncreaseProdSplitQty(in_UpdatePrintedQty);
				}				
			}
		}
	}

	public void UpdatePrintedQty() 
	{		
		for(TillOrderItem mOrderItem : OrderItems )
		{			
			mOrderItem.SetItemPrintedQty(mOrderItem.GetItemSplitQty());	
			if( mSplitBillItems.size() > 0 )
			{							
				mOrderItem.SetItemSplitPrintedQty(mOrderItem.GetItemQty()-mOrderItem.GetItemSplitQty());
			}
		}
	}


}