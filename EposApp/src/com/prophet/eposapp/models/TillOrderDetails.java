﻿package com.prophet.eposapp.models;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class TillOrderDetails
{
	public boolean OptionAddVATToPrice;
	public CustomerDetails OrderCustDetails;
	

	public TillOrderDetails()
	{
		OrderCustDetails = new CustomerDetails();
		mSplitPayList = new ArrayList<TillSplitPayDetails>();
		/*
		OrderSplitPayData = new DataTable();
		OrderSplitPayData.Columns.Add("ID", Integer.class);
		OrderSplitPayData.Columns.Add("SPLIYPAYTYPECODE", Integer.class);
		OrderSplitPayData.Columns.Add("SPLITPAYTYPEDESC", String.class);
		OrderSplitPayData.Columns.Add("SPLITPAYAMOUNT", Double.class);
		*/
	}
	boolean mPrintedToKitchen;
	
	public void SetPrintedToKitchen(boolean in_Value) 
	{	
		mPrintedToKitchen = in_Value;		
	}
	
	public boolean GetPrintedToKitchen() 
	{	
		return mPrintedToKitchen;		
	}	
		
	boolean mBillPrinted;
	public void SetBillPrinted(boolean in_Value) 
	{	
		mBillPrinted = in_Value;		
	}
	
	public boolean GetBillPrinted() 
	{	
		return mBillPrinted;		
	}	
	
	ArrayList<TillSplitPayDetails> mSplitPayList;
	public final ArrayList<TillSplitPayDetails> getOrderSplitPayList()
	{
		return mSplitPayList;
	}
	public final void setOrderSplitPayItem(TillSplitPayDetails in_SplitPayItem)
	{
		mSplitPayList.add(in_SplitPayItem);
	}
	

	private ORDER_STATUS privateOrderStatus;
	public final ORDER_STATUS getOrderStatus()
	{
		return privateOrderStatus;
	}
	public final void setOrderStatus(ORDER_STATUS value)
	{
		privateOrderStatus = value;
	}
	private ORDERTYPE privateOrderType;
	public final ORDERTYPE getOrderType()
	{
		return privateOrderType;
	}
	public final void setOrderType(ORDERTYPE value)
	{
		privateOrderType = value;
	}

	private int privateOrderID;
	public final int getOrderID()
	{
		return privateOrderID;
	}
	public final void setOrderID(int value)
	{
		privateOrderID = value;
	}
	private int privateOrderNo;
	public final int getOrderNo()
	{
		return privateOrderNo;
	}
	public final void setOrderNo(int value)
	{
		privateOrderNo = value;
	}
	private java.util.Date privateOrderDate = new java.util.Date(0);
	public final java.util.Date getOrderDate()
	{

		return privateOrderDate;
	}
	public final void setOrderDate(java.util.Date value)
	{
		privateOrderDate = value;
	}
	public final String getOrderDateString()
	{	
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy",Locale.UK);		
		return sdf.format(privateOrderDate);
	}
	
	
	private int privateOrderTableID;
	private int privateOrderTableNo;	
	public final int getOrderTableNo()
	{
		return privateOrderTableNo;
	}
	public final void setOrderTable(int in_TableID, int in_TableNo)
	{
		privateOrderTableID = in_TableID;
		privateOrderTableNo = in_TableNo;
	}

	private int privateOrderTableCovers;
	public final int getOrderTableCovers()
	{
		return privateOrderTableCovers;
	}
	public final void setOrderTableCovers(int value)
	{
		privateOrderTableCovers = value;
	}
	private String privateOrderTime;
	public final String getOrderTime()
	{
		return privateOrderTime;
	}
	public final void setOrderTime(String value)
	{
		privateOrderTime = value;
	}
	private String privateOrderDateReq;
	public final String getOrderDateReq()
	{
		return privateOrderDateReq;
	}
	public final void setOrderDateReq(String value)
	{
		privateOrderDateReq = value;
	}
	private String privateOrderTimeReq;
	public final String getOrderTimeReq()
	{
		return privateOrderTimeReq;
	}
	public final void setOrderTimeReq(String value)
	{
		privateOrderTimeReq = value;
	}
	private int privateOrderDelDriverID;
	public final int getOrderDelDriverID()
	{
		return privateOrderDelDriverID;
	}
	public final void setOrderDelDriverID(int value)
	{
		privateOrderDelDriverID = value;
	}
	private String privateOrderDelDriver;
	public final String getOrderDelDriver()
	{
		return privateOrderDelDriver;
	}
	public final void setOrderDelDriver(String value)
	{
		privateOrderDelDriver = value;
	}
	private double privateOrderDelDistance;
	public final double getOrderDelDistance()
	{
		return privateOrderDelDistance;
	}
	public final void setOrderDelDistance(double value)
	{
		privateOrderDelDistance = value;
	}
	private boolean privateOrderDeliveryOverride;
	public final boolean getOrderDeliveryOverride()
	{
		return privateOrderDeliveryOverride;
	}
	public final void setOrderDeliveryOverride(boolean value)
	{
		privateOrderDeliveryOverride = value;
	}
	private int privateOrderDeliveryVATCode;
	public final int getOrderDeliveryVATCode()
	{
		return privateOrderDeliveryVATCode;
	}
	public final void setOrderDeliveryVATCode(int value)
	{
		privateOrderDeliveryVATCode = value;
	}
	private double privateOrderDeliveryNET;
	public final double getOrderDeliveryNET()
	{
		return privateOrderDeliveryNET;
	}
	public final void setOrderDeliveryNET(double value)
	{
		privateOrderDeliveryNET = value;
	}
	private double privateOrderDeliveryVAT;
	public final double getOrderDeliveryVAT()
	{
		return privateOrderDeliveryVAT;
	}
	public final void setOrderDeliveryVAT(double value)
	{
		privateOrderDeliveryVAT = value;
	}
	private double privateOrderDeliveryTotal;
	public final double getOrderDeliveryTotal()
	{
		return privateOrderDeliveryTotal;
	}
	public final void setOrderDeliveryTotal(double value)
	{
		privateOrderDeliveryTotal = value;
	}

	private double privateOrderToppingNET;
	public final double getOrderToppingNET()
	{
		return privateOrderToppingNET;
	}
	public final void setOrderToppingNET(double value)
	{
		privateOrderToppingNET = value;
	}
	private double privateOrderToppingVAT;
	public final double getOrderToppingVAT()
	{
		return privateOrderToppingVAT;
	}
	public final void setOrderToppingVAT(double value)
	{
		privateOrderToppingVAT = value;
	}
	private double privateOrderNET;
	public final double getOrderNET()
	{
		return privateOrderNET;
	}
	public final void setOrderNET(double value)
	{
		privateOrderNET = value;
	}
	private double privateOrderVAT;
	public final double getOrderVAT()
	{
		return privateOrderVAT;
	}
	public final void setOrderVAT(double value)
	{
		privateOrderVAT = value;
	}
	private double privateOrderTotal;
	public final double getOrderTotal()
	{
		return privateOrderTotal;
	}
	public final void setOrderTotal(double value)
	{
		privateOrderTotal = value;
	}
	private double privateOrderDiscount;
	public final double getOrderDiscount()
	{
		return privateOrderDiscount;
	}
	public final void setOrderDiscount(double value)
	{
		privateOrderDiscount = value;
	}
	private double privateOrderPayment;
	public final double getOrderPayment()
	{
		return privateOrderPayment;
	}
	public final void setOrderPayment(double value)
	{
		privateOrderPayment = value;
	}
	private int privateOrderVATCode;
	public final int getOrderVATCode()
	{
		return privateOrderVATCode;
	}
	public final void setOrderVATCode(int value)
	{
		privateOrderVATCode = value;
	}
	private int privateOrderTransID;
	public final int getOrderTransID()
	{
		return privateOrderTransID;
	}
	public final void setOrderTransID(int value)
	{
		privateOrderTransID = value;
	}

	private int privateOrderPayTypeCode;
	public final int getOrderPayTypeCode()
	{
		return privateOrderPayTypeCode;
	}
	public final void setOrderPayTypeCode(int value)
	{
		privateOrderPayTypeCode = value;
	}
	private String privateOrderPayTypeDesc;
	public final String getOrderPayTypeDesc()
	{
		return privateOrderPayTypeDesc;
	}
	public final void setOrderPayTypeDesc(String value)
	{
		privateOrderPayTypeDesc = value;
	}
	private double privateOrderAmountDue;
	public final double getOrderAmountDue()
	{
		return privateOrderAmountDue;
	}
	public final void setOrderAmountDue(double value)
	{
		privateOrderAmountDue = value;
	}
	private double privateOrderChangeDue;
	public final double getOrderChangeDue()
	{
		return privateOrderChangeDue;
	}
	public final void setOrderChangeDue(double value)
	{
		privateOrderChangeDue = value;
	}
	private double privateOrderDiscountPerc;
	public final double getOrderDiscountPerc()
	{
		return privateOrderDiscountPerc;
	}
	public final void setOrderDiscountPerc(double value)
	{
		privateOrderDiscountPerc = value;
	}
	private double privateOrderDiscountAmt;
	public final double getOrderDiscountAmt()
	{
		return privateOrderDiscountAmt;
	}
	public final void setOrderDiscountAmt(double value)
	{
		privateOrderDiscountAmt = value;
	}
	private double privateOrderDeposit;
	public final double getOrderDeposit()
	{
		return privateOrderDeposit;
	}
	public final void setOrderDeposit(double value)
	{
		privateOrderDeposit = value;
	}
	private double privateOrderTipCash;
	public final double getOrderTipCash()
	{
		return privateOrderTipCash;
	}
	public final void setOrderTipCash(double value)
	{
		privateOrderTipCash = value;
	}
	private double privateOrderTipCC;
	public final double getOrderTipCC()
	{
		return privateOrderTipCC;
	}
	public final void setOrderTipCC(double value)
	{
		privateOrderTipCC = value;
	}
	private double privateOrderServiceChargePerc;
	public final double getOrderServiceChargePerc()
	{
		return privateOrderServiceChargePerc;
	}
	public final void setOrderServiceChargePerc(double value)
	{
		privateOrderServiceChargePerc = value;
	}
	private double privateOrderServiceChargeAmt;
	public final double getOrderServiceChargeAmt()
	{
		return privateOrderServiceChargeAmt;
	}
	public final void setOrderServiceChargeAmt(double value)
	{
		privateOrderServiceChargeAmt = value;
	}

	//public DataTable OrderSplitPayData;

	private boolean privatebInsertOrder;
	public final boolean getbInsertOrder()
	{
		return privatebInsertOrder;
	}
	public final void setbInsertOrder(boolean value)
	{
		privatebInsertOrder = value;
	}
	private int privateItemIDCounter;
	public final int getItemIDCounter()
	{
		return privateItemIDCounter;
	}
	public final void setItemIDCounter(int value)
	{
		privateItemIDCounter = value;
	}

	public final String getCustDetails()
	{
		String TmpCustDetails = "";
		TmpCustDetails += OrderCustDetails.CustNo;

		if (OrderCustDetails.CustName.compareTo("") != 0)
		{
			TmpCustDetails += ", " + OrderCustDetails.CustName;
		}

		if (OrderCustDetails.CustAdd1.compareTo("") != 0)
		{
			TmpCustDetails += ", " + OrderCustDetails.CustAdd1;
		}

		if (OrderCustDetails.CustAdd2.compareTo("") != 0)
		{
			if (OrderCustDetails.CustAdd1.compareTo("") == 0)
			{
				TmpCustDetails += ", ";
			}

			TmpCustDetails += " " + OrderCustDetails.CustAdd2;
		}

		return TmpCustDetails;
	}

	public final String getOrderTypeDesc()
	{
		String TmpDesc = "";
		switch (getOrderType())
		{
			case ORD_TYPE_TAKEAWAY:
				TmpDesc = "T";
				break;
			case ORD_TYPE_COLLECTION:
				TmpDesc = "C";
				break;
			case ORD_TYPE_DELIVERY:
				TmpDesc = "D";
				break;
			case ORD_TYPE_TABLE:
				TmpDesc = "R";
				break;
		}

		return TmpDesc;
	}

	public final String getOrderTypeDescLongIncDriver()
	{
		String TmpDesc = "";
		switch (getOrderType())
		{
			case ORD_TYPE_TAKEAWAY:
				TmpDesc = "Takeaway";
				break;
			case ORD_TYPE_COLLECTION:
				TmpDesc = "Collection";
				break;
			case ORD_TYPE_DELIVERY:
				TmpDesc = "Delivery: " + getOrderDelDriver();
				break;
			case ORD_TYPE_TABLE:
				TmpDesc = "Table";
				break;
		}

		return TmpDesc;
	}

	public final String getOrderStatusDesc()
	{
		String TmpDesc = "";
		/*
		switch (getOrderStatus())
		{
			case ORDER_STATUS.ORDSTATUS_NONE:
				TmpDesc = "";
				break;
			case ORDER_STATUS.ORDSTATUS_OUTFORDELIVERY:
				TmpDesc = "Out For Delivery";
				break;
			case ORDER_STATUS.ORDSTATUS_PAID:
				TmpDesc = "Paid";
				break;
			case ORDER_STATUS.ORDSTATUS_PARTPAID:
				TmpDesc = "Part Paid";
				break;
			case ORDER_STATUS.ORDSTATUS_PREPARING:
				TmpDesc = "Preparing";
				break;
			case ORDER_STATUS.ORDSTATUS_READY:
				TmpDesc = "Ready";
				break;
			case ORDER_STATUS.ORDSTATUS_REQUIREPAYMENT:
				TmpDesc = "Require Payment";
				break;
			case ORDER_STATUS.ORDSTATUS_RETURNED:
				TmpDesc = "Returned";
				break;
			case ORDER_STATUS.ORDSTATUS_UNPAID:
				TmpDesc = "Unpaid";
				break;
		}
		*/

		return TmpDesc;
	}
	public final String getOrderDateTimeDesc()
	{
		//return getOrderDate().ToShortDateString() + " " + getOrderTime().toString();
		return "";
	}
	public void ClearOrderDetails() 
	{
		SetPrintedToKitchen(false);
		SetBillPrinted(false);
		
		setOrderID(0);
		setOrderNo(1);
		setOrderStatus(ORDER_STATUS.ORDSTATUS_NONE);
		setOrderDate(new Date());
		setOrderTime("");
		setOrderDateReq("");
		setOrderTimeReq("");
		setOrderTotal(0);
		setOrderDelDriverID(0);
		setOrderDelDriver("");
		setOrderDelDistance(0);

		setOrderDeliveryOverride(false);
		setOrderDeliveryVATCode(0);
		setOrderDeliveryNET(0);
		setOrderDeliveryVAT(0);
		setOrderDeliveryTotal(0);
		setOrderDiscount(0);
		setOrderToppingNET(0);
		setOrderToppingVAT(0);
		setOrderNET(0);
		setOrderVAT(0);
		setOrderPayment(0);
		setOrderVATCode(0);

		setOrderPayTypeCode(0);
		setOrderPayTypeDesc("");
		setOrderChangeDue(0);
		setOrderDiscountPerc(0);
		setOrderDiscountAmt(0);
		setOrderDeposit(0);
		setOrderTipCash(0);
		setOrderTipCC(0);
		setOrderServiceChargePerc(0);
		setOrderServiceChargeAmt(0);
		setOrderTransID(0);
		setOrderTable(0,0);
		setOrderTableCovers(0);
		setbInsertOrder(false);
		
		
		mSplitPayList.clear();
		
		
	}


}