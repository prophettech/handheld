﻿package com.prophet.eposapp.models;

import java.util.ArrayList;

public class TillOrderItem
{
	private ModelHelper mModelHelper;
	boolean mOverridePrice;
	private TillProductOffer ProductOfferItem;
	private TillProduct ProductH1;
	private TillProduct ProductH2;
	private TillProduct ProductHalfHalf;

	private ITEM_TYPE ItemType;
	private int ItemID;
	private double ItemQty;
	private double ItemNET;
	private double ItemVAT;
	private double ItemVATRate;
	private double ItemVatableAmount;
	private int ItemVATCode;
	private double ItemPrice;
	private String ItemDesc;
	private String ItemMinusToppingDescH1;
	private String ItemPlusToppingDescH1;
	private String ItemMinusToppingDescH2;
	private String ItemPlusToppingDescH2;

	private double ItemToppingNET;
	private double ItemToppingCharge;
	private double ItemToppingVAT;
	private double ItemToppingVatableAmount;

	private boolean IsOffer;
	//private boolean IsSpendOffer;

	private boolean mOptionUseStdHHPrice;
	private boolean mOptionIncToppingsInHHPrice;
	private boolean mOptionAddVATToPrice;

	private String ItemFreeText;
	
	private double ItemSplitQty;
	private double ItemPrintedQty;
	private double ItemSplitPrintedQty;	

	private boolean mHappyHour;

	public TillOrderItem()
	{
		IsOffer = false;
		//IsSpendOffer = false;
		ItemQty = 0;
		ItemNET = 0;
		ItemVAT = 0;
		ItemVATRate = 0;
		ItemVATCode = 0;
		ItemVatableAmount = 0;
		ItemToppingCharge = 0;
		ItemToppingVAT = 0;
		ItemToppingNET = 0;
		ItemToppingVatableAmount = 0;
		ItemDesc = "";
		ItemMinusToppingDescH1 = "";
		ItemPlusToppingDescH1 = "";
		ItemMinusToppingDescH2 = "";
		ItemPlusToppingDescH2 = "";
		mOptionUseStdHHPrice = false;
		mOptionIncToppingsInHHPrice = false;
		mOptionAddVATToPrice = false;
		ItemFreeText = "";
		mOverridePrice = false;
		ItemSplitQty = 0;
		ItemPrintedQty = 0;
		ItemSplitPrintedQty = 0;
		mHappyHour = false;
	}
	
	public final void SetModelHelper(ModelHelper in_ModelHelper)
	{
		mModelHelper = in_ModelHelper;
	}	

	public final void SetItemOptions(boolean in_StdHHPrice, boolean in_IncToppings, boolean in_AddVATToPrice)
	{
		mOptionUseStdHHPrice = in_StdHHPrice;
		mOptionIncToppingsInHHPrice = in_IncToppings;			
		mOptionAddVATToPrice = in_AddVATToPrice;
	}
	
	public final void SetHappyHour(boolean in_Value)
	{
		mHappyHour = in_Value;
	}	

	public final int GetItemID()
	{
		return ItemID;
	}

	/*
	public final ITEM_TYPE GetItemType()
	{
		return ItemType;
	}
	*/
	public final String GetItemFreeText()
	{
		return ItemFreeText;
	}	

	public final void SetItemFreeText(String in_FreeText)
	{
		ItemFreeText = in_FreeText;
	}
	
	public final void SetItemQty(double in_Qty)
	{
		ItemSplitQty += in_Qty-ItemQty;
		ItemQty = in_Qty;
		CalcItemTotal();		
	}	
	
	public final void SetItemSplitQty(double in_Qty)
	{
		ItemSplitQty = in_Qty;
	}	
	
	public final void SetItemPrintedQty(double in_Qty)
	{
		ItemPrintedQty = in_Qty;
	}	
	public final void SetItemSplitPrintedQty(double in_Qty)
	{
		ItemSplitPrintedQty = in_Qty;
	}		
	
	public final void SetItemPrice(double in_Price)
	{
		ItemPrice = in_Price;
		mOverridePrice = true;
	}	
	
	public final String GetItemDesc()
	{
		return ItemDesc;
	}

	public final String GetItemMinusToppingDescH1()
	{
		return ItemMinusToppingDescH1;
	}

	public final String GetItemPlusToppingDescH1()
	{
		return ItemPlusToppingDescH1;
	}

	public final String GetItemMinusToppingDescH2()
	{
		return ItemMinusToppingDescH2;
	}

	public final String GetItemPlusToppingDescH2()
	{
		return ItemPlusToppingDescH2;
	}

	public final int GetItemVATCode()
	{
		return ItemVATCode;
	}

	public final double GetItemVATRate()
	{
		return ItemVATRate;
	}
	
	public final double GetItemSplitQty()
	{
		return ItemSplitQty;
	}
	
	public final double GetItemSplitPrintedQty()
	{
		return ItemSplitPrintedQty;
	}
	
	public final double GetItemPrintedQty()
	{
		return ItemPrintedQty;
	}

	public final double GetItemQty()
	{
		return ItemQty;
	}

	public final double GetItemPrice()
	{
		return ItemPrice;
	}

	public final double GetItemNET()
	{
		return ItemNET;
	}

	public final double GetItemVAT()
	{
		return ItemVAT;
	}

	public final double GetItemToppingNET()
	{
		return ItemToppingNET;
	}

	public final double GetItemToppingVAT()
	{
		return ItemToppingVAT;
	}

	public final double GetItemVatableAmount()
	{
		return ItemVatableAmount;
	}

	public final double GetItemToppingVatableAmount()
	{
		return ItemToppingVatableAmount;
	}

	public final double GetItemTotal()
	{
		if (mOptionAddVATToPrice)
		{
			return ItemNET;
		}
		else
		{
			return ItemNET + ItemVAT;
		}
	}

	public final double GetItemToppingCharge()
	{
		if (mOptionAddVATToPrice)
		{
			return ItemToppingNET;
		}
		else
		{
			return ItemToppingNET + ItemToppingVAT;
		}
		//return ItemToppingChargeTotal;
	}

	public final boolean GetItemHalfHalf()
	{
		if (ItemType == ITEM_TYPE.PT_HH)
		{
			return true;
		}

		return false;
	}

	public final TillProduct GetProduct1()
	{
		return ProductH1;
	}

	public final TillProduct GetProduct2()
	{
		return ProductH2;
	}

	public final TillProduct GetProductHalfHalf()
	{
		return ProductHalfHalf;
	}

	public final TillProductOffer GetProductOffer()
	{
		return ProductOfferItem;
	}

	public final boolean GetItemOffer()
	{
		return IsOffer;
	}

	public boolean DecreaseItemSplitQty()
	{
		boolean mSetPrintQty = false;
	    ItemSplitQty -= 1;
	    
	    if(ItemSplitQty < ItemPrintedQty )
	    {
	    	ItemPrintedQty -= 1;
		    ItemSplitPrintedQty += 1;
		    mSetPrintQty = true;
	    }                   
	            
	    return mSetPrintQty;       
	} 
	  
	public void IncreaseProdSplitQty(boolean in_UpdatePrintedQty)
	{
		ItemSplitQty += 1;   
	    
	    if( in_UpdatePrintedQty )
	    {
	    	ItemPrintedQty += 1;             
	    	ItemSplitPrintedQty -= 1;                
	    }	   
	} 

	/*
	public final void SetProduct(int in_ID, double in_Qty, TillProduct in_Product1, TillProduct in_Product2, TillProduct in_ProductHalfHalf, boolean in_HalfHalf)
	{
		SetProduct(in_ID, in_Qty, in_Product1, in_Product2, in_ProductHalfHalf, in_HalfHalf, false);
	}
	*/

	public final void SetProduct(int in_ID, double in_Qty, TillProduct in_Product1, TillProduct in_Product2, TillProduct in_ProductHalfHalf, boolean in_HalfHalf, boolean in_Loading)
	{
		ItemID = in_ID;
		if (in_HalfHalf)
		{
			ItemType = ITEM_TYPE.PT_HH;
		}
		else
		{
			ItemType = ITEM_TYPE.PT_STD;
		}
		ItemQty = in_Qty;
		ItemSplitQty = in_Qty;
		SetProduct1(in_Product1);
		SetProduct2(in_Product2);
		SetProductHalfHalf(in_ProductHalfHalf);
		if (!in_Loading)
		{
			UpdateItemDetails();
		}
	}

	public final void SetProduct1(TillProduct in_Product)
	{
		if (in_Product != null)
		{
			ProductH1 = new TillProduct();
			ProductH1.SetModelHelper(mModelHelper);
			ProductH1.SetProductOptions(mOptionAddVATToPrice);
			ProductH1.CopyProduct(in_Product);
		}
	}
	public final void SetProduct2(TillProduct in_Product)
	{
		if (in_Product != null)
		{
			ProductH2 = new TillProduct();
			//ProductH2.SetModelHelper(_ModelHelper);
			ProductH2.SetProductOptions(mOptionAddVATToPrice);
			ProductH2.CopyProduct(in_Product);
		}
	}

	public final void SetProductHalfHalf(TillProduct in_Product)
	{
		if (in_Product != null)
		{
			ProductHalfHalf = new TillProduct();
			ProductHalfHalf.CopyProduct(in_Product);
		}
	}

	public final void SetProductOffer(int in_ID, double in_Qty, TillProductOffer in_Offer, boolean in_LoadingOffer)
	{
		ItemID = in_ID;
		ItemQty = in_Qty;
		ItemSplitQty = in_Qty;
		ItemType = ITEM_TYPE.PT_OFFER;
		if (in_Offer != null)
		{
			IsOffer = true;
			ProductOfferItem = new TillProductOffer(in_Offer.GetPOCode());
			ProductOfferItem.CopyOffer(in_Offer,in_LoadingOffer);
			ItemDesc = ProductOfferItem.GetPODesc();;
			ItemPrice = ProductOfferItem.GetPOPrice();
			UpdateItemDetails();
		}
	}

	public final void UpdateItemDetails()
	{
		if (!IsOffer)
		{
			boolean bUpdate = true;
			if (ProductH1 == null)
			{
				bUpdate = false;
			}
/*
			if (ItemType == ITEM_TYPE.PT_HH)
			{
				if (ProductH2 == null)
				{
					bUpdate = false;
				}
			}*/

			if (bUpdate)
			{
				GenerateItemDesc();
				GenerateItemToppingDescH1();
				GenerateItemToppingDescH2();
				//ProductH1.ProductToppingCharges = 0;
				ProductH1.CalculateItemToppingCharge();
				if (ItemType == ITEM_TYPE.PT_HH)
				{
					//ProductH2.ProductToppingCharges = 0;
					ProductH2.CalculateItemToppingCharge();
				}
				CalcItemPrice();
			}
		}
		else
		{
			//ProductH1.ProductToppingCharges = 0;
			//ProductH1.CalculateItemToppingCharge();
			ItemToppingCharge = ProductOfferItem.GetOfferToppingCharges();
		}
		CalcItemTotal();
	}

	private void CalcItemPrice()
	{
		//OptionUseStdHHPrice = false;
		//OptionIncToppingsInHHPrice = false;
		if( !mOverridePrice )
		{
			
			double H1Price = 0;
			double H2Price = 0;
			double H1TopCharge = 0;
			double H2TopCharge = 0;
			
	
			ItemPrice = ProductH1.ProductPrice;
			
			if( mHappyHour &&  ProductH1.ProductPriceHH > -1 )
			{
				ItemPrice = ProductH1.ProductPriceHH;				
			}

			ItemVatableAmount = ProductH1.ProductVatableAmount;
			ItemVATCode = ProductH1.ProductVATCode;
	
			ItemToppingVAT = ProductH1.ProductToppingVAT;
			ItemToppingCharge = ProductH1.ProductToppingCharges;
			ItemToppingVatableAmount = ProductH1.ProductToppingVatableAmount;
	
			
			
			if (ItemType == ITEM_TYPE.PT_HH)
			{
				H1Price = ProductH1.ProductPrice;
				H1TopCharge = ItemToppingCharge;
	
				H2Price = ProductH2.ProductPrice;
				H2TopCharge = ProductH2.ProductToppingCharges;
	
				ItemToppingCharge = H1TopCharge + H2TopCharge;
				ItemToppingVAT += ProductH2.ProductToppingVAT;
	
				if (mOptionUseStdHHPrice)
				{
					H1Price = ProductHalfHalf.ProductPrice;
					H2Price = ProductHalfHalf.ProductPrice;
					ItemVATCode = ProductHalfHalf.ProductVATCode;
					ItemVatableAmount = ProductHalfHalf.ProductVatableAmount;
					ItemToppingVatableAmount = ProductHalfHalf.ProductToppingVatableAmount;
				}
	
				if (mOptionIncToppingsInHHPrice)
				{
					H1Price += H1TopCharge;
					H2Price += H2TopCharge;
					ItemToppingCharge = 0;
				}
	
				ItemPrice = H1Price;
				if (H2Price > H1Price)
				{
					ItemPrice = H2Price;
					ItemVATCode = ProductH2.ProductVATCode;
					ItemVatableAmount = ProductH2.ProductVatableAmount;
					ItemToppingVatableAmount = ProductH2.ProductToppingVatableAmount;
				}
			}
			
		}
	}

	private void CalcItemTotal()
	{
		CalculateItemVAT();
		double TmpItemPrice = ItemPrice;
		double TmpItemToppingCharge = ItemToppingCharge;
		if (!mOptionAddVATToPrice)
		{
			TmpItemPrice -= ItemVAT;
			TmpItemToppingCharge -= ItemToppingVAT;
		}
		ItemNET = ItemQty * TmpItemPrice;
		ItemToppingNET = ItemQty * TmpItemToppingCharge;

		//ItemToppingChargeTotal = ItemQty * ItemToppingCharge;            
		// TODO: Option Inc topping price
		//ItemNET = ItemQty * (ProductH1.ProductPrice + ProductH1.ProductToppingCharges);

	}

	private void CalculateItemVAT()
	{
		ItemVAT = 0;
		ItemVATRate = 0;
		
		if( mModelHelper != null )
		{		
			double VatableAmt = 0;
			if (ItemVATCode > 0)
			{
				if (ItemVatableAmount > 0)
				{
					VatableAmt = ItemVatableAmount;
				}
				else
				{
					VatableAmt = ItemPrice;
				}
				int VATCalcType = 1;
				if (mOptionAddVATToPrice)
				{
					VATCalcType = 0;
				}
	
				VatableAmt = ItemQty * VatableAmt;
				ArrayList<Double> retVal = mModelHelper.CalcVAT(VatableAmt, 1, VATCalcType);
				
				ItemVATRate = retVal.get(0);			
				ItemVAT = retVal.get(1);
			}		
		}
	}

	private void GenerateItemDesc()
	{
		// TODO
		if (ItemType == ITEM_TYPE.PT_HH)
		{
			ItemDesc = ProductHalfHalf.ProductDesc;
		}
		else if (ItemType == ITEM_TYPE.PT_OFFER || ItemType == ITEM_TYPE.PT_SPENDOFFER)
		{

		}
		else
		{
			
			//ItemDesc = ProductH1.ProductDesc;
			
			ItemDesc = ProductH1.GenerateProductDesc();
			
			/*
			if( ProductH1.ProductOptions != null )
			{
		        ArrayList<TillProductOptionsButGroup> tmpButGroupList;
		        ArrayList<TillProductOptionsButGroupButton> tmpButtonList;
		        String optionsDesc = "";
		        tmpButGroupList = ProductH1.ProductOptions.GetButGroups();
		        
	    		for (TillProductOptionsButGroup butGroup : tmpButGroupList)        
	            {
	                if (butGroup.GetButGroupPartOfProduct() == 0)	
	                {
	                	if( optionsDesc == "" )
	                	{
		                	optionsDesc += " with ";	                		
	                	}
	                	else
	                	{
	                		optionsDesc += " and ";		                		
	                	}
	                	
	                	tmpButtonList = butGroup.GetButtons();
                        for (TillProductOptionsButGroupButton optionBut : tmpButtonList) 
                        {
                            if (optionBut.GetButSelected() == 1)
                            {
                            	optionsDesc += optionBut.GetButtonDesc();
                                break;
                            }
                        }	                	
	                }
	            }
	    		ItemDesc += optionsDesc;
			}
			*/
		}
	}

	private void GenerateItemToppingDescH1()
	{
		
		if (ProductH1 != null && ProductH1.ProductToppingsList != null)
		{
			ProductH1.GenerateProductToppingDesc();
			ItemMinusToppingDescH1 = ProductH1.GetProductMinusToppingDesc();
			ItemPlusToppingDescH1 = ProductH1.GetProductPlusToppingDesc();
		}
	}

	private void GenerateItemToppingDescH2()
	{
		/*
		if (ProductH2 != null && ProductH2.ProductToppingsList != null)
		{
			ProductH2.GenerateProductToppingDesc();
			ItemMinusToppingDescH2 = ProductH2.GetProductMinusToppingDesc();
			ItemPlusToppingDescH2 = ProductH2.GetProductPlusToppingDesc();
		}
		*/
	}
/*
	public final void SetTopping(TillToppings in_Topping, boolean in_Half2)
	{
		if (in_Half2)
		{
			if (ProductH2 != null)
			{
				ProductH2.AddTopping(in_Topping, in_Topping.ToppingState);
			}
		}
		else
		{
			if (ProductH1 != null)
			{
				ProductH1.AddTopping(in_Topping, in_Topping.ToppingState);
			}
		}
	}
	*/



	/*
	public void CalculateItemToppingCharge(bool in_Half2)
	{
	    int VatableToppings = 0;
	    int MeatMax = 0;
	    int VegMax = 0;
	    int MeatCount = 0;
	    int VegCount = 0;
	    int FreeTop = 0;

	    double TPStdPrice1, TPHHPrice1;
	    double TPStdPrice2, TPHHPrice2;
	    double TPStdPrice3, TPHHPrice3;
	    double TPStdPrice4, TPHHPrice4;
	    double TPStdPrice5, TPHHPrice5;
	    double TPStdPrice6, TPHHPrice6;
	    double TPStdPrice7, TPHHPrice7;
	    double TPStdPrice8, TPHHPrice8;
	    int TP3Count, TP4Count, TP5Count, TP6Count, TP7Count, TP8Count;
	    double ToppingCharge = 0;

	    TPStdPrice1 = TPStdPrice2 = TPStdPrice3 = TPStdPrice4 = TPStdPrice5 = TPStdPrice6 = TPStdPrice7 = TPStdPrice8 = 0;
	    TPHHPrice1 = TPHHPrice2 = TPHHPrice3 = TPHHPrice4 = TPHHPrice5 = TPHHPrice6 = TPHHPrice7 = TPHHPrice8 = 0;
	    TP3Count = TP4Count = TP5Count = TP6Count = TP7Count = TP8Count = 0;

	    List<TillToppings> TmpTopList;
	    List<TillToppingPrice> TmpTopPriceList;
	    if (in_Half2)
	    {
	        TmpTopPriceList = ProductH2.ProductToppingPricesList;
	        TmpTopList = ProductH2.ProductToppingsList;
	    }
	    else
	    {
	        TmpTopPriceList = ProductH1.ProductToppingPricesList;
	        TmpTopList = ProductH1.ProductToppingsList;
	    }

	    foreach (TillToppingPrice ProdToppingPrices in TmpTopPriceList)
	    {
	        switch (ProdToppingPrices.TPType)
	        {
	            case 1: TPStdPrice1 = ProdToppingPrices.TPStdPrice; TPHHPrice1 = ProdToppingPrices.TPHappyHourPrice; break;
	            case 2: TPStdPrice2 = ProdToppingPrices.TPStdPrice; TPHHPrice2 = ProdToppingPrices.TPHappyHourPrice; break;
	            case 3: TPStdPrice3 = ProdToppingPrices.TPStdPrice; TPHHPrice3 = ProdToppingPrices.TPHappyHourPrice; break;
	            case 4: TPStdPrice4 = ProdToppingPrices.TPStdPrice; TPHHPrice4 = ProdToppingPrices.TPHappyHourPrice; break;
	            case 5: TPStdPrice5 = ProdToppingPrices.TPStdPrice; TPHHPrice5 = ProdToppingPrices.TPHappyHourPrice; break;
	            case 6: TPStdPrice6 = ProdToppingPrices.TPStdPrice; TPHHPrice6 = ProdToppingPrices.TPHappyHourPrice; break;
	            case 7: TPStdPrice7 = ProdToppingPrices.TPStdPrice; TPHHPrice7 = ProdToppingPrices.TPHappyHourPrice; break;
	            case 8: TPStdPrice8 = ProdToppingPrices.TPStdPrice; TPHHPrice8 = ProdToppingPrices.TPHappyHourPrice; break;
	        }
	    }

	    foreach (TillToppings ProdTopping in TmpTopList)
	    {
	        if (ProdTopping.ToppingState == TOPPING_STATE_CODE.TOP_ADDED)
	        {
	            switch (ProdTopping.ToppingType)    // Keep count of different topping types
	            {
	                case 1: ++VegCount; break;
	                case 2: ++MeatCount; break;
	                case 3: ++TP3Count; break;
	                case 4: ++TP4Count; break;
	                case 5: ++TP5Count; break;
	                case 6: ++TP6Count; break;
	                case 7: ++TP7Count; break;
	                case 8: ++TP8Count; break;
	            }
	            if (ProdTopping.ToppingVATCode == 1)
	            {
	                VatableToppings++;
	            }
	        }
	        else if (ProdTopping.ToppingState == TOPPING_STATE_CODE.TOP_FIXED_REMOVED)
	        {
	            switch (ProdTopping.ToppingType)    // Keep count of different topping types
	            {
	                case 1: ++VegMax; --VegCount; break;   // Count Veg/Meat Max
	                case 2: ++MeatMax; --MeatCount; break;
	            }
	        }
	        else if (ProdTopping.ToppingState == TOPPING_STATE_CODE.TOP_FIXED)
	        {
	            switch (ProdTopping.ToppingType)
	            {
	                case 1: ++VegMax; ++VegCount; break;
	                case 2: ++MeatMax; ++MeatCount; break;
	            }
	        }

	        if (MeatCount < 0) MeatCount = 0;
	        if (VegCount < 0) VegCount = 0;

	    }

	    if ((MeatCount <= MeatMax) && (VegCount <= VegMax))
	    {
	        // No charge
	    }
	    else
	    {
	        if (MeatCount > MeatMax)   // Extra meat toppings added
	        {
	            MeatCount = MeatCount - MeatMax;
	            VegCount = VegCount - VegMax;
	        }
	        else
	        {
	            FreeTop = MeatMax - MeatCount;   // Get freetop if meat toppings removed more than those added
	            VegCount = VegCount - VegMax - FreeTop;
	            MeatCount = 0;
	        }

	        if (MeatCount < 0) MeatCount = 0;
	        if (VegCount < 0) VegCount = 0;

	        // Calculate extra topping charge
	        ToppingCharge += TPStdPrice1 * VegCount;
	        ToppingCharge += TPStdPrice2 * MeatCount;
	        ToppingCharge += TPStdPrice3 * TP3Count;
	        ToppingCharge += TPStdPrice4 * TP4Count;
	        ToppingCharge += TPStdPrice5 * TP5Count;
	        ToppingCharge += TPStdPrice6 * TP6Count;
	        ToppingCharge += TPStdPrice7 * TP7Count;
	        ToppingCharge += TPStdPrice8 * TP8Count;

	        int VATCalcType = 1;
	        if (OptionAddVATToPrice)
	            VATCalcType = 0;

	        if (in_Half2)
	        {
	            ProductH2.ProductToppingCharges += ToppingCharge;
	            ProductH2.ProductToppingVatableAmount = TPStdPrice2 * VatableToppings;
	            ProductH2.ProductToppingVAT = _ModelHelper.CalcVAT(ProductH2.ProductToppingVatableAmount, 1, VATCalcType, ref ItemVATRate);
	            ProductH2.ProductToppingNET = ProductH2.ProductToppingCharges - ProductH2.ProductToppingVAT;
	        }
	        else
	        {
	            ProductH1.ProductToppingCharges += ToppingCharge;
	            ProductH1.ProductToppingVatableAmount = TPStdPrice2 * VatableToppings;
	            ProductH1.ProductToppingVAT = _ModelHelper.CalcVAT(ProductH1.ProductToppingVatableAmount, 1, VATCalcType, ref ItemVATRate);
	            ProductH1.ProductToppingNET = ProductH1.ProductToppingCharges - ProductH1.ProductToppingVAT;
	        }
	    }
	}
	 */ 
}