﻿package com.prophet.eposapp.models;

import java.util.ArrayList;

public class TillProduct
{
	private ModelHelper mModelHelper;
	private boolean OptionAddVATToPrice;

	public String ProductCode;
	public String ProductDesc;
	public double ProductPrice;
	public double ProductPriceHH;
	public double ProductFixedPrice;
	public int ProductVATCode;
	public double ProductVATRate;
	public double ProductVatableAmount;
	public double ProductVatableHHAmount;
	public double ProductToppingNET;
	public double ProductToppingVAT;
	public TillProductOptions ProductOptions;
	public ArrayList<TillToppings> ProductToppingsList;
	public ArrayList<TillToppingPrice> ProductToppingPricesList;
	public String ProductMinusToppingDesc;
	public String ProductPlusToppingDesc;
	public int ProductOptionsID;
	public int ProductToppingsID;
	public int ProductTopType;
	public int ProductClassID;
	public int ProductParentClassID;
	public String ProductParentCode;
	public String ProductParentDesc;

	public double ProductSubPrice;
	public double ProductSubPriceHH;

	public double ProductToppingCharges;
	public double ProductToppingVatableAmount;

	public boolean ProductOffer;
	
	public int ProductSectionPos;
	public String ProductSectionDesc;
	
	public int ProductButG1ID;
	public int ProductButG2ID;
	
	public int ProductTopBut1;
	public int ProductTopBut2;
	
	public int PrintToKitchen;
	
	public TillProduct()
	{
		ProductOptions = new TillProductOptions();
		ProductToppingsList = new ArrayList<TillToppings>();
		ProductToppingPricesList = new java.util.ArrayList<TillToppingPrice>();
		ClearProduct();
	}

	
	public final void SetModelHelper(ModelHelper in_ModelHelper)
	{
		mModelHelper = in_ModelHelper;
	}	

	public final void SetProductOptions(boolean in_AddVATToPrice)
	{
		OptionAddVATToPrice = in_AddVATToPrice;
	}

	public final void ClearProduct()
	{
		ProductCode = "";
		ProductDesc = "";
		ProductPrice = 0;
		ProductPriceHH = 0;
		ProductVATCode = 0;
		ProductVATRate = 0;
		ProductVatableAmount = 0;
		ProductVatableHHAmount = 0;
		ProductOptionsID = 0;
		ProductToppingsID = 0;
		ProductToppingNET = 0;
		ProductToppingVAT = 0;
		ProductToppingCharges = 0;
		ProductToppingVatableAmount = 0;
		ProductTopType = 0;
		ProductClassID = 0;
		ProductParentClassID = 0;
		ProductParentCode = "";
		ProductParentDesc = "";
		ProductOffer = false;
		ProductOptions.ClearOptions();
		ProductToppingsList.clear();
		ProductToppingPricesList.clear();
		ProductMinusToppingDesc = "";
		ProductPlusToppingDesc = "";
		ProductButG1ID = 0;
		ProductButG2ID = 0;
		ProductTopBut1 = 0;
		ProductTopBut2 = 0;
		PrintToKitchen = 1;
	}

	public final void CopyProduct(TillProduct in_ProductToCopy)
	{
		mModelHelper = in_ProductToCopy.mModelHelper;
		OptionAddVATToPrice = in_ProductToCopy.OptionAddVATToPrice;
		ProductCode = in_ProductToCopy.ProductCode;
		ProductDesc = in_ProductToCopy.ProductDesc;
		ProductPrice = in_ProductToCopy.ProductPrice;
		ProductPriceHH = in_ProductToCopy.ProductPriceHH;
		ProductVATCode = in_ProductToCopy.ProductVATCode;
		ProductVATRate = in_ProductToCopy.ProductVATRate;
		ProductToppingNET = in_ProductToCopy.ProductToppingNET;
		ProductToppingVAT = in_ProductToCopy.ProductToppingVAT;
		ProductVatableAmount = in_ProductToCopy.ProductVatableAmount;
		ProductVatableHHAmount = in_ProductToCopy.ProductVatableHHAmount;
		ProductOptionsID = in_ProductToCopy.ProductOptionsID;
		ProductToppingsID = in_ProductToCopy.ProductToppingsID;
		ProductTopType = in_ProductToCopy.ProductTopType;
		ProductClassID = in_ProductToCopy.ProductClassID;
		ProductParentClassID = in_ProductToCopy.ProductParentClassID;
		ProductParentCode = in_ProductToCopy.ProductParentCode;
		ProductParentDesc = in_ProductToCopy.ProductParentDesc;
		ProductMinusToppingDesc = in_ProductToCopy.ProductMinusToppingDesc;
		ProductPlusToppingDesc = in_ProductToCopy.ProductPlusToppingDesc;
		ProductSectionPos = in_ProductToCopy.ProductSectionPos;
		ProductSectionDesc = in_ProductToCopy.ProductSectionDesc;
		ProductButG1ID = in_ProductToCopy.ProductButG1ID;
		ProductButG2ID = in_ProductToCopy.ProductButG2ID;
		ProductTopBut1 = in_ProductToCopy.ProductTopBut1;
		ProductTopBut2 = in_ProductToCopy.ProductTopBut2;
		PrintToKitchen = in_ProductToCopy.PrintToKitchen;
		ProductToppingCharges = in_ProductToCopy.ProductToppingCharges;
		ProductToppingVatableAmount = in_ProductToCopy.ProductToppingVatableAmount;

		if (in_ProductToCopy.ProductOptions != null)
		{
			ProductOptions.CopyProductOptions(in_ProductToCopy.ProductOptions);
		}

		
		if (in_ProductToCopy.ProductToppingsList != null)
		{
			CopyProductToppings(in_ProductToCopy.ProductToppingsList);
		}

		
		if (in_ProductToCopy.ProductToppingPricesList != null)
		{
			CopyProductToppingPrices(in_ProductToCopy.ProductToppingPricesList);
		}
		
	}

	
	public final void CopyProductToppings(java.util.ArrayList<TillToppings> in_ProductToppingsToCopy)
	{
		ProductToppingsList.clear();
		for (TillToppings ProdTopping : in_ProductToppingsToCopy)
		{
			AddTopping(ProdTopping, ProdTopping.ToppingState, false);
		}
	}

	public final void AddTopping(TillToppings in_Topping, TOPPING_STATE_CODE in_TopState, boolean in_AddPrefix)
	{
		TillToppings NewTopping = new TillToppings();
		NewTopping.ToppingID = ProductToppingsList.size() + 1;
		NewTopping.ToppingCode = in_Topping.ToppingCode;			
		NewTopping.ToppingDesc = in_Topping.ToppingDesc;
		if( in_AddPrefix )
			NewTopping.ToppingDesc = "+"+NewTopping.ToppingDesc;
		NewTopping.ToppingType = in_Topping.ToppingType;
		NewTopping.ToppingVATCode = in_Topping.ToppingVATCode;
		NewTopping.ToppingState = in_TopState;
		NewTopping.ToppingFixedPrice = in_Topping.ToppingFixedPrice;
		ProductToppingsList.add(NewTopping);
	}

	public final void RemoveTopping(int in_TopID)
	{
		for (TillToppings topDetails : ProductToppingsList)        
        {
			if (topDetails.ToppingID == in_TopID)
			{
				if (topDetails.ToppingState == TOPPING_STATE_CODE.TOP_FIXED)
				{
					topDetails.ToppingState = TOPPING_STATE_CODE.TOP_FIXED_REMOVED;
					topDetails.ToppingDesc = "-"+topDetails.ToppingDesc;
				}
				else if (topDetails.ToppingState == TOPPING_STATE_CODE.TOP_FIXED_REMOVED)
				{					
					topDetails.ToppingDesc = topDetails.ToppingDesc.substring(1);
					topDetails.ToppingState = TOPPING_STATE_CODE.TOP_FIXED;
				}
				else if (topDetails.ToppingState == TOPPING_STATE_CODE.TOP_ADDED)
				{
					ProductToppingsList.remove(topDetails);
				}
				break;
			}			
        }
	}

	public final void CopyProductToppingPrices(ArrayList<TillToppingPrice> in_ProductToppingPricesToCopy)
	{
		ProductToppingPricesList.clear();
		for (TillToppingPrice ProdToppingPrices : in_ProductToppingPricesToCopy)
		{
			TillToppingPrice NewToppingPrice = new TillToppingPrice();
			NewToppingPrice.TPType = ProdToppingPrices.TPType;
			NewToppingPrice.TPStdPrice = ProdToppingPrices.TPStdPrice;
			NewToppingPrice.TPHappyHourPrice = ProdToppingPrices.TPHappyHourPrice;
			ProductToppingPricesList.add(NewToppingPrice);
		}
	}	

	public final void GenerateProductToppingDesc()
	{
		ProductMinusToppingDesc = "";
		ProductPlusToppingDesc = "";
		if (ProductToppingsList != null)
		{
			for (TillToppings ProdTopping : ProductToppingsList)
			{
				if (ProdTopping.ToppingState == TOPPING_STATE_CODE.TOP_FIXED_REMOVED)
				{
					if (ProductMinusToppingDesc.compareTo("") != 0)
					{
						ProductMinusToppingDesc += ", ";
					}
					//ProductMinusToppingDesc += "-" + ProdTopping.ToppingDesc;
					ProductMinusToppingDesc += ProdTopping.ToppingDesc;
				}
				else if (ProdTopping.ToppingState == TOPPING_STATE_CODE.TOP_ADDED)
				{
					if (ProductPlusToppingDesc.compareTo("") != 0)
					{
						ProductPlusToppingDesc += ", ";
					}
					//ProductPlusToppingDesc += "+" + ProdTopping.ToppingDesc;
					ProductPlusToppingDesc += ProdTopping.ToppingDesc;
				}
			}
		}
	}
	
	public final String GenerateProductDesc()
	{
		String TmpProdDesc = ProductDesc;
		if( ProductOptions != null )
		{
	        ArrayList<TillProductOptionsButGroup> tmpButGroupList;
	        ArrayList<TillProductOptionsButGroupButton> tmpButtonList;
	        String optionsDesc = "";
	        tmpButGroupList = ProductOptions.GetButGroups();
	        
    		for (TillProductOptionsButGroup butGroup : tmpButGroupList)        
            {
                if (butGroup.GetButGroupPartOfProduct() == 0)	
                {
                	if( optionsDesc == "" )
                	{
	                	optionsDesc += " with ";	                		
                	}
                	else
                	{
                		optionsDesc += " and ";		                		
                	}
                	
                	tmpButtonList = butGroup.GetButtons();
                    for (TillProductOptionsButGroupButton optionBut : tmpButtonList) 
                    {
                        if (optionBut.GetButSelected() == 1)
                        {
                        	optionsDesc += optionBut.GetButtonDesc();
                            break;
                        }
                    }	                	
                }
            }
    		TmpProdDesc += optionsDesc;
		}
		ProductDesc = TmpProdDesc;
		return ProductDesc;
	}

	public final String GetProductMinusToppingDesc()
	{
		return ProductMinusToppingDesc;
	}

	public final String GetProductPlusToppingDesc()
	{
		return ProductPlusToppingDesc;
	}

	public final void CalculateItemToppingCharge()
	{
		
		int VatableToppings = 0;
		int MeatMax = 0;
		int VegMax = 0;
		int MeatCount = 0;
		int VegCount = 0;
		int FreeTop = 0;

		double TPStdPrice1;
		double TPStdPrice2;
		double TPStdPrice3;
		double TPStdPrice4;
		double TPStdPrice5;
		double TPStdPrice6;
		double TPStdPrice7;
		double TPStdPrice8;		
		/*
		double TPStdPrice1, TPHHPrice1;
		double TPStdPrice2, TPHHPrice2;
		double TPStdPrice3, TPHHPrice3;
		double TPStdPrice4, TPHHPrice4;
		double TPStdPrice5, TPHHPrice5;
		double TPStdPrice6, TPHHPrice6;
		double TPStdPrice7, TPHHPrice7;
		double TPStdPrice8, TPHHPrice8;
		*/
		
		int TP3Count, TP4Count, TP5Count, TP6Count, TP7Count, TP8Count;
		double ToppingCharge = 0;

		TPStdPrice1 = TPStdPrice2 = TPStdPrice3 = TPStdPrice4 = TPStdPrice5 = TPStdPrice6 = TPStdPrice7 = TPStdPrice8 = 0;
		//TPHHPrice1 = TPHHPrice2 = TPHHPrice3 = TPHHPrice4 = TPHHPrice5 = TPHHPrice6 = TPHHPrice7 = TPHHPrice8 = 0;
		TP3Count = TP4Count = TP5Count = TP6Count = TP7Count = TP8Count = 0;

		for (TillToppingPrice ProdToppingPrices : ProductToppingPricesList)
		{
			switch (ProdToppingPrices.TPType)
			{
				case 1:
					TPStdPrice1 = ProdToppingPrices.TPStdPrice;
					//TPHHPrice1 = ProdToppingPrices.TPHappyHourPrice;
					break;
				case 2:
					TPStdPrice2 = ProdToppingPrices.TPStdPrice;
					//TPHHPrice2 = ProdToppingPrices.TPHappyHourPrice;
					break;
				case 3:
					TPStdPrice3 = ProdToppingPrices.TPStdPrice;
					//TPHHPrice3 = ProdToppingPrices.TPHappyHourPrice;
					break;
				case 4:
					TPStdPrice4 = ProdToppingPrices.TPStdPrice;
					//TPHHPrice4 = ProdToppingPrices.TPHappyHourPrice;
					break;
				case 5:
					TPStdPrice5 = ProdToppingPrices.TPStdPrice;
					//TPHHPrice5 = ProdToppingPrices.TPHappyHourPrice;
					break;
				case 6:
					TPStdPrice6 = ProdToppingPrices.TPStdPrice;
					//TPHHPrice6 = ProdToppingPrices.TPHappyHourPrice;
					break;
				case 7:
					TPStdPrice7 = ProdToppingPrices.TPStdPrice;
					//TPHHPrice7 = ProdToppingPrices.TPHappyHourPrice;
					break;
				case 8:
					TPStdPrice8 = ProdToppingPrices.TPStdPrice;
					//TPHHPrice8 = ProdToppingPrices.TPHappyHourPrice;
					break;
			}
		}
		

		
		for (TillToppings ProdTopping : ProductToppingsList)
		{
			if( ProdTopping.ToppingFixedPrice >= 0 )
			{
				ToppingCharge += ProdTopping.ToppingFixedPrice;				
			}
			else
			{
				if (ProdTopping.ToppingState == TOPPING_STATE_CODE.TOP_ADDED  )
				{
					switch (ProdTopping.ToppingType) // Keep count of different topping types
					{
						case 1:
							++VegCount;
							break;
						case 2:
							++MeatCount;
							break;
						case 3:
							++TP3Count;
							break;
						case 4:
							++TP4Count;
							break;
						case 5:
							++TP5Count;
							break;
						case 6:
							++TP6Count;
							break;
						case 7:
							++TP7Count;
							break;
						case 8:
							++TP8Count;
							break;
					}
					if (ProdTopping.ToppingVATCode == 1)
					{
						VatableToppings++;
					}
				}
				else if (ProdTopping.ToppingState == TOPPING_STATE_CODE.TOP_FIXED_REMOVED)
				{
					switch (ProdTopping.ToppingType) // Keep count of different topping types
					{
						case 1: // Count Veg/Meat Max
							++VegMax;
							--VegCount;
							break;
						case 2:
							++MeatMax;
							--MeatCount;
							break;
					}
				}
				else if (ProdTopping.ToppingState == TOPPING_STATE_CODE.TOP_FIXED)
				{
					switch (ProdTopping.ToppingType)
					{
						case 1:
							++VegMax;
							++VegCount;
							break;
						case 2:
							++MeatMax;
							++MeatCount;
							break;
					}
				}
	
				if (MeatCount < 0)
				{
					MeatCount = 0;
				}
				if (VegCount < 0)
				{
					VegCount = 0;
				}
			}

		}
		
		ProductToppingCharges += ToppingCharge; // Add Fixed Topping charges

		if ((MeatCount <= MeatMax) && (VegCount <= VegMax))
		{
			// No charge
		}
		else
		{
			if (MeatCount > MeatMax) // Extra meat toppings added
			{
				MeatCount = MeatCount - MeatMax;
				VegCount = VegCount - VegMax;
			}
			else
			{
				FreeTop = MeatMax - MeatCount; // Get freetop if meat toppings removed more than those added
				VegCount = VegCount - VegMax - FreeTop;
				MeatCount = 0;
			}

			if (MeatCount < 0)
			{
				MeatCount = 0;
			}
			if (VegCount < 0)
			{
				VegCount = 0;
			}

			// Calculate extra topping charge
			ToppingCharge += TPStdPrice1 * VegCount;
			ToppingCharge += TPStdPrice2 * MeatCount;
			ToppingCharge += TPStdPrice3 * TP3Count;
			ToppingCharge += TPStdPrice4 * TP4Count;
			ToppingCharge += TPStdPrice5 * TP5Count;
			ToppingCharge += TPStdPrice6 * TP6Count;
			ToppingCharge += TPStdPrice7 * TP7Count;
			ToppingCharge += TPStdPrice8 * TP8Count;

			int VATCalcType = 1;
			if (OptionAddVATToPrice)
			{
				VATCalcType = 0;
			}

			//double TmpVATRate = 0;
			ProductToppingCharges += ToppingCharge;
			ProductToppingVatableAmount = TPStdPrice2 * VatableToppings;
			ProductToppingVAT = 0;
			
			if (mModelHelper != null)
			{
				ArrayList<Double> retVal = mModelHelper.CalcVAT(ProductToppingVatableAmount, 1, VATCalcType);	
				ProductToppingVAT = retVal.get(1);
			}
			
			ProductToppingNET = ProductToppingCharges - ProductToppingVAT;
		}
		
		/*
		ProductToppingCharges = 0;
		ProductToppingVatableAmount = 0;
		ProductToppingNET = 0;
		ProductToppingVAT = 0;
		*/
	}
}