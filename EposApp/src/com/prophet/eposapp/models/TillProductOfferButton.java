package com.prophet.eposapp.models;

public class TillProductOfferButton 
{
	
	public String mButDesc;
	public OFFER_ITEM_TYPE mButParentType;
	public OFFER_ITEM_TYPE mButType;
	public int mButAction;
	public String mButProdCode;
	public String mButProdDesc;
	public int mButProdToppingID;
	public int mButProdCatID;
	public boolean mButProdSelected;
	public int mButGroupID;
	public int mButItemID;

	public TillProduct mProduct;
}
