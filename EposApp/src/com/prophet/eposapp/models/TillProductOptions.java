﻿package com.prophet.eposapp.models;

public class TillProductOptions
{
	private int ProdOptionID;
	private boolean ProdOptionHalfHalf;
	private String ProdOptionName;

	private java.util.ArrayList<TillProductOptionsButGroup> ProdOptionsButGroupList;

	public TillProductOptions()
	{
		ProdOptionsButGroupList = new java.util.ArrayList<TillProductOptionsButGroup>();
		ClearOptions();
	}

	public final void ClearOptions()
	{
		ProdOptionID = 0;
		ProdOptionHalfHalf = false;
		ProdOptionName = "";
		ProdOptionsButGroupList.clear();
	}

	public final void SetProductOptionsDetails(int in_ID, boolean in_HalfHalf, String in_Name)
	{
		ProdOptionID = in_ID;
		ProdOptionHalfHalf = in_HalfHalf;
		ProdOptionName = in_Name;
	}

	public final void AddButGroup(TillProductOptionsButGroup in_ButGroup)
	{
		ProdOptionsButGroupList.add(in_ButGroup);
	}

	public final java.util.ArrayList<TillProductOptionsButGroup> GetButGroups()
	{
		return ProdOptionsButGroupList;
	}

	public final boolean GetOptionHalfHalf()
	{
		return ProdOptionHalfHalf;
	}

	public final int GetOptionID()
	{
		return ProdOptionID;
	}

	public final void CopyProductOptions(TillProductOptions in_ProductOptionsToCopy)
	{
		SetProductOptionsDetails(in_ProductOptionsToCopy.ProdOptionID, in_ProductOptionsToCopy.ProdOptionHalfHalf, in_ProductOptionsToCopy.ProdOptionName);

		TillProductOptionsButGroup NewProductOptionsButGroup;
		ProdOptionsButGroupList.clear();
		for (TillProductOptionsButGroup ProductOptionsButGroup : in_ProductOptionsToCopy.ProdOptionsButGroupList)
		{
			NewProductOptionsButGroup = new TillProductOptionsButGroup();
			NewProductOptionsButGroup.CopyButGroup(ProductOptionsButGroup);
			AddButGroup(NewProductOptionsButGroup);
		}

	}

}