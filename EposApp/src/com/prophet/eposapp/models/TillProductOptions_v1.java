package com.prophet.eposapp.models;

public class TillProductOptions_v1 
{
	private int ProdOptionID;
	private boolean ProdOptionHalfHalf;
	private String ProdOptionName;
	private int ProdOptionButGroup1;
	private int ProdOptionButGroup2;

	public TillProductOptions_v1()
	{
		ClearOptions();
	}

	public final void ClearOptions()
	{
		ProdOptionID = 0;
		ProdOptionHalfHalf = false;
		ProdOptionName = "";
		ProdOptionButGroup1 = 0;
		ProdOptionButGroup2 = 0;

	}

	public final void SetProductOptionsDetails(int in_ID, int in_BG1, int in_BG2, boolean in_HalfHalf, String in_Name)
	{
		ProdOptionID = in_ID;
		ProdOptionHalfHalf = in_HalfHalf;
		ProdOptionName = in_Name;
		ProdOptionButGroup1 = in_BG1;
		ProdOptionButGroup2 = in_BG2;
	}
	
	public final int GetOptionButG1ID()
	{
		return ProdOptionButGroup1;
	}
	
	public final int GetOptionButG2ID()
	{
		return ProdOptionButGroup2;
	}

	public final boolean GetOptionHalfHalf()
	{
		return ProdOptionHalfHalf;
	}

	public final int GetOptionID()
	{
		return ProdOptionID;
	}

}
