package com.prophet.eposapp.models;

import java.util.ArrayList;

public class TillSplitBill 
{
	int mSplitCount;
	int mSplitID;
	ArrayList<String> mSplitRefList;
	ArrayList<TillSplitItem> mSplitItemList;
	  
	public TillSplitBill()
	{
		mSplitCount = 0;
		mSplitID = 0;
		mSplitRefList = new ArrayList<String>();
		mSplitItemList = new ArrayList<TillSplitItem>();
	}
	
	public void ClearSplitBill()
	{
		mSplitCount = 0;
		mSplitID = 0; 
		mSplitRefList.clear(); 
		mSplitItemList.clear(); 
	}  
	
	public void LoadSplitBill(ArrayList<TillSplitItem> in_SplitBillList)
	{
	    ClearSplitBill();
	    
	    mSplitItemList = in_SplitBillList;
	    
		for( TillSplitItem mSplitItem : in_SplitBillList )
		{				
			if( mSplitItem.mSplitID > mSplitCount )
			{
				AddSplit();
				mSplitRefList.set(mSplitRefList.size()-1, mSplitItem.mSplitRef);   
				/*
				mSplitCount = mSplitItem.mSplitID;
				
				int mSplitRefListCount = mSplitRefList.size();
		        while( mSplitRefListCount < mSplitCount )
		        {
		            AddSplit();
		            mSplitRefListCount = mSplitRefList.size();			            
		        }
		        mSplitRefList.set(mSplitRefList.size()-1, mSplitItem.mSplitRef);   
		        */ 
			}			
		}
	    
	    mSplitID = 1;

	}  
	
	public void NewSplitBill()
	{
		ClearSplitBill();
	    AddSplit();
	}  
	
	public void AddSplit()
	{
		mSplitCount++;
		mSplitID = mSplitCount;
		mSplitRefList.add("Split " + Integer.toString(mSplitID));		
	}  
	
	public void NextSplit()
	{
	    if( mSplitID == mSplitCount )
	    {
	        AddSplit();
	    }
	    else
	    {
	        mSplitID++;
	    }
	} 
	  
	public void PrevSplit()
	{
		if( mSplitID > 1 )
	    {
	        mSplitID--;   
	    }    
	}   

	public String GetSplitBillRef()
	{
		if( mSplitCount > 0 )
		{
			return mSplitRefList.get(mSplitID-1);
		}
		return "";
	}
	
	public int GetSplitBillCount()
	{
		return mSplitCount;
	}
	
	public int GetSplitBillID()
	{
		return mSplitID;		
	}
	
	public void AddSplitItem(int in_ItemID, boolean in_UpdatePrintQty)
	{ 
	    boolean mAddNewSplitItem=true;
	    
	    if( GetSplitBillCount() == 0 )
	    {
	    	AddSplit(); 
	    }
	    
	    int mItemCount = mSplitItemList.size();
	    for( int i=0; i < mItemCount; i++ )
	    {
	    	if( mSplitItemList.get(i).mSplitID == mSplitID && mSplitItemList.get(i).mItemID == in_ItemID )
	    	{
	    		mSplitItemList.get(i).mItemQty += 1;
	            if( in_UpdatePrintQty )
	            	mSplitItemList.get(i).mItemPrintedQty += 1; 
	                
	            mAddNewSplitItem = false;
	            break;
	    	}
	    }
	    
	    if( mAddNewSplitItem )
	    {
	    	TillSplitItem mNewSplitItem = new TillSplitItem();     

	        mNewSplitItem.mSplitID = mSplitID;
	        mNewSplitItem.mSplitRef = GetSplitBillRef(); 
	        mNewSplitItem.mItemID = in_ItemID;
	        mNewSplitItem.mItemQty = 1;
	        mNewSplitItem.mItemPrintedQty = 0;
	        if( in_UpdatePrintQty )
	            mNewSplitItem.mItemPrintedQty = 1;

	        mSplitItemList.add(mNewSplitItem);  
	    }    
	} 
	
	public boolean RemoveSplitItem(int in_ItemID)
	{ 
	    boolean mUpdatePrintedQty=false;

	    int mItemCount = mSplitItemList.size();
	    
	    for( int i=0; i < mItemCount; i++ )
	    {
	    	if( mSplitItemList.get(i).mSplitID == mSplitID && mSplitItemList.get(i).mItemID == in_ItemID )
	    	{	            
	            if( mSplitItemList.get(i).mItemPrintedQty > mSplitItemList.get(i).mItemQty-1 )
	            {
	            	mSplitItemList.get(i).mItemPrintedQty -= 1; 
	                mUpdatePrintedQty=true;
	            }  
	            if( mSplitItemList.get(i).mItemQty == 1 )
	            {    
	            	mSplitItemList.remove(i);          	
	            }
	            else
	            {
	            	mSplitItemList.get(i).mItemQty -= 1;
	            } 	  
	            break;    
	    	}
	    }  
	    return mUpdatePrintedQty;		  
	}
	  
	public void DeleteSplitItem( int in_ItemID)
	{ 
		  
	}
	
	
	public ArrayList<TillSplitItem> GetSplitItemsList()
	{ 
		ArrayList<TillSplitItem> mTmpSplitItems = new ArrayList<TillSplitItem>();
	    
	    int mItemCount = mSplitItemList.size();
	    for( int i=0; i < mItemCount; i++ )
	    {
	    	if( mSplitItemList.get(i).mSplitID == mSplitID )
	    	{
	    		mTmpSplitItems.add(mSplitItemList.get(i));
	    	}            
	    }     
	    return mTmpSplitItems;  
	}
	
	public ArrayList<TillSplitItem> GetAllSplitItemsList()
	{ 
	    return mSplitItemList;  
	}

	public void UpdateSplitPrintedQty()
	{ 
	    int mItemCount = mSplitItemList.size();
	    for( int i=0; i < mItemCount; i++ )
	    {
	    	 mSplitItemList.get(i).mItemPrintedQty = mSplitItemList.get(i).mItemQty; 
	    }
	}  
}
