package com.prophet.eposapp.models;

public class TillTables 
{
	private int mTableID;
	private int mTableNo;
	private String mTableDesc;
	
	private int mTableOrderID;
	private boolean mTableBooked;
	private String mTableOrderAccRef;	
	private int mTableOrderState;
	
	public TillTables()
	{
		ClearTableStatus();			
	}
	
	public int GetTableID() {
		return mTableID;
	}
	
	public void SetTableID(int mTableID) {
		this.mTableID = mTableID;
	}
	
	public int GetTableNo() {
		return mTableNo;
	}
	
	public void SetTableNo(int mTableNo) {
		this.mTableNo = mTableNo;
	}
	
	public String GetTableDesc() {
		return mTableDesc;
	}
	
	public void SetTableDesc(String mTableDesc) {
		this.mTableDesc = mTableDesc;
	}
	
	public void ClearTableStatus() 
	{
		mTableBooked = false;	
		mTableOrderID = 0;
		mTableOrderAccRef = "";	
		mTableOrderState = 0;
	}

	public void SetTableStatus(String in_orderAccRef, int in_orderID, int in_OrderState) 
	{
		mTableBooked = true;
		mTableOrderAccRef = in_orderAccRef;
		mTableOrderID = in_orderID;
		mTableOrderState = in_OrderState;
	}
	
	public boolean GetTableBookedStatus() {
		return mTableBooked;
	}
	
	public String GetTableOrderAccRef() {
		return mTableOrderAccRef;
	}
	
	public int GetTableOrderID() {
		return mTableOrderID;
	}
	
	public int GetTableOrderState() {
		return mTableOrderState;
	}

}
