package com.prophet.eposapp.presenters;

import java.io.Serializable;
import java.util.ArrayList;


import com.prophet.eposapp.interfaces.IMenu;
import com.prophet.eposapp.models.ModelManager;
import com.prophet.eposapp.models.OFFER_ITEM_TYPE;
import com.prophet.eposapp.models.TillProductOfferItem;
import com.prophet.eposapp.models.TillProductOfferItemGroup;
import com.prophet.eposapp.models.TillToppings;

public class PresenterMenu implements Serializable 
{
	private static final long serialVersionUID = 1L;
	ModelManager mModelManager;
	IMenu mIMenu;
	
	public PresenterMenu(ModelManager in_ModelManager,IMenu in_IMenu)
	{
		mIMenu = in_IMenu;
		mModelManager = in_ModelManager;			
	}
	
    public String GetMenuErrorMsg()
    {
        return mModelManager.mModelMenu.GetMenuErrorMsg();
    }
	
	public void LoadMenu() 
    {    
    	mModelManager.mModelMenu.LoadMenu(); 
    	mModelManager.mModelHelper.LoadHelperData();
    	mIMenu.mCategoryList = mModelManager.mModelMenu.GetCategoryList();
    	mIMenu.mFullProductList = mModelManager.mModelMenu.GetProductList();    
    	GetHappyHourMode();
    }	
	
	public void GetHappyHourMode() 
    {   
		mIMenu.mMenuHappyHourMode = mModelManager.mModelMenu.GetMenuHappyHourMode();		
    }
	
	public void SetHappyHourMode() 
    {   
		mIMenu.mMenuHappyHourMode = false;
		if( !mModelManager.mModelMenu.GetMenuHappyHourMode() )
		{
			mIMenu.mMenuHappyHourMode = true;			
		}
		
		mModelManager.mModelMenu.SetMenuHappyHourMode(mIMenu.mMenuHappyHourMode);	
    }
    
	public void SetActiveProducts() 
    {    
		mIMenu.mActiveProductList = mModelManager.mModelMenu.SetActiveProducts(mIMenu.mSelectedCatID);
    }	
	
    public void SetSelectedProduct()
    {
    	mIMenu.mSelectedProduct1 = mModelManager.mModelMenu.GetSelectedProduct1();
    	mIMenu.mSelectedProductOptions = mModelManager.mModelMenu.GetSelectedProductOptions();
    	mIMenu.mSelectedTopGroupToppings = mModelManager.mModelMenu.GetTopGroupToppingsList();
    	//mIMenu._SelectedProduct2 = mModelManager._ModelMenu.GetSelectedProduct2();
    }
    
    public int POButClicked()
    {
    	int prodAction = mModelManager.mModelMenu.SelectPOProduct(mIMenu.mSelectedPOButID);
        if (prodAction > 0)
        {
            SetSelectedProduct();
            if (prodAction == 1)
            {
            	AddProductToOffer();
            }
        }
    	return prodAction;
    }

    public int ProductClicked()
    {
        int prodAction = mModelManager.mModelMenu.SelectProduct(mIMenu.mSelectedProdID);

        if (prodAction > 0)
        {
            SetSelectedProduct();
            if (prodAction == 1)
            {
                if (mIMenu.mMenuOfferMode)
                {
                    AddProductToOffer();
                }
                else
                {
                	mModelManager.mModelOrder.AddItem(mIMenu.mSelectedProduct1, null, null, false, mIMenu.mMenuHappyHourMode);
                }
            }
        }

        return prodAction;
    }

	public int SelectProductOption()
	{
        return mModelManager.mModelMenu.SelectProductOption(mIMenu.mSelectedOptionButGroupID,mIMenu.mSelectedOptionButID);
	}

	public boolean AddProductToOrder() 
	{
        boolean bSuccess = false;

        if (mModelManager.mModelMenu.CanAddItemToOrder())
        {
            mModelManager.mModelOrder.AddItem(mModelManager.mModelMenu.GetProductToAdd1(), mModelManager.mModelMenu.GetProductToAdd2(),mModelManager.mModelMenu.GetProductToAddHalfHalf(), mModelManager.mModelMenu.GetHalfHalf(),mIMenu.mMenuHappyHourMode);
            bSuccess = true;
        }

        return bSuccess;
	}
	
    public boolean AddOfferToOrder()
    {
        boolean bSuccess = false;

        if (mModelManager.mModelMenu.CanAddOfferToOrder())
        {
        	SetSelectedProductOffer(); // Offer changes in CanAddOfferToOrder so set again
            if (mIMenu.mSelectedPO != null)
            {
                mIMenu.mSelectedPO.SetProductOfferDetails(mIMenu.mSelectedPODesc, mIMenu.mSelectedPOPrice);   
                mModelManager.mModelOrder.AddOffer(mIMenu.mSelectedPO,mIMenu.mMenuHappyHourMode);
                bSuccess = true;
            }
            mIMenu.mMenuOfferMode = false;
        }
        

        return bSuccess;
    }

	public void ResetMenu() 
	{
        mModelManager.mModelMenu.ResetMenu();		
	}

	public void ResetSelectedProduct() 
	{
        mModelManager.mModelMenu.ResetSelectedProduct();
        SetSelectedProduct();		
	}
	
    public void AddProductTopping(TillToppings in_Topping)
    {
        mModelManager.mModelMenu.AddProductTopping(in_Topping);
    }

    public void RemoveProductTopping(int in_TopID)
    {
        mModelManager.mModelMenu.RemoveProductTopping(in_TopID);
    }
    
    public void SetMenuOfferMode()
    {
        mModelManager.mModelMenu.SetMenuOfferMode(mIMenu.mMenuOfferMode);
        if (!mIMenu.mMenuOfferMode)
        {
            mIMenu.mSelectedPO = null;
        }
    }
    
    public void GetProductOffer()
    {
        mIMenu.mSelectedPO = mModelManager.mModelMenu.GetProductOffer(mIMenu.mSelectedPOCode);                    
    }
    
    public void SetSelectedProductOffer()
    {
        mIMenu.mSelectedPO = mModelManager.mModelMenu.GetSelectedProductOffer();                    
    }

	public void CreatePOItemProducts() 
	{
		mIMenu.mActivePOButtonList = mModelManager.mModelMenu.CreatePOItemProducts(mIMenu.mSelectedPOItemGroupID); 			 
	}
	
    public boolean AddProductToOffer()
    {
    	boolean bSuccess = false;

        if (mModelManager.mModelMenu.CanAddItemToOrder())
        {
        	if( mIMenu.mPOItemSelectionMode )
        	{
        		mIMenu.mSelectedPOItemID = mIMenu.mSelectedPOButID+1;        		
        	}
            mModelManager.mModelMenu.SetPOItem(mIMenu.mSelectedPOItemGroupID, mIMenu.mSelectedPOItemID);
            SetSelectedProductOffer();
            bSuccess = true;
        }
        return bSuccess;
    }

	public void GetPOItemAction() 
	{
		mIMenu.mPOItemAction = 0;  
		if( mIMenu.mSelectedPO != null && mIMenu.mSelectedPOItemGroupID > 0 )
		{			
			TillProductOfferItemGroup POItemGroup = mIMenu.mSelectedPO.GetPOItemGroupByID(mIMenu.mSelectedPOItemGroupID);
			if( POItemGroup != null )
			{
				ArrayList<TillProductOfferItem> POItemList = POItemGroup.GetPOItemList();
				OFFER_ITEM_TYPE POGroupType = POItemGroup.GetPOItemGroupType();
				
				ArrayList<String> retItemDetails = POItemList.get(0).GetPOItemDetails(); 
				if( POGroupType == OFFER_ITEM_TYPE.OFFERITEMTYPE_OR )
				{
					CreatePOItemProducts();	
					mIMenu.mPOItemAction = 3;  
				}
				else if( POGroupType == OFFER_ITEM_TYPE.OFFERITEMTYPE_SELECT )
				{
					mIMenu.mSelectedProdID = 0;
    				mIMenu.mSelectedCatID = Integer.parseInt(retItemDetails.get(6));
    				SetActiveProducts();  
    				mIMenu.mPOItemAction = 3;  
				}
				else if( POGroupType == OFFER_ITEM_TYPE.OFFERITEMTYPE_TOPPINGS )
				{
					mIMenu.mSelectedProdID = 0;
					mIMenu.mSelectedCatID = 0;
					int prodAction = 0;
					if( retItemDetails.get(7).compareTo("1") == 0 )
					{
						//mIMenu.mSelectedProduct1 = POItemList.get(0).GetPOProduct1();
						//mIMenu.mSelectedProductOptions = mIMenu.mSelectedProduct1.ProductOptions;										
						prodAction = mModelManager.mModelMenu.SelectExistingPOItem(mIMenu.mSelectedPOItemGroupID);
						SetSelectedProduct();
					}
					else
					{
						CreatePOItemProducts();
						prodAction = ProductClicked();						
					}
					
					if (prodAction == 2) // Options 
					{					  
						if( mIMenu.mSelectedProductOptions != null )
						{
							if( mIMenu.mSelectedProductOptions.GetButGroups().size() > 0 )
							{
								mIMenu.mPOItemAction = 1;                	                        		
							}
							else if( mIMenu.mSelectedTopGroupToppings.size() > 0 || mIMenu.mSelectedProduct1.ProductToppingsList.size() > 0 )
							{
								mIMenu.mPOItemAction = 2;                        			
							}
						}					
					}						      					
				}     
			}
		}		
	}
	
    public void SetProductOfferORSelection()
    {
        mModelManager.mModelMenu.SetProductOfferORSelection(mIMenu.mSelectedPOItemGroupID, mIMenu.mSelectedPOItemID);
    }
}
