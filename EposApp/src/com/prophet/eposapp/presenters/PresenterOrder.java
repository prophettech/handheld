package com.prophet.eposapp.presenters;
import java.io.Serializable;

import com.prophet.eposapp.interfaces.IOrder;
import com.prophet.eposapp.models.ModelManager;
import com.prophet.eposapp.models.ORDERTYPE;




public class PresenterOrder  implements Serializable 
{
	private static final long serialVersionUID = 1L;
	ModelManager mModelManager;
    IOrder mIOrder;
    
	
	public PresenterOrder(ModelManager in_ModelManager)
	{
		mModelManager = in_ModelManager;			
	}
	
    public void AttachInterface(IOrder in_IOrder)
    {
        mIOrder = in_IOrder;
    }
    
    public void SetOrder()
    {
        mIOrder.mCurrentOrderDetails = mModelManager.mModelOrder.GetOrderDetails();
        GetOrderItems();
    }

    public void GetOrderChangedFlag()
    {
    	mIOrder.mOrderChanged = mModelManager.mModelOrder.GetOrderChangedFlag();    	
    }
    
    public void ResetOrderChangedFlag()
    {
    	mModelManager.mModelOrder.ResetOrderChangedFlag();
    }   
    
    public void GetOrderItems()
    {
        mIOrder.mCurrentOrderItems = mModelManager.mModelOrder.GetOrderItems();
    }
    
    public void GetOrderItemDetails()
    {
        mIOrder.mSelectedOrderItemDetails = mModelManager.mModelOrder.GetOrderItemDetails(mIOrder.mCurrentOrderItemID);
    }
	
    public void NewOrder()
    {
    	mIOrder.mOrderRes = true;
    	mIOrder.mLoadOrderID = 0;
    	mIOrder.mOrderErrorMsg = "";
    	if( mIOrder.mNewOrderType == ORDERTYPE.ORD_TYPE_TABLE )
    	{
    		mModelManager.mModelOrder.NewTableOrder(mIOrder.mSelectedTableID, mIOrder.mSelectedTableNo);    		
    	}
    	else
    	{    		  
    		if( mIOrder.mNewOrderType == ORDERTYPE.ORD_TYPE_BAR )
    		{
    			mModelManager.mModelOrder.NewBarOrder();    			
    		}
    		else
    		{
    			mModelManager.mModelOrder.NewOrder();      			
    		}    		
    	}
    	SetPayType("Cash");
    	SetOrder();
    	
    }
    
	public void LoadOrder() 
    {    
		mIOrder.mOrderErrorMsg = "";
		mIOrder.mOrderRes = false;
        if (mModelManager.mModelOrder.LoadOrder(mIOrder.mLoadOrderID))
        {
        	GetOrderItems();
        	mModelManager.mModelSplitBill.LoadSplitBill(mModelManager.mModelOrder.GetSplitBillItems());
        	SetPayTypeDesc(mIOrder.mCurrentOrderDetails.getOrderPayTypeCode());
        	mIOrder.mOrderErrorMsg = mModelManager.mModelOrder.GetOrderErrorMsg();
            mIOrder.mOrderRes = true;
        }

        mIOrder.mOrderErrorMsg = mModelManager.mModelOrder.GetOrderErrorMsg();
        ResetOrderChangedFlag();
    }
	
	public void SaveOrder() 
    {    
		mIOrder.mOrderErrorMsg = "";
		mModelManager.mModelOrder.SetSplitBillItems(mModelManager.mModelSplitBill.GetAllSplitItemsList());
        if (mModelManager.mModelOrder.SaveOrder())
        {
            mIOrder.mOrderRes = true;
            SetOrder();
        }
        else
        {
        	mIOrder.mOrderErrorMsg = mModelManager.mModelOrder.GetOrderErrorMsg();
        	mIOrder.mOrderRes = false;
        }
    }
	
    public boolean DeleteCurrentOrderItem()
    {
        int DelOrderItemIdx = mModelManager.mModelOrder.DeleteCurrentOrderItem(mIOrder.mCurrentOrderItemID);
        if (DelOrderItemIdx >=0)
        {
            //mIOrder.mCurrentOrderItemID = mModelManager.mModelOrder.GetOrderItemID(DelOrderItemIdx);
            return true;
        }
        
        return false;
    }

	public void UpdateEditedOrderItem() 
	{
		mModelManager.mModelOrder.UpdateEditedOrderItem(mIOrder.mCurrentOrderItemID,mIOrder.mEditedOrderItemQty,mIOrder.mEditedOrderItemPrice,mIOrder.mEditedOrderItemFreeText);		
	}
	
	public void SetOrderTableCovers() 
	{
		mModelManager.mModelOrder.SetOrderTableCovers(mIOrder.mSelectedTableCovers);	
	}    
	
    public void SetPayment()
    {
        mModelManager.mModelOrder.SetPayment(mIOrder.mOrderPaymentAmount,mIOrder.mOrderPaymentAdd);                   
    }

	public void SetServiceChargeRate() 
	{
        mModelManager.mModelOrder.SetServiceChargeRate(mIOrder.mOrderServiceRate);                   		
	}
    
	public void SetServiceChargeAmt() 
	{
        mModelManager.mModelOrder.SetServiceChargeAmt(mIOrder.mOrderServiceAmount);                   		
	}
	
	public void SetDiscountRate() 
	{
        mModelManager.mModelOrder.SetDiscountRate(mIOrder.mOrderDiscountRate);                   		
	}
	
	public void SetDiscountAmt() 
	{
        mModelManager.mModelOrder.SetDiscountAmt(mIOrder.mOrderDiscountAmount);                   		
	}

	public void SetPayType(String in_PayTypeDesc) 
	{
        int PayTypeCode = -1;
        String PayTypeDesc = "";
        
        PayTypeCode = mModelManager.mModelHelper.LookupPayType(in_PayTypeDesc);
        if( PayTypeCode >= 0)
        {
            PayTypeDesc = in_PayTypeDesc;
        }
        
        mModelManager.mModelOrder.SetPayType(PayTypeCode, PayTypeDesc);
		
	}
	
	public void SetPayTypeDesc(int in_PayTypeCode) 
	{
        String PayTypeDesc = "";
        
        PayTypeDesc = mModelManager.mModelHelper.LookupPayTypeDesc(in_PayTypeCode);
        if( PayTypeDesc.compareTo("") == 0)
        {
        	// Payment type not found so set it to cash
        	SetPayType("Cash");           
        }
        else
        {
        	 mModelManager.mModelOrder.SetPayType(in_PayTypeCode, PayTypeDesc);
        }
	}

	public void PayOrder() 
	{
        if (mModelManager.mModelOrder.PayOrder())
        {
            SaveOrder();
        }
        else
        {
            mIOrder.mOrderErrorMsg = mModelManager.mModelOrder.GetOrderErrorMsg();
            mIOrder.mOrderRes = false;
        }
	}
	
    public void AddSplitPayment()
    {
        int PayTypeCode = -1;
        String PayTypeDesc = "";

        PayTypeCode = mModelManager.mModelHelper.LookupPayType( mIOrder.mSplitPayTypeDesc);
        if( PayTypeCode >= 0)
        {
            PayTypeDesc = mIOrder.mSplitPayTypeDesc;
        }

        mModelManager.mModelOrder.AddSplitPayment(PayTypeCode, PayTypeDesc, mIOrder.mSplitPayAmount, mIOrder.mOrderSplitPayIdx);
    }

    public void RemoveSplitPayment()
    {
        mModelManager.mModelOrder.RemoveSplitPayment(mIOrder.mOrderSplitPayIdx);
    }
}
