package com.prophet.eposapp.presenters;

import com.prophet.eposapp.interfaces.IOrder;
import com.prophet.eposapp.models.ModelManager;

public class PresenterReport 
{
    ModelManager mModelManager;
    IOrder mIOrder;

    public PresenterReport(ModelManager in_ModelManager)
    {
        mModelManager = in_ModelManager;
    }

    public void AttachOrderInterface( IOrder in_IOrder)
    {
        mIOrder = in_IOrder;
    }

    public void PrintOrder(int in_PrintType)
    {
        int OrderIDF = mIOrder.mCurrentOrderDetails.getOrderID();
        int OrderIDT = mIOrder.mCurrentOrderDetails.getOrderID();
        int mSplitBill = 0;
        if( mModelManager.mModelSplitBill.GetSplitBillCount() > 0 )
        	mSplitBill = 1;
             
        mIOrder.mOrderRes = true;
        mModelManager.mModelReport.PrintOrder(OrderIDF,OrderIDT, in_PrintType, mSplitBill);
        mIOrder.mOrderErrorMsg = mModelManager.mModelReport.GetErrorMsg();
        if( mIOrder.mOrderErrorMsg.compareTo("") == 0 )
        {
        	mModelManager.mModelOrder.UpdatePrintedQty();
        	mModelManager.mModelSplitBill.UpdateSplitPrintedQty();
        	if( in_PrintType == 1 )
        	{
        		mModelManager.mModelOrder.SetPrintedToKitchen(true);        		
        	}
        	else if( in_PrintType == 3 )
        	{
        		mModelManager.mModelOrder.SetBillPrinted(true);        		
        	}
        }
        else
        {
        	mIOrder.mOrderRes = false;
        }
    }
}
