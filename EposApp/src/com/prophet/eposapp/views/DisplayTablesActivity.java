package com.prophet.eposapp.views;

import java.util.Timer;
import java.util.TimerTask;

import com.prophet.eposapp.DisplayTablesAdapter;
import com.prophet.eposapp.EposApp;
import com.prophet.eposapp.R;
import com.prophet.eposapp.interfaces.IMenu;
import com.prophet.eposapp.interfaces.IOrder;
import com.prophet.eposapp.interfaces.ITables;
import com.prophet.eposapp.models.ORDERTYPE;
import com.prophet.eposapp.models.TillTables;
import com.prophet.eposapp.presenters.PresenterMenu;
import com.prophet.eposapp.presenters.PresenterOrder;
import com.prophet.eposapp.presenters.PresenterSecurity;
import com.prophet.eposapp.presenters.PresenterSplitBill;
import com.prophet.eposapp.presenters.PresenterTables;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.AdapterView.OnItemClickListener;

public class DisplayTablesActivity extends Activity
{
	boolean mDisplayingTable;
	
	PresenterTables mPresenterTables;
    ITables mITables;    
	IOrder mIOrder;
	IMenu mIMenu;
	PresenterOrder mPresenterOrder;
	PresenterSplitBill mPresenterSplitBill;
	PresenterSecurity mPresenterSecurity;
	PresenterMenu mPresenterMenu;
	Timer mTableRefresh;
	
    static final int UPDATE_TABLE_COVERS = 1;
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_tables);
		// Show the Up button in the action bar.
		mITables = ((EposApp)getApplication()).GetITables();
		mPresenterTables = ((EposApp)getApplication()).GetPresenterTables();
		mPresenterOrder = ((EposApp)getApplication()).GetPresenterOrder();
		mIOrder = ((EposApp)getApplication()).GetIOrder();			
		mPresenterSplitBill = ((EposApp)getApplication()).GetPresenterSplitBill();
		mPresenterSecurity = ((EposApp)getApplication()).GetPresenterSecurity();
		mIMenu = ((EposApp)getApplication()).GetIMenu();
		mPresenterMenu = ((EposApp)getApplication()).GetPresenterMenu();
		
		new LoadTableData().execute(); 	
		mTableRefresh = new Timer();
		mTableRefresh.schedule(new RefreshTablesTask(), 20000, 20000);		
	}
	
	private void DisplayTablesGrid() 
	{
		
        //if(mIMenu != null)
        //{        	
            runOnUiThread
            (
            	new Runnable() 
            	{
            		public void run()
            		{     
            		    GridView gridview = (GridView) findViewById(R.id.tablesgridview);
            			
            		    if( gridview != null )
            		    {          		   
            		    	gridview.setAdapter(new DisplayTablesAdapter(DisplayTablesActivity.this,mITables.mTablesList));               	
            		    	gridview.setOnItemClickListener(tableGridOnItemClickListener);
            		    }
                	}             
            	}
            );             
        //}		
	}
	
	OnItemClickListener tableGridOnItemClickListener = new OnItemClickListener()
	{	  
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
		{			
				TillTables tmpTable = (TillTables)parent.getItemAtPosition(position);	
				mIOrder.mSelectedTableID = tmpTable.GetTableID();
				mIOrder.mSelectedTableNo = tmpTable.GetTableNo();
				mIOrder.mNewOrderType = ORDERTYPE.ORD_TYPE_TABLE; 
				mIOrder.mLoadOrderID = 0;
				if(tmpTable.GetTableBookedStatus() )
				{
					mIOrder.mLoadOrderID = tmpTable.GetTableOrderID();
				}
				
				new NewTableOrder().execute();
		}	
	};
	
	public void DisplayMenu()
	{
		Intent menuIntent =  new Intent(this, MainMenuActivity.class);
		startActivity(menuIntent);					
	}
	
	public void BarOrderEvent(View view) 
	{	
		mIOrder.mNewOrderType = ORDERTYPE.ORD_TYPE_BAR;
		new NewTableOrder().execute();
		//displayMenu();
	}	
	
	public void TakeawayOrderEvent(View view) 
	{	
		mIOrder.mNewOrderType = ORDERTYPE.ORD_TYPE_TAKEAWAY; 
		new NewTableOrder().execute();
		//displayMenu();
	}	
	
	public void LogoutUserEvent(View view) 
	{		
		new ProcessLogout().execute();		
	}	
	
	public void HappyHourEvent(View view) 
	{	
		mPresenterMenu.GetHappyHourMode();
		if( mIMenu.mMenuHappyHourMode )
		{
			DisplayHappyHourMsgBox("Disable Happy Hour?");			
		}
		else
		{
			DisplayHappyHourMsgBox("Enable Happy Hour?");						
		}	
		
	}	
	
	private void SetHappyHourButtonColour() 
	{
		Button but = (Button) findViewById(R.id.butHappyHour);		
		but.setBackgroundColor(Color.parseColor("#FFFFB9"));
		but.setTextColor(Color.BLACK);
		
		mPresenterMenu.GetHappyHourMode();
		if( mIMenu.mMenuHappyHourMode )
		{
			but.setBackgroundColor(Color.parseColor("#FFFF80"));
			but.setTextColor(Color.RED);
		}		
	}
	
	public void DisplayHappyHourMsgBox(String in_Msg) 
	{    
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle("Happy Hour");
		builder.setMessage(in_Msg);

		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() 
		{			
			public void onClick(DialogInterface dialog, int which) 
			{							
				dialog.dismiss();	
				mPresenterMenu.SetHappyHourMode();
				SetHappyHourButtonColour();
			}			
		});

		builder.setNegativeButton("No", new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();
	}
	
	public void displayLogin()
	{
		Intent loginIntent =  new Intent(this, LoginActivity.class);
		startActivity(loginIntent);					
	}	
	
	public void DisplayTableCovers()
	{
		Intent coversIntent =  new Intent(this, TableCoversActivity.class);
		startActivityForResult(coversIntent,UPDATE_TABLE_COVERS);	
		//startActivity(coversIntent);					
	}	
	
	

	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) 
    {
        if (requestCode == UPDATE_TABLE_COVERS) 
        {
            if (resultCode == RESULT_OK)
            {
        		mPresenterOrder.SetOrderTableCovers();
        		mPresenterOrder.ResetOrderChangedFlag();
        		DisplayMenu();
            }
        }
    }    
	
	 /**
     * Background Async Task to Load all data by making HTTP Request
     * */
    class LoadTableData extends AsyncTask<String, String, String> 
    {

        private ProgressDialog pDialog;
        
    	LoadTableData( )
    	{
    	
    	}
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() 
        {         
        	super.onPreExecute();
            pDialog = new ProgressDialog(DisplayTablesActivity.this);
            pDialog.setMessage("Loading Tables. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();            
        }
        
        protected String doInBackground(String... args) 
        {      
        	//String errorMsg = "";
        	try
        	{
        		mPresenterTables.loadTables();
        	}
        	catch(Exception e)
        	{
        		//errorMsg = e.getMessage();
        		return null;
        		
        	}
            return "1";
        }
        
        protected void onPostExecute(String file_url) 
        {
            // dismiss the dialog after getting all products
            pDialog.dismiss();       
            DisplayTablesGrid();
            SetHappyHourButtonColour();
        }  
    }
    
    class NewTableOrder extends AsyncTask<String, String, String> 
    {

        private ProgressDialog pDialog;
        
        NewTableOrder(  )
    	{
    	
    	}
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() 
        {         
        	super.onPreExecute();
            pDialog = new ProgressDialog(DisplayTablesActivity.this);
            String mMsg = "New Order. Please wait...";            
            if( mIOrder.mLoadOrderID > 0 )
            	mMsg = "Loading Order. Please wait...";
            
            pDialog.setMessage(mMsg);
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();   
            
        }
        
        protected String doInBackground(String... args) 
        {      
        	//String errorMsg = "";
        	try
        	{
        		if( mIOrder.mLoadOrderID > 0 )
        		{
        			mPresenterOrder.LoadOrder();   
        			mPresenterSplitBill.SetSplitBill();
        		}
        		else
        		{     
        			mPresenterSplitBill.ClearSplitBill();
        			mPresenterOrder.NewOrder();        			
        		}				
        	}
        	catch(Exception e)
        	{
        		//errorMsg = e.getMessage();
        		return null;
        		
        	}
            return "1";
        }
        
        protected void onPostExecute(String file_url) 
        {
            pDialog.dismiss();
            if( mIOrder.mOrderRes )
            {            	 
        		if( mIOrder.mLoadOrderID > 0 || mIOrder.mCurrentOrderDetails.getOrderType() != ORDERTYPE.ORD_TYPE_TABLE )
        		{
        			DisplayMenu();
        		}   
        		else
        		{
        			DisplayTableCovers();        			
        		}
            }           
            else
            {
    			AlertDialog alertDialog;
    			alertDialog = new AlertDialog.Builder(DisplayTablesActivity.this).create();
    			alertDialog.setTitle("");
    			alertDialog.setMessage(mIOrder.mOrderErrorMsg);
    			alertDialog.show();            	
            }
            
      
        }  
    }    
    
	class ProcessLogout extends AsyncTask<String, String, String> 
	{
		   private ProgressDialog pDialog;

		   ProcessLogout( )
	    	{
	    	
	    	}
	        /**
	         * Before starting background thread Show Progress Dialog
	         * */
	        @Override
	        protected void onPreExecute() 
	        {         
	        	super.onPreExecute();
	            pDialog = new ProgressDialog(DisplayTablesActivity.this);
	            pDialog.setMessage("Logging out. Please wait...");
	            pDialog.setIndeterminate(false);
	            pDialog.setCancelable(false);
	            pDialog.show();            
	        }
	        
	        protected String doInBackground(String... args) 
	        {      
	    		mPresenterSecurity.ProcessLogout();
	            return "1";
	        }
	        
	        protected void onPostExecute(String file_url) 
	        {
	            pDialog.dismiss();    
	            displayLogin();
	        } 	       
	}
	
	class RefreshTablesTask extends TimerTask 
	{
	   public void run() 
	   {
		   runOnUiThread(new Runnable() 
		   {
               @Override
               public void run() 
               {
                   new LoadTableData().execute(); 
               }
           });		   	
	   }		
	}
}
