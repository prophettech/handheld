package com.prophet.eposapp.views;

import com.prophet.eposapp.EposApp;
import com.prophet.eposapp.R;
import com.prophet.eposapp.interfaces.IMenu;
import com.prophet.eposapp.interfaces.IOrder;
import com.prophet.eposapp.presenters.PresenterMenu;
import com.prophet.eposapp.presenters.PresenterOrder;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class EditItemActivity  extends Activity
{
	IMenu mIMenu;
	IOrder mIOrder;
	PresenterMenu mPresenterMenu;	
	PresenterOrder mPresenterOrder;
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edititem);
		// Show the Up button in the action bar.
		
		mPresenterMenu = ((EposApp)getApplication()).GetPresenterMenu();
		mIMenu = ((EposApp)getApplication()).GetIMenu();

		mPresenterOrder = ((EposApp)getApplication()).GetPresenterOrder();
		mIOrder = ((EposApp)getApplication()).GetIOrder();

		DisplayItemDetails();
		
	}
	
	private void DisplayItemDetails() 
	{
        if(mIOrder != null)
        {       	
        	mIOrder.mSelectedOrderItemDetails = null;
        	mPresenterOrder.GetOrderItemDetails();
        	if(mIOrder.mSelectedOrderItemDetails != null )
        	{
        		final double mItemQty = mIOrder.mSelectedOrderItemDetails.GetItemQty();
        		final double mItemPrice = mIOrder.mSelectedOrderItemDetails.GetItemPrice();
        		String tmpText = mIOrder.mSelectedOrderItemDetails.GetItemFreeText();
        		if( tmpText.compareTo("") != 0 )
        		{
        			tmpText = tmpText.substring(1, tmpText.length()-1);	// Remove brackets
        		}
        		final String mItemText = tmpText;
        		
        		this.setTitle("Edit Item: " + mIOrder.mSelectedOrderItemDetails.GetItemDesc() );
        		runOnUiThread
        		(
        			new Runnable() 
        			{
        				public void run()
        				{           					
        					EditText ei = (EditText) findViewById(R.id.etQty);		
        					ei.setText(Double.toString(mItemQty));            			

        					ei = (EditText) findViewById(R.id.etPrice);		
         					ei.setText(Double.toString(mItemPrice));     
         					
        					ei = (EditText) findViewById(R.id.etFreeText);		
         					ei.setText(mItemText);            					
        				}             
        			}
        		);   
        	}
        }	        
	}
	
	public void EditItemUpdate() 
	{
		String mTmpText = "";
		mIOrder.mEditedOrderItemQty= 0;
		mIOrder.mEditedOrderItemPrice = 0;
		mIOrder.mEditedOrderItemFreeText = "";
			
		EditText ei = (EditText) findViewById(R.id.etQty);		
		mTmpText = ei.getText().toString();    
		if( mTmpText.compareTo("") != 0 )
		{
			mIOrder.mEditedOrderItemQty = Double.parseDouble(mTmpText);			
		}

		ei = (EditText) findViewById(R.id.etPrice);	
		mTmpText = ei.getText().toString();    
		if( mTmpText.compareTo("") != 0 )
		{
			mIOrder.mEditedOrderItemPrice = Double.parseDouble(mTmpText);			
		}	
			
		ei = (EditText) findViewById(R.id.etFreeText);	
		mTmpText = ei.getText().toString(); 
		if( mTmpText.compareTo("") != 0 )
		{		
			mIOrder.mEditedOrderItemFreeText = "(" + mTmpText + ")";  // Add brackets  
		}

		mPresenterOrder.UpdateEditedOrderItem();	    
		
	}
	
	public void EditItemSplitBillButton(View view) 
	{
		
		Intent SplitBillIntent =  new Intent(this, SplitBillActivity.class);
		startActivity(SplitBillIntent);	
		CloseEditItem();		
	}	
	
	public void EditItemDeleteButton(View view) 
	{    			
		DisplayConfirmDeleteBox();			
	}		
	
	public void DisplayConfirmDeleteBox() 
	{    
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle("Delete Item?");
		builder.setMessage("Are you sure want to delete item: " + mIOrder.mSelectedOrderItemDetails.GetItemDesc() + "?" );

		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() 
		{			
			public void onClick(DialogInterface dialog, int which) 
			{					
				mPresenterOrder.DeleteCurrentOrderItem();
				dialog.dismiss();
				setResult(Activity.RESULT_OK);
				CloseEditItem();
			}			
		});

		builder.setNegativeButton("No", new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();
	}
		
	public void EditItemDoneButton(View view) 
	{	
		EditItemUpdate();
		setResult(Activity.RESULT_OK);
		CloseEditItem();
	}	
		
	public void EditItemCancelButton(View view) 
	{
		CloseEditItem();
	}
	
	public void CloseEditItem() 
	{			
		EditItemActivity.this.finish();	
	}
}
