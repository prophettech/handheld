package com.prophet.eposapp.views;

import java.util.ArrayList;



import com.prophet.eposapp.EposApp;
import com.prophet.eposapp.ProdOptionsAdapter;
import com.prophet.eposapp.R;
import com.prophet.eposapp.interfaces.IMenu;
import com.prophet.eposapp.interfaces.IOrder;
import com.prophet.eposapp.models.TillProductOptions;
import com.prophet.eposapp.models.TillProductOptionsButGroup;
import com.prophet.eposapp.models.TillProductOptionsButGroupButton;
import com.prophet.eposapp.presenters.PresenterMenu;
import com.prophet.eposapp.presenters.PresenterOrder;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.AdapterView.OnItemClickListener;

public class ProdOptionsActivity extends Activity 
{
	IMenu mIMenu;
	PresenterMenu mPresenterMenu;
	IOrder mIOrder;
	PresenterOrder mPresenterOrder;
	
	//static final int OPTIONS_DONE = 1;
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_prodoptions);
		setFinishOnTouchOutside(false);
		
		
		mPresenterMenu = ((EposApp)getApplication()).GetPresenterMenu();
		mIMenu = ((EposApp)getApplication()).GetIMenu();

		mPresenterOrder = ((EposApp)getApplication()).GetPresenterOrder();
		mIOrder = ((EposApp)getApplication()).GetIOrder();
		
		if( mIMenu.mSelectedProduct1 != null )
		{
			setTitle(mIMenu.mSelectedProduct1.ProductDesc + " Options");
		}
		else
		{
			setTitle("Product Options");			
		}
		
		DisplayProdOptions1Grid();
		DisplayProdOptions2Grid();
	}
	
	private void DisplayProdOptions1Grid() 
	{
        //if(mIMenu != null)
        //{        	
            runOnUiThread
            (
            	new Runnable() 
            	{
            		public void run()
            		{     
            		    TillProductOptions tmpProductOptions = mIMenu.mSelectedProductOptions;
            		    ArrayList<TillProductOptionsButGroupButton> tmpButtons = new  ArrayList<TillProductOptionsButGroupButton>(); 
            		    if(tmpProductOptions != null )
            		    {
            		    	ArrayList<TillProductOptionsButGroup> tmpButGroupList = tmpProductOptions.GetButGroups();
            		    	
            		    	
            				for (TillProductOptionsButGroup ButGroup : tmpButGroupList) 
            				{
            					if(ButGroup.GetButGroupNo() == 1 )
            					{
            						tmpButtons = ButGroup.GetButtons();
            						break;
            					}
            				}     
            				
            		    	
            		    	GridView gridview = (GridView) findViewById(R.id.prodOptions1gridview);
            		    
            		    	if( gridview != null )
            		    	{          		   
            		    		gridview.setAdapter(new ProdOptionsAdapter(ProdOptionsActivity.this,tmpButtons));               	
            		    		gridview.setOnItemClickListener(ProdOptionsGridOnItemClickListener);
            		    	}
            		    }
                	}             
            	}
            );             
        //}		
	}
	private void DisplayProdOptions2Grid() 
	{    	
            runOnUiThread
            (
            	new Runnable() 
            	{
            		public void run()
            		{     
            		    TillProductOptions tmpProductOptions = mIMenu.mSelectedProductOptions;
            		    ArrayList<TillProductOptionsButGroupButton> tmpButtons = new  ArrayList<TillProductOptionsButGroupButton>(); 
            		    if(tmpProductOptions != null )
            		    {
            		    	ArrayList<TillProductOptionsButGroup> tmpButGroupList = tmpProductOptions.GetButGroups();
            		    	
            		    	
            				for (TillProductOptionsButGroup ButGroup : tmpButGroupList) 
            				{
            					if(ButGroup.GetButGroupNo() == 2 )
            					{
            						tmpButtons = ButGroup.GetButtons();
            						break;
            					}
            				}    
            		    	
            		    	GridView gridview = (GridView) findViewById(R.id.prodOptions2gridview);
            		    
            		    	if( gridview != null )
            		    	{          		   
            		    		gridview.setAdapter(new ProdOptionsAdapter(ProdOptionsActivity.this,tmpButtons));               	
            		    		gridview.setOnItemClickListener(ProdOptionsGridOnItemClickListener);
            		    	}
            		    }
                	}             
            	}
            );             	
	}
	
	OnItemClickListener ProdOptionsGridOnItemClickListener = new OnItemClickListener()
	{	  
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
		{	
			TillProductOptionsButGroupButton tmpProdOptionButton = (TillProductOptionsButGroupButton)parent.getItemAtPosition(position);
			tmpProdOptionButton.GetButGroupID();
			mIMenu.mSelectedOptionButGroupID = tmpProdOptionButton.GetButGroupID(); 
            mIMenu.mSelectedOptionButID = tmpProdOptionButton.GetButID();
            
            if (mIMenu.mSelectedOptionButID == -1)
            {
            	// Toppings
            }
            else if (mIMenu.mSelectedOptionButID == -2)
            {
                // Half & Half option button            	
            }
            else
            {
                int prodOptAction = mPresenterMenu.SelectProductOption();
                if (prodOptAction == 1)
                {
                    //DisplayMenuProductOptionsPopup(); // TODO
                }
                else
                {
                	DisplayProdOptions1Grid(); // To update buttons with selection colour
                	DisplayProdOptions2Grid();
                    if (prodOptAction == 2)
                    {
                        //DisplayMenuProductSubPrices();
                    }
                }
            }			
		}	
	};
	
	public void ProdOptionsToppings(View view) 
	{	
        // Toppings option button
    	if( mIMenu.mSelectedProduct1.ProductToppingsList.size() > 0 || (mIMenu.mSelectedProductOptions != null && mIMenu.mSelectedProductOptions.GetButGroups().size() > 0)  )
    	{
    		Intent prodToppingsIntent =  new Intent(this, ProdToppingsActivity.class);
    		startActivity(prodToppingsIntent);	

    		/*
			AlertDialog alertDialog;
			alertDialog = new AlertDialog.Builder(ProdOptionsActivity.this).create();
			alertDialog.setTitle("");
			alertDialog.setMessage(Integer.toString(mIMenu.mSelectedProduct1.ProductToppingsList.size()) + " toppings");
			alertDialog.show();  
			*/          		
    	}
	}	
	
	public void ProdOptionsCancel(View view) 
	{	
		ProdOptionsActivity.this.finish();
	}	
	
	public void ProdOptionsDone(View view) 
	{	
        if (mIMenu.mMenuOfferMode)
        {
            if (mPresenterMenu.AddProductToOffer())
            {            	       
            	mPresenterMenu.ResetSelectedProduct();
            	mPresenterMenu.ResetMenu(); 
                setResult(Activity.RESULT_OK);
                ProdOptionsActivity.this.finish();
            }
        }
        else
        {
            if (mPresenterMenu.AddProductToOrder())
            {            	
                mPresenterMenu.ResetSelectedProduct();
                mPresenterMenu.ResetMenu();                
                setResult(Activity.RESULT_OK);
                ProdOptionsActivity.this.finish();
            }
            else
            {
    			AlertDialog alertDialog;
    			alertDialog = new AlertDialog.Builder(ProdOptionsActivity.this).create();
    			alertDialog.setTitle("");
    			alertDialog.setMessage(mPresenterMenu.GetMenuErrorMsg());
    			alertDialog.show();
            }
        }
	}		
}
