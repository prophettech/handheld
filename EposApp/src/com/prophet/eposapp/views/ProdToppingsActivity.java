package com.prophet.eposapp.views;

import com.prophet.eposapp.EposApp;
import com.prophet.eposapp.ProdToppingsAdapter;
import com.prophet.eposapp.R;
import com.prophet.eposapp.interfaces.IMenu;
import com.prophet.eposapp.interfaces.IOrder;

import com.prophet.eposapp.models.TillToppings;
import com.prophet.eposapp.presenters.PresenterMenu;
import com.prophet.eposapp.presenters.PresenterOrder;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import android.widget.AdapterView.OnItemClickListener;

public class ProdToppingsActivity  extends Activity 
{
	IMenu mIMenu;
	PresenterMenu mPresenterMenu;
	IOrder mIOrder;
	PresenterOrder mPresenterOrder;
	
	static final int REFRESH_ORDER = 1;
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_prodtoppings);
		setFinishOnTouchOutside(false);
		
		
		mPresenterMenu = ((EposApp)getApplication()).GetPresenterMenu();
		mIMenu = ((EposApp)getApplication()).GetIMenu();

		mPresenterOrder = ((EposApp)getApplication()).GetPresenterOrder();
		mIOrder = ((EposApp)getApplication()).GetIOrder();
		
		if( mIMenu.mSelectedProduct1 != null )
		{
			setTitle(mIMenu.mSelectedProduct1.ProductDesc + " Toppings");
		}
		else
		{
			setTitle("Toppings");			
		}
		
		DisplayProdToppingsAvailableGrid();
		DisplayProdToppingsGrid();
	}
	
	private void DisplayProdToppingsAvailableGrid() 
	{
        //if(mIMenu != null)
        //{        	
            runOnUiThread
            (
            	new Runnable() 
            	{
            		public void run()
            		{     
        		    	GridView gridview = (GridView) findViewById(R.id.prodToppings1gridview);
            		    
        		    	if( gridview != null )
        		    	{          		   
        		    		gridview.setAdapter(new ProdToppingsAdapter(ProdToppingsActivity.this,mIMenu.mSelectedTopGroupToppings));               	
        		    		gridview.setOnItemClickListener(ProdToppingsAvailableGridOnItemClickListener);
        		    	}
                	}             
            	}
            );             
        //}		
	}
	private void DisplayProdToppingsGrid() 
	{    	
            runOnUiThread
            (
            	new Runnable() 
            	{
            		public void run()
            		{     
        		    	GridView gridview = (GridView) findViewById(R.id.prodToppings2gridview);
            		    
        		    	if( gridview != null )
        		    	{          		   
        		    		gridview.setAdapter(new ProdToppingsAdapter(ProdToppingsActivity.this,mIMenu.mSelectedProduct1.ProductToppingsList));               	
        		    		gridview.setOnItemClickListener(ProdToppingsGridOnItemClickListener);
        		    	}
                	}             
            	}
            );             	
	}
	
	OnItemClickListener ProdToppingsGridOnItemClickListener = new OnItemClickListener()
	{	  
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
		{	
			mPresenterMenu.RemoveProductTopping(((TillToppings)parent.getItemAtPosition(position)).ToppingID);
			DisplayProdToppingsGrid();
			ScrollProdToppings(position-1);
		}	
	};
	
	OnItemClickListener ProdToppingsAvailableGridOnItemClickListener = new OnItemClickListener()
	{	  
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
		{	
			mPresenterMenu.AddProductTopping((TillToppings)parent.getItemAtPosition(position));
			DisplayProdToppingsGrid();
			ScrollProdToppings(mIMenu.mSelectedProduct1.ProductToppingsList.size()-1);
		}	
	};
	
	public void ScrollProdToppings(int in_ScrolPos) 
	{	
    	GridView gridview = (GridView) findViewById(R.id.prodToppings2gridview);		    
    	if( gridview != null )
    	{    
    		gridview.smoothScrollToPosition(in_ScrolPos);			
    	}
	}	
	
	public void ProdToppingsCancel(View view) 
	{	
		ProdToppingsActivity.this.finish();
	}	
	
	public void ProdToppingsDone(View view) 
	{	
        if (mIMenu.mMenuOfferMode)
        {
        	// TODO
        	if( mIMenu.mProdOptionsDisplayed == false)
        	{
	            if (mPresenterMenu.AddProductToOffer())
	            {    
	            	mPresenterMenu.ResetSelectedProduct();
	            	mPresenterMenu.ResetMenu(); 
		            setResult(Activity.RESULT_OK);
		            ProdToppingsActivity.this.finish();  	
	            }
        	}
        	else
        	{
        		ProdToppingsActivity.this.finish();	
        	}
        }
        else
        {
        	if( mIMenu.mProdOptionsDisplayed == false)
        	{
                if (mPresenterMenu.AddProductToOrder())
                {            	
                    mPresenterMenu.ResetSelectedProduct();
                    mPresenterMenu.ResetMenu();                
                    setResult(Activity.RESULT_OK);
                    ProdToppingsActivity.this.finish();
                }
                else
                {
                    //MessageBox.Show(_PresenterMenu.GetMenuErrorMsg());
        			AlertDialog alertDialog;
        			alertDialog = new AlertDialog.Builder(ProdToppingsActivity.this).create();
        			alertDialog.setTitle("");
        			alertDialog.setMessage(mPresenterMenu.GetMenuErrorMsg());
        			alertDialog.show();
                }        		
        	}
        	else
        	{
        		ProdToppingsActivity.this.finish();	
        	}
        }
	}	
}
