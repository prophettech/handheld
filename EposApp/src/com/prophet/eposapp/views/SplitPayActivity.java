package com.prophet.eposapp.views;


import java.util.Locale;
import com.prophet.eposapp.EposApp;
import com.prophet.eposapp.R;
import com.prophet.eposapp.SplitPayAdapter;
import com.prophet.eposapp.interfaces.IOrder;

import com.prophet.eposapp.models.TillSplitPayDetails;
import com.prophet.eposapp.presenters.PresenterOrder;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.AdapterView.OnItemClickListener;

public class SplitPayActivity  extends Activity
{
	IOrder mIOrder;	
	PresenterOrder mPresenterOrder;
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splitpay);

		

		mPresenterOrder = ((EposApp)getApplication()).GetPresenterOrder();
		mIOrder = ((EposApp)getApplication()).GetIOrder();
		setTitle("Split Payment Amount Due �"+String.format("%.2f", mIOrder.mCurrentOrderDetails.getOrderTotal()));
		
    	mIOrder.mOrderSplitPayIdx = -1;
    	mIOrder.mSplitPayAmount = 0;
		mIOrder.mSplitPayTypeDesc = "Cash";
		SetSplitPayTypeValue();
		SetSplitPayAmtValue();
		DisplaySplitPayItems();
	}
	
	public void DisplaySplitPayItems() 
	{	
        if(mIOrder != null)
        {                	
            runOnUiThread
            (
            	new Runnable() 
            	{
            		public void run()
            		{    
            			if( mIOrder.mCurrentOrderDetails != null && mIOrder.mCurrentOrderDetails.getOrderSplitPayList().size() > 0)
            			{            				
            		    	GridView gridview = (GridView) findViewById(R.id.gvsplitpaylist);
            		    
            		    	if( gridview != null )
            		    	{          		   
            		    		gridview.setAdapter(new SplitPayAdapter(SplitPayActivity.this,mIOrder.mCurrentOrderDetails.getOrderSplitPayList()));               	
            		    		gridview.setOnItemClickListener(SpliyPayOnItemClickListener);
            		    	}
            			}
                	}             
            	}
            );             
        }	        		
	}
	
	OnItemClickListener SpliyPayOnItemClickListener = new OnItemClickListener()
	{	  
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
		{
			TillSplitPayDetails tmpSplitPayItem = (TillSplitPayDetails)parent.getItemAtPosition(position);
	    	mIOrder.mOrderSplitPayIdx = tmpSplitPayItem.getSpliyPayID();
	    	mIOrder.mSplitPayAmount = tmpSplitPayItem.getSpliyPayAmount();
			mIOrder.mSplitPayTypeDesc = tmpSplitPayItem.getSpliyPayDesc();
			SetSplitPayTypeValue();
			SetSplitPayAmtValue();
		}	
	};
	
	public void SetSplitPayTypeValue() 
	{	
		int butID = 0;
		String mPayTypeDesc = mIOrder.mSplitPayTypeDesc;
		mPayTypeDesc = mPayTypeDesc.toUpperCase(Locale.ENGLISH);
		if( mPayTypeDesc.compareTo("CC") == 0 )
		{    			
			butID = R.id.butpaytypecc;          			
		}
		else if( mPayTypeDesc.compareTo("CHQ") == 0 )
		{    		
			butID = R.id.butpaytypechq;           			
		}
		else if( mPayTypeDesc.compareTo("ACC") == 0 )
		{    			
			butID = R.id.butpaytypeacc;           			
		}  
		else
		{
			butID = R.id.butpaytypecash;        			
		}
	
		//Reset pay type colours
		SetPayTypeColour(R.id.butpaytypecash, Color.parseColor("#00E171"));
		SetPayTypeColour(R.id.butpaytypecc, Color.parseColor("#00E171"));
		SetPayTypeColour(R.id.butpaytypechq, Color.parseColor("#00E171"));
		SetPayTypeColour(R.id.butpaytypeacc, Color.parseColor("#00E171"));
		
		// Set selected pay type colour
		SetPayTypeColour(butID, Color.parseColor("#03AD57"));		
	}
	
	public void SetSplitPayAmtValue() 
	{	
    	EditText et = (EditText) findViewById(R.id.etSplitPayAmt);	
    	if( et != null )
    	{
    		if( mIOrder.mSplitPayAmount == 0 )
    		{
    			et.setText("");    		
    		}
    		else
    		{
    			et.setText(String.format("%.2f", mIOrder.mSplitPayAmount));    			
    		}
    	} 		
	}
	
	public void SplitPayTypeButtonEvent(View view) 
	{	
		mIOrder.mSplitPayTypeDesc = ((Button)view).getText().toString();
		SetSplitPayTypeValue();
	}   
	
    private void SetPayTypeColour(int in_ButID, int in_Color)
    {
		Button but = (Button)findViewById(in_ButID);
    	if( but != null )
    	{	
    		but.setBackgroundColor(in_Color);        		
    	}		
	}
    
    private void AddSplitPayment()
    {
    	EditText et = (EditText) findViewById(R.id.etSplitPayAmt);
    	if( et != null )
    	{
			String TmpText = et.getText().toString(); 			
			
			mIOrder.mSplitPayAmount = 0;
			if( TmpText.compareTo("") != 0 )
			{
				mIOrder.mSplitPayAmount = Double.parseDouble(TmpText);			
			}	
			if( mIOrder.mSplitPayAmount > 0 )
			{
	            mPresenterOrder.AddSplitPayment();
	            mIOrder.mOrderSplitPayIdx = -1;
	            mIOrder.mSplitPayAmount = 0;
	            et.setText("");
			}
			else
			{
	    		AlertDialog alertDialog;
	    		alertDialog = new AlertDialog.Builder(SplitPayActivity.this).create();
	    		alertDialog.setTitle("");
	    		alertDialog.setMessage("Split Amount must be greater than 0");
	    		alertDialog.show();   			
			}  
    	}
    }
	
	public void AddSplitPayButtonEvent(View view) 
	{
		//nputMethodManager inputManager = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE); 
	    //inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);	
		AddSplitPayment();
		DisplaySplitPayItems();		   				    				    
	}
	
	public void RemoveSplitPayButtonEvent(View view) 
	{
		mPresenterOrder.RemoveSplitPayment();
		DisplaySplitPayItems();		
        mIOrder.mOrderSplitPayIdx = -1;
        mIOrder.mSplitPayAmount = 0;
        SetSplitPayAmtValue();
	}	
	
	public void SplitPayDoneButton(View view) 
	{
		setResult(Activity.RESULT_OK);
		CloseSplitPay();		
	}	
	
	public void CloseSplitPay() 
	{
		SplitPayActivity.this.finish();	
	}	
	
	
	
}
