package com.prophet.eposapp.views;

import java.util.ArrayList;
import com.prophet.eposapp.DisplayTableCoversAdapter;
import com.prophet.eposapp.EposApp;
import com.prophet.eposapp.R;
import com.prophet.eposapp.interfaces.IOrder;
import com.prophet.eposapp.presenters.PresenterOrder;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class TableCoversActivity  extends Activity
{
	int mTableCovers;
	int mTableCoversOrig;
	IOrder mIOrder;
	PresenterOrder mPresenterOrder;
	@Override
	
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tablecovers);
		setFinishOnTouchOutside(false);
		setTitle("Table Covers");

		mPresenterOrder = ((EposApp)getApplication()).GetPresenterOrder();
		mIOrder = ((EposApp)getApplication()).GetIOrder();	
		mTableCovers = mIOrder.mCurrentOrderDetails.getOrderTableCovers();
		mTableCoversOrig = mTableCovers;
		SetTableCovers();
		DisplayTableCoversGrid();
	}
	
	private void DisplayTableCoversGrid() 
	{
        //if(mIMenu != null)
        //{        	
            runOnUiThread
            (
            	new Runnable() 
            	{
            		public void run()
            		{     
            		    GridView gridview = (GridView) findViewById(R.id.gvTableCovers);
            			
            		    if( gridview != null )
            		    {          		   
            		    	ArrayList<String> mTableCoversList = new ArrayList<String>();
            		    	for(int i=1; i <= 64; i++ )
            		    	{
            		    		mTableCoversList.add(Integer.toString(i));            		    		
            		    	}
            		    	gridview.setAdapter(new DisplayTableCoversAdapter(TableCoversActivity.this,mTableCoversList));               	
            		    	gridview.setOnItemClickListener(tableCoversGridOnItemClickListener);
            		    }
                	}             
            	}
            );             
        //}		
	}
	OnItemClickListener tableCoversGridOnItemClickListener = new OnItemClickListener()
	{	  
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
		{		
			String mTmpStr = (String)parent.getItemAtPosition(position);
			mTableCovers = Integer.parseInt(mTmpStr);
			SetTableCovers();
		}	
	};
	
	public void SetTableCovers() 
	{				
		if( mTableCovers == 0 )
			mTableCovers = 2;	
		
		TextView tv = (TextView) findViewById(R.id.tvCovers);	
		if( tv != null )
		{	
			tv.setText(Integer.toString(mTableCovers));
		}
	}
	

	
	public void CoversCancelButtonEvent(View view) 
	{	
		mTableCovers = mTableCoversOrig;
		CloseTableCovers();
	}	
	
	public void CoversDoneButtonEvent(View view) 
	{			
		TextView tv = (TextView) findViewById(R.id.tvCovers);	
		if( tv != null )
		{	
			if( tv.getText().toString().compareTo("") != 0 )
			{
				mTableCovers = Integer.parseInt(tv.getText().toString());				
			}
		}
		CloseTableCovers();
	}	
	
	public void CloseTableCovers() 
	{		
		mIOrder.mSelectedTableCovers = mTableCovers;	
		setResult(Activity.RESULT_OK);
		TableCoversActivity.this.finish();
	}
	

}
