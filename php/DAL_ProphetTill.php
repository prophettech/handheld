<?php
//error_reporting(0);
require_once('Till_datatypes.php');


class DAL_ProphetTill
{
  private $ButtonDataList = array();
  private $host;
  private $username;
  private $password;
  private $dbconn;
  private $dbtrans;
  private $ErrorMsg;
  private $TmpOrderData;

  private $UserDBHost;
  private $UserDBUsername;
  private $UserDBPassword;
  private $UserDBConn;
  private $UserDBTrans;

  private $TRANS_STATE_SAVED;
  private $TRANS_STATE_PRINTED_KITCHEN;
  private $TRANS_STATE_PRINTED;
  private $TRANS_STATE_POSTED;

  function __construct()
  {
    $this->host = "localhost:c:/netshare/db/mtal.gdb";
    $this->username = "SYSDBA";
    $this->password = "masterkey";

    $this->UserDBHost = "localhost:c:/netshare/db/userdb.gdb";
    $this->UserDBUsername = "SYSDBA";
    $this->UserDBPassword = "masterkey";

    // TRANS_STATE from dataaccess.h in Accounts/Till
    $this->TRANS_STATE_SAVED = 1;
    $this->TRANS_STATE_PRINTED = 2;
    $this->TRANS_STATE_POSTED = 4;
    $this->TRANS_STATE_PRINTED_KITCHEN = 12;
  }

  public function GetErrorMsg()
  {
    return $this->ErrorMsg;
  }

  public function OpenDB()
  {

    $this->dbconn = ibase_connect ($this->host, $this->username, $this->password);
  }

  public function CloseDB()
  {
    ibase_close($this->dbconn);
  }

  public function OpenUsersDB()
  {
    $this->UserDBConn = ibase_connect ($this->UserDBHost, $this->UserDBUsername, $this->UserDBPassword);
  }

  public function CloseUsersDB()
  {
    ibase_close($this->UserDBConn);
  }

  public function RunSelectQry($in_SelectQry)
  {
    //print $in_SelectQry;
    $this->dbtrans = ibase_trans( IBASE_DEFAULT,$this->dbconn );
    $result = ibase_query ($this->dbtrans, $in_SelectQry);

    return $result;
  }

  public function RunUserSelectQry($in_SelectQry)
  {
    $this->UserDBTrans = ibase_trans( IBASE_DEFAULT, $this->UserDBConn );
    $result = ibase_query ($this->UserDBTrans, $in_SelectQry);

    return $result;
  }

  public function FreeQry($in_FreeQry)
  {
    ibase_free_result($in_FreeQry);
  }

  // in_CatType 0 = All, 1 = Main Categories, 2 = Sub Categories
  public function GetCategoryList($in_CatType)
  {
    //$this->ButtonDataList = null;
    $this->OpenDB();
    if( $in_CatType == 0 )
    {
      $qryResult = $this->RunSelectQry("SELECT * FROM EPOS_CLASS ORDER BY POS");
    }
    else if( $in_CatType == 1 )
    {
      $qryResult = $this->RunSelectQry("SELECT * FROM EPOS_CLASS WHERE SUBCAT = 0 ORDER BY POS");
    }
    else if( $in_CatType == 2 )
    {
      $qryResult = $this->RunSelectQry("SELECT * FROM EPOS_CLASS WHERE SUBCAT = 1 ORDER BY POS");
    }
    $CatList = array();
    while( $row = ibase_fetch_row($qryResult) )
    {
      $cat = array();
      $cat["But_ID"] = $row[0];
      $cat["But_Text1"] = $row[1];
      $cat["But_Colour_Default"] = $row[8]; // Cat Back Colour
      $cat["But_FontColour_Default"] = $row[9]; // Cat Font Colour
      if( $cat["But_Colour_Default"] == null )
        $cat["But_Colour_Default"] = -1;
      if( $cat["But_FontColour_Default"] == null )
        $cat["But_FontColour_Default"] = -1;
      $CatList[] = $cat;
      //$NewCat = new ButtonData();
      //$NewCat->But_ID = $row[0];    // Cat ID
      //$NewCat->But_Text1 = $row[1]; // Cat Desc
      //$NewCat->But_Text2 = $row[8]; // Cat Back Colour
      //$NewCat->But_Text3 = $row[9]; // Cat Font Colour
      //$this->ButtonDataList[] = $NewCat;
    }
    $this->FreeQry($qryResult);
    $this->CloseDB();
    return $CatList;
    //return $this->ButtonDataList;

  }

  public function ANDROID_GetProductList($in_SubProds)
  {
    $ProdList = array();
    //$this->ButtonDataList = null;
    $this->OpenDB();

    $strSQL = "SELECT STOCK_DETAILS.STOCK_CODE, STOCK_DETAILS.DESCRIPTION, STOCK_DETAILS_EPOS.PLU_CLASS, STOCK_DETAILS.SALE_PRICE, STOCK_DETAILS_EPOS.TOPPING_ID, " .
              " STOCK_DETAILS_EPOS.HAPPY_HOUR_PRICE, STOCK_DETAILS.VAT_CODE, STOCK_DETAILS_EPOS.PRODUCT_OFFER, PLU_COLOUR, PLU_FONTCOLOUR, " .
              " STOCK_DETAILS_EPOS.EPOS_PROD_ID, STOCK_DETAILS_EPOS.SECTION_ID,STOCK_DETAILS_EPOS.PLU_PARENTCLASS, STOCK_DETAILS_EPOS.SECTION_ID, " .
              " STOCK_DETAILS_EPOS.PARENT_STOCKCODE, STOCK_DETAILS_EPOS.PRODUCT_OFFER,STOCK_DETAILS_EPOS.EPOS_PROD_ID,STOCK_DETAILS_EPOS.PLU_SALE_PRICE " .
              " FROM STOCK_DETAILS " .
              " JOIN STOCK_DETAILS_EPOS ON STOCK_DETAILS.STOCK_CODE = STOCK_DETAILS_EPOS.STOCK_CODE ";


    if( $in_SubProds == 0)
    {
      $strSQL .= " WHERE (STOCK_DETAILS_EPOS.PLU_CLASS = STOCK_DETAILS_EPOS.PLU_PARENTCLASS)";
    }
    else
    {
      $strSQL .= " WHERE (STOCK_DETAILS_EPOS.PLU_CLASS != STOCK_DETAILS_EPOS.PLU_PARENTCLASS)";
    }

    $strSQL .= " ORDER BY PLU_PARENTCLASS, PLU_NO, STOCK_CODE";
    $qryResult = $this->RunSelectQry( $strSQL );

    while( $row = ibase_fetch_row($qryResult) )
    {
    // Use short field names to reduce data transfer size
    // F1: PCode
    // F2: PDesc
    // F3: PPrice
    // F4: PClass
    // F5: PParentClass
    // F6: PSectionID
    // F7: POptionsID
    // F8: PHHPrice
    // F9: PVATCode
    // F10: PButGroup1ID
    // F11: PButGroup2ID
    // F12: PParentCode
    // F13: PProductOffer
    // F14: PPrintToKitchen
    // F15: PPLUSalePrice
    // F16: PTP1
    // F17: PTP2
    // F18: PTP3
    // F19: PTP4
    // F20: PTP5
    // F21: PTP6
    // F22: PTP7
    // F23: PTP8
      $ProdDetails = array();
      $ProdDetails["F1"] = $row[0];    // PCode
      $ProdDetails["F2"] = $row[1];    // PDesc

      $ProdDetails["F3"] = -1;         // PPrice
      if ($row[3] == '0' || $row[3] > '' )
        $ProdDetails["F3"] = $row[3];

      $ProdDetails["F4"] = $row[2];    // PClass
      $ProdDetails["F5"] = $row[12];   // PParentClass
      $ProdDetails["F6"] = $row[13];   // PSectionID
      if( $ProdDetails["F6"] == null )
        $ProdDetails["F6"] = 0;

      $ProdDetails["F7"] = $row[4];    // POptionsID
      if( $ProdDetails["F7"] == null )
        $ProdDetails["F7"] = 0;

      $ProdDetails["F8"] = -1;         // PHHPrice
      if ($row[5] == '0' || $row[5] > '' )
        $ProdDetails["F8"] = $row[5];

      $ProdDetails["F9"] = $row[6];    // PVATCode
      if( $ProdDetails["F9"] == null )
        $ProdDetails["F9"] = 0;

      //$ProdDetails["F10"] = $row[14];  // PButGroup1ID
      //if( $ProdDetails["F10"] == null )
      //  $ProdDetails["F10"] = 0;

      //$ProdDetails["F11"] = $row[15];  // PButGroup2ID
      //if( $ProdDetails["F11"] == null )
      //  $ProdDetails["F11"] = 0;

      $ProdDetails["F12"] = $row[14];   // PParentCode
      $ProdDetails["F13"] = $row[15];   // PProductOffer
      if( $ProdDetails["F13"] == null )
        $ProdDetails["F13"] = 0;

      //$ProdDetails["F14"] = $row[18];  // PPrintToKitchen

      $ProdDetails["F15"] = -1;          // PPLUSalePrice
      if ($row[17] == '0' || $row[17] > '' )
        $ProdDetails["F15"] = $row[17];

      //$ProdDetails["F16"] = $row[20];  // PTP1
      //$ProdDetails["F17"] = $row[21];  // PTP2
      //$ProdDetails["F18"] = $row[22];  // PTP3
      //$ProdDetails["F19"] = $row[23]; // PTP4
      //$ProdDetails["F20"] = $row[24]; // PTP5
      //$ProdDetails["F21"] = $row[25]; // PTP6
      //$ProdDetails["F22"] = $row[26]; // PTP7
      //$ProdDetails["F23"] = $row[27]; // PTP8



      $ProdList[] = $ProdDetails;

    }
    $this->FreeQry($qryResult);
    $this->CloseDB();

    return $ProdList;
  }

  public function GetProdOptionsList()
  {
    $ProdOptionsList = array();
    $this->OpenDB();
    $qryResult = $this->RunSelectQry( "select ID, BUTG1LISTID,BUTG2LISTID,  HALFHALF, NAME from EPOS_TOPPING_GROUP order by ID" );
    while( $row = ibase_fetch_row($qryResult) )
    {
      $prodOption = array();
      $prodOption["But_ID"] = $row[0];
      $prodOption["But_Text1"] = $row[1];
      if( $prodOption["But_Text1"] == null )
        $prodOption["But_Text1"] = 0;
      $prodOption["But_Text2"] = $row[2];
      if( $prodOption["But_Text2"] == null )
        $prodOption["But_Text2"] = 0;
      $prodOption["But_Text3"] = $row[3];
      if( $prodOption["But_Text3"] == null )
        $prodOption["But_Text3"] = 0 ;

      $prodOption["But_Text4"] = $row[4];
      $ProdOptionsList[] = $prodOption;
    }
    $this->FreeQry($qryResult);
    $this->CloseDB();
    return $ProdOptionsList;
  }

  public function GetProdOptionButtonsList()
  {
    $ButtonsList = array();
    $this->OpenDB();
    $qryResult = $this->RunSelectQry( "select ID, BUT_TEXT1, BUT_TEXT2, BUT_TEXT3, BUT_TEXT4, BUT_TEXT5, BUT_GROUP_NAME, GROUP_TYPE from EPOS_TOPGROUPLIST order by ID" );
    while( $row = ibase_fetch_row($qryResult) )
    {
      $Button = array();
      $Button["But_ID"] = $row[0];
      $Button["But_Text1"] = $row[1];
      $Button["But_Text2"] = $row[2];
      $Button["But_Text3"] = $row[3];
      $Button["But_Text4"] = $row[4];
      $Button["But_Text5"] = $row[5];
      $Button["But_Text6"] = $row[6];

      $Button["But_Text7"] = $row[7];
      if( $Button["But_Text7"] == null )
        $Button["But_Text7"] = 0;

      $ButtonsList[] = $Button;
    }
    $this->FreeQry($qryResult);
    $this->CloseDB();
    return $ButtonsList;
  }

  public function GetProdOptionToppingsList()
  {
    $ToppingList = array();
    $this->OpenDB();
    $qryResult = $this->RunSelectQry( "select EPOS_TOPPING_GROUP_ITEMS.ID, EPOS_TOPPING_GROUP_ITEMS.TOPPINGGROUPID, EPOS_TOPPING_GROUP_ITEMS.PRODCODE, " .
                                      " STOCK_DETAILS.DESCRIPTION, STOCK_DETAILS_EPOS.TOP_TYPE, STOCK_DETAILS_EPOS.PLU_SALE_PRICE " .
                                      " from EPOS_TOPPING_GROUP_ITEMS " .
                                      " left join STOCK_DETAILS on EPOS_TOPPING_GROUP_ITEMS.PRODCODE = STOCK_DETAILS.STOCK_CODE " .
                                      " left join STOCK_DETAILS_EPOS on STOCK_DETAILS.STOCK_CODE = STOCK_DETAILS_EPOS.STOCK_CODE " .
                                      " order by EPOS_TOPPING_GROUP_ITEMS.ID");
    while( $row = ibase_fetch_row($qryResult) )
    {
      $Topping = array();
      $Topping["TopID"] = $row[0];
      $Topping["TopGroupID"] = $row[1];
      $Topping["TopProdCode"] = $row[2];
      $Topping["TopDesc"] = $row[3];
      $Topping["TopType"] = $row[4];
      $Topping["TopFixedPrice"] = $row[5];

      $ToppingList[] = $Topping;
    }
    $this->FreeQry($qryResult);
    $this->CloseDB();
    return $ToppingList;
  }



  public function GetProdToppingsList($in_ProdCode)
  {
    $ToppingList = array();
    $this->OpenDB();
    $qryResult = $this->RunSelectQry( "select STOCK_COMPONENT.COMP_CODE, STOCK_COMPONENT.STOCK_CODE, STOCK_COMPONENT.SUB_ASSEMBLY_CODE, " .
                                      " STOCK_COMPONENT.QUANTITY, STOCK_DETAILS.DESCRIPTION, STOCK_DETAILS_EPOS.TOP_TYPE" .
                                      " from STOCK_COMPONENT " .
                                      " left join STOCK_DETAILS on sub_assembly_code = STOCK_DETAILS.STOCK_CODE " .
                                      " left join STOCK_DETAILS_EPOS on STOCK_DETAILS.STOCK_CODE = STOCK_DETAILS_EPOS.STOCK_CODE " .
                                      " order by STOCK_COMPONENT.COMP_CODE");
    while( $row = ibase_fetch_row($qryResult) )
    {
      $Topping = array();
      $Topping["TopID"] = $row[0];
      $Topping["TopProdCode"] = $row[1];
      $Topping["TopSubCode"] = $row[2];
      $Topping["TopQty"] = $row[3];
      $Topping["TopDesc"] = $row[4];
      $Topping["TopType"] = $row[5];

      $ToppingList[] = $Topping;
    }
    $this->FreeQry($qryResult);
    $this->CloseDB();
    return $ToppingList;
  }



  public function GetNextOrderNo($in_OpenDB=true)
  {
      $this->OpenDB();
      $qryResult = $this->RunSelectQry("SELECT COUNTER_NO FROM INV_CRED_COUNTER ORDER BY COUNTER_NO DESC");
      $row = ibase_fetch_row($qryResult);
      $LastOrderNo = $row[0];
      $this->FreeQry($qryResult);
      $this->CloseDB();

      return ($LastOrderNo+=1);
  }

  public function ANDROID_SaveOrder($in_orderDetailsObj)
  {

    $RetryCount = 0;
    $SaveOrder = true;
    $OrderResult = false;
    $retry = true;
    $DoCommit = true;

    $this->TmpOrderData = $in_orderDetailsObj;
    //print_r($in_orderDetailsObj);
    $InsertingOrder = $this->TmpOrderData->O18;
    $OrderTotal = $this->TmpOrderData->O6;
    //print "insert = " . $InsertingOrder;
    do
    {
      $DoCommit = false;

      try
      {
        $this->OpenDB();
        $this->dbtrans = ibase_trans( IBASE_DEFAULT,$this->dbconn );


        if( $InsertingOrder == 1 )
        {

          $this->TmpOrderData->O1 = $this->GetNextInvID();

          // Get next order no
          //print "order no before: " . $this->TmpOrderData->O2;
          $this->TmpOrderData->O2 = $this->CheckOrderNo();
          //print "order no after: " . $this->TmpOrderData->O2;
          if( $this->TmpOrderData->O2 == 0 )  // Error if OrderNo is 0
          {
            $SaveOrder = false;
          }
          else
          {
            $SaveOrder = $this->InsertCounterNo($this->TmpOrderData->O2);
          }
        }
        else
        {
          // Delete order items
          $SaveOrder = $this->DeleteOrderItems();
        }

        if( $SaveOrder )
        {
          // Insert/Update the order
          if( $this->UpdateOrder($InsertingOrder) )
          {
            if( $this->$OrderTotal > 0.00) // Items to insert
            {
                // Insert Order Items
                if( $this->InsertOrderItems() )
                {
                  //$DoCommit = true;

                  // Insert Split Bill Items
                  if( $this->InsertSplitBillItems() )     // Ray 23/03/12. Added
                  {
                    if( $this->InsertSplitPayItems() )     // Ray 23/05/13. Added
                    {
                      $DoCommit = true;
                    }
                  }
                }
                else
                {
                  $this->ErrorMsg = "ERROR 0003: Order Items could not be inserted";
                }
          }
         else
         {
             $this->ErrorMsg = "ERROR 0d3: no order total";
                $DoCommit = true;
         }
        }
        else
        {
            $this->ErrorMsg = "ERROR 0101: Order Items could not be inserted";
        }

          if( $DoCommit )
          {
            if( ibase_commit($this->dbtrans) )
            {
              $OrderResult = true;
              $retry = false;

            }
            else
            {
              ibase_rollback($this->dbtrans);
              $this->ErrorMsg = "ERROR 0004: DB Qry Commit Error";
            }
          }
          else
          {
            ibase_rollback($this->dbtrans);
          }
        }
      }
      catch( Exception $e )
      {
        ibase_rollback($this->dbtrans);
        $this->ErrorMsg = "ERROR 0001: DB Exception: " . $e;
      }
      $this->CloseDB();
      if( $RetryCount < 1 )
      {
        $RetryCount++;
      }
      else
      {
        $retry = false;
        if( $this->ErrorMsg == "" )
          $this->ErrorMsg = "ERROR 0007: DB Busy";
      }
    } while( $retry );

    //print "oid " . $this->TmpOrderData->orderID . "<br />";
    $retValues = array();
    $SavedOrderID = 0;
    if( $OrderResult == true )
    {
      $SavedOrderID = $this->TmpOrderData->O1;
      $retValues[] = $SavedOrderID;
      $retValues[] = $this->TmpOrderData->O17;
    }

    $this->TmpOrderData = null;

    return $retValues;
    //return $SavedOrderID;
    //return $OrderResult;

  }

  private function UpdateOrder($in_InsertOrder)
  {
  			// Use short field names to reduce data  size
		    // O1: orderID
		    // O2: orderNo
		    // O3: orderDate
		    // O4: orderTableNo
		    // O5: orderPaidAmt
		    // O6: orderTotal
		    // O7: orderAccRef
		    // O8: orderPrepayment
		    // O9: orderNet
		    // O10: orderVat
		    // O11: orderVATCode
		    // O12: orderDiscAmt
		    // O13: orderDiscRate
		    // O14: orderTableCovers
		    // O15: orderDeliveryType
		    // O16: orderPayType
		    // O17: orderLastTransID
		    // O18: orderInsert
		    // O19: orderDeposit
		    // O20: orderTip
		    // O21: orderTipCC
		    // O22: orderServiceCharge
		    // O23: orderServiceChargePerc
			// O24: orderPrintedToKitchen
			// O25: orderBillPrinted

    $OrderID = $this->TmpOrderData->O1;
    $OrderNo = $this->TmpOrderData->O2;
    //$OrderDateTest = str_replace('/','.',$this->TmpOrderData->orderDate);
    //$OrderDate = '03/13/13';
    $OrderDate = str_replace('/','.',$this->TmpOrderData->O3);
    $OrderTableNum = $this->TmpOrderData->O4;
    $PaidAmt = $this->TmpOrderData->O5;
    $OrderTotal =  $this->TmpOrderData->O6;
    $OrderAccRef =  $this->TmpOrderData->O7;
    $OrderPrePay = $this->TmpOrderData->O8;
    $OrderBalance = $OrderTotal - $OrderPrePay;
    $NetAmount = $this->TmpOrderData->O9;
    $VATAmount = $this->TmpOrderData->O10;
    $VatCode = $this->TmpOrderData->O11;
    $DiscAmt = $this->TmpOrderData->O12;
    $LoyaltyDisc = $this->TmpOrderData->O12;
    $LoyaltyDiscRate = $this->TmpOrderData->O13;
    $Covers = $this->TmpOrderData->O14;
    $OrderType = $this->TmpOrderData->O15;
    //$OrderType  = $OrderDateTest; // TODO: Remove
    $PayTypeCode = $this->TmpOrderData->O16;
    $CurInvTransID = $this->TmpOrderData->O17;
    $OrderDeposit = $this->TmpOrderData->O19;
    $OrderTip = $this->TmpOrderData->O20;
    $OrderTipCC = $this->TmpOrderData->O21;
    $OrderServiceCharge = $this->TmpOrderData->O22;
    $OrderServiceChargePerc = $this->TmpOrderData->O23;
    $OrderPrintedToKitchen = $this->TmpOrderData->O24;
    $OrderPrintedBill = $this->TmpOrderData->O25;

               /*
    $TRANS_STATE_SAVED = 1;
    $TRANS_STATE_PRINTED = 2;
    $TRANS_STATE_POSTED = 4;
    $TRANS_STATE_PRINTED_KITCHEN = 12;
    $TransStateCode = $TRANS_STATE_SAVED;
    */

    $TransStateCode = $this->TRANS_STATE_SAVED;
    if( $OrderPrintedToKitchen == 1 )
    {
      $TransStateCode = $this->TRANS_STATE_PRINTED_KITCHEN;
    }

    if( $OrderPrintedBill == 1 )
    {
      $TransStateCode = $this->TRANS_STATE_PRINTED;
    }

    $NewInvTransID = $this->GetInvTransID();
    $OrdTime = date('H:i', time() );

    if( $OrderTableNum == "0" )   // Ray 01/11/12. Added
      $OrderTableNum = "";

    if( $in_InsertOrder )
    {
      // Insert
      $qrySQL = "INSERT INTO customer_invoice (INVOICE_NO, EPOS_TABLE_NUM, INVOICE_DATE, TRANS_TYPE_CODE, TRANS_STATE_CODE, ID, PREPAYMENT, CUST_ACCREF, INVOICE_TOTAL_GROSS, BALANCE, PREPAYMENT_REFERENCE, PAYMENT_TYPE_CODE, NET, VAT, VAT_CODE, CARRIAGE, CARRIAGE_VAT_CODE, CARRIAGE_VAT, CARRIAGE_GROSS, DISCOUNT, LOYALITY_DISCOUNT, LOYALITY_DISCOUNT_RATE,CUSTOMER_ORDER_NO,NOTE_2,LASTTRANSID, NOTE_1, PAYMENTBANKACC, PAYMENTPOSTTYPE, ROUTE_DATE, INV_DEPOSIT, INV_TIP, INV_TIPCC, INV_SERVICECHARGE, INV_SERVICECHARGEPERC, TRANS_REFERENCE_CODE )
                    VALUES ($OrderNo, '$OrderTableNum', '$OrderDate', 1, $TransStateCode, $OrderID, $PaidAmt,'$OrderAccRef',$OrderTotal, $OrderBalance, 'Till Payment', $PayTypeCode,$NetAmount,$VATAmount,$VatCode,0,0,0,0,$DiscAmt,$LoyaltyDisc,$LoyaltyDiscRate, '$Covers', '$OrderType',$NewInvTransID, '$OrdTime',1200, 1, '$OrderDate', $OrderDeposit, $OrderTip, $OrderTipCC, $OrderServiceCharge, $OrderServiceChargePerc, 0)";
    }
    else
    {
      // Update
      $qrySQL = "UPDATE customer_invoice SET INVOICE_NO = $OrderNo,
                                             EPOS_TABLE_NUM = '$OrderTableNum',
                                             INVOICE_DATE = '$OrderDate',
                                             TRANS_TYPE_CODE = 1,
                                             TRANS_STATE_CODE = $TransStateCode,
                                             PREPAYMENT = $PaidAmt,
                                             CUST_ACCREF = '$OrderAccRef',
                                             INVOICE_TOTAL_GROSS = $OrderTotal,
                                             BALANCE = $OrderBalance,
                                             PAYMENT_TYPE_CODE = $PayTypeCode,
                                             NET = $NetAmount,
                                             VAT = $VATAmount,
                                             VAT_CODE = $VatCode,
                                             CARRIAGE = 0,
                                             CARRIAGE_VAT_CODE = 0,
                                             CARRIAGE_VAT = 0,
                                             CARRIAGE_GROSS = 0,
                                             DISCOUNT = $DiscAmt,
                                             LOYALITY_DISCOUNT = $LoyaltyDisc,
                                             LOYALITY_DISCOUNT_RATE = $LoyaltyDiscRate,
                                             CUSTOMER_ORDER_NO = '$Covers',
                                             NOTE_2 = '$OrderType',
                                             LASTTRANSID = $NewInvTransID,
                                             PAYMENTBANKACC = 1200,
                                             PAYMENTPOSTTYPE = 1,
                                             ROUTE_DATE = '$OrderDate',
                                             INV_DEPOSIT = $OrderDeposit,
                                             INV_TIP = $OrderTip,
                                             INV_TIPCC = $OrderTipCC,
                                             INV_SERVICECHARGE = $OrderServiceCharge,
                                             INV_SERVICECHARGEPERC = $OrderServiceChargePerc,
                                             TRANS_REFERENCE_CODE = 0,
                                             NOTE_1 = '$OrdTime'
                                             WHERE ID = $OrderID "";

      if( $CurInvTransID > 0 )
      {
        $qrySQL .= " AND LASTTRANSID=$CurInvTransID";
      }
    }

      $qry = ibase_prepare($this->dbtrans,$qrySQL);
      $qryResult = ibase_execute($qry);

      if( $qryResult )
      {
        $recs = ibase_affected_rows($this->dbtrans);
        if($recs > 0 )
        {
          $this->TmpOrderData->O17 = $NewInvTransID;
          return true;
        }
        else if($recs == 0)
        {

        }
        else
        {
          $this->ErrorMsg = "ERROR 0008: Order cannot be saved as it has been changed by another user ";
          //$this->ErrorMsg = $qrySQL;
          return false;
        }
      }
      else
      {
          $this->ErrorMsg = "ERROR 0002: Order could not be inserted/updated";
          return false;
      }

    return true;
  }

  private function InsertOrderItems()
  {
    $ItemInserted = false;
    $ItemCount = count($this->TmpOrderData->orderItems);
    $ItemReceiptID = 0;
    for( $i=0; $i < $ItemCount; $i++ )
    {
      			// Use short field names to reduce data  size
		    // I1: itemCode
		    // I2: itemDesc
		    // I3: itemQty
		    // I4: itemSalePrice
		    // I5: itemNET
		    // I6: itemVAT
		    // I7: itemVATCode
		    // I8: itemVATRate
		    // I9: itemID
		    // I10: itemPosID
		    // I11: itemSubPosID
		    // I12: itemProdType
		    // I13: itemProdClass
		    // I14: itemParentProdCode
		    // I15: itemParentProdDesc
		    // I16: itemVatableAmt
		    // I17: itemExtraToppingPrice
		    // I18: itemLinkID
		    // I19: itemSectionPos
		    // I20: itemSectionDesc
		    // I21: itemTopBut1
		    // I22: itemTopBut2
		    // I23: itemFreeText
			// I24: itemSplitQty
			// I25: itemPrintedQty
			// I26: temSplitPrintedQty
			// I27: itemMDID
			// I28: itemPrintToKitchen
			// I29: itemToppingList
      $ItemReceiptID = $this->TmpOrderData->orderItems[$i]->I9;
      $ItemMasterID = $ItemReceiptID;

      $ItemSectionPos = $this->TmpOrderData->orderItems[$i]->I19;
      $ItemSectionDesc = $this->TmpOrderData->orderItems[$i]->I20;

      $ItemCat              = $this->TmpOrderData->orderItems[$i]->I13;
      $ItemType             = 0;//$this->TmpOrderData->orderItems[$i]->itemProdType;
      $ItemCode             = $this->TmpOrderData->orderItems[$i]->I1;
      $ItemDesc             = $this->TmpOrderData->orderItems[$i]->I2;
      $ItemQty              = $this->TmpOrderData->orderItems[$i]->I3;
      $ItemSplitQty         = $this->TmpOrderData->orderItems[$i]->I24;
      $ItemPrintedQty       = $this->TmpOrderData->orderItems[$i]->I25;
      $ItemSplitPrintedQty  = $this->TmpOrderData->orderItems[$i]->I26;
      $ItemPrice            = $this->TmpOrderData->orderItems[$i]->I4;
      $ItemNET              = $this->TmpOrderData->orderItems[$i]->I5;
      $ItemVAT              = $this->TmpOrderData->orderItems[$i]->I6;
      $ItemVATCode          = $this->TmpOrderData->orderItems[$i]->I7;
      $ItemVATRate          = $this->TmpOrderData->orderItems[$i]->I8;
      $ItemTopBut1          = $this->TmpOrderData->orderItems[$i]->I21;
      $ItemTopBut2          = $this->TmpOrderData->orderItems[$i]->I22;
      $ItemToppings         = $this->TmpOrderData->orderItems[$i]->I29;
      $InvID                = $this->TmpOrderData->O1;
      $OrderNo              = $this->TmpOrderData->O2;
      $ItemMDID             = $this->TmpOrderData->orderItems[$i]->I27;
      $FreeText             = $this->TmpOrderData->orderItems[$i]->I23;
      $PrintToKitchen       = $this->TmpOrderData->orderItems[$i]->I28;
     	$ExtraToppingPrice    = $this->TmpOrderData->orderItems[$i]->I17;

      if( $ItemMDID == "" )
        $ItemMDID = 0;

      if( $ItemVATCode == "" )
        $ItemVATCode = 0;

      if( $ItemVATRate == "" )
        $ItemVATRate = 0;

      //print_r($ItemToppings);

      $InvItemID = $this->GetNextInvItemID();
      if( $ItemPrice == "" )
        $ItemPrice = 0;

      $ItemSplitBillID =  0;
      $ItemSplitBillRef =  '';

      $ItemCostPrice = 0;
      //$ItemVatAmount = 0;
      $ItemVatableAmount = 0;
      //$ItemVatRate = 0;
      $ValidItem = 1;
      $ItemAllowSpendOffer = 0;
      $ItemToppingType = "";
      //$ExtraToppingPrice = 0;
      //$ItemTable = 1;
      //$PrintToKitchen = 1;

      $ItemTable = 1;
     	if( $PrintToKitchen == 0 )
     	  $ItemTable = 10;

      if( $InvItemID > 0 )
      {
        $qrySQL =  "INSERT INTO customer_invoice_items(ITEM_CODE, INV_ID,INVOICE_NO, STOCK_CODE, STOCK_DESC,QUANTITY,UNIT_SALE_PRICE,UNIT_COST_PRICE,NET,VAT,VAT_CODE,VAT_RATE,NOMINAL_CODE)
                          VALUES( $InvItemID, $InvID, $OrderNo, '$ItemCode', '$ItemDesc', $ItemQty, $ItemPrice,0,$ItemNET,$ItemVAT,$ItemVATCode,$ItemVATRate,4000 )";
        $InvItemCode = $InvItemID;

        $qry = ibase_prepare($this->dbtrans,$qrySQL);
        $qryResult = ibase_execute($qry);

        if( $qryResult )
        {
          $InvItemID = $this->GetNextInvItemID();
          if( $InvItemID > 0  )
          {
            // Insert Inv Item Epos record
            if( $ItemTopBut1 > 0 )
            {
              if( $ItemTopBut2 == null )
                $ItemTopBut2 = 0;

                $qrySQL = "INSERT INTO customer_invoice_items_epos(EPOS_ITEM_ID, INV_ID, MASTERID,PRODCODE,PRODDESC,QTY,SPLITQTY,PRINTEDQTY, SPLITPRINTEDQTY, UNITPRICE,PRODTYPE,PRODCLASS, ID, MD_ID, MD_ALLOWSPENDOFFER, VATAMOUNT, VATABLEAMOUNT, VALIDITEM, TOPPINGTYPE, ITEMTABLE, VATRATE, COSTPRICE, EXTRATOPPINGSPRICE, TOPBUT1, TOPBUT2,SECTIONPOS,SECTIONDESC, INV_ITEM_CODE )
                                  VALUES( $InvItemID, $InvID, $ItemReceiptID,'$ItemCode','$ItemDesc',$ItemQty,$ItemSplitQty,$ItemPrintedQty,$ItemSplitPrintedQty,$ItemPrice+$ItemVAT,$ItemType,$ItemCat, $ItemReceiptID, $ItemMDID, $ItemAllowSpendOffer,$ItemVAT,$ItemVatableAmount,$ValidItem,'$ItemToppingType',$ItemTable,$ItemVATRate,$ItemCostPrice,$ExtraToppingPrice, $ItemTopBut1, $ItemTopBut2,$ItemSectionPos, '$ItemSectionDesc', $InvItemCode)";
            }
            else
            {
                $qrySQL = "INSERT INTO customer_invoice_items_epos(EPOS_ITEM_ID, INV_ID, MASTERID,PRODCODE,PRODDESC,QTY,SPLITQTY,PRINTEDQTY, SPLITPRINTEDQTY, UNITPRICE,PRODTYPE,PRODCLASS, ID, MD_ID, MD_ALLOWSPENDOFFER, VATAMOUNT, VATABLEAMOUNT, VALIDITEM, TOPPINGTYPE, ITEMTABLE, VATRATE, COSTPRICE, EXTRATOPPINGSPRICE,SECTIONPOS,SECTIONDESC, INV_ITEM_CODE  )
                                  VALUES( $InvItemID, $InvID, $ItemReceiptID,'$ItemCode','$ItemDesc',$ItemQty,$ItemSplitQty,$ItemPrintedQty,$ItemSplitPrintedQty,$ItemPrice+$ItemVAT,$ItemType,$ItemCat, $ItemReceiptID,$ItemMDID, $ItemAllowSpendOffer,$ItemVAT,$ItemVatableAmount,$ValidItem,'$ItemToppingType',$ItemTable,$ItemVATRate,$ItemCostPrice,$ExtraToppingPrice, $ItemSectionPos, '$ItemSectionDesc', $InvItemCode)"";
            }

            $qry = ibase_prepare($this->dbtrans,$qrySQL);
            $qryResult = ibase_execute($qry);
            if( $qryResult )
            {
              $ItemInserted = true;
              $TopCount = count($ItemToppings);
              // Use short field names to reduce data size
					    // T1: itemTopState
					    // T2: itemTopCode
					    // T3: itemTopDesc
					    // T4: itemTopID

              for( $t=0; $t<$TopCount; $t++ )
              {
                ++$ItemReceiptID;
                $TopCode = $ItemToppings[$t]->T2;
                $TopDesc = $ItemToppings[$t]->T3;

                $ItemTable = 2;
                if( $PrintToKitchen == 0 )
                {
                  $ItemTable = 11;
                }

                $ItemTopType = 0;
                if( $ItemToppings[$t]->T1 == 2 ) // Plus toppings
                {
                  $InsertTopItem = true;
                  if( $ItemToppingType == 'MD' )
                  {
                    $TopToppingType = "MD+";
                  }
                  else
                  {
                    $TopToppingType = "+";
                    $ItemTopType = 2;
                  }
                }
                else if( $ItemToppings[$t]->T1 == 1 )  // Minus toppings
                {
                  if( $ItemToppingType == 'MD' )
                  {
                    $TopToppingType = "MD-";
                  }
                  else
                  {
                    $TopToppingType = "-";
                    $ItemTopType = 3;
                  }
                }

                  $InvItemID = $this->GetNextInvItemID();
                  if( $InvItemID > 0  )
                  {
                    $qrySQL =  "INSERT INTO customer_invoice_items(ITEM_CODE, INV_ID,INVOICE_NO, STOCK_CODE, STOCK_DESC,QUANTITY,UNIT_SALE_PRICE,UNIT_COST_PRICE,NET,VAT,VAT_CODE,NOMINAL_CODE)
                             VALUES( $InvItemID, $InvID, $OrderNo, '$TopCode', '$TopDesc', $ItemQty, 0,0,0,0,0,4000)";

                    $qry = ibase_prepare($this->dbtrans,$qrySQL);
                    $qryResult = ibase_execute($qry);

                    if( $qryResult )
                    {
                      $InvItemID = $this->GetNextInvItemID();
                      if( $InvItemID > 0  )
                      {
                          $qrySQL = "INSERT INTO customer_invoice_items_epos(EPOS_ITEM_ID, INV_ID, MASTERID,PRODCODE,PRODDESC,QTY,SPLITQTY,UNITPRICE,PRODTYPE,PRODCLASS, TOPPINGTYPE, ID, MD_ID, MD_ALLOWSPENDOFFER, VATAMOUNT, VATABLEAMOUNT, VALIDITEM, ITEMTABLE, VATRATE, COSTPRICE )
                                  VALUES( $InvItemID, $InvID,$ItemMasterID,'$TopCode','$TopDesc',$ItemQty,$ItemSplitQty,0,$ItemTopType, $ItemCat, '$TopToppingType',$ItemReceiptID, $ItemMDID, $ItemAllowSpendOffer,$ItemVAT,$ItemVatableAmount,$ValidItem,$ItemTable,$ItemVATRate,$ItemCostPrice)";

                        $qry = ibase_prepare($this->dbtrans,$qrySQL);
                        $qryResult = ibase_execute($qry);

                        if( !$qryResult )
                        {
                          $ItemInserted = false;
                          break;  // Error, exit loop
                        }
                      }
                      else
                      {
                        $ItemInserted = false;
                        break;  // Error, exit loop
                      }
                    }
                    else
                    {
                      $ItemInserted = false;
                      break;  // Error, exit loop
                    }
                  }
                  else
                  {
                    $ItemInserted = false;
                    break;  // Error, exit loop
                  }


              }

              if( $InsertTopItem && $ExtraToppingPrice > 0 )
              {
                $InvItemID = $this->GetNextInvItemID();
                if( $InvItemID > 0  )
                {

                  $qrySQL =  "INSERT INTO customer_invoice_items(ITEM_CODE, INV_ID,INVOICE_NO, STOCK_CODE, STOCK_DESC,QUANTITY,UNIT_SALE_PRICE,UNIT_COST_PRICE,NET,VAT,VAT_CODE,NOMINAL_CODE)
                             VALUES( $InvItemID, $InvID, $OrderNo, 'TOPITEM', 'Toppings', 1, $ExtraToppingPrice,0,0,0,0,4000)";

                  $qry = ibase_prepare($this->dbtrans,$qrySQL);
                  $qryResult = ibase_execute($qry);

                  if( $qryResult )
                  {
                    $InvItemID = $this->GetNextInvItemID();

                    $TopReceiptID = ++$ItemReceiptID;
                    // Insert Inv Item Epos record
                    $qrySQL = "INSERT INTO customer_invoice_items_epos(EPOS_ITEM_ID, INV_ID, MASTERID,PRODCODE,PRODDESC,QTY,SPLITQTY,UNITPRICE,PRODTYPE,PRODCLASS, ID, MD_ID, MD_ALLOWSPENDOFFER, VATAMOUNT, VATABLEAMOUNT, VALIDITEM, TOPPINGTYPE, ITEMTABLE, VATRATE, COSTPRICE,SECTIONPOS,SECTIONDESC )
                                VALUES( $InvItemID, $InvID, $ItemMasterID,'TOPITEM','Toppings',1,1,$ExtraToppingPrice,0,0, $TopReceiptID,$ItemMDID, 0,0,0,1,'TP',2,0,0,$ItemSectionPos, '$ItemSectionDesc')";

                    $qry = ibase_prepare($this->dbtrans,$qrySQL);
                    $qryResult = ibase_execute($qry);
                    if( !$qryResult )
                    {
                      $ItemInserted = false;
                      break;  // Error, exit loop
                    }
                  }
                  else
                  {
                    $ItemInserted = false;
                    break;  // Error, exit loop
                  }
                }
              }

            }
            else
            {
              $ItemInserted = false;
              break;  // Error, exit loop
            }
          }
        }
      }
      else
      {
        break;
      }
      if( $ItemInserted && $FreeText > "" )
      {
        // Insert Free text item
          $InvItemID = $this->GetNextInvItemID();
          if( $InvItemID > 0  )
          {
            // Insert Inv Item Epos record
            $qrySQL = "INSERT INTO customer_invoice_items_epos(EPOS_ITEM_ID, INV_ID, MASTERID,PRODDESC,ID, ITEMTABLE,SECTIONPOS,SECTIONDESC )
                              VALUES( $InvItemID, $InvID, $ItemMasterID,'$FreeText',0, 4,$ItemSectionPos, '$ItemSectionDesc')";


            $qry = ibase_prepare($this->dbtrans,$qrySQL);
            $qryResult = ibase_execute($qry);
            if( !$qryResult )
            {
              $ItemInserted = false;
              break;
            }
          }
      }
    }
    return $ItemInserted;
  }

  private function InsertSplitBillItems()
  {
    $SplitBillItemsInserted = true;
    $InvID = $this->TmpOrderData->O1;
    $SplitBillItemsList = $this->TmpOrderData->splitItems;
    $qrySQL = "delete from CUSTOMER_INVOICE_SPLIT_BILL where SPLITINVID = " . $InvID;
    $qry = ibase_prepare($this->dbtrans,$qrySQL);
    $qryResult = ibase_execute($qry);

    if( $qryResult )
    {
        $SplitBillItemCount = count($SplitBillItemsList);
        if( $SplitBillItemCount > 0 )
        {
          for( $o=0; $o<$SplitBillItemCount; $o++ )
          {

            $SplitID = $this->GetNextSplitID();
            $SplitNo = $SplitBillItemsList[$o]->splitNo;
            $SplitRef = $SplitBillItemsList[$o]->splitRef;
            $SplitItemID = $SplitBillItemsList[$o]->splitItemID;
            $SplitQty = $SplitBillItemsList[$o]->splitItemQty;
            $SplitPrintedQty = $SplitBillItemsList[$o]->splitItemPrintedQty;
            if( $SplitNo > 0 )
            {
              $qrySQL = "INSERT INTO CUSTOMER_INVOICE_SPLIT_BILL( SPLITID,SPLITNO,SPLITREF,SPLITITEMID,SPLITQTY,SPLITPRINTEDQTY,SPLITINVID )
                              VALUES( $SplitID,$SplitNo, '$SplitRef', $SplitItemID, $SplitQty, $SplitPrintedQty, $InvID)";
              //print $qrySQL;
              $qry = ibase_prepare($this->dbtrans,$qrySQL);
              $qryResult = ibase_execute($qry);
              if( $qryResult )
              {
              }
              else
              {
                $SplitBillItemsInserted = false;
              }
            }
          }
        }

    }

    return $SplitBillItemsInserted;

  }

  private function InsertSplitPayItems()
  {
    $SplitPayItemsInserted = true;
    $InvID = $this->TmpOrderData->O1;
    $SplitPayItemsList = $this->TmpOrderData->splitPayItems;
    $qrySQL = "delete from CUSTOMER_INVOICE_SPLIT_PAYMENTS where INVID = " . $InvID;
    $qry = ibase_prepare($this->dbtrans,$qrySQL);
    $qryResult = ibase_execute($qry);

    if( $qryResult )
    {
        $SplitPayItemCount = count($SplitPayItemsList);
        if( $SplitPayItemCount > 0 )
        {
          for( $o=0; $o<$SplitPayItemCount; $o++ )
          {

            $SplitPayID = $this->GetNextSplitPayID();
            $SplitPayTypeCode = $SplitPayItemsList[$o]->splitPayTypeCode;
            $SplitPayTypeDesc = $SplitPayItemsList[$o]->splitPayTypeDesc;
            $SplitPayAmt = $SplitPayItemsList[$o]->splitPayAmt;
            $OrderDate = str_replace('/','.',$this->TmpOrderData->orderDate);
            $OrdTime = date('H:i', time() );

            if( $SplitPayID > 0 )
            {

              $qrySQL = "INSERT INTO CUSTOMER_INVOICE_SPLIT_PAYMENTS( PAYID,PAYTYPECODE,PAYTYPE,PAYAMOUNT,INVID,PAYDATE,PAYTIME )
                              VALUES( $SplitPayID, $SplitPayTypeCode, '$SplitPayTypeDesc', $SplitPayAmt, $InvID, '$OrderDate', '$OrdTime')";

              $qry = ibase_prepare($this->dbtrans,$qrySQL);
              $qryResult = ibase_execute($qry);
              if( $qryResult )
              {
              }
              else
              {
                $SplitPayItemsInserted = false;
              }
            }
          }
        }

    }

    return $SplitPayItemsInserted;

  }


  private function CheckOrderNo()
  {
    $NewOrderNo = 0;
    $OrderNo = $this->TmpOrderData->O2;
    $qry = ibase_prepare($this->dbtrans,"SELECT COUNTER_NO FROM INV_CRED_COUNTER WHERE COUNTER_NO = $OrderNo");
    $qryResult = ibase_execute($qry);

    //print "<br /> CheckOrderNo orderno: " . $OrderNo;

    if( $qryResult )
    {
      $row = ibase_fetch_row($qryResult);
      $OrderNoUsed = $row[0];
      $this->FreeQry($qryResult);
      if( $OrderNoUsed > 0 )
      {
        // Order No used, so get next one
        $qry = ibase_prepare($this->dbtrans,"SELECT COUNTER_NO FROM INV_CRED_COUNTER ORDER BY COUNTER_NO DESC");

        $qryResult = ibase_execute($qry);
        $row = ibase_fetch_row($qryResult);
        $NewOrderNo = $row[0]+1;

      }
      else
      {
        // Order No hasn't been used
        $NewOrderNo = $OrderNo;
      }
    }
    else
    {
      // Error
      $this->ErrorMsg = "ERROR 0005: DB Counter Check Qry Error";
    }
    //print "<br /> CheckOrderNo neworderno: " . $NewOrderNo;
    return $NewOrderNo;
  }

  private function DeleteOrderItems()
  {
    $ItemsDeleted = false;
    $OrderID = $this->TmpOrderData->O1;

    $qry = ibase_prepare($this->dbtrans,"delete FROM CUSTOMER_INVOICE_ITEMS WHERE INV_ID = $OrderID");
    $qryResult = ibase_execute($qry);
    if( $qryResult )
    {
      $qry = ibase_prepare($this->dbtrans,"delete FROM CUSTOMER_INVOICE_ITEMS_EPOS WHERE INV_ID = $OrderID");
      $qryResult = ibase_execute($qry);
      if( $qryResult )
      {
        $ItemsDeleted = true;
      }
      else
      {
        // Error
        $this->ErrorMsg = "ERROR 0006: Could not delete invoice items";
      }
    }
    else
    {
      // Error
      $this->ErrorMsg = "ERROR 0006: Could not delete invoice items";
    }

    return $ItemsDeleted;
  }

  public function GetOrderDetails($in_OrderID)
  {
      $this->ButtonDataList = null;
      $this->OpenDB();
      //$this->dbtrans = ibase_trans( IBASE_DEFAULT,$this->dbconn );
      $qryResult = $this->RunSelectQry( "SELECT ID, INVOICE_NO, INVOICE_DATE, EPOS_TABLE_NUM,CUST_ACCREF, " .
                                        "CUSTOMER_CONTACT,ADDRESS_1,ADDRESS_2,ADDRESS_3,ADDRESS_4,POSTCODE, " .
                                        "ROUTE_ID, NOTE_1, NOTE_2,PAYMENT_TYPE_CODE,CARRIAGE,DISCOUNT,LOYALITY_DISCOUNT_RATE, " .
                                        "CUSTOMER_ORDER_NO, LASTTRANSID, TRANS_STATE_CODE, BALANCE, INVOICE_TOTAL_GROSS " .
                                        "FROM CUSTOMER_INVOICE WHERE ID = $in_OrderID");

      while( $row = ibase_fetch_row($qryResult) )
      {

        $NewOrderDetails = array();
        $NewOrderDetails["orderID"]           = $row[0]; // ID
        $NewOrderDetails["orderNo"]           = $row[1]; // Order No
        $NewOrderDetails["orderDate"]         = $row[2]; // Date
        $NewOrderDetails["orderTableNo"]      = $row[3]; // Table Num
        $NewOrderDetails["orderAccRef"]       = $row[4]; // A/C Ref
        $NewOrderDetails["orderContactName"]  = $row[5]; // Customer Name
        $NewOrderDetails["orderAdd1"]         = $row[6]; // Add 1
        $NewOrderDetails["orderAdd2"]         = $row[7]; // Add 2
        $NewOrderDetails["orderAdd3"]         = $row[8]; // Add 3
        $NewOrderDetails["orderAdd4"]         = $row[9]; // Add 4
        $NewOrderDetails["orderAddPostcode"]  = $row[10]; // Postcode
        $NewOrderDetails["orderRouteID"]      = $row[11]; // Route ID
        $NewOrderDetails["orderNote1"]        = $row[12]; // Note 1
        $NewOrderDetails["orderNote2"]        = $row[13]; // Note 2
        $NewOrderDetails["orderPayType"]      = $row[14]; // Payment Type
        $NewOrderDetails["orderCarriage"]     = $row[15]; // Carriage
        $NewOrderDetails["orderDiscount"]     = $row[16]; // Discount
        $NewOrderDetails["orderDiscountRate"] = $row[17]; // Loyalty Discount rate
        $NewOrderDetails["orderCovers"]       = $row[18]; // Covers
        $NewOrderDetails["orderLastTransID"]  = $row[19]; // Last Trans ID
        $NewOrderDetails["orderTransState"]   = $row[20]; // Trans State code
        $NewOrderDetails["orderBalance"]      = $row[21]; // Balance
        $NewOrderDetails["orderTotal"]        = $row[22]; // Total
        $this->ButtonDataList[] = $NewOrderDetails;

      }

      $this->FreeQry($qryResult);
      $this->CloseDB();

      return $this->ButtonDataList;
  }

  public function GetOrderItemDetails($in_OrderID)
  {
      $this->ButtonDataList = null;
      $this->OpenDB();
      //$this->dbtrans = ibase_trans( IBASE_DEFAULT,$this->dbconn );

      $qryResult = $this->RunSelectQry( "SELECT PRODCODE, PRODDESC, QTY, UNITPRICE, ID, PRODTYPE, PRODCLASS, MASTERID, TOPPINGTYPE, SPLITBILLID, SPLITBILLREF, MD_ID, EXTRATOPPINGSPRICE, ITEMTABLE, SPLITQTY, PRINTEDQTY, SPLITPRINTEDQTY, TOPBUT1, TOPBUT2, SECTIONPOS, SECTIONDESC " .
                                        "FROM CUSTOMER_INVOICE_ITEMS_EPOS WHERE INV_ID = $in_OrderID ORDER BY EPOS_ITEM_ID,ID,MASTERID ");

      while( $row = ibase_fetch_row($qryResult) )
      {
        $NewItem = array();
        $NewItem["itemCode"]            = $row[0]; // Prod Code
        $NewItem["itemDesc"]            = $row[1]; // Desc
        $NewItem["itemQty"]             = $row[2]; // Qty
        $NewItem["itemPrice"]           = $row[3]; // Price
        $NewItem["itemReceiptID"]       = $row[4]; // Receipt ID
        $NewItem["itemProdType"]        = $row[5]; // Prod Type
        $NewItem["itemProdCat"]         = $row[6]; // Prod Category
        $NewItem["itemMasterID"]        = $row[7]; // Master ID
        $NewItem["itemTopType"]         = $row[8]; // Topping Type
        $NewItem["itemSplitBillID"]     = $row[9]; // Split Bill ID
        $NewItem["itemSplitBillRef"]    = $row[10]; // Split Bill Ref
        $NewItem["itemMDID"]            = $row[11]; // MD ID
        $NewItem["itemTopPrice"]        = $row[12]; // Topping Price
        $NewItem["itemItemTable"]       = $row[13]; // Item Table
        $NewItem["itemSplitQty"]        = $row[14]; // Split Qty
        $NewItem["itemPrintedQty"]      = $row[15]; // Printed Qty
        $NewItem["itemSplitPrintedQty"] = $row[16]; // Split Printed Qty
        $NewItem["itemTopBut1"]         = $row[17]; // Top But 1
        $NewItem["itemTopBut2"]         = $row[18]; // Top But 2
        $NewItem["itemSectionPos"]      = $row[19]; // Section Pos
        $NewItem["itemSectionDesc"]     = $row[20]; // Section Desc

        if( $NewItem["itemSplitQty"] == null )
          $NewItem["itemSplitQty"] = 0;

        if( $NewItem["itemPrintedQty"] == null )
          $NewItem["itemPrintedQty"] = 0;

        if( $NewItem["itemSplitPrintedQty"] == null )
          $NewItem["itemSplitPrintedQty"] = 0;

        $this->ButtonDataList[] = $NewItem;
      }

      $this->FreeQry($qryResult);
      $this->CloseDB();

      return $this->ButtonDataList;
  }

  public function GetOrderSplitBillItems($in_OrderID)
  {
      $this->ButtonDataList = null;
      $this->OpenDB();

      $qryResult = $this->RunSelectQry( "SELECT * FROM CUSTOMER_INVOICE_SPLIT_BILL WHERE SPLITINVID = $in_OrderID ORDER BY SPLITNO ");

      while( $row = ibase_fetch_row($qryResult) )
      {
        $NewItem = array();
        $NewItem["SplitNo"] = $row[1]; // split No
        $NewItem["SplitRef"] = $row[2]; // Ref
        $NewItem["SplitItemID"] = $row[3]; // Item ID
        $NewItem["SplitItemQty"] = $row[4]; // Qty
        $NewItem["SplitItemPrintedQty"] = $row[5]; // Printed Qty

        $this->ButtonDataList[] = $NewItem;
      }

      $this->FreeQry($qryResult);
      $this->CloseDB();

      return $this->ButtonDataList;
  }

  public function GetOrderSplitPayItems($in_OrderID)
  {
      $this->ButtonDataList = null;
      $this->OpenDB();

      $qryResult = $this->RunSelectQry( "SELECT PAYTYPECODE,PAYTYPE,PAYAMOUNT FROM CUSTOMER_INVOICE_SPLIT_PAYMENTS WHERE INVID = $in_OrderID ORDER BY PAYID ");

      while( $row = ibase_fetch_row($qryResult) )
      {
        $NewItem = array();
        $NewItem["SplitPayTypeCode"] = $row[0];
        $NewItem["SplitPayTypeDesc"] = $row[1];
        $NewItem["SplitPayAmt"] = $row[2];

        $this->ButtonDataList[] = $NewItem;
      }

      $this->FreeQry($qryResult);
      $this->CloseDB();

      return $this->ButtonDataList;
  }

  private function InsertCounterNo($in_OrderNo)
  {
    $CounterSQL =  "INSERT INTO inv_cred_counter (COUNTER_NO) VALUES ( $in_OrderNo )";
    $qry = ibase_prepare($this->dbtrans,$CounterSQL);
    $qryResult = ibase_execute($qry);
    if( !$qryResult )
    {
      return false;
    }

    return true;
  }


  private function GetInvTransID()
  {
    $NextInvTransID = 0;
    $qrySQL =  "SELECT GEN_ID(INVOICETRANSID,1 ) FROM RDB\$DATABASE";
    $qry = ibase_prepare($this->dbtrans,$qrySQL);
    $qryResult = ibase_execute($qry);
    if( $qryResult )
    {
      $row = ibase_fetch_row($qryResult);
      $NextInvTransID = $row[0];
    }
    return $NextInvTransID;
  }

  private function GetNextSplitID()
  {
    $NextInvID = 0;
    $qrySQL =  "SELECT GEN_ID(EPOSSPLITBILLID,1 ) FROM RDB\$DATABASE";
    $qry = ibase_prepare($this->dbtrans,$qrySQL);
    $qryResult = ibase_execute($qry);
    if( $qryResult )
    {
      $row = ibase_fetch_row($qryResult);
      $NextInvID = $row[0];
    }
    return $NextInvID;
  }

  private function GetNextSplitPayID()
  {
    $NextInvID = 0;
    $qrySQL =  "SELECT GEN_ID(CUSTRECEIPTSNO,1 ) FROM RDB\$DATABASE";
    $qry = ibase_prepare($this->dbtrans,$qrySQL);
    $qryResult = ibase_execute($qry);
    if( $qryResult )
    {
      $row = ibase_fetch_row($qryResult);
      $NextInvID = $row[0];
    }
    return $NextInvID;
  }


  private function GetNextInvID()
  {
    $NextInvID = 0;
    $qrySQL =  "SELECT GEN_ID(CUSTINVNO,1 ) FROM RDB\$DATABASE";
    $qry = ibase_prepare($this->dbtrans,$qrySQL);
    $qryResult = ibase_execute($qry);
    if( $qryResult )
    {
      $row = ibase_fetch_row($qryResult);
      $NextInvID = $row[0];
    }
    return $NextInvID;
  }

  private function GetNextInvItemID()
  {
    $NextInvItemID = 0;
    $qrySQL =  "SELECT GEN_ID(CUSTINVITEMNO,1 ) FROM RDB\$DATABASE";
    $qry = ibase_prepare($this->dbtrans,$qrySQL);
    $qryResult = ibase_execute($qry);
    if( $qryResult )
    {
      $row = ibase_fetch_row($qryResult);
      $NextInvItemID = $row[0];
    }
    return $NextInvItemID;
  }

  public function GetSectionList()
  {
    $SectionList = array();
    $this->OpenDB();
    $qryResult = $this->RunSelectQry( "SELECT ID, POS, SECTION_DESC FROM EPOS_SECTIONS ORDER BY ID" );
    while( $row = ibase_fetch_row($qryResult) )
    {
      $section = array();
      $section["But_ID"] = $row[0];
      $section["But_Text1"] = $row[1];
      $section["But_Text2"] = $row[2];
      $SectionList[] = $section;

    }
    $this->FreeQry($qryResult);
    $this->CloseDB();
    return $SectionList;
  }

  public function GetProductOfferList()
  {
    $MDList = array();
    $this->OpenDB();
    $qryResult = $this->RunSelectQry( "SELECT ID, MD_PARENT_PRODCODE, BUT_TYPE, BUT_LISTLINKID, SELECTED_TEXT FROM EPOS_MD WHERE MD_PARENT_PRODCODE > '' AND BUT_LISTLINKID is not null AND BUT_TYPE is not null ORDER BY MD_PARENT_PRODCODE, ID" );      //  AND SELECTED_COLOUR = 1
    while( $row = ibase_fetch_row($qryResult) )
    {
      $MD = array();
      $MD["MD_ID"] = $row[0];
      $MD["MD_ParentCode"] = $row[1];
      $MD["MD_ButType"] = $row[2];
      $MD["MD_ButListLinkID"] = $row[3];
      $MD["MD_ButDesc"] = $row[4];
      $MDList[] = $MD;

    }
    $this->FreeQry($qryResult);
    $this->CloseDB();
    return $MDList;
  }

  public function GetProductOfferItemList()
  {
    $MDList = array();
    $this->OpenDB();
    $qryResult = $this->RunSelectQry( "SELECT ID, MD_BUT_ID, MD_BUT_TEXT, MD_BUT_ACTION, MD_BUT_TOPPINGID, PRODCODE, PROD_CATNAME FROM EPOS_MDBUTLIST WHERE MD_BUT_ID is not null ORDER BY MD_BUT_ID,ID" );    // AND MD_BUT_ACTION = 9
    while( $row = ibase_fetch_row($qryResult) )
    {
      $MD = array();
      $MD["MDItem_ID"] = $row[0];
      $MD["MDItem_ButID"] = $row[1];
      $MD["MDItem_ButText"] = $row[2];
      $MD["MDItem_ButAction"] = $row[3];
      if( $MD["MDItem_ButAction"] == null )
        $MD["MDItem_ButAction"] = -1;
      $MD["MDItem_ButTopID"] = $row[4];
      if( $MD["MDItem_ButTopID"] == null )
        $MD["MDItem_ButTopID"] = -1;
      $MD["MDItem_ButProdCode"] = $row[5];
      $MD["MDItem_ButCat"] = $row[6];
      if( $MD["MDItem_ButCat"] == null )
        $MD["MDItem_ButCat"] = -1;

      $MDList[] = $MD;

    }
    $this->FreeQry($qryResult);
    $this->CloseDB();
    return $MDList;
  }

  public function GetTableOrders($in_DateF, $in_DateT)
  {
    //$this->ButtonDataList = null;
    $TableOrderList = array();
    $this->OpenDB();

    $qryResult = $this->RunSelectQry( "SELECT INVOICE_NO, INVOICE_DATE, CUST_ACCREF, CUSTOMER_CONTACT, PREPAYMENT, ORDER_NO, INVOICE_TOTAL_GROSS, NOTE_1, NOTE_2, ROUTE_ID, ADDRESS_1, ADDRESS_2, POSTCODE, EPOS_TABLE_NUM, ID, TRANS_STATE_CODE " .
                                        "FROM CUSTOMER_INVOICE " .
                                        "WHERE (INVOICE_DATE >= '$in_DateF' AND INVOICE_DATE <= '$in_DateT') AND EPOS_TABLE_NUM > '' AND PREPAYMENT < INVOICE_TOTAL_GROSS " .
                                        "ORDER BY INVOICE_NO" );

    while( $row = ibase_fetch_row($qryResult) )
    {
      $tableOrder = array();
      $tableOrder["But_ID"] = $row[0];       // Inv No
      $tableOrder["But_Text1"] = $row[2];    // A/C Ref
      $tableOrder["But_Text2"] = $row[3];    // Contact name
      $tableOrder["But_Text3"] = $row[13];   // Table ID
      $tableOrder["But_Text4"] = $row[14];   // ID
      $tableOrder["But_Text5"] = $row[15];   // Trans State
      $TableOrderList[] = $tableOrder;
    /*
      $NewBut = new TillDataPacket();
      $NewBut->But_Data[] = $row[0];   // Inv No
      $NewBut->But_Data[] = $row[1];   // Inv Date
      $NewBut->But_Data[] = $row[2];   // A/C Ref
      $NewBut->But_Data[] = $row[3];   // Contact name
      $NewBut->But_Data[] = $row[4];   // Prepayment
      $NewBut->But_Data[] = $row[5];   // Order No
      $NewBut->But_Data[] = $row[6];   // Order Total
      $NewBut->But_Data[] = $row[7];   // Note 1
      $NewBut->But_Data[] = $row[8];   // Note 2
      $NewBut->But_Data[] = $row[9];   // Route ID
      $NewBut->But_Data[] = $row[10];   // Add 1
      $NewBut->But_Data[] = $row[11];   // Add 2
      $NewBut->But_Data[] = $row[12];   // Postcode
      $NewBut->But_Data[] = $row[13];   // Table ID
      $NewBut->But_Data[] = $row[14];   // ID
      $this->ButtonDataList[] = $NewBut;
      */

    }

    $this->FreeQry($qryResult);
    $this->CloseDB();

    return $TableOrderList;
    //return $this->ButtonDataList;

  }

  public function GetTableList()
  {
    //$this->ButtonDataList = null;
    $TableList = array();
    $this->OpenDB();
    $qryResult = $this->RunSelectQry( "SELECT TABLE_ID, TABLE_NUM, TABLE_DESC1 FROM EPOS_TABLES ORDER BY TABLE_ID" );
    while( $row = ibase_fetch_row($qryResult) )
    {
      $table = array();
      $table["But_ID"] = $row[0];
      $table["But_Text1"] = $row[1];
      $table["But_Text2"] = $row[2];
      $TableList[] = $table;
    /*
      $NewBut = new TillDataPacket();
      $NewBut->But_Data[] = $row[0];   // ID
      $NewBut->But_Data[] = $row[1];   // Table Num
      $NewBut->But_Data[] = $row[2];   // Table Desc
      $this->ButtonDataList[] = $NewBut;
      */
    }
    $this->FreeQry($qryResult);
    $this->CloseDB();
    return $TableList;
    //return $this->ButtonDataList;

  }

  public function GetTableNo($in_TableID)
  {
    $TableNum = 0;
    $this->OpenDB();
    $qryResult = $this->RunSelectQry( "SELECT TABLE_ID, TABLE_NUM FROM EPOS_TABLES WHERE TABLE_ID = $in_TableID" );
    while( $row = ibase_fetch_row($qryResult) )
    {
      $TableNum = $row[1];   // Table Num
    }
    $this->FreeQry($qryResult);
    $this->CloseDB();
    return $TableNum;

  }

  private function GetNextReportTableID()
  {
    $ReportTableID = 0;
    $qrySQL =  "SELECT GEN_ID(REPORTTABLEID,1 ) FROM RDB\$DATABASE";
    $qry = ibase_prepare($this->dbtrans,$qrySQL);
    $qryResult = ibase_execute($qry);
    if( $qryResult )
    {
      $row = ibase_fetch_row($qryResult);
      $ReportTableID = $row[0];
    }
    return $ReportTableID;
  }



  public function PaymentTypeExists($in_PayTypeDesc)
  {
    $PayTypeCode = -1;
    $this->OpenDB();
    $qryResult = $this->RunSelectQry( "SELECT * FROM PAYMENT_TYPE WHERE PAYMENT_TYPE_DESCRIPTION = '$in_PayTypeDesc'" );
    while( $row = ibase_fetch_row($qryResult) )
    {
      $PayTypeCode = $row[0];   // Table Num
    }
    $this->FreeQry($qryResult);
    $this->CloseDB();
    return $PayTypeCode;
  }

  public function GetPayTypesList()
  {
    $PayTypeList = array();
    $this->OpenDB();
    $qryResult = $this->RunSelectQry( "SELECT PAYMENT_TYPE_CODE,PAYMENT_TYPE_DESCRIPTION FROM PAYMENT_TYPE ORDER BY PAYMENT_TYPE_CODE" );
    while( $row = ibase_fetch_row($qryResult) )
    {
      $paytype = array();
      $paytype["PayTypeCode"] = $row[0];
      $paytype["PayTypeDesc"] = $row[1];
      $PayTypeList[] = $paytype;
    }
    $this->FreeQry($qryResult);
    $this->CloseDB();
    return $PayTypeList;
  }

  public function GetVATCodesList()
  {
    $VatCodeList = array();
    $this->OpenDB();

    $qryResult = $this->RunSelectQry( "SELECT VAT_CODE,VAT_NAME,VAT_PERCENT FROM VAT_CODES ORDER BY VAT_CODE" );
    while( $row = ibase_fetch_row($qryResult) )
    {
      $vatcode = array();
      $vatcode["VATCode"] = $row[0];
      $vatcode["VATDesc"] = $row[1];
      $vatcode["VATRate"] = $row[2];
      $VatCodeList[] = $vatcode;
    }
    $this->FreeQry($qryResult);
    $this->CloseDB();
    return $VatCodeList;
  }

  public function GetEposOptions()
  {
    $OptionsList = array();
    $qrySQL =  "SELECT ACCUMULATEQTY,NETDISCOUNT,PRINTNEWORDERITEMSONLY,SHOPTIMEFROM,SHOPTIMETO   FROM EPOS_OPTIONS";

    $this->OpenDB();
    $qryResult = $this->RunSelectQry($qrySQL);
    while( $row = ibase_fetch_row($qryResult) )
    {
      $option = array();
      $option["AccumulatedQty"] = $row[0];
      $option["NETDiscount"] = $row[1];
      $option["PrintNewItemsOnly"] = $row[2];
      $option["ShopTimeF"] = $row[3];
      $option["ShopTimeT"] = $row[4];
      $OptionsList[] = $option;
    }
    $this->CloseDB();

    return $OptionsList;
  }

  public function SaveOrderReportData($in_ReportData)
  {
    $rptID = 0;
    $OrderReportDataSaved = false;
    $CommitData = true;
    $this->OpenDB();
    $this->dbtrans = ibase_trans( IBASE_DEFAULT,$this->dbconn );


    // Insert report data
    $ItemCount = count($in_ReportData);
    for( $i=0; $i < $ItemCount; $i++ )
    {
      $tmpID = $this->GetNextReportTableID();

      if( $rptID == 0 )
        $rptID = $tmpID;

      $rpt_F1 = $in_ReportData[$i]->rpt_F1;
      $rpt_F2 = $in_ReportData[$i]->rpt_F2;
      $rpt_F3 = $in_ReportData[$i]->rpt_F3;
      $rpt_F4 = $in_ReportData[$i]->rpt_F4;
      $rpt_F5 = $in_ReportData[$i]->rpt_F5;
      $rpt_F6 = $in_ReportData[$i]->rpt_F6;
      $rpt_F7 = $in_ReportData[$i]->rpt_F7;
      $rpt_F8 = $in_ReportData[$i]->rpt_F8;
      $rpt_F10 = $in_ReportData[$i]->rpt_F10;
      $rpt_F11 = $in_ReportData[$i]->rpt_F11;
      $rpt_F14 = $in_ReportData[$i]->rpt_F14;
      $rpt_F16 = $in_ReportData[$i]->rpt_F16;

      if( $rpt_F6 == "+" )
      {
        $qrySQL =  "INSERT INTO report_data(ID,RPT_ID,FIELD1_FLOAT,FIELD3_FLOAT,FIELD5_FLOAT,FIELD6_STRING,FIELD7_STRING,FIELD8_STRING,FIELD10_STRING,FIELD11_MEMO,FIELD14_FLOAT,FIELD16_STRING)
                            VALUES( $tmpID,$rptID, $rpt_F1, $rpt_F3, $rpt_F5, '$rpt_F6', '$rpt_F7', '$rpt_F8', '$rpt_F10', '$rpt_F11',$rpt_F14,'$rpt_F16')";
      }
      else if( $rpt_F6 == "-" )
      {
        $qrySQL =  "INSERT INTO report_data(ID,RPT_ID,FIELD1_FLOAT,FIELD5_FLOAT,FIELD6_STRING,FIELD7_STRING,FIELD8_STRING,FIELD10_STRING,FIELD11_MEMO,FIELD14_FLOAT,FIELD16_STRING)
                            VALUES( $tmpID,$rptID, $rpt_F1, $rpt_F5, '$rpt_F6', '$rpt_F7', '$rpt_F8', '$rpt_F10', '$rpt_F11',$rpt_F14,'$rpt_F16')";
      }
      else if( $rpt_F6 == "MDI" )
      {
        $qrySQL =  "INSERT INTO report_data(ID,RPT_ID,FIELD1_FLOAT,FIELD5_FLOAT,FIELD8_STRING,FIELD10_STRING,FIELD11_MEMO,FIELD14_FLOAT,FIELD16_STRING)
                            VALUES( $tmpID,$rptID, $rpt_F1, $rpt_F5, '$rpt_F8', '$rpt_F10', '$rpt_F11',$rpt_F14,'$rpt_F16')";
      }
      else if( $rpt_F6 == "i" )
      {
        $qrySQL =  "INSERT INTO report_data(ID,RPT_ID,FIELD1_FLOAT,FIELD5_FLOAT,FIELD6_STRING,FIELD8_STRING,FIELD10_STRING,FIELD11_MEMO,FIELD14_FLOAT,FIELD16_STRING)
                            VALUES( $tmpID,$rptID, $rpt_F1, $rpt_F5, '$rpt_F6', '$rpt_F8', '$rpt_F10', '$rpt_F11',$rpt_F14,'$rpt_F16')";
      }
      else if( $rpt_F2 == "" || $rpt_F2 == "0" )
      {
        $qrySQL =  "INSERT INTO report_data(ID,RPT_ID,FIELD1_FLOAT,FIELD3_FLOAT,FIELD4_FLOAT,FIELD5_FLOAT,FIELD6_STRING,FIELD7_STRING,FIELD8_STRING,FIELD10_STRING,FIELD11_MEMO,FIELD14_FLOAT,FIELD16_STRING)
                            VALUES( $tmpID,$rptID, $rpt_F1, $rpt_F3, $rpt_F4, $rpt_F5, '$rpt_F6', '$rpt_F7', '$rpt_F8', '$rpt_F10', '$rpt_F11',$rpt_F14,'$rpt_F16')";
      }
      else
      {
        $qrySQL =  "INSERT INTO report_data(ID,RPT_ID,FIELD1_FLOAT,FIELD2_FLOAT,FIELD3_FLOAT,FIELD4_FLOAT,FIELD5_FLOAT,FIELD6_STRING,FIELD7_STRING,FIELD8_STRING,FIELD10_STRING,FIELD11_MEMO,FIELD14_FLOAT,FIELD16_STRING)
                            VALUES( $tmpID,$rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, '$rpt_F6', '$rpt_F7', '$rpt_F8', '$rpt_F10', '$rpt_F11',$rpt_F14,'$rpt_F16')";
      }

      //print $qrySQL . "<br />";

      $qry = ibase_prepare($this->dbtrans,$qrySQL);
      $qryResult = ibase_execute($qry);
      if( !$qryResult )
      {
        break;
      }
    }
    if( $qryResult )
    {
      if( ibase_commit($this->dbtrans) )
      {
        $OrderReportDataSaved = true;
      }
      else
      {
        ibase_rollback($this->dbtrans);
        $this->ErrorMsg = "ERROR 2001: SaveOrderReportData failed";
      }
    }


    $this->CloseDB();

    return $rptID;

  }

  public function ValidateUser($in_Passcode, &$out_UserLoggedIn, &$out_UserSessionID)
  {
    $UserNum = 0;
    $this->OpenUsersDB();
    $qryResult = $this->RunUserSelectQry( "SELECT USER_NO, LOGGED_IN_EPOS, EPOS_SESSIONID FROM USERS WHERE USER_PASSWORD = '$in_Passcode' AND USER_NAME != 'epos'" );

    while( $row = ibase_fetch_row($qryResult) )
    {
      $UserNum = $row[0];
      if( $row[1] == 'Y')
        $out_UserLoggedIn  = true;

      $out_UserSessionID = $row[1];
      break;
    }

    $this->FreeQry($qryResult);
    $this->CloseUsersDB();
    return $UserNum;

  }

  public function GetMaxUsers()
  {
    $MaxUsers = 0;  // Unlimited
    $this->OpenDB();
    $qryResult = $this->RunSelectQry( "SELECT MAX_USERS FROM SYSTEM" );
    while( $row = ibase_fetch_row($qryResult) )
    {
      $MaxUsers = $row[0];
    }
    $this->FreeQry($qryResult);
    $this->CloseDB();
    return $MaxUsers;
  }

  public function GetLoggedInUsers()
  {
    $UserCount = 0;
    $this->OpenUsersDB();
    $qryResult = $this->RunUserSelectQry( "SELECT USER_NO FROM USERS WHERE LOGGED_IN_EPOS = 'Y'" );
    while( $row = ibase_fetch_row($qryResult) )
    {
      $UserCount++;
    }
    $this->FreeQry($qryResult);
    $this->CloseUsersDB();
    return $UserCount;
  }

  public function SetLoggedInUser($in_UserNum, $in_SessionID)
  {
    $UserLoggedIn = false;
    $this->OpenUsersDB();
    $this->UserDBTrans = ibase_trans( IBASE_DEFAULT,$this->UserDBConn );
    $SessionInt = (int) $in_SessionID;
    $qrySQL = "UPDATE USERS SET LOGGED_IN_EPOS = 'Y', EPOS_SESSIONID=$in_SessionID  WHERE USER_NO = $in_UserNum";

    $qry = ibase_prepare($this->UserDBTrans,$qrySQL);
    $qryResult = ibase_execute($qry);

    if( $qryResult )
    {
      if( ibase_commit($this->UserDBTrans) )
      {
        $UserLoggedIn = true;
      }
      else
      {
        ibase_rollback($this->UserDBTrans);
        $this->ErrorMsg = "ERROR 000x: DB Qry Commit Error (SetLoggedInUser)";
        print $this->ErrorMsg;
      }
    }
    $this->CloseUsersDB();
    return $UserLoggedIn;
  }

  public function SetLoggedOutUser($in_UserNum, $in_UserSessionID)
  {
    $UserLoggedOut = false;
    $this->OpenUsersDB();
    $this->UserDBTrans = ibase_trans( IBASE_DEFAULT,$this->UserDBConn );
    $qrySQL = "UPDATE USERS SET LOGGED_IN_EPOS = '', EPOS_SESSIONID=0 WHERE USER_NO = $in_UserNum AND EPOS_SESSIONID=$in_UserSessionID";
    $qry = ibase_prepare($this->UserDBTrans,$qrySQL);
    $qryResult = ibase_execute($qry);

    if( $qryResult )
    {
      if( ibase_commit($this->UserDBTrans) )
      {
        $UserLoggedOut = true;
      }
      else
      {
        ibase_rollback($this->UserDBTrans);
        $this->ErrorMsg = "ERROR 000x: DB Qry Commit Error (SetLoggedOutUser)";
        print $this->ErrorMsg;
      }
    }
    $this->CloseUsersDB();
    return $UserLoggedOut;
  }

  public function GetNextUserSessionID()
  {
    $SessionID = 0;
    $qrySQL =  "SELECT GEN_ID(USER_SESSION_ID,1 ) FROM RDB\$DATABASE";

    $this->OpenUsersDB();
    $qryResult = $this->RunUserSelectQry($qrySQL);
    if( $qryResult )
    {
      $row = ibase_fetch_row($qryResult);
      $SessionID = $row[0];
    }
    $this->CloseUsersDB();

    return $SessionID;

  }


  public function GetUserSessionID($in_UserNum)
  {
    $UserSessionID = 0;
    $qrySQL =  "SELECT EPOS_SESSIONID FROM USERS WHERE USER_NO = $in_UserNum";

    $this->OpenUsersDB();
    $qryResult = $this->RunUserSelectQry($qrySQL);
    if( $qryResult )
    {
      $row = ibase_fetch_row($qryResult);
      $UserSessionID = $row[0];
    }
    $this->CloseUsersDB();

    return $UserSessionID;
  }



}



?>
