<?php

require_once('DAL_ProphetTill.php');


class Till_Options
{
  private $DalTill;  
  private $ErrorMsg;
  private $AccumulatedQty;
  private $PrintOnlyNewItems;      
  
  function __construct() 
  {
    $this->DalTill = new DAL_ProphetTill(); 
  }
  
  public function LoadOptions()
  {  
    $this->AccumulatedQty = 0;
    $this->PrintOnlyNewItems = 0;    
    
    $UserOptions = $this->DalTill->GetEposOptions();
    
    if( count($UserOptions) > 0 )
    {   
      $this->AccumulatedQty = $UserOptions[0]["AccumulatedQty"];   
      $this->PrintOnlyNewItems = $UserOptions[0]["PrintNewItemsOnly"];       
    }  

  }   
  
  public function GetOption_AccumulatedQty()
  { 
    return $this->AccumulatedQty;  
  }
  
  public function GetOption_PrintOnlyNewItems()
  { 
    return $this->PrintOnlyNewItems;  
  }
}
?>
