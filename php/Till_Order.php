<?php
require_once('Till_datatypes.php');
require_once('Till_Options.php');
require_once('Till_productoffer.php');
require_once('DAL_ProphetTill.php');

  function SortByCat($a, $b)
  {
        $al = $a->GetProdCat();
        $bl = $b->GetProdCat();
        if( $al == $bl )
        {
            return 0;
        }
        return ($al > $bl) ? +1 : -1;
  }

  function SortByID($a, $b)
  {
        $al = $a->GetProdID();
        $bl = $b->GetProdID();
        if( $al == $bl )
        {
            return 0;
        }
        return ($al > $bl) ? +1 : -1;
  }

  function SortBySectionPos($a, $b)
  {
        $al = $a->GetProdSectionPos();
        $bl = $b->GetProdSectionPos();
        if( $al == $bl )
        {
            return 0;
        }
        return ($al > $bl) ? +1 : -1;
  }
class Till_OrderItem
{
  // Main order item data is in a Data Transfer Object
  // so it can easily be passed to DAL or populated by DAL
  private $DTO_OrderItem;

  // Various helper variables for order item
  private $SubLinkID;
  private $ProdExtraDesc;

  private $ProdHalfHalf;  // Is this a Half & Half Product: True/False
  // Half 1
  private $ProdCodeHalf1;
  private $ProdDescHalf1;
  private $ProdPriceH1;
  private $ProdPriceH1HH;
  private $ProdPriceH1Top;
  private $ProdPriceH1TopHH;
  private $ProdTopPanelH1;
  // Half 2
  private $ProdCodeHalf2;
  private $ProdDescHalf2;
  private $ProdTopPanelH2;
  private $ProdPriceH2;
  private $ProdPriceH2HH;
  private $ProdPriceH2Top;
  private $ProdPriceH2TopHH;

  private $ProdPriceBand1;
  private $ProdPriceBand2;

  // Topping counters
  private $TopVegMax;
  private $TopMeatMax;
  private $TopVegCnt;
  private $TopMeatCnt;
  private $TopH1VegMax;
  private $TopH1MeatMax;
  private $TopH1VegCnt;
  private $TopH1MeatCnt;
  private $TopH2VegMax;
  private $TopH2MeatMax;
  private $TopH2VegCnt;
  private $TopH2MeatCnt;
  private $TopH1FreeCnt;
  private $TopH2FreeCnt;



  function __construct()
  {
    $this->DTO_OrderItem = new OrderItemData();
  }

  public function CreateProduct($in_ID, $in_Code, $in_Desc, $in_Qty, $in_SplitQty, $in_UnitPrice, $in_Type, $in_Class, $in_Toppings, $in_SplitBillID, $in_SplitBillDesc, $in_ToppingPrice, $in_LoadingOrder, $in_PrintToKitchen, $in_FreeText, $in_TopBut1, $in_TopBut2, $in_SectionPos, $in_SectionDesc)
  {
    $this->DTO_OrderItem->ReceiptID = $in_ID;
    $this->DTO_OrderItem->ItemMasterID = $in_ID;
    $this->DTO_OrderItem->ItemCode = $in_Code;
    $this->DTO_OrderItem->ItemDesc = $in_Desc;
    $this->DTO_OrderItem->ItemQty = $in_Qty;
    $this->DTO_OrderItem->ItemSplitQty = $in_SplitQty;     // Ray 22/03/12. Added
    $this->DTO_OrderItem->ItemUnitPrice = $in_UnitPrice;
    $this->GetProdNET();
    $this->DTO_OrderItem->ItemVAT = 0; // TODO
    $this->DTO_OrderItem->ItemProdType = 0;
    $this->DTO_OrderItem->ItemProdClass = $in_Class;
    $this->DTO_OrderItem->ItemSplitBillID = $in_SplitBillID;
    $this->DTO_OrderItem->ItemSplitBillRef = $in_SplitBillDesc;
    $this->DTO_OrderItem->ItemExtraToppingPrice = $in_ToppingPrice;
    $this->DTO_OrderItem->ItemPrintToKitchen = $in_PrintToKitchen;
    $this->DTO_OrderItem->ItemFreeText = $in_FreeText;

    $this->DTO_OrderItem->ItemTopBut1 = $in_TopBut1;
    $this->DTO_OrderItem->ItemTopBut2 = $in_TopBut2;
    $this->DTO_OrderItem->ItemMarkToPrint = 1;

    $this->DTO_OrderItem->ItemSectionPos = $in_SectionPos; // Ray 30/10/12. Added
    $this->DTO_OrderItem->ItemSectionDesc = $in_SectionDesc; // Ray 30/10/12. Added

    //print "top price = " . $this->DTO_OrderItem->ItemTopPrice;
    //print "top price = " . $in_ToppingPrice;

    $this->CreateProductToppings($in_Toppings,$in_ID, $in_LoadingOrder);
  }

  public function CreateProductOffer($in_ID, $in_Offer, $in_Qty, $in_SplitQty, $in_Class, $in_SplitBillID, $in_SplitBillDesc, $in_PrintToKitchen, $in_SectionPos, $in_SectionDesc)
  {
    // Create a copy of the offer
    $this->DTO_OrderItem->ReceiptID = $in_ID;
    $this->DTO_OrderItem->ItemMasterID = $in_ID;
    $this->DTO_OrderItem->ItemCode = $in_Offer->GetOfferCode();
    $this->DTO_OrderItem->ItemDesc = $in_Offer->GetOfferDesc();
    $this->DTO_OrderItem->ItemQty = $in_Qty;
    $this->DTO_OrderItem->ItemSplitQty = $in_SplitQty;      // Ray 30/03/12. Added
    $this->DTO_OrderItem->ItemUnitPrice = $in_Offer->GetOfferPrice();
    $this->DTO_OrderItem->ItemProdType = 3;
    $this->DTO_OrderItem->ItemProdClass = $in_Class;
    $this->DTO_OrderItem->ItemSplitBillID = $in_SplitBillID;
    $this->DTO_OrderItem->ItemSplitBillRef = $in_SplitBillDesc;
    $this->DTO_OrderItem->ItemOffer = $in_Offer;
    $this->DTO_OrderItem->ItemPrintToKitchen = $in_PrintToKitchen;

    $this->DTO_OrderItem->ItemExtraToppingPrice = $in_Offer->GetOfferTopPrice();
    $this->DTO_OrderItem->ItemTopBut1 = 0;
    $this->DTO_OrderItem->ItemTopBut2 = 0;
    $this->DTO_OrderItem->ItemMarkToPrint = 1;

    $this->DTO_OrderItem->ItemSectionPos = $in_SectionPos; // Ray 30/10/12. Added
    $this->DTO_OrderItem->ItemSectionDesc = $in_SectionDesc; // Ray 30/10/12. Added

  }


  public function CreateProductToppings($in_Toppings, $in_ID, $in_LoadingOrder)
  {
    unset($this->DTO_OrderItem->ItemPlusToppings);
    unset($this->DTO_OrderItem->ItemMinusToppings);

      $TopCount = count($in_Toppings);

      for( $i = 0; $i < $TopCount; $i++ )
      {
        $TopID = $in_Toppings[$i]->GetTopID();
        $TopCode = $in_Toppings[$i]->GetTopCode();
        $TopDesc = $in_Toppings[$i]->GetTopDesc();
        $TopType = $in_Toppings[$i]->GetTopType();
        $TopPriceType = $in_Toppings[$i]->GetTopPriceType();
        $TopPrice = $in_Toppings[$i]->GetTopPrice();
        $this->DTO_OrderItem->ItemExtraToppingPrice += $TopPrice;
        //print "CreateProductToppings: " . $TopPrice;

        if( $in_LoadingOrder == 0 )
        {
          if( $TopType == 3 )
          {
            $TopDesc = "+" . $TopDesc;
          }
          else if( $TopType == 2 )
          {
            $TopDesc = "-" . $TopDesc;
          }
        }

        $TopReceiptID = ++$in_ID;
        $NewProdTopping = new Till_ProdTopping($TopID,$TopCode,$TopDesc,$TopPriceType,$TopPrice,$TopReceiptID,$TopType);
        if( $TopType == 2  )
        {
          $this->DTO_OrderItem->ItemMinusToppings[] = $NewProdTopping;
        }
        else  // Store fixed toppings in case item is edited
        {
          $this->DTO_OrderItem->ItemPlusToppings[] = $NewProdTopping;
        }
      }

  }
  public function SetProdMarkToPrint($in_MarkToPrint)
  {
    $this->DTO_OrderItem->ItemMarkToPrint = $in_MarkToPrint;
  }

  public function GetProdTopBut1()
  {
    return $this->DTO_OrderItem->ItemTopBut1;
  }

  public function GetProdTopBut2()
  {
    return $this->DTO_OrderItem->ItemTopBut2;
  }

  public function GetProdCode()
  {
    return $this->DTO_OrderItem->ItemCode;
  }

  public function GetProdDesc()
  {
    return $this->DTO_OrderItem->ItemDesc;
  }

  public function GetProdQty()
  {
    return $this->DTO_OrderItem->ItemQty;
  }

  public function GetProdSplitQty()
  {
    return $this->DTO_OrderItem->ItemSplitQty;
  }


  public function GetProdPrice()
  {
    return $this->DTO_OrderItem->ItemUnitPrice;
  }

  public function GetProdNET($in_HappyHour=false)
  {
    if($in_HappyHour)
    {

    }
    else
    {
      $this->DTO_OrderItem->ItemNET = $this->DTO_OrderItem->ItemQty*$this->DTO_OrderItem->ItemUnitPrice;
    }
    return $this->DTO_OrderItem->ItemNET;
  }

  public function GetProdVAT()
  {
    return $this->DTO_OrderItem->ItemVAT;
  }

  public function GetProdTopPrice()
  {
    return $this->DTO_OrderItem->ItemTopPrice;
  }

  public function GetProdTotalToppingPrice()
  {
    return $this->DTO_OrderItem->ItemQty*$this->DTO_OrderItem->ItemExtraToppingPrice;
  }

  public function GetProdDTO()
  {
    return $this->DTO_OrderItem;
  }
  public function GetProdCat()
  {
    return $this->DTO_OrderItem->ItemProdClass;

  }
  public function GetProdID()
  {
    return $this->DTO_OrderItem->ReceiptID;
  }

  public function GetProdPlusToppings()
  {
    return $this->DTO_OrderItem->ItemPlusToppings;
  }

  public function GetProdMinusToppings()
  {
    return $this->DTO_OrderItem->ItemMinusToppings;
  }

  public function GetProdSectionPos()
  {
    return $this->DTO_OrderItem->ItemSectionPos;
  }

  public function ToppingsChangedFlag()
  {
    $ToppingsChanged = false;

    $TopCount = count($this->DTO_OrderItem->ItemPlusToppings);
    print_r($this->DTO_OrderItem->ItemPlusToppings);
    for($j=0; $j < $TopCount; $j++ )
    {
      if( $this->DTO_OrderItem->ItemPlusToppings[$j]->GetTopType() == 3 )
      {
        $ToppingsChanged = true;
        break;
      }
    }
    if( !$ToppingsChanged )
    {
      $TopCount = count($this->DTO_OrderItem->ItemMinusToppings);
      for($j=0; $j < $TopCount; $j++ )
      {
        if( $this->DTO_OrderItem->ItemMinusToppings[$j]->GetTopType() == 2 )
        {
          $ToppingsChanged = true;
          break;
        }
      }
    }
    return $ToppingsChanged;
  }


  public function IncreaseProdQty()
  {
    $this->DTO_OrderItem->ItemQty += 1;
    $this->DTO_OrderItem->ItemSplitQty += 1;
  }

  public function SetProdPrintedQty($in_PrintedQty)
  {
    $this->DTO_OrderItem->ItemPrintedQty = $in_PrintedQty;
  }

  public function SetProdSplitPrintedQty($in_SplitPrintedQty)
  {
    $this->DTO_OrderItem->ItemSplitPrintedQty = $in_SplitPrintedQty;
  }

  public function GetProdPrintedQty()
  {
    return $this->DTO_OrderItem->ItemPrintedQty;
  }

  public function GetProdSplitPrintedQty()
  {
    return $this->DTO_OrderItem->ItemSplitPrintedQty;
  }

  public function GetProdFreeText()
  {
    return $this->DTO_OrderItem->ItemFreeText;
  }

  public function DecreaseProdSplitQty()
  {
    $SetPrintQty = false;
    $this->DTO_OrderItem->ItemSplitQty -= 1;

    if($this->DTO_OrderItem->ItemSplitQty < $this->DTO_OrderItem->ItemPrintedQty )
    {
      $this->DTO_OrderItem->ItemPrintedQty -= 1;
      $this->DTO_OrderItem->ItemSplitPrintedQty += 1;
      $SetPrintQty = true;
    }

    return $SetPrintQty;
  }

  public function IncreaseProdSplitQty($in_UpdatePrintedQty)
  {
    $this->DTO_OrderItem->ItemSplitQty += 1;

    if( $in_UpdatePrintedQty )
    {
      $this->DTO_OrderItem->ItemPrintedQty += 1;
      $this->DTO_OrderItem->ItemSplitPrintedQty -= 1;
    }

  }

  public function GetProdSplitBillID()
  {
    return $this->DTO_OrderItem->ItemSplitBillID;
  }

  public function SetProdSplitBillRef($in_SplitRef)
  {
    $this->DTO_OrderItem->ItemSplitBillRef = $in_SplitRef;
  }

  public function SetProdSplitBillID($in_SplitID)
  {
    $this->DTO_OrderItem->ItemSplitBillID = $in_SplitID;
  }

  public function IsItemOffer()
  {
    if( $this->DTO_OrderItem->ItemOffer != null )
    {
      return true;
    }

    return false;
  }

  public function GetItemOffer()
  {
    return $this->DTO_OrderItem->ItemOffer;
  }

  public function DeleteItem()
  {
  }

  public function UpdateItem($in_ItemQty, $in_ItemPrice, $in_FreeText)
  {
    $TmpQty = $this->DTO_OrderItem->ItemQty;
    $this->DTO_OrderItem->ItemQty = $in_ItemQty;
    $this->DTO_OrderItem->ItemSplitQty += $in_ItemQty-$TmpQty;     // Ray 22/03/12. Added
    $this->DTO_OrderItem->ItemUnitPrice = $in_ItemPrice;
    $this->DTO_OrderItem->ItemFreeText = "";
    if( $in_FreeText > "" )
    {
      $this->DTO_OrderItem->ItemFreeText = "(" . $in_FreeText . ")";             // Ray 04/04/12. Added
    }
  }



}

class Till_Order
{
  private $DalTill;
  private $DTO_Order;
  private $OrderItemsList = array();

  private $InsertingOrder;
  private $LoadingOrder;
  private $ReceiptIDCounter;
  private $OrderType;

  private $SplitBillList = array();
  private $ReportItemList = array();

  private $AmtPaid;
  private $OrderNeedsSaving;
  private $PrintSections;

  private $mOptions;

  function __construct($in_Options)
  {
    $this->DalTill = new DAL_ProphetTill();
    $this->DTO_Order = new OrderData();
    $SplitCount = 1;
    $CurSplit = 1;
    $this->OrderNeedsSaving = 0;
    $this->LoadingOrder = 0;

    $this->mOptions = $in_Options;

  }

  public function GetPrintSectionsFlag()
  {
    return $this->PrintSections;
  }


  public function ClearOrder()
  {
    $this->DTO_Order->Ord_ID = 0;
    $this->DTO_Order->Ord_No = 0;
    $this->DTO_Order->Ord_Date = 0;
    $this->DTO_Order->Ord_TableNo = 0;
    $this->DTO_Order->ReceiptIDCounter = 1;
    $this->DTO_Order->Ord_Total = 0;
    $this->DTO_Order->Ord_Balance = 0;
    $this->DTO_Order->Ord_PaidAmt = 0;
    $this->DTO_Order->Ord_Net = 0;
    $this->DTO_Order->Ord_Vat = 0;
    $this->DTO_Order->Ord_Delivery = 0;
    $this->DTO_Order->Ord_VATCode = 0;
    $this->DTO_Order->Ord_DiscAmt = 0;
    $this->DTO_Order->Ord_DiscRate = 0;
    $this->DTO_Order->Ord_TableCovers = '0';
    $this->DTO_Order->Ord_DeliveryType = '';
    $this->DTO_Order->Ord_Status = 0;
    $this->DTO_Order->Ord_LastTransID = 0;
    $this->OrderNeedsSaving = 0;
    $this->LoadingOrder = 0;


    $this->OrderItemsList = array();
    //$this->DTO_Order->Ord_ItemsList = null;
    $this->DTO_Order->Ord_ItemsList = array();
    unset($this->SplitBillList);
  }

  public function GetErrorMsg()
  {
    return $this->DalTill->GetErrorMsg();
  }

  public function SetSplitBillList($in_SplitBillList)
  {
    $this->SplitBillList = $in_SplitBillList;
  }

  public function GetSplitBillList()
  {
    return $this->SplitBillList;
  }

  public function NewOrder($in_OrderType, $in_TableID=0)
  {
    $this->ClearOrder();
    $this->InsertingOrder = true;
    $this->OrderType = $in_OrderType;

    $this->DTO_Order->Ord_No = $this->DalTill->GetNextOrderNo();
    date_default_timezone_set('Europe/London');
    $this->DTO_Order->Ord_Date = date('m/d/y');

    if( $this->OrderType == 1 )  // Table
    {
      $this->DTO_Order->Ord_AccRef = "COUNTER";
    }
    else if( $this->OrderType == 2 )  // Bar
    {
      $this->DTO_Order->Ord_AccRef = "BAR";
    }
    else if( $this->OrderType == 3 )  // Takeaway
    {
      $this->DTO_Order->Ord_AccRef = "TAKEAWAY";
    }

    $this->DTO_Order->Ord_PayType = 1;
    $this->UpdateOrderPayType($this->DTO_Order->Ord_PayType);  // To set payment description


    if( $in_TableID > 0 )
    {
      $this->DTO_Order->Ord_TableID = $in_TableID;
      $this->DTO_Order->Ord_TableNo = $this->DalTill->GetTableNo($in_TableID);
    }



    //echo("<BR>order no = " . $this->OrderNo);
  }
  public function GetOrderType()
  {
    return $this->OrderType;
  }


  public function LoadOrder($in_OrderID)
  {
    $this->ClearOrder();
    // Get main order details
    $OrderDetails = $this->DalTill->GetOrderDetails($in_OrderID);

    if( count($OrderDetails) > 0 )
    {
      // Set order fields
      $this->InsertingOrder = false;
      $this->LoadingOrder   = 1;
      $this->DTO_Order->Ord_ID            = $OrderDetails[0]["orderID"];
      $this->DTO_Order->Ord_No            = $OrderDetails[0]["orderNo"];
      $this->DTO_Order->Ord_Date          = $OrderDetails[0]["orderDate"];
      $this->DTO_Order->Ord_TableNo       = $OrderDetails[0]["orderTableNo"];
      $this->DTO_Order->Ord_AccRef        = $OrderDetails[0]["orderAccRef"];
      $this->DTO_Order->Ord_TableID       = $OrderDetails[0]["orderTableNo"];
      $this->DTO_Order->Ord_PayType       = $OrderDetails[0]["orderPayType"];
      $this->DTO_Order->Ord_TableCovers   = $OrderDetails[0]["orderCovers"];
      $this->DTO_Order->Ord_LastTransID   = $OrderDetails[0]["orderLastTransID"];


     // $this->DTO_Order->Ord_PaidAmt       = $OrderDetails[0]->But_Data[3];  // TODO
     // $this->DTO_Order->Ord_Balance       = $OrderDetails[0]->But_Data[3];  // TODO

      /*
      $this->DTO_Order->Ord_AccRef        = $OrderDetails[4];
      $this->DTO_Order->Ord_CustName      = $OrderDetails[5];
      $this->DTO_Order->Ord_Add1          = $OrderDetails[6];
      $this->DTO_Order->Ord_Add2          = $OrderDetails[7];
      $this->DTO_Order->Ord_Add3          = $OrderDetails[8];
      $this->DTO_Order->Ord_Add4          = $OrderDetails[9];
      $this->DTO_Order->Ord_Postcode      = $OrderDetails[10];
      $this->DTO_Order->Ord_RouteID       = $OrderDetails[11];
      $this->DTO_Order->Ord_Note1         = $OrderDetails[12];
      $this->DTO_Order->Ord_Note2         = $OrderDetails[13];
      $this->DTO_Order->Ord_PayType       = $OrderDetails[14];
      $this->DTO_Order->Ord_Carraige      = $OrderDetails[15];
      $this->DTO_Order->Ord_Discount      = $OrderDetails[16];
      $this->DTO_Order->Ord_LoyaltyDisc   = $OrderDetails[17];
      */
      $this->UpdateOrderPayType($this->DTO_Order->Ord_PayType);  // To set payment description

      $this->OrderType = 1;

      if( $this->DTO_Order->Ord_AccRef == "Bar" )
      {
        $this->OrderType = 2;
      }
      else if( $this->DTO_Order->Ord_AccRef == "Takeaway" )
      {
        $this->OrderType = 3;
      }


      // Get Order Items
      $OrderItems = $this->DalTill->GetOrderItemDetails($in_OrderID);
      $ItemCount = count($OrderItems);
      $MasterID = 0;
      $ToppingList = array();
      $ToppingCount = 0;
      $ItemCounter = 1;
      //print "ic $ItemCount";

      $InsList = array();    // Ray 04/04/12. Added


      for( $i=0; $i < $ItemCount; $i++ )
      {
          $ItemID               = $OrderItems[$i]["itemReceiptID"];
          $ItemCode             = $OrderItems[$i]["itemCode"];
          $ItemDesc             = $OrderItems[$i]["itemDesc"];
          $ItemQty              = $OrderItems[$i]["itemQty"];
          $ItemSplitQty         = $OrderItems[$i]["itemSplitQty"];
          $ItemPrintedQty       = $OrderItems[$i]["itemPrintedQty"];
          $ItemSplitPrintedQty  = $OrderItems[$i]["itemSplitPrintedQty"];
          $ItemPrice            = $OrderItems[$i]["itemPrice"];

          $ItemCat          = $OrderItems[$i]["itemProdCat"];
          $ItemMasterID     = $OrderItems[$i]["itemMasterID"];
          $ItemSplitBillID  = $OrderItems[$i]["itemSplitBillID"];
          $ItemSplitBillRef = $OrderItems[$i]["itemSplitBillRef"];
          $ItemMDID         = $OrderItems[$i]["itemMDID"];
          $ItemToppingPrice = $OrderItems[$i]["itemTopPrice"];
          $ItemTopBut1      = $OrderItems[$i]["itemTopBut1"];
          $ItemTopBut2      = $OrderItems[$i]["itemTopBut2"];
          $ItemSectionPos   = $OrderItems[$i]["itemSectionPos"];
          $ItemSectionDesc  = $OrderItems[$i]["itemSectionDesc"];

          $ItemPrintToKitchen  = 1;
          if( $OrderItems[$i]["itemTable"] == 10 )
            $ItemPrintToKitchen  = 0;

          //$ItemPrintToKitchen = 1;
          $FreeText = "";

          if( $ItemID > $this->DTO_Order->ReceiptIDCounter )  // Ray 05/04/12. Added
            $this->DTO_Order->ReceiptIDCounter = $ItemID;

          if( $OrderItems[$i]["itemItemTable"] == 10 || $OrderItems[$i]["itemItemTable"] == 11 )
          {
            $ItemPrintToKitchen = 0;
          }


        //if( $OrderItems[$i]->But_Data[5] == 3 ) // Offer
        if( $OrderItems[$i]["itemMDID"] > 0 ) // Offer
        {
          $ItemOffer = new Till_ProductOffer();
          $ItemOffer->SetOfferDetails($ItemCode,$ItemDesc,$ItemPrice);


          $ItemID               = $OrderItems[$i]["itemReceiptID"];
          $ItemQty              = $OrderItems[$i]["itemQty"];
          $ItemSplitQty         = $OrderItems[$i]["itemSplitQty"];
          $ItemPrintedQty       = $OrderItems[$i]["itemPrintedQty"];
          $ItemSplitPrintedQty  = $OrderItems[$i]["itemSplitPrintedQty"];
          $ItemPrice            = $OrderItems[$i]["itemPrice"];
          $ItemSectionPos       = $OrderItems[$i]["itemSectionPos"];
          $ItemSectionDesc      = $OrderItems[$i]["itemSectionDesc"];

          while( $OrderItems[++$i]["itemMDID"] == $ItemMDID )
          {

          /*
            $ItemID       = $OrderItems[$i]->But_Data[4];
            $ItemCode     = $OrderItems[$i]->But_Data[0];
            $ItemDesc     = $OrderItems[$i]->But_Data[1];
            $ItemQty      = $OrderItems[$i]->But_Data[2];
            $ItemSplitQty = $OrderItems[$i]->But_Data[14];   // Ray 30/03/12. Added
            $ItemPrice    = $OrderItems[$i]->But_Data[3];

            $ItemCat          = $OrderItems[$i]->But_Data[6];
            $ItemMasterID     = $OrderItems[$i]->But_Data[7];
            //$ItemSplitBillID  = $OrderItems[$i]->But_Data[9];
            //$ItemSplitBillRef = $OrderItems[$i]->But_Data[10];
            $ItemMDID         = $OrderItems[$i]->But_Data[11];
            $ItemToppingPrice = $OrderItems[$i]->But_Data[12];
            $ItemMasterID = $OrderItems[$i]->But_Data[7];

            $ItemTopBut1      = $OrderItems[$i]->But_Data[17];
            $ItemTopBut2      = $OrderItems[$i]->But_Data[18];
            */


            $ItemCode             = $OrderItems[$i]["itemCode"];
            $ItemDesc             = $OrderItems[$i]["itemDesc"];

            $ItemCat          = $OrderItems[$i]["itemProdCat"];
            $ItemMasterID     = $OrderItems[$i]["itemMasterID"];
            $ItemMDID         = $OrderItems[$i]["itemMDID"];
            $ItemToppingPrice = $OrderItems[$i]["itemTopPrice"];
            $ItemTopBut1      = $OrderItems[$i]["itemTopBut1"];
            $ItemTopBut2      = $OrderItems[$i]["itemTopBut2"];

            //$ItemPrintToKitchen  = $OrderItems[$i]["itemPrintToKitchen"];

            $ItemPrintToKitchen  = 1;
            if( $OrderItems[$i]["itemTable"] == 10 )
              $ItemPrintToKitchen  = 0;


            while( $OrderItems[$i]["itemMasterID"] == $ItemMasterID )
            {
            /*
              if( $OrderItems[$i]->But_Data[8] == 'TP' )
              {
              }
              else if( $OrderItems[$i]->But_Data[8] == 'MD+' )
              {
                $ItemToppingPrice = 0;
                // Check if next item is topitem
                if( $OrderItems[++$i]->But_Data[8] == 'TP' )
                {
                  $ItemToppingPrice = $OrderItems[$i]->But_Data[3];
                }
                --$i;

                $NewProdTopping = new Till_ProdTopping(++$ToppingCount,$OrderItems[$i]->But_Data[0], $OrderItems[$i]->But_Data[1],$OrderItems[$i]->But_Data[2],$ItemToppingPrice, $OrderItems[$i]->But_Data[4], 3);
                $ToppingList[] = $NewProdTopping;
              }
              else if( $OrderItems[$i]->But_Data[8] == 'MD-' )
              {
                $NewProdTopping = new Till_ProdTopping(++$ToppingCount,$OrderItems[$i]->But_Data[0], $OrderItems[$i]->But_Data[1],0,0, $OrderItems[$i]->But_Data[4], 2);
                $ToppingList[] = $NewProdTopping;
              }
              */
              $i++;
            }

            $ItemOfferButton = new Till_ProductOfferButton();
            $ItemOfferButton->SetSelectedProduct($ItemCode,$ItemDesc,0,$ToppingList,1);
            $ItemOffer->SetOfferItemButton($ItemOfferButton);
            unset($ToppingList);
            --$i;

          }
          //print "load:   itemid: " . $ItemID . "<br /><br />";
          //print_r($ItemOffer);
          $this->AddOfferItem($ItemOffer, $ItemQty, $ItemSplitQty, $ItemCat, $ItemSplitBillID, $ItemSplitBillRef,$ItemPrintToKitchen,$ItemSectionPos,$ItemSectionDesc,$ItemID);

        }
        else
        {


          // Get Toppings
          while( $OrderItems[++$i]["itemMasterID"] == $ItemMasterID )
          {

            if( $OrderItems[$i]["itemTopType"] == '-')  // Minus topping
            {
              $NewProdTopping = new Till_ProdTopping(++$ToppingCount,$OrderItems[$i]["itemCode"], $OrderItems[$i]["itemDesc"],0,0, $OrderItems[$i]["itemReceiptID"], 2);
              $ToppingList[] = $NewProdTopping;
            }
            else if( $OrderItems[$i]["itemTopType"] == '+' )  // Extra topping
            {
              $NewProdTopping = new Till_ProdTopping(++$ToppingCount,$OrderItems[$i]["itemCode"], $OrderItems[$i]["itemDesc"],$OrderItems[$i]["itemQty"],$OrderItems[$i]["itemPrice"], $OrderItems[$i]["itemReceiptID"], 3);
              $ToppingList[] = $NewProdTopping;
            }
            else if( $OrderItems[$i]["itemItemTable"] == '3'  ) // Instruction
            {
              //$NewProdIns = new Till_ProdIns($OrderItems[$i]->But_Data[0], 0);
              //$InsList[] = $NewProdIns;
            }
            else if( $OrderItems[$i]["itemItemTable"] == '4' ) // Free text
            {
              $FreeText = $OrderItems[$i]["itemDesc"];
            }

          }

          $this->AddItem($ItemCode,$ItemDesc,$ItemQty,$ItemSplitQty,$ItemPrice,$ItemCat,$ToppingList,$ItemSplitBillID,$ItemSplitBillRef,$ItemToppingPrice,$ItemPrintToKitchen,$FreeText,$ItemTopBut1,$ItemTopBut2,$ItemSectionPos,$ItemSectionDesc,$ItemID);

        }
          $LastIdx = count($this->OrderItemsList)-1;       // Ray 27/03/12. Added
          $this->OrderItemsList[$LastIdx]->SetProdPrintedQty($ItemPrintedQty);
          $this->OrderItemsList[$LastIdx]->SetProdSplitPrintedQty($ItemSplitPrintedQty);

        --$i;




          unset($ToppingList);
          $ToppingCount = 0;

      }


      // Ray 23/03/12. Added
      // Get Split Bill Items


      $SplitBillItems = $this->DalTill->GetOrderSplitBillItems($in_OrderID);
      $SplitBillCount = count($SplitBillItems);

      for( $i=0; $i < $SplitBillCount; $i++ )
      {
          $NewSplitBillItem = new SplitItem;

          $NewSplitBillItem->SplitID        = $SplitBillItems[$i]["SplitNo"];    // split No
          $NewSplitBillItem->SplitRef       = $SplitBillItems[$i]["SplitRef"];    // Ref
          $NewSplitBillItem->ItemID         = $SplitBillItems[$i]["SplitItemID"];    // Item ID
          $NewSplitBillItem->ItemQty        = $SplitBillItems[$i]["SplitItemQty"];    // Qty
          $NewSplitBillItem->ItemPrintedQty = $SplitBillItems[$i]["SplitItemPrintedQty"];    // Printed Qty

          $this->SplitBillList[] = $NewSplitBillItem;

       }

    }

    $this->OrderNeedsSaving = 0;
    $this->LoadingOrder = 0;
  }

  public function SaveOrder($in_UpdatePrintedQty)
  {
    $this->CreateOrderData();
    $this->DTO_Order->Ord_UpdatePrintedQty = $in_UpdatePrintedQty;
    $saveres = $this->DalTill->SaveOrder($this->DTO_Order,$this->InsertingOrder);
    if( $saveres )
    {
      $this->InsertingOrder = false;
      $this->OrderNeedsSaving = 0;
    }
    return $saveres;
  }

  public function CreateOrderData()
  {
    $this->DTO_Order->Ord_ItemsList = array();
    $ItemCount = count($this->OrderItemsList);

    for( $i=0; $i < $ItemCount; $i++)
    {
      $this->DTO_Order->Ord_ItemsList[] = $this->OrderItemsList[$i]->GetProdDTO();
    }
    if( $this->DTO_Order->Ord_PaidAmt > 0 )
    {
        if( $this->DTO_Order->Ord_PaidAmt >= $this->DTO_Order->Ord_Total )
        {
            $this->DTO_Order->Ord_Prepayment = $this->DTO_Order->Ord_Total;
        }
        else
        {
            $this->DTO_Order->Ord_Prepayment = $this->DTO_Order->Ord_Total - $this->DTO_Order->Ord_PaidAmt;
        }
    }
    else
    {
        $this->DTO_Order->Ord_Prepayment = 0;
    }

    $this->DTO_Order->Ord_SplitItemsList = $this->SplitBillList;

  }

  public function PrepareOrderForPrinting($in_PrintType, $printerName)
  {
    // Add items to report_data table ready for printing
    //usort($this->OrderItemsList,'SortByCat');
    //usort($this->OrderItemsList,'SortBySectionPos');


    //$CurOptions = $_SESSION['TillOptionsObj'];
    $PrintOnlyNewItems = $this->mOptions->GetOption_PrintOnlyNewItems();
    //$PrintOnlyNewItems = 0;

    // 1: Print, 2: Pay & Print, 3: Print Bill

    $rpt_F1 = "";
    $rpt_F2 = "";
    $rpt_F3 = "";
    $rpt_F4 = "";
    $rpt_F5 = "";
    $rpt_F6 = "";
    $rpt_F7 = "";
    $rpt_F8 = "";
    $rpt_F10 = "";
    $rpt_F11 = "";

    $InvToBePrinted = false;
    unset($this->ReportItemList);
    $this->CreateOrderData();

    $OrderID = $this->DTO_Order->Ord_ID;
    $ItemCount = count($this->DTO_Order->Ord_ItemsList);
    $this->PrintSections = "0";

    for( $i=0; $i < $ItemCount; $i++ )
    {

      $ItemMasterID         = $this->DTO_Order->Ord_ItemsList[$i]->ItemMasterID;
      $ItemCat              = $this->DTO_Order->Ord_ItemsList[$i]->ItemProdClass;
      $ItemType             = $this->DTO_Order->Ord_ItemsList[$i]->ItemProdType;
      $ItemCode             = $this->DTO_Order->Ord_ItemsList[$i]->ItemCode;
      $ItemDesc             = $this->DTO_Order->Ord_ItemsList[$i]->ItemDesc;
      $ItemQty              = $this->DTO_Order->Ord_ItemsList[$i]->ItemQty;
      $ItemSplitQty         = $this->DTO_Order->Ord_ItemsList[$i]->ItemSplitQty;
      $ItemPrintedQty       = $this->DTO_Order->Ord_ItemsList[$i]->ItemPrintedQty;
      $ItemPrice            = $this->DTO_Order->Ord_ItemsList[$i]->ItemUnitPrice;
      $ItemNET              = $this->DTO_Order->Ord_ItemsList[$i]->ItemNET;
      $ItemSplitID          = $this->DTO_Order->Ord_ItemsList[$i]->ItemSplitBillID;
      $ItemSplitRef         = $this->DTO_Order->Ord_ItemsList[$i]->ItemSplitBillRef;
      $ItemPrintToKitchen   = $this->DTO_Order->Ord_ItemsList[$i]->ItemPrintToKitchen;
      $ItemFreeText         = $this->DTO_Order->Ord_ItemsList[$i]->ItemFreeText;
      $ItemMarkToPrint      = $this->DTO_Order->Ord_ItemsList[$i]->ItemMarkToPrint;
      $ItemSectionPos       = $this->DTO_Order->Ord_ItemsList[$i]->ItemSectionPos;
      $ItemSectionDesc      = $this->DTO_Order->Ord_ItemsList[$i]->ItemSectionDesc;


      if( $ItemSectionPos > "" )
        $this->PrintSections = "1";

      if( $ItemSplitQty > 0 )
      {
        $rpt_F1 = $rpt_F2 = $rpt_F3 = $rpt_F4 = $rpt_F5 = $rpt_F6 = $rpt_F7 = $rpt_F8 = $rpt_F10 = $rpt_F11 =  $rpt_F14 =  $rpt_F16 = "";

        // Print Bill type = 3, always print all items
        $PrintItem = 1;
        if( $in_PrintType != 3 )
        {

          if( $in_PrintType == 1 && $PrintOnlyNewItems == 1 )
          {
            if( $ItemMarkToPrint != 1 )
            {
                continue;
              //$PrintItem = 0;
            }
          }


          if( $ItemSplitQty*$ItemPrice > 0 )
          {
            $ItemQty = $ItemSplitQty;

            if( $PrintOnlyNewItems == 1 )
            {
              if( $ItemSplitQty > $ItemPrintedQty )
              {
                $ItemQty = $ItemSplitQty - $ItemPrintedQty;
              }
              else
              {
                $PrintItem = 0;
              }
            }
          }
        }

        if( $PrintItem == 1 )
        {
          $rptID = 0;
          $rpt_F1 = $OrderID;
          $rpt_F2 = 0;
          $rpt_F3 = 0;
          $rpt_F4 = 0;

          if( $ItemQty*$ItemPrice > 0 )
          {
            $rpt_F2 = $ItemQty;
            $rpt_F3 = $ItemQty*$ItemPrice;
            $rpt_F4 = $ItemPrice;
          }

          $rpt_F5 = 0;
          $rpt_F6 = "";
          $rpt_F7 = "";
          $rpt_F8 = $ItemPrintToKitchen;
          $rpt_F10 = "Unassigned";
          $rpt_F11 = $ItemDesc;
          $rpt_F14 = $ItemSectionPos;
          $rpt_F16 = $ItemSectionDesc;

          if ($ItemPrintToKitchen != 0 && $printerName == "Kitchen Printer"){
            $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
          }
          if ($printerName == "Till Printer" && $ItemPrintToKitchen == 0){
            $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
          }
          $InvToBePrinted = true;
          if( $ItemType == 3 ) // Offer
          {
            // Insert offer items

            $ItemOffer = $this->DTO_Order->Ord_ItemsList[$i]->ItemOffer;
            $OfferItems = $ItemOffer->GetOfferItems();
            $OfferItemsCount = count($OfferItems);

            for( $j=0; $j<$OfferItemsCount; $j++ )
            {
              $rpt_F2 = $rpt_F3 = $rpt_F4 = $rpt_F5 = $rpt_F6 = $rpt_F7 = $rpt_F8 = $rpt_F10 =  $rpt_F11 = "";
              $ItemDesc = "   " . $OfferItems[$j]->GetButtonSelectedText();

              $rpt_F5 = 0;
              $rpt_F6 = "MDI";
              $rpt_F8 = $ItemPrintToKitchen;
              $rpt_F10 = "Unassigned";
              $rpt_F11 = $ItemDesc;

              if ($ItemPrintToKitchen != 0 && $printerName == "Kitchen Printer"){
                $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
              }
              if ($printerName == "Till Printer" && $ItemPrintToKitchen == 0){
                $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
              }
              $ItemPlusToppings = $OfferItems[$j]->GetButtonSelectedPlusToppings();
              $TopCount = count($ItemPlusToppings);
              if( $TopCount > 0 )
              {
                $TopPrice = 0;
                $TopPrice = $ItemQty*$OfferItems[$j]->GetButtonSelectedExtraToppingPrice();
                $ToppingLine = "";

                for( $t=0; $t<$TopCount; $t++ )
                {
                  if( $ItemPlusToppings[$t]->GetTopType() == 3 )
                  {
                    if( $ToppingLine > "" )
                      $ToppingLine .= ", ";

                    $ToppingLine .= $ItemPlusToppings[$t]->GetTopDesc();
                  }
                }

                if( $ToppingLine > "" )
                {
                    $ToppingLine = "      " . $ToppingLine;
                    $rpt_F2 = "";
                    $rpt_F3 = $TopPrice;
                    $rpt_F4 = "";
                    $rpt_F5 = 0;
                    $rpt_F6 = "+";
                    $rpt_F7 = "";
                    $rpt_F8 = 1;
                    $rpt_F10 = "Unassigned";
                    $rpt_F11 = $ToppingLine;

                    if ($ItemPrintToKitchen != 0 && $printerName == "Kitchen Printer"){
                      $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
                    }
                    if ($printerName == "Till Printer" && $ItemPrintToKitchen == 0){
                      $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
                    }
                }
              }

              $ItemMinusToppings =  $OfferItems[$j]->GetButtonSelectedMinusToppings();;
              $TopCount = count($ItemMinusToppings);

              if( $TopCount > 0 )
              {
                $TopPrice = 0;
                $ToppingLine = "";

                for( $t=0; $t<$TopCount; $t++ )
                {
                  if( $ItemMinusToppings[$t]->GetTopType() == 2 )
                  {
                    if( $ToppingLine > "" )
                      $ToppingLine .= ", ";

                    $ToppingLine .= $ItemMinusToppings[$t]->GetTopDesc();
                  }
                }

                if( $ToppingLine > "" )
                {
                  $ToppingLine = "   " . $ToppingLine;

                  $rpt_F2 = "";
                  $rpt_F3 = "";
                  $rpt_F4 = "";
                  $rpt_F5 = 0;
                  $rpt_F6 = "-";
                  $rpt_F7 = "";
                  $rpt_F8 = 1;
                  $rpt_F10 = "Unassigned";
                  $rpt_F11 = $ToppingLine;

                  if ($ItemPrintToKitchen != 0 && $printerName == "Kitchen Printer"){
                    $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
                  }
                  if ($printerName == "Till Printer" && $ItemPrintToKitchen == 0){
                    $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
                  }
                }
              }
            }
          }
          else
          {

              $ItemPlusToppings = $this->DTO_Order->Ord_ItemsList[$i]->ItemPlusToppings;

              $TopCount = count($ItemPlusToppings);
              if( $TopCount > 0 )
              {
                $TopPrice = 0;
                $TopPrice = $ItemQty*$this->TmpOrderData->Ord_ItemsList[$i]->ItemExtraToppingPrice;
                $ToppingLine = "";
                for( $t=0; $t<$TopCount; $t++ )
                {
                  if( $ItemPlusToppings[$t]->GetTopType() == 3 )
                  {
                    if( $ToppingLine > "" )
                      $ToppingLine .= ", ";

                    $ToppingLine .= $ItemPlusToppings[$t]->GetTopDesc();
                  }
                }

                if( $ToppingLine > "" )
                {
                  $ToppingLine = "   " . $ToppingLine;

                  $rpt_F2 = "";
                  $rpt_F3 = $TopPrice;
                  $rpt_F4 = "";
                  $rpt_F5 = 0;
                  $rpt_F6 = "+";
                  $rpt_F7 = "";
                  $rpt_F8 = 1;
                  $rpt_F10 = "Unassigned";
                  $rpt_F11 = $ToppingLine;

                  if ($ItemPrintToKitchen != 0 && $printerName == "Kitchen Printer"){
                    $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
                  }
                  if ($printerName == "Till Printer" && $ItemPrintToKitchen == 0){
                    $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
                  }
                }
              }
              $ItemMinusToppings = $this->DTO_Order->Ord_ItemsList[$i]->ItemMinusToppings;
              $TopCount = count($ItemMinusToppings);

              if( $TopCount > 0 )
              {
                $TopPrice = 0;
                $ToppingLine = "";

                for( $t=0; $t<$TopCount; $t++ )
                {
                  if( $ItemMinusToppings[$t]->GetTopType() == 2 )
                  {
                    if( $ToppingLine > "" )
                      $ToppingLine .= ", ";

                    $ToppingLine .= $ItemMinusToppings[$t]->GetTopDesc();
                  }
                }

                if( $ToppingLine > "" )
                {
                  $ToppingLine = "   " . $ToppingLine;

                  $rpt_F2 = "";
                  $rpt_F3 = "";
                  $rpt_F4 = "";
                  $rpt_F5 = 0;
                  $rpt_F6 = "-";
                  $rpt_F7 = "";
                  $rpt_F8 = 1;
                  $rpt_F10 = "Unassigned";
                  $rpt_F11 = $ToppingLine;

                  if ($ItemPrintToKitchen != 0 && $printerName == "Kitchen Printer"){
                    $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
                  }
                  if ($printerName == "Till Printer" && $ItemPrintToKitchen == 0){
                    $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
                  }
                }
              }

          }

          if( $ItemFreeText > "" )   // Ray 04/04/12. Added
          {
            $rpt_F2 = "";
            $rpt_F3 = "";
            $rpt_F4 = "";
            $rpt_F5 = 0;
            $rpt_F6 = "i";
            $rpt_F7 = "";
            $rpt_F8 = 1;
            $rpt_F10 = "Unassigned";
            $rpt_F11 = "   " . $ItemFreeText;

            if ($ItemPrintToKitchen != 0 && $printerName == "Kitchen Printer"){
              $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
            }
            if ($printerName == "Till Printer" && $ItemPrintToKitchen == 0){
              $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
            }
                    }
        }
      }


    }

    $ItemCount = count($this->DTO_Order->Ord_SplitItemsList);

    for( $i=0; $i < $ItemCount; $i++ )
    {
      $SplitItem = $this->DTO_Order->Ord_SplitItemsList[$i];

      $OrderItem = $this->GetOrderItemDetails($SplitItem->ItemID);

      //print_r($OrderItem);
      //print "<br /><br />";
      if( $OrderItem )
      {
      $OrderItemDTO = $OrderItem->GetProdDTO();
      $ItemMasterID = $OrderItemDTO->ItemMasterID;
      $ItemCat      = $OrderItemDTO->ItemProdClass;
      $ItemType     = $OrderItemDTO->ItemProdType;
      $ItemCode     = $OrderItemDTO->ItemCode;
      $ItemDesc     = $OrderItemDTO->ItemDesc;
      $ItemPrice    = $OrderItemDTO->ItemUnitPrice;
      $ItemPrintToKitchen = $OrderItemDTO->ItemPrintToKitchen;
      $ItemFreeText = $OrderItemDTO->ItemFreeText;
      $ItemMarkToPrint = $OrderItemDTO->ItemMarkToPrint;
      $ItemSectionPos = $OrderItemDTO->ItemSectionPos;
      $ItemSectionDesc = $OrderItemDTO->ItemSectionDesc;

      $ItemQty          = $SplitItem->ItemQty;
      $ItemPrintedQty   = $SplitItem->ItemPrintedQty;

      $rpt_F1 = $rpt_F2 = $rpt_F3 = $rpt_F4 = $rpt_F5 = $rpt_F6 = $rpt_F7 = $rpt_F8 = $rpt_F10 =  $rpt_F11 = $rpt_F14 = $rpt_F16 = "";

      $rptID = 0;
      $rpt_F1 = $OrderID;


      // Print Bill type = 3, always print all items
      $PrintItem = 1;
      if( $in_PrintType != 3 )
      {
        if( $in_PrintType == 1 && $PrintOnlyNewItems == 1 )
        {
          if( $ItemMarkToPrint != 1 )
          {
              continue;
            //$PrintItem = 0;
          }
        }


            if( $PrintOnlyNewItems == 1 )
            {
              if( $ItemQty > $ItemPrintedQty )
              {
                $ItemQty = $ItemQty - $ItemPrintedQty;
              }
              else
              {
                $PrintItem = 0;
              }
            }
      }


      if( $PrintItem == 1 )
      {
        $InvToBePrinted = true;
        $rpt_F2 = 0;
        $rpt_F3 = 0;
        $rpt_F4 = 0;

        if( $ItemQty*$ItemPrice > 0 )
        {
          $rpt_F2 = $ItemQty;
          $rpt_F3 = $ItemQty*$ItemPrice;
          $rpt_F4 = $ItemPrice;
        }

          $rpt_F5 = $SplitItem->SplitID;
          $rpt_F6 = "";
          $rpt_F7 = "";
          $rpt_F8 = $ItemPrintToKitchen;
          $rpt_F10 = $SplitItem->SplitRef;
          $rpt_F11 = $ItemDesc;
          $rpt_F14 = $ItemSectionPos;
          $rpt_F16 = $ItemSectionDesc;

          if ($ItemPrintToKitchen != 0 && $printerName == "Kitchen Printer"){
            $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
          }
          if ($printerName == "Till Printer" && $ItemPrintToKitchen == 0){
            $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
          }
          if( $ItemType == 3 ) // Offer
          {
            // Insert offer items

            $ItemOffer = $OrderItemDTO->ItemOffer;
            $OfferItems = $ItemOffer->GetOfferItems();
            $OfferItemsCount = count($OfferItems);

            for( $j=0; $j<$OfferItemsCount; $j++ )
            {
              $rpt_F2 = $rpt_F3 = $rpt_F4 = $rpt_F5 = $rpt_F6 = $rpt_F7 = $rpt_F8 = $rpt_F10 =  $rpt_F11 = "";
              $ItemDesc = "   " . $OfferItems[$j]->GetButtonSelectedText();

              $rpt_F5 = $SplitItem->SplitID;
              $rpt_F6 = "MDI";
              $rpt_F8 = $ItemPrintToKitchen;
              $rpt_F10 = $SplitItem->SplitRef;
              $rpt_F11 = $ItemDesc;

              if ($ItemPrintToKitchen != 0 && $printerName == "Kitchen Printer"){
                $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
              }
              if ($printerName == "Till Printer" && $ItemPrintToKitchen == 0){
                $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
              }
              $ItemPlusToppings = $OfferItems[$j]->GetButtonSelectedPlusToppings();
              $TopCount = count($ItemPlusToppings);
              if( $TopCount > 0 )
              {
                $TopPrice = 0;
                $TopPrice = $ItemQty*$OfferItems[$j]->GetButtonSelectedExtraToppingPrice();
                $ToppingLine = "";

                for( $t=0; $t<$TopCount; $t++ )
                {
                  if( $ItemPlusToppings[$t]->GetTopType() == 3 )
                  {
                    if( $ToppingLine > "" )
                      $ToppingLine .= ", ";

                    $ToppingLine .= $ItemPlusToppings[$t]->GetTopDesc();
                  }
                }

                if( $ToppingLine > "" )
                {
                    $ToppingLine = "      " . $ToppingLine;
                    $rpt_F2 = "";
                    $rpt_F3 = $TopPrice;
                    $rpt_F4 = "";
                    $rpt_F5 = $SplitItem->SplitID;
                    $rpt_F6 = "+";
                    $rpt_F7 = "";
                    $rpt_F8 = 1;
                    $rpt_F10 = $SplitItem->SplitRef;
                    $rpt_F11 = $ToppingLine;

                    if ($ItemPrintToKitchen != 0 && $printerName == "Kitchen Printer"){
                      $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
                    }
                    if ($printerName == "Till Printer" && $ItemPrintToKitchen == 0){
                      $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
                    }
                                  }
              }

              $ItemMinusToppings =  $OfferItems[$j]->GetButtonSelectedMinusToppings();;
              $TopCount = count($ItemMinusToppings);

              if( $TopCount > 0 )
              {
                $TopPrice = 0;
                $ToppingLine = "";

                for( $t=0; $t<$TopCount; $t++ )
                {
                  if( $ItemMinusToppings[$t]->GetTopType() == 2 )
                  {
                    if( $ToppingLine > "" )
                      $ToppingLine .= ", ";

                    $ToppingLine .= $ItemMinusToppings[$t]->GetTopDesc();
                  }
                }

                if( $ToppingLine > "" )
                {
                  $ToppingLine = "   " . $ToppingLine;

                  $rpt_F2 = "";
                  $rpt_F3 = "";
                  $rpt_F4 = "";
                  $rpt_F5 = $SplitItem->SplitID;
                  $rpt_F6 = "-";
                  $rpt_F7 = "";
                  $rpt_F8 = 1;
                  $rpt_F10 = $SplitItem->SplitRef;
                  $rpt_F11 = $ToppingLine;

                  if ($ItemPrintToKitchen != 0 && $printerName == "Kitchen Printer"){
                    $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
                  }
                  if ($printerName == "Till Printer" && $ItemPrintToKitchen == 0){
                    $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
                  }
                                }
              }
            }
          }
          else
          {
              $ItemPlusToppings = $OrderItemDTO->ItemPlusToppings;
              $TopCount = count($ItemPlusToppings);

              if( $TopCount > 0 )
              {
                $TopPrice = 0;
                $TopPrice = $ItemQty*$OrderItemDTO->ItemExtraToppingPrice;
                $ToppingLine = "";
                for( $t=0; $t<$TopCount; $t++ )
                {
                  if( $ItemPlusToppings[$t]->GetTopType() == 3 )
                  {
                    if( $ToppingLine > "" )
                      $ToppingLine .= ", ";

                    $ToppingLine .= $ItemPlusToppings[$t]->GetTopDesc();
                  }
                }

                if( $ToppingLine > "" )
                {
                  $ToppingLine = "   " . $ToppingLine;

                  $rpt_F2 = "";
                  $rpt_F3 = $TopPrice;
                  $rpt_F4 = "";
                  $rpt_F5 = $SplitItem->SplitID;
                  $rpt_F6 = "+";
                  $rpt_F7 = "";
                  $rpt_F8 = 1;
                  $rpt_F10 = $SplitItem->SplitRef;
                  $rpt_F11 = $ToppingLine;

                  if ($ItemPrintToKitchen != 0 && $printerName == "Kitchen Printer"){
                    $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
                  }
                  if ($printerName == "Till Printer" && $ItemPrintToKitchen == 0){
                    $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
                  }
                                }
              }
              $ItemMinusToppings = $OrderItemDTO->ItemMinusToppings;
              $TopCount = count($ItemMinusToppings);

              if( $TopCount > 0 )
              {
                $TopPrice = 0;
                $ToppingLine = "";

                for( $t=0; $t<$TopCount; $t++ )
                {
                  if( $ItemMinusToppings[$t]->GetTopType() == 2 )
                  {
                    if( $ToppingLine > "" )
                      $ToppingLine .= ", ";

                    $ToppingLine .= $ItemMinusToppings[$t]->GetTopDesc();
                  }
                }

                if( $ToppingLine > "" )
                {
                  $ToppingLine = "   " . $ToppingLine;

                  $rpt_F2 = "";
                  $rpt_F3 = "";
                  $rpt_F4 = "";
                  $rpt_F5 = $SplitItem->SplitID;
                  $rpt_F6 = "-";
                  $rpt_F7 = "";
                  $rpt_F8 = 1;
                  $rpt_F10 = $SplitItem->SplitRef;
                  $rpt_F11 = $ToppingLine;

                  if ($ItemPrintToKitchen != 0 && $printerName == "Kitchen Printer"){
                    $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
                  }
                  if ($printerName == "Till Printer" && $ItemPrintToKitchen == 0){
                    $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
                  }
                 }
              }
          }

        if( $ItemFreeText > "" )   // Ray 04/04/12. Added
        {
          $rpt_F2 = "";
          $rpt_F3 = "";
          $rpt_F4 = "";
          $rpt_F5 = $SplitItem->SplitID;
          $rpt_F6 = "i";
          $rpt_F7 = "";
          $rpt_F8 = 1;
          $rpt_F10 = $SplitItem->SplitRef;
          $rpt_F11 = "   " . $ItemFreeText;

          if ($ItemPrintToKitchen != 0 && $printerName == "Kitchen Printer"){
            $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
          }
          if ($printerName == "Till Printer" && $ItemPrintToKitchen == 0){
            $this->InsertReportItem($rptID, $rpt_F1, $rpt_F2, $rpt_F3, $rpt_F4, $rpt_F5, $rpt_F6, $rpt_F7, $rpt_F8, $rpt_F10, $rpt_F11, $rpt_F14, $rpt_F16);
          }
                }
      }
      }
    }

    $rptID = $this->DalTill->SaveOrderReportData($this->ReportItemList);
    
    usort($this->OrderItemsList,'SortByID');
    if( !$InvToBePrinted )
      $rptID = -1;

    return $rptID;
  }

  public function InsertReportItem($in_rptID, $in_F1, $in_F2, $in_F3, $in_F4, $in_F5, $in_F6, $in_F7, $in_F8, $in_F10, $in_F11, $in_F14, $in_F16)
  {

    //print "$in_F1 . $in_F3 . $in_F4 . $in_F5 . $in_F6 . $in_F7 . $in_F8 . $in_F10 . $in_F11";
    $NewReportItem = new ReportItem;
    $NewReportItem->rpt_ID = in_rptID;
    $NewReportItem->rpt_F1 = $in_F1;
    $NewReportItem->rpt_F2 = $in_F2;
    $NewReportItem->rpt_F3 = $in_F3;
    $NewReportItem->rpt_F4 = $in_F4;
    $NewReportItem->rpt_F11 = $in_F11;
    $NewReportItem->rpt_F6 = $in_F6;
    $NewReportItem->rpt_F7 = $in_F7;
    $NewReportItem->rpt_F5 = $in_F5;
    $NewReportItem->rpt_F10 = $in_F10;
    $NewReportItem->rpt_F8 = $in_F8;
    $NewReportItem->rpt_F14 = $in_F14;
    $NewReportItem->rpt_F16 = $in_F16;

    $this->ReportItemList[] = $NewReportItem;

  }

  public function AddItem($in_Code, $in_Desc, $in_Qty, $in_SplitQty, $in_UnitPrice, $in_Class, $in_Toppings, $in_SplitBillID, $in_SplitBillDesc, $in_ToppingPrice, $in_PrintToKitchen, $in_FreeText, $in_TopBut1, $in_TopBut2, $in_SectionPos, $in_SectionDesc, $in_ItemID)
  {
    $NewOrderItem = new Till_OrderItem();
    //$NewOrderItem->CreateProduct($this->DTO_Order->ReceiptIDCounter++,$in_Code,$in_Desc,$in_Qty,$in_SplitQty,$in_UnitPrice,0,$in_Class, $in_Toppings, $in_SplitBillID, $in_SplitBillDesc, $in_ToppingPrice, $this->LoadingOrder, $in_PrintToKitchen, $in_FreeText, $in_TopBut1, $in_TopBut2,$in_SectionPos, $in_SectionDesc);     // , $in_ProdSubLinkID, $in_SubProductModifierList
    if( $in_ItemID == 0 )
      $in_ItemID = $this->DTO_Order->ReceiptIDCounter++;

    $NewOrderItem->CreateProduct($in_ItemID,$in_Code,$in_Desc,$in_Qty,$in_SplitQty,$in_UnitPrice,0,$in_Class, $in_Toppings, $in_SplitBillID, $in_SplitBillDesc, $in_ToppingPrice, $this->LoadingOrder, $in_PrintToKitchen, $in_FreeText, $in_TopBut1, $in_TopBut2,$in_SectionPos, $in_SectionDesc);     // , $in_ProdSubLinkID, $in_SubProductModifierList
    if( $in_ItemID > $this->DTO_Order->ReceiptIDCounter )
      $this->DTO_Order->ReceiptIDCounter = $in_ItemID;
    $this->OrderItemsList[] = $NewOrderItem;
    $this->OrderNeedsSaving = 1;

  }

  public function AddOfferItem($in_Offer, $in_Qty, $in_SplitQty, $in_Class, $in_SplitBillID, $in_SplitBillDesc, $in_PrintToKitchen, $in_SectionPos, $in_SectionDesc, $in_ItemID)
  {
    $NewOrderItem = new Till_OrderItem();
//    $NewOrderItem->CreateProductOffer($this->DTO_Order->ReceiptIDCounter++, $in_Offer, $in_Qty, $in_SplitQty, $in_Class, $in_SplitBillID, $in_SplitBillDesc, $in_PrintToKitchen, $in_SectionPos, $in_SectionDesc);
    if( $in_ItemID == 0 )
      $in_ItemID = $this->DTO_Order->ReceiptIDCounter++;
    $NewOrderItem->CreateProductOffer($in_ItemID, $in_Offer, $in_Qty, $in_SplitQty, $in_Class, $in_SplitBillID, $in_SplitBillDesc, $in_PrintToKitchen, $in_SectionPos, $in_SectionDesc);
    if( $in_ItemID > $this->DTO_Order->ReceiptIDCounter )
      $this->DTO_Order->ReceiptIDCounter = $in_ItemID;
    $this->OrderItemsList[] = $NewOrderItem;
    $this->OrderNeedsSaving = 1;
  }

  public function DeleteItem()
  {
  }

  public function GetOrderID()
  {
    return $this->DTO_Order->Ord_ID;
  }

  public function GetOrderNo()
  {
    return $this->DTO_Order->Ord_No;
  }

  public function GetTableNo()
  {
    return $this->DTO_Order->Ord_TableNo;
  }

  public function GetOrderItems()
  {
    return $this->OrderItemsList;
  }

  public function GetOrderTotal()
  {
    $this->DTO_Order->Ord_Total = 0;
    $this->DTO_Order->Ord_Net = 0;
    $this->DTO_Order->Ord_Vat = 0;

    $ItemCount = count($this->OrderItemsList);
    for( $i=0; $i < $ItemCount; $i++)
    {
      $this->DTO_Order->Ord_Net += $this->OrderItemsList[$i]->GetProdNET();
      $this->DTO_Order->Ord_Vat += $this->OrderItemsList[$i]->GetProdVAT();
      $this->DTO_Order->Ord_Net += $this->OrderItemsList[$i]->GetProdTotalToppingPrice();
    }
    $this->DTO_Order->Ord_Total = $this->DTO_Order->Ord_Net + $this->DTO_Order->Ord_Vat;
    $this->DTO_Order->Ord_DiscAmt = $this->DTO_Order->Ord_Total/100 * $this->DTO_Order->Ord_DiscRate;

    $this->DTO_Order->Ord_Total -= $this->DTO_Order->Ord_DiscAmt;

    return $this->DTO_Order->Ord_Total;
  }

  public function GetOrderBalance()
  {
    return $this->DTO_Order->Ord_Balance;
  }

  public function GetOrderAmtDue()
  {

    $AmtDue = $this->DTO_Order->Ord_Total - $this->DTO_Order->Ord_PaidAmt;
    if( $AmtDue < 0 )
      $AmtDue = 0;

    return $AmtDue;
  }

  public function GetOrderAmtPaid()
  {
    return $this->DTO_Order->Ord_PaidAmt;
  }

  public function GetOrderDetails()
  {
    return $this->DTO_Order;
  }

  public function PayFullAmount()
  {
     $this->DTO_Order->Ord_PaidAmt = $this->DTO_Order->Ord_Total;
  }

  public function PayOrder($in_UpdatedPrintedQty)
  {
    $PaySuccess = false;

    if( $this->DTO_Order->Ord_PaidAmt >= $this->DTO_Order->Ord_Total )
    {
      if( $this->SaveOrder($in_UpdatedPrintedQty) )
      {
        $PaySuccess = true;
      }
    }

    return $PaySuccess;
  }

  public function GetOrderDiscRate()
  {
    return $this->DTO_Order->Ord_DiscRate;
  }

  public function GetOrderDiscAmount()
  {
    return $this->DTO_Order->Ord_DiscAmt;
  }

  public function UpdateOrderDiscRate($in_DiscRate)
  {
    if( $in_DiscRate < 0 || $in_DiscRate == '' )
      $in_DiscRate = 0;

    $this->DTO_Order->Ord_DiscRate = $in_DiscRate;
  }

  public function UpdateOrderDiscAmount($in_DiscAmt)
  {
    if( $in_DiscAmt < 0 || $in_DiscAmt == '' )
      $in_DiscAmt = 0;

    $this->DTO_Order->Ord_DiscAmt = $in_DiscAmt;
    $this->DTO_Order->Ord_DiscRate =  number_format(($this->DTO_Order->Ord_DiscAmt / $this->DTO_Order->Ord_Total)*100,0);
  }

  public function GetOrderPayTypeDesc()
  {
    return $this->DTO_Order->Ord_PayTypeDesc;
  }


  public function UpdateOrderPayType($in_PayType)
  {
    $PayTypeDesc="";
    if( $in_PayType == 1 )
    {
      $PayTypeDesc = "Cash";

    }
    else if( $in_PayType == 2 )
    {
      $PayTypeDesc = "CC";
    }
    else if( $in_PayType == 3 )
    {
      $PayTypeDesc = "Chq";
    }
    else if( $in_PayType == 4 )
    {
      $PayTypeDesc = "Acc";
    }

    $PayTypeCode = $this->DalTill->PaymentTypeExists($PayTypeDesc);

    if( $PayTypeCode >= 0 )
    {
      $this->DTO_Order->Ord_PayType = $PayTypeCode;
      $this->DTO_Order->Ord_PayTypeDesc = $PayTypeDesc;
    }
    else
    {
      $this->DTO_Order->Ord_PayType = 1;
      $this->DTO_Order->Ord_PayTypeDesc = 'Cash';
    }
  }



  public function UpdateItemSplitRef($in_SplitID, $in_SplitRef, $in_CheckID)
  {
    $ItemCount = count($this->OrderItemsList);
    for( $i=0; $i < $ItemCount; $i++)
    {
      if( $in_CheckID )
      {
        if( $this->OrderItemsList[$i]->GetProdSplitBillID() == $in_SplitID )
        {
          $this->OrderItemsList[$i]->SetProdSplitBillRef($in_SplitRef);
        }
      }
      else
      {
        $this->OrderItemsList[$i]->SetProdSplitBillRef($in_SplitRef);
        $this->OrderItemsList[$i]->SetProdSplitBillID($in_SplitID);
      }
    }
  }

  public function SetItemSplitID($in_SplitID, $in_SplitRef, $in_ItemID)
  {
    $ItemCount = count($this->OrderItemsList);
    for( $i=0; $i < $ItemCount; $i++)
    {
      if( $this->OrderItemsList[$i]->GetProdID() == $in_ItemID )
      {
        $this->OrderItemsList[$i]->SetProdSplitBillRef($in_SplitRef);
        $this->OrderItemsList[$i]->SetProdSplitBillID($in_SplitID);
      }
    }
    $this->OrderNeedsSaving = 1;
  }

  public function DecreaseItemSplitQty($in_ItemID)
  {
    $ItemCount = count($this->OrderItemsList);
    for( $i=0; $i < $ItemCount; $i++)
    {
      if( $this->OrderItemsList[$i]->GetProdID() == $in_ItemID )
      {
        $this->OrderNeedsSaving = 1;
        return $this->OrderItemsList[$i]->DecreaseProdSplitQty();
      }
    }

    return false;
  }

  public function IncreaseItemSplitQty($in_ItemID, $in_UpdatePrintedQty)
  {
    $ItemCount = count($this->OrderItemsList);
    for( $i=0; $i < $ItemCount; $i++)
    {
      if( $this->OrderItemsList[$i]->GetProdID() == $in_ItemID )
      {
        $this->OrderNeedsSaving = 1;
        $this->OrderItemsList[$i]->IncreaseProdSplitQty($in_UpdatePrintedQty);
        break;
      }
    }
  }


  public function UpdateTableCovers($in_Covers)
  {
    $this->DTO_Order->Ord_TableCovers = $in_Covers;
  }

  public function GetOrderTableCovers()
  {
    return $this->DTO_Order->Ord_TableCovers;
  }

  public function UpdateOrderItem($in_ItemID,$in_ItemQty,$in_ItemPrice, $in_ItemFreeText)
  {
    $ItemCount = count($this->OrderItemsList);

    for( $i=0; $i < $ItemCount; $i++)
    {
      if( $this->OrderItemsList[$i]->GetProdID() == $in_ItemID )
      {
        $this->OrderItemsList[$i]->UpdateItem($in_ItemQty,$in_ItemPrice,$in_ItemFreeText);
        $this->OrderNeedsSaving = 1;
        break;
      }
    }
  }

  public function ItemSearch($in_ProdCode, $in_ProdPrice)
  {
    $out_ItemID = 0;
    $ItemCount = count($this->OrderItemsList);

    for( $i=0; $i < $ItemCount; $i++)
    {
      if( $this->OrderItemsList[$i]->GetProdCode() == $in_ProdCode && $this->OrderItemsList[$i]->GetProdPrice() == $in_ProdPrice )
      {
        //if( !$this->OrderItemsList[$i]->ToppingsChangedFlag() )
        //{
          $out_ItemID = $this->OrderItemsList[$i]->GetProdID();
        //}
        break;
      }
    }
    return $out_ItemID;
  }

  public function ItemWithOptionsSearch($in_ProdCode, $in_ProdPrice, $in_TopBut1ID, $in_TopBut2ID)
  {
    $out_ItemID = 0;
    $ItemCount = count($this->OrderItemsList);

    for( $i=0; $i < $ItemCount; $i++)
    {
      if( $this->OrderItemsList[$i]->GetProdCode() == $in_ProdCode && $this->OrderItemsList[$i]->GetProdPrice() == $in_ProdPrice )
      {
        if( $in_TopBut1ID > 0 && $this->OrderItemsList[$i]->GetProdTopBut1() == $in_TopBut1ID )
        {
          $ItemFound = true;
          if( $in_TopBut2ID > 0  )
          {
            if( $this->OrderItemsList[$i]->GetProdTopBut2() == $in_TopBut2ID )
            {
              $ItemFound = true;
            }
            else
            {
              $ItemFound = false;
            }
          }
          if( $ItemFound )
          {
            //if( !$this->OrderItemsList[$i]->ToppingsChangedFlag() )
            //{
              $out_ItemID = $this->OrderItemsList[$i]->GetProdID();
            //}
          }
        }
        break;
      }
    }
    return $out_ItemID;
  }

  public function IncreaseProdQty($in_ItemID)
  {
    $ItemCount = count($this->OrderItemsList);

    for( $i=0; $i < $ItemCount; $i++)
    {
      if( $this->OrderItemsList[$i]->GetProdID() == $in_ItemID )
      {
        $this->OrderItemsList[$i]->IncreaseProdQty();
        $this->OrderNeedsSaving = 1;
        break;
      }
    }
  }

  public function DeleteOrderItem($in_ItemID)
  {
    $ItemCount = count($this->OrderItemsList);

    for( $i=0; $i < $ItemCount; $i++)
    {
      if( $this->OrderItemsList[$i]->GetProdID() == $in_ItemID )
      {
        unset($this->OrderItemsList[$i]);
        $this->OrderItemsList = array_values($this->OrderItemsList);
        $this->OrderNeedsSaving = 1;
        break;
      }
    }
  }

  public function GetOrderItemDetails($in_ItemID)
  {
    $ItemCount = count($this->OrderItemsList);

    for( $i=0; $i < $ItemCount; $i++ )
    {
      //print "GetOrderItemDetails:  prod id: " . $this->OrderItemsList[$i]->GetProdID() . "    itemid: " . $in_ItemID . "<br />";
      if( $this->OrderItemsList[$i]->GetProdID() == $in_ItemID )
      {
        return $this->OrderItemsList[$i];
      }
    }
    return null;
  }

  public function OrderNeedsSavingFlag()
  {
    //print "OrderNeedsSavingFlag";
    return $this->OrderNeedsSaving;
  }

  // Ray 23/03/12. Added
  public function UpdatePrintedQty()
  {
    $SplitBill = false;
    if( count($this->SplitBillList) > 0 )
    {
      $SplitBill = true;
    }

    $ItemCount = count($this->OrderItemsList);

    for( $i=0; $i < $ItemCount; $i++ )
    {
      // TODO Check if split bill
      if( $this->OrderItemsList[$i]->GetProdSplitQty() > 0 )
      {
        if( $SplitBill )
        {
          $this->OrderItemsList[$i]->SetProdPrintedQty( $this->OrderItemsList[$i]->GetProdSplitQty() );
          $this->OrderItemsList[$i]->SetProdSplitPrintedQty( $this->OrderItemsList[$i]->GetProdQty()-$this->OrderItemsList[$i]->GetProdSplitQty() );
        }
        else
        {
          $this->OrderItemsList[$i]->SetProdPrintedQty( $this->OrderItemsList[$i]->GetProdQty() );
        }
        $this->OrderNeedsSaving = 1;
      }
    }
    return null;
  }

  // Ray 23/03/12. Added
  public function UpdateSplitPrintedQty()
  {
    $SplitBill = false;
    if( count($this->SplitBillList) > 0 )
    {
      $SplitBill = true;
    }

    if( $SplitBill )
    {
      $ItemCount = count($this->OrderItemsList);

      for( $i=0; $i < $ItemCount; $i++ )
      {
        if( $this->OrderItemsList[$i]->GetProdSplitQty() == 0 )
        {
          $this->OrderItemsList[$i]->SetProdSplitPrintedQty( $this->OrderItemsList[$i]->GetProdQty()-$this->OrderItemsList[$i]->GetProdPrintedQty() );
        }
      }
      $this->OrderNeedsSaving = 1;
    }
    return null;
  }

  // Ray 10/04/12. Added
  public function ResetMarkToPrint()
  {
    $ItemCount = count($this->OrderItemsList);

    for( $i=0; $i < $ItemCount; $i++ )
    {
        $this->OrderItemsList[$i]->SetProdMarkToPrint(0);
        $this->OrderNeedsSaving = 1;
    }

  }



}


?>
