<?php

require_once('Till_datatypes.php');

class Till_SplitBill
{
  private $SplitCount;
  private $CurSplitID;
  private $SplitRefList = array();
  private $SplitItemList = array();
    
  function __construct() 
  {
    $this->CurSplitID = 0;   
    $this->SplitCount = 0;     
  }  
    
  public function ClearSplitBill()
  {
    $this->SplitCount = 0;
    $this->CurSplitID = 0;      
    unset($this->SplitRefList); 
    unset($this->SplitItemList); 
  }  
  
  public function NewSplitBill()
  {
    $this->ClearSplitBill();
    $this->AddSplit();
  }  
  
  public function LoadSplitBill($in_SplitBillList)
  {
    $this->ClearSplitBill();
    
    $this->SplitItemList = $in_SplitBillList;

    $SplitCount = 0;
    $SplitListCount = count($in_SplitBillList);
    for( $i=0; $i<$SplitListCount; $i++ )
    {
      if( $in_SplitBillList[$i]->SplitID > $SplitCount )
      {
        $SplitCount = $in_SplitBillList[$i]->SplitID;
        
        $SplitRefListCount = count($this->SplitRefList);
        while( $SplitRefListCount < $SplitCount )
        {
          $this->AddSplit();
          $SplitRefListCount = count($this->SplitRefList);
        }
        $this->SplitRefList[$i] = $in_SplitBillList[$i]->SplitRef;         
        //$SplitCount++;         
      }
    }

    $this->CurSplitID = 1;
        
    /*
    $SplitCount = count($in_SplitBillList);
    for( $i=0; $i<$SplitCount; $i++ )
    {
      $this->AddSplit();
      $this->SplitRefList[$i] = $in_SplitBillList[$i]->But_Data[1];    
    }
    $this->CurSplitID = 1;
    */
  }  
  

  public function AddSplit()
  {  
    $this->SplitCount++;
    $this->CurSplitID = $this->SplitCount;
    $this->SplitRefList[] = "Split " . $this->CurSplitID;
  } 
  
  public function NextSplit()
  {
    if( $this->CurSplitID == $this->SplitCount )
    {
        $this->AddSplit();
    }
    else
    {
        $this->CurSplitID++;
    }
  } 
  
  public function PrevSplit()
  {
    if( $this->CurSplitID > 1 )
    {
        $this->CurSplitID--;   
    }    
  }   
  
  public function GetCurSplitBillRef()
  {
    if( $this->SplitCount > 0  )
    {
        return $this->SplitRefList[$this->CurSplitID-1];
    }

    return ""; 
  }  
  
  public function GetCurSplitBillCount()
  { 
    return $this->SplitCount;
  } 
  
  public function GetCurSplitBillID()
  { 
    return $this->CurSplitID;
  }
  
  public function UpdateCurSplitBillRef($in_SplitDesc)
  { 
    $this->SplitRefList[$this->CurSplitID-1] = $in_SplitDesc;
  }

  public function AddSplitItem($in_ItemID,$in_UpdatePrintQty)
  { 
    $AddNewSplitItem=true;
    
    if( $this->GetCurSplitBillCount() == 0 )
    {
      $this->AddSplit(); 
    }
    
    $ItemCount = count($this->SplitItemList);
    for( $i=0; $i < $ItemCount; $i++ )
    {
      if( $this->SplitItemList[$i]->SplitID == $this->CurSplitID && $this->SplitItemList[$i]->ItemID == $in_ItemID )
      {
            $this->SplitItemList[$i]->ItemQty += 1;
            if( in_UpdatePrintQty )
                $this->SplitItemList[$i]->ItemPrintedQty += 1; 
                
            $AddNewSplitItem = false;      
      }
    }
    
    if( $AddNewSplitItem )
    {
        $NewSplitItem = new SplitItem();     

        $NewSplitItem->SplitID = $this->CurSplitID;
        $NewSplitItem->SplitRef = $this->GetCurSplitBillRef(); 
        $NewSplitItem->ItemID = $in_ItemID;
        $NewSplitItem->ItemQty = 1;
        $NewSplitItem->ItemPrintedQty = 0;
        if( $in_UpdatePrintQty )
            $NewSplitItem->ItemPrintedQty = 1;

        $this->SplitItemList[] = $NewSplitItem;  
    }    

  }  
  
  public function RemoveSplitItem($in_ItemID)
  { 
    $UpdatePrintedQty=false;
    //print " RemoveSplitItem " . $in_ItemID . "<br />";
    $ItemCount = count($this->SplitItemList);
    for( $i=0; $i < $ItemCount; $i++ )
    {
      //print $this->SplitItemList[$i]->SplitID . " " . $this->CurSplitID . "<br />";
     // print "split itemid: " . $this->SplitItemList[$i]->ItemID . " in_itemid " . $in_ItemID . "<br />";
      if( $this->SplitItemList[$i]->SplitID == $this->CurSplitID && $this->SplitItemList[$i]->ItemID == $in_ItemID )
      {
            
            if( $this->SplitItemList[$i]->ItemPrintedQty > $this->SplitItemList[$i]->ItemQty-1 )
            {
                $this->SplitItemList[$i]->ItemPrintedQty -= 1; 
                $UpdatePrintedQty=true;
            }  
            if( $this->SplitItemList[$i]->ItemQty == 1 )
            {               
              unset($this->SplitItemList[$i]);
              $this->SplitItemList = array_values($this->SplitItemList);
            }
            else
            {
              $this->SplitItemList[$i]->ItemQty -= 1;
            } 
  
            break;    
      }
    }  
    return $UpdatePrintedQty;
  }
  public function DeleteSplitItem($in_ItemID)
  { 
    $ItemCount = count($this->SplitItemList);
    for( $i=0; $i < $ItemCount; $i++ )
    {  
      if( $this->SplitItemList[$i]->ItemID == $in_ItemID )
      {
        unset($this->SplitItemList[$i]);  
        $this->SplitItemList = array_values($this->SplitItemList);
      }
    }  
  }  
  
  public function GetSplitItemsList()
  { 
    $CurSplitItems = array();
    
    $ItemCount = count($this->SplitItemList);
    for( $i=0; $i < $ItemCount; $i++ )
    {
      if( $this->SplitItemList[$i]->SplitID == $this->CurSplitID )
      {
        $CurSplitItems[] = $this->SplitItemList[$i];
      }            
    }     
    return $CurSplitItems;  
  }
  

  public function GetAllSplitItemsList()
  { 
    return $this->SplitItemList;  
  }

  public function UpdateSplitPrintedQty()
  { 
    $ItemCount = count($this->SplitItemList);
    for( $i=0; $i < $ItemCount; $i++ )
    {
      $this->SplitItemList[$i]->ItemPrintedQty = $this->SplitItemList[$i]->ItemQty; 
    }
  }  
  

}
?>
