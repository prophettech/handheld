<?php                     
//error_reporting(0);

  class TillDataPacket
  {
    public $But_Data = array();
  }
  
  class ButtonData
  {
    public $But_ID;
    //public $But_Type;
    public $But_Text1;
    /*
    public $But_Text2;
    public $But_Text3;
    public $But_Text4;
    public $But_Text5;
    public $But_Text6;
    public $But_Text7;
    public $But_Text8;
    public $But_Display_Type;
    public $But_Function;
    public $But_Colour_Default;
    public $But_FontColour_Default;    
    public $But_Colour_Selected;
    public $But_Image_Path;    
    public $But_Offer;       
    public $But_SectionID;  
    */
    
             
    
  }
  
class Till_ProdTopping
{
  private $TopID;
  private $TopCode;
  private $TopDesc;
  private $TopType; // 1: Fixed, 2: Fixed minus, 3: Extra
  private $TopPriceType; 
  private $TopPrice;    // Fixed price
  private $TopReceiptID;
  
  function __construct($in_TopID,$in_TopCode,$in_TopDesc, $in_TopPriceType, $in_TopPrice, $in_TopReceiptID, $in_TopType=1) 
  {
    $this->TopID = $in_TopID;   
    $this->TopCode = $in_TopCode;   
    $this->TopDesc = $in_TopDesc;    
    $this->TopType = $in_TopType;  
    if( $this->TopType == null )
      $this->TopType = 0;
      
    $this->TopPriceType = $in_TopPriceType; 
    $this->TopPrice = $in_TopPrice;
    $this->TopReceiptID = $in_TopReceiptID; 
  }  
  
  public function GetTopID()
  {
    return $this->TopID;  
  }  
  
  public function GetTopCode()
  {
    return $this->TopCode;
  }
  
  public function GetTopDesc()
  {
    return $this->TopDesc;
  }
  
  public function GetTopType()
  {
    return $this->TopType;
  }  
  
  public function SetTopType($in_TopType)
  {  
    $this->TopType = $in_TopType;
  }   
  
  public function GetTopPriceType()
  {  
    return $this->TopPriceType;
  }  
  
  public function GetTopPrice()
  {  
    return $this->TopPrice;
  }    

  public function GetTopReceiptID()
  {
    return $this->TopReceiptID;
  }   
}

class Till_ProdIns
{
  private $InsID;
  private $InsDesc;
  
  function __construct($in_InsID,$in_InsDesc) 
  {
    $this->InsID = $in_InsID;   
    $this->InsDesc = $in_InsDesc; 
  }    
}




  class OrderItemData
  {
    public $ItemCode;
    public $ItemDesc;
    public $ItemQty;
    public $ItemSplitQty;         // Ray 22/03/12. Added
    public $ItemPrintedQty;       // Ray 22/03/12. Added
    public $ItemSplitPrintedQty;  // Ray 22/03/12. Added
    public $ItemUnitPrice;
    public $ItemPriceHH;

    public $ItemCostPrice;
    public $ItemVatRate; 
    public $ItemVatableAmount;    
    public $ItemVatAmount;
    public $ItemNET;
    public $ItemTopPrice;    
    
    public $ReceiptID;
    public $ItemMasterID;
    public $ItemMealDealID;  
    public $ItemProdType;
    public $ItemProdClass;

    public $ItemValid;             
    public $ItemPLUNo;
    public $ItemTable; 
    public $ItemToppingType;
    public $ItemExtraToppingPrice;
    public $ItemTopBut1;
    public $ItemTopBut2;
    public $ItemTopBut3;
    public $ItemSplitBillID;
    public $ItemSplitBillRef; 

    public $ItemOffer; 
        
    public $ItemPlusToppings = array();
    public $ItemMinusToppings = array();
     
    public $ItemPrintToKitchen;      
    public $ItemFreeText;   // Ray 04/04/12. Added
    public $ItemMarkToPrint;   // Ray 10/04/12. Added    
    public $ItemSectionPos;     // Ray 30/10/12. Added        
    public $ItemSectionDesc;     // Ray 30/10/12. Added         
  }  
  
  class OrderData
  {
    public $Ord_ID;
    public $Ord_No;
    public $Ord_TableID;
    public $Ord_TableNo;
    public $Ord_Date;
    public $Ord_Total;    
    public $Ord_Balance;  
    public $Ord_PaidAmt;   
    public $Ord_Prepayment;   
    public $Ord_AccRef;
    public $Ord_Time;
    public $Ord_Net;
    public $Ord_Vat;
    public $Ord_Delivery;   
    public $Ord_VATCode;
    public $Ord_DiscAmt;    
    public $Ord_DiscRate;   
    public $Ord_TableCovers;
    public $Ord_DeliveryType; 
    public $Ord_Status;     
    public $Ord_PayType;   
    public $Ord_PayTypeDesc;      
    public $Ord_LastTransID;    
    public $Ord_SplitItemsList;   // Ray 23/03/12. Added
    public $Ord_UpdatePrintedQty; // Ray 27/03/12. Added
         
    public $Ord_ItemsList = array();       
    
  
  }
  
class DisplayOrderItem
{
  public $ItemID;
  public $ItemDescMain;
  public $ItemDesc;
  public $ItemTopDesc;
  public $ItemPrice;
  public $ItemQty;
  public $ItemNET;
  public $ItemTopPrice;     
  public $ItemPos;  
  public $SplitBillID;     
  public $ItemFreeText;
}  
  
class SplitItem
{       
  public $SplitID;
  public $ItemID;
  public $ItemQty;
  public $ItemPrintedQty;
  public $SplitRef;
};

class ReportItem
{       
  public $rpt_ID;
  public $rpt_F1;
  public $rpt_F2;
  public $rpt_F3;
  public $rpt_F4;
  public $rpt_F5;
  public $rpt_F6;
  public $rpt_F7;
  public $rpt_F8;
  public $rpt_F10;
  public $rpt_F11;
  public $rpt_F14;
  public $rpt_F16;  
};

?>
