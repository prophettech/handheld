<?php
require_once('Till_datatypes.php');

class Till_ProductOffer
{
  private $OfferCode; 
  private $OfferDesc;  
  private $OfferPrice;    
  private $ItemList = array();
  
       
  public function SetOfferDetails($in_OfferCode, $in_OfferDesc, $in_OfferPrice)
  {
    $this->OfferCode = $in_OfferCode;
    $this->OfferDesc = $in_OfferDesc;    
    $this->OfferPrice = $in_OfferPrice;        
  }
  
  public function GetOfferCode()
  {   
    return $this->OfferCode; 
  }  
  
  public function GetOfferDesc()
  {   
    return $this->OfferDesc; 
  }
  
  public function GetOfferPrice()
  {   
    return $this->OfferPrice; 
  }
  
  public function GetOfferTopPrice()
  {   
    $TopPrice = 0;
    $ItemCount = count($this->ItemList);  
    for( $i=0; $i<$ItemCount; $i++ )
    {
      $TopPrice += $this->ItemList[$i]->GetButtonSelectedExtraToppingPrice();
    }
    return $TopPrice; 
  }  
  
  public function SetOfferItemButton($in_ItemButton)
  {   
    $this->ItemList[] = $in_ItemButton; 
  }
  
  public function GetOfferItems()
  {   
    return $this->ItemList; 
  }
  
  public function SetOfferItem($in_ItemID, $in_ProdCode, $in_ProdDesc, $in_SubItemID, $in_Toppings)
  {   
    // Find the item
    $ItemCount = count($this->ItemList);  
    for( $i=0; $i<$ItemCount; $i++ )
    {
      if( $in_ItemID == $this->ItemList[$i]->GetButtonID() )
      {
        $this->ItemList[$i]->SetSelectedProduct($in_ProdCode, $in_ProdDesc, $in_SubItemID, $in_Toppings,0);  
        break;
      }
    }
  }  
  
  public function SetOfferItemID($in_ItemID, $in_SubItemID)
  {  
    // Find the item
    $ItemCount = count($this->ItemList);  
    for( $i=0; $i<$ItemCount; $i++ )
    {
      if( $in_ItemID == $this->ItemList[$i]->GetButtonID() )
      {
        $this->ItemList[$i]->SetSelectedOrItem($in_SubItemID);  
        break;
      }
    }
  }
  
  public function IsOfferValid()
  {  
    $ValidOffer = true;
    $ItemCount = count($this->ItemList);  
    for( $i=0; $i<$ItemCount; $i++ )
    {
      if( !$this->ItemList[$i]->IsValidItem() )
      {
        $ValidOffer = false;        
        break;
      }
    }  
    return $ValidOffer;
  }  
  
}

class Till_ProductOfferButton
{
  private $ButID;
  private $ButText;
  private $ButType;
  private $ButCatID;  
  private $ButList = array();
  private $ButSelectedProdID;
  private $ButSelectedProdCode;
  private $ButSelectedProdDesc;
  private $ButSelectedProdExtraToppingPrice;
  //private $ButSelectedToppings = array();
  private $ButSelectedPlusToppings = array();
  private $ButSelectedMinusToppings = array();
  private $ValidItem;
  //private $ButSelectedProdToppings;   // TODO
    
  public function SetButtonDetails($in_ButType, $in_ButList)
  {
    $this->ButType = $in_ButType;
    if( $in_ButType == 4 )
    {
      $this->ButID = $in_ButList[0]->But_Data[1];    
      $this->ButText = "";    
      $this->ButList = $in_ButList;   
    }
    else
    {
      $this->ButID = $in_ButList[0]->But_Data[1];    
      $this->ButText = $in_ButList[0]->But_Data[2];
      $this->ButCatID = $in_ButList[0]->But_Data[6];    
      $this->ButList = null;         
    }  
    $this->ButSelectedProdID = 0;
    $this->ButSelectedProdCode = "";
    $this->ButSelectedProdDesc = "";
    $this->ButSelectedProdExtraToppingPrice = 0;
       
  } 
  
  public function GetButtonID()
  {
    return $this->ButID;
  }  
  
  public function GetButtonText()
  {  
    return $this->ButText;   
  }   
  
  public function GetButtonSelectedID()
  { 
    return $this->ButSelectedProdID;     
  }  
    
  public function GetButtonSelectedText()
  { 
    return $this->ButSelectedProdDesc;     
  }   
  
  public function GetButtonSelectedCode()
  { 
    return $this->ButSelectedProdCode;     
  }      
  
  public function GetButtonSelectedExtraToppingPrice()
  { 
    return $this->ButSelectedProdExtraToppingPrice;     
  }      

  public function GetButtonSelectedPlusToppings()
  { 
    return $this->ButSelectedPlusToppings;     
  }     
  
  public function GetButtonSelectedMinusToppings()
  { 
    return $this->ButSelectedMinusToppings;     
  }     

  public function GetButtonType()
  {
    return $this->ButType;
  }  
  
  public function GetButtonList()
  {
    return $this->ButList;
  }    
  
  public function GetButtonCatID()
  {
    return $this->ButCatID;
  }    
 
    
  public function SetSelectedProduct($in_ProdCode, $in_ProdDesc, $in_ProdID, $in_ProdToppings, $in_LoadingOrder)
  {
    $this->ButSelectedProdID = $in_ProdID;
    $this->ButSelectedProdCode = $in_ProdCode;
    $this->ButSelectedProdDesc = $in_ProdDesc; 
    $this->ValidItem = true; 
    unset($this->ButSelectedPlusToppings);
    unset($this->ButSelectedMinusToppings);
        
      $TopCount = count($in_ProdToppings);

      for( $i = 0; $i < $TopCount; $i++ ) 
      {        
        $TopID = $in_ProdToppings[$i]->GetTopID();                                                         
        $TopCode = $in_ProdToppings[$i]->GetTopCode();                                                         
        $TopDesc = $in_ProdToppings[$i]->GetTopDesc();                                                         
        $TopType = $in_ProdToppings[$i]->GetTopType();     
        $TopPriceType = $in_ProdToppings[$i]->GetTopPriceType();     
        $TopPrice = $in_ProdToppings[$i]->GetTopPrice(); 
        $this->ButSelectedProdExtraToppingPrice += $TopPrice;  
        if( $in_LoadingOrder == 0 && $TopType == 3 )
        {
          $TopDesc = "+" . $TopDesc;
        }                                               
        $NewProdTopping = new Till_ProdTopping($TopID,$TopCode,$TopDesc,$TopPriceType,$TopPrice,$TopReceiptID,$TopType);
        if( $TopType == 2 )
        {
          $this->ButSelectedMinusToppings[] = $NewProdTopping;                
        }
        else  
        {
          $this->ButSelectedPlusToppings[] = $NewProdTopping;               
        }
      }   
  }  
  
  public function SetSelectedOrItem($in_SubItemCode)
  {

    $ItemCount = count($this->ButList);  
    for( $i=0; $i<$ItemCount; $i++ )
    {
      if( $in_SubItemCode == $this->ButList[$i]->But_Data[0] )
      {
        
        $this->ButSelectedProdID = $in_SubItemCode;
        $this->ButSelectedProdCode = $this->ButList[$i]->But_Data[5];
        $this->ButSelectedProdDesc = $this->ButList[$i]->But_Data[2];
        $this->ValidItem = true;         
        break;
      }
    }
  }  
  
  public function IsValidItem()
  {
    return $this->ValidItem;
  }   
    
    

}
?>
