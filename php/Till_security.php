<?php
//error_reporting(0);
require_once('DAL_ProphetTill.php');

class Till_Security 
{
  private $DalTill;  
  private $ErrorMsg;
  private $CurUserNum;
  private $CurSessionID;  
  
  function __construct() 
  {
    $this->DalTill = new DAL_ProphetTill(); 
  }
  
  public function GetErrorMsg()
  {   
    return $this->ErrorMsg;
  }   
  
  public function GetUserNum()
  {   
    return $this->CurUserNum;
  }  
  
  public function GetUserSessionID()
  {                                 
      return $this->CurSessionID;
  }  
  
  public function GetUserSessionIDFromDB()
  {  
    return $this->DalTill->GetUserSessionID($this->CurUserNum);
  }        
    
  public function ValidateUser($in_Passcode)
  {
    $UserLoggedIn = false;
    $UserSessionID = 0;
    $UserNum = $this->DalTill->ValidateUser($in_Passcode, $UserLoggedIn, $UserSessionID);  
   
    $ValidUser = 0; 
//
    if( $UserNum > 0 )
    {    
      if( $UserLoggedIn )
      {
        $ValidUser = -1;  
      }   
      else
      {
        $ValidUser = 1;           
      }
      $this->CurUserNum = $UserNum; 
    } 
    else
    {
      $this->ErrorMsg = "Invalid Passcode";
    }
    
    return $ValidUser;
  }
  
  public function ProcessLogin($in_UserAlreadyLoggedIn)
  {
    $ValidUser = false;
    $this->CurSessionID = 0; 

    if( $this->CheckUserCount($in_UserAlreadyLoggedIn) )
    {
      $SessionID = $this->DalTill->GetNextUserSessionID();
      if( $this->DalTill->SetLoggedInUser($this->CurUserNum,$SessionID) )
      {
          $ValidUser = true;    
          //$_SESSION['TillLoggedIn'] = $this->CurUserNum;  
          $this->CurSessionID = $SessionID;     

      }  
      else
      {
        $this->ErrorMsg = "Login could not be completed";
      }
    }
    else
    {
      // Max users exceeded
      $this->ErrorMsg = "The maximum number of users are logged in";
    }
    return $ValidUser;       
  }

  public function CheckUserCount($in_UserAlreadyLoggedIn)
  {
    $MaxUsers = $this->DalTill->GetMaxUsers();  
    $LoggedInUsers = $this->DalTill->GetLoggedInUsers();  

    if( $in_UserAlreadyLoggedIn && $MaxUsers > 0 )
      $MaxUsers += 1;        

    if( $MaxUsers > 0 && ($LoggedInUsers+1 > $MaxUsers) )
    {
      return false;
    }
    else
    {    
    }
    
    return true;  
  }


  public function LogoutUser($in_UserNo, $in_SessionID)
  {    
    if( $in_UserNo > 0 )
    {
      if( $this->DalTill->SetLoggedOutUser($in_UserNo,$in_SessionID) )
      { 
        return true;
      }
      $this->CurUserNum = 0;  
      return false;    
    }
    else
    {
      return true;
    }            
  /*
  
    if( $this->CurUserNum > 0 )
    {
      if( $this->DalTill->SetLoggedOutUser($this->CurUserNum,$this->CurSessionID) )
      { 
        return true;
      }
      $this->CurUserNum = 0;  
   
      //$_SESSION['TillLoggedIn'] = 0;  
      //$_SESSION['TillKeypadCode'] = "";  
      return false;    
    }
    else
    {
      return true;
    }
    */
  }
}
?>
