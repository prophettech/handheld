<?php
  error_reporting(0);
  require_once('DAL_ProphetTill.php');

  $TillDB = new DAL_ProphetTill();
 
  $response = array();
  $response["success"] = 1;  
  $response["paytypes"] = array();
  
  $PayTypesData = $TillDB->GetPayTypesList();  
  $PayTypesCount = count($PayTypesData);
  if( $PayTypesCount > 0 )
  {      
    $response["paytypes"] = array();             
    for($j=0; $j < $PayTypesCount; $j++ )
    {
      array_push($response["paytypes"], $PayTypesData[$j]);   
    }   
  }
  
  $VATCodesData = $TillDB->GetVATCodesList();  
  $VATCodesCount = count($VATCodesData);  
  if( $VATCodesCount > 0 )
  {      
    $response["vatcodes"] = array();             
    for($j=0; $j < $VATCodesCount; $j++ )
    {
      array_push($response["vatcodes"], $VATCodesData[$j]);   
    }   
  }
 
  $jsonStr = json_encode($response);  
  echo gzencode($jsonStr);
  //echo json_encode($response); 
  
  

?>