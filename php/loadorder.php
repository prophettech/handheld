<?php
//error_reporting(0);
require_once('DAL_ProphetTill.php');
              
$response = array();  
$response["success"] = 0;
$response["message"] = "";  

if (isset($_GET['orderID']))
{ 
  $orderID = $_GET['orderID']; 

  $TillDB = new DAL_ProphetTill();
  
  $OrderDetails = $TillDB->GetOrderDetails($orderID);  
  $orderCount = count($OrderDetails);

  if( $orderCount > 0 )
  {      

      
    $response["orderData"] = array();            
    for($i=0; $i < $orderCount; $i++ )
    {      
      array_push($response["orderData"], $OrderDetails[$i]);   
    }   
    
    // Get Order Items
    $response["orderItemData"] = array();
    $OrderItems = $TillDB->GetOrderItemDetails($orderID);
    $ItemCount = count($OrderItems);
  
    for($i=0; $i < $ItemCount; $i++ )
    {      
      array_push($response["orderItemData"], $OrderItems[$i]);   
    }   
    
    // Get Split Bill Items
    $response["splitItemData"] = array();
    $SplitItems = $TillDB->GetOrderSplitBillItems($orderID);
    $ItemCount = count($SplitItems);
  
    for($i=0; $i < $ItemCount; $i++ )
    {      
      array_push($response["splitItemData"], $SplitItems[$i]);   
    }   
    
    // Get Split Pay Items
    $response["splitPayData"] = array();
    $SplitPay = $TillDB->GetOrderSplitPayItems($orderID);
    $ItemCount = count($SplitPay);
  
    for($i=0; $i < $ItemCount; $i++ )
    {      
      array_push($response["splitPayData"], $SplitPay[$i]);   
    }       
    
    
    $response["success"] = 1;
  }
  else
  {
    $response["message"] = "Order not found";         
  }   
  $jsonStr = json_encode($response);  
  echo gzencode($jsonStr);
  //echo json_encode($response); 
}
  
  

?>