<?php
require_once('Till_SplitBill.php');
require_once('Till_datatypes.php');
require_once('Till_Order.php');
require_once('Till_printmanager.php');
require_once('Till_Options.php');


$rptID = 0;
$UserOrder = null;
$DisplayError = false;

$response = array();
$response["success"] = 0;
$response["message"] = "";

if (isset($_GET['TillOrderID']))
{
  $orderID = $_GET['TillOrderID'];
  $PrintOrderType = $_GET['TillPrintOrderType'];
  $SplitBill = $_GET['TillSplitBill'];

  $mOptions = new Till_Options();
  $mOptions->LoadOptions();

  $UserOrder = new Till_Order($mOptions);

  if( $UserOrder )
  {
    $UserOrder->LoadOrder($orderID);
    $TillPrinterObj = new Till_PrintManager();
    $getprt = $TillPrinterObj->getPrinter();
    $printers = serialize($getprt);
    $printers=unserialize($printers);

    foreach ($printers as $PrintDest) {

      if ($PrintDest["NAME"] == "Till Printer" || $PrintDest["NAME"] == "Kitchen Printer"){
      $rptID = $UserOrder->PrepareOrderForPrinting($PrintOrderType, $PrintDest["NAME"]);
      $PrintSections = $UserOrder->GetPrintSectionsFlag();
      $ptr = serialize($PrintDest);
      $PrintDest = unserialize($ptr);
      if($rptID > 0)
      {
        $TillPrinterObj = new Till_PrintManager();
        $TillPrinterObj = $TillPrinterObj->PrintOrder($UserOrder->GetOrderID(),$rptID,$SplitBill,$PrintOrderType,$PrintSections, $PrintDest["NAME"]);
      }else{
        $DisplayError = true;
      }
      }
    }

    //$rptID = $UserOrder->PrepareOrderForPrinting($PrintOrderType, $printerName);
    /*$PrintSections = $UserOrder->GetPrintSectionsFlag();

    if( $rptID > 0 )
    {
      $TillPrinterObj = $TillPrinterObj->PrintOrder( $UserOrder->GetOrderID(),$rptID,$SplitBill,$PrintOrderType,$PrintSections );

      /*
      if( $PrintOrderType == 1 )
      {
        $UserOrder->ResetMarkToPrint();
      }
      */
    /*}
    else
    {
      $DisplayError = true;
    }*/

    if( $DisplayError )
    {
        $OrderErrorMsg = $UserOrder->GetErrorMsg();
        if( $rptID == -1 )
        {
          //$CurOptions = $_SESSION['TillOptionsObj'];
          $PrintOnlyNewItems = $mOptions->GetOption_PrintOnlyNewItems();
          if( $PrintOnlyNewItems == 1 )
          {
            $OrderErrorMsg = "Order could not be printed. No new items have been added.";
          }
          else
          {
            $OrderErrorMsg = "ERROR: Order could not be printed.";
          }
        }
        if ($rptID == 0) {
          $response["success"] = 1;
          $OrderErrorMsg = "No Items for the other printer";
        }
        $response["message"] = $OrderErrorMsg;
    }
    else
    {
      $response["success"] = 1;
    }
  }
}

  $jsonStr = json_encode($response);
  //echo gzencode($jsonStr);
  echo json_encode($response);
?>
