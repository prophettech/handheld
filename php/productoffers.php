<?php
  error_reporting(0);
  require_once('DAL_ProphetTill.php');

  $TillDB = new DAL_ProphetTill();

  $MDData = $TillDB->GetProductOfferList();  
  $MDCount = count($MDData);

  
  $response = array();
  $response["success"] = 0;  
  $response["message"] = "";    
  
  if( $MDCount > 0 )
  {      
    $response["offerlist"] = array();
          
    for($j=0; $j < $MDCount; $j++ )
    {
      array_push($response["offerlist"], $MDData[$j]);   
    }   
    
    $MDItemData = $TillDB->GetProductOfferItemList();  
    $MDItemCount = count($MDItemData);
    
    if( $MDItemCount > 0 )
    {      
        $response["offeritemlist"] = array();
          
        for($j=0; $j < $MDItemCount; $j++ )
        {
          array_push($response["offeritemlist"], $MDItemData[$j]);   
        }   
    }
    
    $response["success"] = 1;
  }
  else
  {
    $response["message"] = "No Offers found";         
  }   
  $jsonStr = json_encode($response);  
  echo gzencode($jsonStr); 
  //echo json_encode($response); 
  
  
?>
