<?php
  error_reporting(0);
  require_once('DAL_ProphetTill.php');

  $TillDB = new DAL_ProphetTill();

  $ProdOptionsData = $TillDB->GetProdOptionsList();  
  $ProdOptionsCount = count($ProdOptionsData);
                       
  $response = array();
  $response["success"] = 0;  
  $response["prodoptions"] = "";  
  $response["prodoptionsbuttons"] = "";    
     
    
  if( $ProdOptionsCount > 0 )
  {      
    $response["prodoptions"] = array();
          
    for($j=0; $j < $ProdOptionsCount; $j++ )
    {
      array_push($response["prodoptions"], $ProdOptionsData[$j]);   
    }   
    
    
    $ProdOptionsButtonsData = $TillDB->GetProdOptionButtonsList();  
    $ProdOptionsButtonsCount = count($ProdOptionsButtonsData);
    if( $ProdOptionsButtonsCount > 0 )
    { 
        $response["prodoptionsbuttons"] = array();
          
        for($j=0; $j < $ProdOptionsButtonsCount; $j++ )
        {
          array_push($response["prodoptionsbuttons"], $ProdOptionsButtonsData[$j]);   
        }         
    }
    
    $ProdOptionsToppingsData = $TillDB->GetProdOptionToppingsList();  
    $ProdOptionsToppingsCount = count($ProdOptionsToppingsData);
    if( $ProdOptionsToppingsCount > 0 )
    { 
        $response["prodoptionstoppings"] = array();
          
        for($j=0; $j < $ProdOptionsToppingsCount; $j++ )
        {
          array_push($response["prodoptionstoppings"], $ProdOptionsToppingsData[$j]);   
        }         
    }
    
    
    $response["success"] = 1;
  }
  else
  {
 
  }   
  $jsonStr = json_encode($response);  
  echo gzencode($jsonStr);
  //echo json_encode($response);   

?>