<?php

  require_once('DAL_ProphetTill.php');

  $TillDB = new DAL_ProphetTill();

  $ProdData = $TillDB->ANDROID_GetProductList(0);  // Main Products
  $ProdCount = count($ProdData);
  $response = array();
  $response["products"] = array();
  $response["subproducts"] = array();

  if( $ProdCount > 0 )
  {     
    for($j=0; $j < $ProdCount; $j++ )
    {
      array_push($response["products"], $ProdData[$j]);   
    }   
    
    
    $ProdData = $TillDB->ANDROID_GetProductList(1);   // Sub Products
    $ProdCount = count($ProdData);
    if( $ProdCount > 0 )
    {   
      for($j=0; $j < $ProdCount; $j++ )
      {
        array_push($response["subproducts"], $ProdData[$j]);   
      }   
    }           
    
    $response["success"] = 1;          
  }
  else
  {
    $response["success"] = 0;
    $response["message"] = "No products found";  
    //echo json_encode($response); 
  } 
  $jsonStr = json_encode($response);  
  echo gzencode($jsonStr);
  //echo json_encode($response); 
  

?>