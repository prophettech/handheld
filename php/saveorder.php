<?php
//error_reporting(0);
require_once('DAL_ProphetTill.php');

  class TestOrderData
  {
    public $orderID;
    public $orderNo;
    public $orderDate;
    public $OrderTableNum;
    public $orderPaidAmt;
    public $orderTotal;    
    public $orderAccRef;  
    public $orderPrepayment;   
    public $orderNet;   
    public $orderVat;
    public $orderVATCode;
    public $orderDiscAmt;
    public $orderDiscRate;
    public $orderTableCovers;   
    public $orderDeliveryType;
    public $orderPayType;    
    public $orderLastTransID;  

  }



/*
$orderDetails = '{"orderTip":0,"orderVATCode":0,"orderTableCovers":"0","orderDeposit":0,"orderTotal":5,"orderID":0,"orderPayType":0, ' .
                '"orderNet":5,"orderDate":"07/05/2013","orderAccRef":"RESTAURANT","orderPaidAmt":0,"orderDeliveryType":"","orderVat":0, ' .
                '"orderItems":[{"itemSectionPos":10,"itemProdType":"PT_STD","itemID":1,"itemSectionDesc":"Pizzas","itemParentProdCode": ' .
                '"P01MARG","itemSplitQty":1,"itemVatableAmt":0,"itemSubPosID":1,"itemNET":5,"itemLinkID":0,"itemPosID":1,"itemExtraToppingPrice":0, '.
                '"itemToppingList":[{"itemTopID":3,"itemTopCode":"TPHAM","itemTopDesc":"Ham","itemTopState":2}, '.
                '{"itemTopID":4,"itemTopCode":"TPMUSHROOM","itemTopDesc":"Mushroom","itemTopState":2}], '.
                '"itemVATCode":0,"itemVATRate":0,"itemProdClass":18,"itemSalePrice":5,"itemQty":1,"itemTopBut1":0, '.
                '"itemTopBut2":0,"itemDesc":"12\" Margherita","itemParentProdDesc":"","itemVAT":0,"itemCode":"P01MARG"}], '.
                '"orderTipCC":0,"orderNo":1,"orderInsert":true,"orderServiceChargePerc":0,"orderLastTransID":0,"orderDiscAmt":0, '.
                '"orderServiceCharge":0,"orderDiscRate":0,"orderPrepayment":0,"orderTableNo":"26"}';
*/
  

  $response = array();  
  $response["success"] = 0;
  $response["message"] = "";
  $response["orderID"] = 0;  
  $response["transID"] = 0;    
  

    
if (isset($_POST['orderDetails']))  
//if( $orderDetails )
{

  try
  {                

    //$orderDetailsObj = json_decode($decodeStr);
    $orderDetailsObj = json_decode($_POST['orderDetails']);
    //$orderDetailsObj = json_decode($orderDetails);
    //print_r($orderDetailsObj);  
      

      
    $TillDB = new DAL_ProphetTill();
    

    //$SavedOrderID = $TillDB->ANDROID_SaveOrder($orderDetailsObj);
    $retResult = $TillDB->ANDROID_SaveOrder($orderDetailsObj);
    $SavedOrderID = 0;
    if( count($retResult) > 0 )
    {
      $SavedOrderID = $retResult[0];
      $NewTransID = $retResult[1];
    }
        
    if( $SavedOrderID > 0 )
    {
      $response["success"] = 1;
      $response["message"] = "Success";       
      $response["orderID"] = $SavedOrderID;    
      $response["transID"] = $NewTransID;                 
    }
    else
    {                   
      $response["message"] = $TillDB->GetErrorMsg();   
    }
   }
   catch(Exception $e)
   {
      $response["message"] = $e;    
   }        
}
else
{
    $response["message"] = "Required field(s) is missing";
}   

  $jsonStr = json_encode($response);  
  echo gzencode($jsonStr);

//echo json_encode($response); 

?>