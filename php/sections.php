<?php
  error_reporting(0);
  require_once('DAL_ProphetTill.php');

  $TillDB = new DAL_ProphetTill();

  $SectionData = $TillDB->GetSectionList();  
  $SectionCount = count($SectionData);

  
  $response = array();
  $response["success"] = 0;  
  $response["message"] = "";    
  
  if( $SectionCount > 0 )
  {      
    $response["sections"] = array();
          
    for($j=0; $j < $SectionCount; $j++ )
    {
      array_push($response["sections"], $SectionData[$j]);   
    }   
    
    $response["success"] = 1;
  }
  else
  {
    $response["message"] = "No Sections found";         
  }   
 
  $jsonStr = json_encode($response);  
  echo gzencode($jsonStr);
  //echo json_encode($response);   
  
  

?>