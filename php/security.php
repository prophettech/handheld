<?php
error_reporting(0);

require_once('Till_security.php');   

function ProcessLogin($in_so, $in_UserAlreadyLoggedIn)
{    
  $ValidLogin = $in_so->ProcessLogin($in_UserAlreadyLoggedIn);

  if( $ValidLogin )
  {
  }
  else
  {
    return $in_so->GetErrorMsg();
  }
  return "";
}


$mSecurity = new Till_Security();  

$response = array();
$response["success"] = 0;  
$response["message"] = "";    
$response["userno"] = 0;    
$response["sessionid"] = 0;   

$functype = $_GET['functype'];
$userpasscode = $_GET['userpasscode']; 

if( $functype == 1 )  // User login
{

    if (isset($_GET['userpasscode']))
    {      
      $ValidUser = $mSecurity->ValidateUser($userpasscode);
      $response["userno"] = $mSecurity->GetUserNum();    
   
      if( $ValidUser == -1 )
      {
        $response["message"] = "This user is already logged in on another client. If you continue, this user will be logged out.";
        $response["success"] = -1;  
      }
      else if ( $ValidUser == 0 )
      {
        $response["message"] = $mSecurity->GetErrorMsg();
      }
      else if ( $ValidUser == 1 )
      {
        $retVal = ProcessLogin($mSecurity,false); 
        $response["sessionid"] = $mSecurity->GetUserSessionID();  
        if( $retVal > "" )
        {
          $response["message"] = $retVal;            
        }
        else
        {
          $response["success"] = 1;  
           
        }
        
      }
    }
}
else if( $functype == 2 )  // User logout
{
  $userno = $_GET['userno']; 
  $sessionid = $_GET['sessionid']; 
  if( $mSecurity->LogoutUser($userno,$sessionid) )
  {
    $response["success"] = 1;  
  }
  else
  {   
  }    
}
else if( $functype == 3 )  // user already logged in
{
  $ValidUser = $mSecurity->ValidateUser($userpasscode);
  $retVal = ProcessLogin($mSecurity, true);
  if( $retVal > "" )
  {
    $response["message"] = $retVal;
  }
  else
  {
    $response["success"] = 1;   
    $response["userno"] = $mSecurity->GetUserNum();    
    $response["sessionid"] = $mSecurity->GetUserSessionID();
  }
}    
 
  $jsonStr = json_encode($response);  
  echo gzencode($jsonStr);
  //echo json_encode($response);   


?>
