<?php

//error_reporting(0);
require_once('DAL_ProphetTill.php');


  
$response = array();  
$response["success"] = 0;
$response["message"] = "";  

if (isset($_GET['DALAction']))
{ 
  $dbAction = $_GET['DALAction']; 

  $TillDB = new DAL_ProphetTill();
             
  if( $dbAction == 1 )
  {    
    $TableData = $TillDB->GetTableList();  
    $TableCount = count($TableData);
                 
    if( $TableCount > 0 )
    {      
      $response["tables"] = array();
           
      for($j=0; $j < $TableCount; $j++ )
      {
        array_push($response["tables"], $TableData[$j]);   
      }       
      $response["success"] = 1;
    }
    else
    {
      $response["message"] = "No Tables found";          
    } 
  }
  else if( $dbAction == 2 )
  { 
    $DateF = date("d.m.Y");
    $DateT = date("d.m.Y");   
    $TableOrdersData = $TillDB->GetTableOrders($DateF,$DateT);  
    $TableOrdersCount = count($TableOrdersData);
      
    if( $TableOrdersCount > 0 )
    {      
      $response["tableOrders"] = array();
           
      for($j=0; $j < $TableOrdersCount; $j++ )
      {
        array_push($response["tableOrders"], $TableOrdersData[$j]);   
      }       
      $response["success"] = 1;
    }
    else
    {     
    } 
  }
}
  
  $jsonStr = json_encode($response);  
  echo gzencode($jsonStr);
  //echo json_encode($response);   
  
  

?>